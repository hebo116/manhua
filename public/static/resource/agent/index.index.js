layui.config({
	    base: '/static/layadmin/' //静态资源所在路径
	  }).extend({
	    index: 'lib/index' //主入口模块
	  }).use(['index','jquery'],function(){
	  var $ = layui.jquery,
	  layer = layui.layer;
	  getNotReadMsg();
	  function getNotReadMsg(){
		  $.ajax({
			  type : 'post',
			  url : '{:url("getReadMessage")}',
			  dataType : 'json',
			  success : function(res){
				  if(res.data.id > 0){
					  layer.open({
						  type: 2,
						  title: res.data.title,
						  closeBtn: 1,
						  shade: [0],
						  area: ['600px','70%'],
						  offset : '100px',
						  time: 0,
						  anim: 2,
						  scrollbar :false,
						  content: [res.data.url,'yes'],
						  cancel: function(index, layero){ 
						      layer.close(index);
						      getNotReadMsg();
						  }  
					});
				  }
			  }
		  });
	  }
	  $('#doRefresh').click(function(){
		  document.getElementById('my_frame_id').contentWindow.location.reload();
	  });
  });