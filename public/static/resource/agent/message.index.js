layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'title', title: '消息标题',align:'center',minWidth:200},
		    {field: 'create_time', title: '发布时间',align:'center',minWidth:200},
		    {title: '是否阅读',align:'center',width:120,templet:function(d){
		    	var str = '未知';
		    	switch(parseInt(d.is_read)){
		    	case 1:
		    		str = '已读';
		    		break;
		    	case 2:
		    		str = '<font class="text-red">未读</font>';
		    		break;
		    	}
		    	return str;
		    }},
		    {title: '操作',align:'center',minWidth:180,templet:function(d){
		    	var $html = '';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="show">查看</a>';
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		switch(obj.event){
			case 'show':
				layPage(field.title,field.show_url,'600px','70%');
			break;
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});