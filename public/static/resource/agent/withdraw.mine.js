layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form,
	laydate = layui.laydate;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
			{title: '充值金额',align:'center',width:140,templet:function(d){
		    	return '¥'+d.charge_money;
		    }},
		    {field: 'charge_num', title: '充值单数',align:'center',width:140},
		    {title: '提现金额',align:'center',width:200,templet:function(d){
		    	var str = '¥'+d.money;
		    	return str;
		    }},
		    {field: 'cur_date',title: '账单日期',align:'center',width:140},
		    {title: '提现账号信息',align:'left',width:300,templet:function(d){
		    	var str = '';
		    	str += d.bank_info.bank_user+'<br />';
		    	str += d.bank_info.bank_name+'<br />';
		    	str += d.bank_info.bank_no;
		    	return str;
		    }},
		    {field: 'create_time', title: '申请时间',align:'center',minWidth:200},
		    {title: '申请状态',align:'center',width:140,templet:function(d){
		    	var str = '';
		    	switch(parseInt(d.status)){
		    	case 0:str='待支付';break;
		    	case 1:str='已支付';break;
		    	}
		    	return str;
		    }},
		    {title: '结算信息',align:'left',width:300,templet:function(d){
		    	var str = '--';
		    	if(parseInt(d.status) === 1){
		    		str = '操作员：'+d.opt_name;
		    		str += '<br />';
		    		str += '支付时间：'+d.pay_time;
		    	}
		    	return str;
		    }},
		]],
		page : true,
		height : 'full-460',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	form.on('radio(auto_type)',function(obj){
		ajaxPost(U('setIsAuto'),{event:obj.value},'',function(){
			layOk('设置成功');
		});
	});
	
	laydate.render({elem:'#between_time',type:'date',range:'~'});	
	
	$('#doAll').click(function(){
		ajaxPost(U('doAll'),{},'确定要提取账户内所有金额吗？',function(){
			layOk('申请成功');
			setTimeout(function(){
				window.location.reload();
			},1000);
		});
	});
	
	$('#setAccount').click(function(){
		var url = U('setAccount');
		layer.open({
		  type: 2,
		  title: '设置结算资料',
		  closeBtn: 1,
		  shade: [0],
		  area: ['500px','355px'],
		  offset : '100px',
		  time: 0,
		  anim: 2,
		  scrollbar :false,
		  content: [url,'yes'],
		  btn : ['确定','取消'],
		  yes : function(index,layero){
			  var iframeWin = window[layero.find('iframe')[0]['name']];
			  iframeWin.layui.form.on('submit(dosubmit)', function(obj){
				  ajaxPost(url,obj.field,'确定要保存吗？',function(d){
					  	layOk('更新成功');
					  	setTimeout(function(){
					  		layer.close(index);
					  	},1500);
				  });
	          });  
			  iframeWin.document.getElementById("dosubmit").click();
		  }
		});
	});
	
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});