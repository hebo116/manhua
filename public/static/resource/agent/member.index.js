layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
			{title: '账号',align:'left',minWidth:200,templet:function(d){
				var str = '';
				str += '<img src="'+d.headimgurl+'" style="width:30px;" />';
				str += '<br />';
				str += d.username+'【'+d.id+'】';
				return str;
			}},
		    {field: 'money',title: '书币余额',align:'center',minWidth:130},
		    {field: 'status_name',title: '用户状态',align:'center',minWidth:100},
		    {field: 'create_time', title: '注册时间',align:'center',minWidth:200},
		    {title: '更多',align:'center',minWidth:120,templet:function(d){
		    	return '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="info">查看详情</a>';
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		if(obj.event === 'info'){
			layPage('用户详情',field.info_url,'90%','80%');
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});