layui.use(['jquery','layer'],function(){
	var myChart = echarts.init(document.getElementById('memberCharts'),'macarons');
	ajaxPost(U('getUserChartData'),{},'',function(res){
		var option = {
			    tooltip : {
			        trigger: 'axis'
			    },
			    calculable : true,
			    xAxis : [
			        {
			            type : 'category',
			            boundaryGap : false,
			            data : res['key']
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value',
			            axisLabel : {
			                formatter: '{value}'
			            }
			        }
			    ],
			    series : [
			        {
			            name:'新增用户',
			            type:'line',
			            data:res['add'],
			            
			            markPoint : {
			                data : [
			                    {type : 'max', name: '最大值'},
			                    {type : 'min', name: '最小值'}
			                ]
			            }
			        }
			    ]
			};
		myChart.setOption(option); 
	});
	
	
});