layui.use(['jquery','form','upload'],function(){
	var $ = layui.jquery, 
	form = layui.form,
	upload = layui.upload;
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			setTimeout(function(){
				location.href = U('index');
			},1000);
		});
	});
	upload.render({
		  elem: '#doUpload',
		  url: U('Upload/doUploadImg'),
		  accept : 'images',
		  exts : 'jpg|jpeg|png|gif',
		  size : 1024,
		  drag : false,
		  done : function(res){
			  if(res.code === 1){
				  var src = res.data.url;
				  $('#showImg').attr('src',src);
				  $('#hideVal').val(src);
			  }else{
				  layError(res.msg);
			  }
		  },
		  error : function(){
			  layError('上传失败,请重试');
		  }
	});
});