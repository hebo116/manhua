layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{type : 'checkbox'},
			{title: '封面',minWidth:100,align:'center',templet:function(d){
				var $html = '<font class="text-red">暂无封面</font>';
				if(d.cover){
					$html = '<img src="'+d.cover+'" style="width:50px;" />';
				}
				return $html;
			}},
		    {field: 'name',title: '小说名称',align:'center',minWidth:260},
		    {title: '收费情况',align:'center',minWidth:150,templet:function(d){
		    	return '前'+d.free_chapter+'章免费，后续章节收费'+d.money+'书币';
		    }},
		    {field: 'hot_num',title: '书籍热度',align:'center',minWidth:120}
		]],
		page : true,
		height : 'full-100',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});