layui.use(['jquery','layer','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	form = layui.form,
	laydate = layui.laydate;
	laydate.render({elem:'#start_date',type:'date'});
	laydate.render({elem:'#end_date',type:'date'});
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			setTimeout(function(){
				window.location.href = U('index');
			},1500);
		});
	});
});