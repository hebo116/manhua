layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form,
	laydate = layui.laydate;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
		    {field: 'order_no', title: '订单号',align:'center',width:200},
		    {title: '用户信息',align:'left',width:260,templet:function(d){
		    	var str = '';
		    	if(d.userinfo){
		    		str += '<div lay-event="userinfo" style="cursor:pointer;">';
		    		str += '<p><img src="'+d.userinfo.headimgurl+'" width="22" />&nbsp;'+d.userinfo.username+'('+d.userinfo.id+')&nbsp[用户详情]</p>';
		    		str += '<p>注册时间：'+d.userinfo.create_time+'</p>';
		    		str += '</div>';
		    	}else{
		    		str = '--';
		    	}
		    	return str;
		    }},
		    {title: '是否扣量',align:'center',width:90,templet:function(d){
		    	var is_count = parseInt(d.is_count);
		    	var str = '否';
		    	if(is_count == 2){
		    		str = '<a class="layui-btn layui-btn-danger layui-btn-xs">是</a>';
		    	}
		    	return str;
		    }},
		    {title: '所属渠道',align:'left',minWidth:120,templet:function(d){
		    	var str = '';
		    	if(d.channel_name){
		    		str = d.channel_name;
		    	}else{
		    		str = '总站';
		    	}
		    	return str;
		    }},
		    {title:'充值历史',width:140,align:'left',templet:function(d){
		    	var str = '--';
		    	if(parseInt(d.charge_num) > 0){
		    		str = '第<font class="text-red">'+d.charge_num+'</font>次充值';
		    		str += '<br />';
		    		str += '累计充值:¥'+d.charge_money;
		    	}
		    	return str;
		    }},
		    {field: 'money', title: '充值金额',align:'center',width:100},
		    {field: 'from_name', title: '订单来源',align:'center',minWidth:160},
		    {field: 'spread', title: '来源链接',align:'center',minWidth:160},
		    {title: '状态',align:'center',width:80,templet:function(d){
		    	var str = '未知';
		    	switch(parseInt(d.status)){
			    	case 0:str = '已关闭';break;
			    	case 1:str = '待支付';break;
			    	case 2:str = '已支付';break;
		    	}
		    	return str;
		    }},
		    {field: 'create_time', title: '下单时间',align:'center',width:150},
		    {title: '更多',align:'center',width:100,templet:function(d){
		    	var str = '--';
		    	if(parseInt(d.is_info) === 1){
		    		str = '<a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="detail">分成明细</a>';
		    	}
		    	return str;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		switch(obj.event){
		case 'detail':
			var str = '';
			str += '<div class="layui-fluid">';
			str += '<div class="layui-row layui-col-space20">';
			str += '<div class="layui-col-md6 layui-col-sm6">';
			str += '<div class="layui-card">';
			str += '<div class="layui-card-header">一级代理：'+field.count_info.channel.name+'</div>';
			str += '<div class="layui-card-body">';
			str += '<div class="layui-block">';
			str += '<p>分佣比例：'+field.count_info.channel.ratio+'</p>';
			str += '<p>分佣金额：'+field.count_info.channel.money+'</p>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			if(field.count_info.agent){
				str += '<div class="layui-col-md6 layui-col-sm6">';
				str += '<div class="layui-card">';
				str += '<div class="layui-card-header">二级代理：'+field.count_info.agent.name+'</div>';
				str += '<div class="layui-card-body">';
				str += '<div class="layui-block">';
				str += '<p>分佣比例：'+field.count_info.agent.ratio+'</p>';
				str += '<p>分佣金额：'+field.count_info.agent.money+'</p>';
				str += '</div>';
				str += '</div>';
				str += '</div>';
				str += '</div>';
			}
			str += '</div>';
			str += '</div>';
			layer.open({
				  type: 1,
				  title: '订单分佣明细',
				  closeBtn: 1,
				  shade: [0],
				  area: ['600px','220px'],
				  offset : '100px',
				  time: 0,
				  anim: 2,
				  scrollbar :false,
				  content: str
			});
			break;
		case 'userinfo':
			layPage('用户详情',field.userinfo_url,'90%','80%');
			break;
		}
		
	});
	laydate.render({elem:'#between_time',type:'datetime',range:'~'});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});