layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form,
	laydate = layui.laydate;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {title: '统计时段',align:'center',minWidth:120,templet:function(d){
		    	return d.create_date+' '+d.hour+'时';
		    }},
		    {field: 'create_time', title: '执行时间',align:'center',minWidth:160},
		    {title: '执行状态',align:'center',minWidth:100,templet:function(d){
		    	var str = '成功';
		    	if(parseInt(d.status) !== 1){
		    		str = '<font class="text-red">失败</font>';
		    		str += '&nbsp;'
		    		str += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="doAdd">执行统计</a>';
		    	}else{
		    		if(d.is_remark === 'yes'){
		    			str += '&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="remark">补发明细</a>';
		    		}
		    	}
		    	return str;
		    }}
		]],
		page : false,
		height : 'full-140',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	laydate.render({elem:'#create_date',max:0});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		switch(obj.event){
		case 'doAdd':
			var data = {create_date:field.create_date,hour:field.hour};
			var asked = '确定继续执行'+data.create_date+' '+field.hour+'时统计吗？';
			ajaxPost(U('doAddPlan'),data,asked,function(){
				layOk('操作成功');
				setTimeout(function(){
					layLoad('数据加载中...');
					table.reload('table-block');
				},1400);
			});
			break;
		case 'remark':
			layPage('补发明细',field.remark_url,'70%','80%');
			break;
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});