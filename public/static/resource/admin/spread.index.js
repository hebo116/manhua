layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form,
	laydate = layui.laydate;
	layLoad('数据加载中...');
	
	var clipboard = new Clipboard('.copy');
	clipboard.on('success', function (e) {
		layOk("复制成功!");
		e.clearSelection();
	});
	clipboard.on('error', function (e) {
		console.error('Action:', e.action);
		console.error('Trigger:', e.trigger);
	});
	
	laydate.render({elem:'#between_time',type:'datetime',range:'~'});
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title:'序号',type:'numbers'},
		    {title: '推广链接',align:'left',minWidth:400,templet:function(d){
		    	var str = d.name+'&nbsp;&nbsp;';
		    	str += '<a href="javascript:void(0);" class="layui-icon layui-icon-link layui-btn-xs" lay-event="copy"></a>';
		    	str += '<a href="javascript:void(0);" class="layui-icon layui-icon-cols layui-btn-xs" lay-event="qrcode"></a>';
		    	str += '<br />'
		    	str += '<a href="javascript:;" style="color:blue;">'+d.url+'&nbsp;</a><a href="javascript:void(0);" class="layui-icon layui-icon-print layui-btn-xs copy" data-clipboard-action="copy" data-clipboard-text="'+d.url+'"></a>'
		    	str += '<br />'
		    	str += '创建时间：'+d.create_time;
		    	return str;
		    }},
		    {title: '推广内容',align:'left',width:260,templet:function(d){
		    	var str = '';
		    	str += '推广书籍：'+d.book_name+'&nbsp[<a href="'+d.book_url+'">书籍详情</a>]<br />';
		    	str += '推广章节：'+d.read_chapter+'<br />';
		    	if(d.sub_chapter){
		    		str += '关注章节：'+d.sub_chapter;
		    	}
		    	return str;
		    }},
		    {field: 'view_num', title: '总点击量',align:'center',width:100},
		    {field: 'add_num', title: '总用户',align:'center',width:100},
		    {title: '充值单数',align:'center',width:110,templet:function(d){
		    	var str = d.charge_num+'笔';
		    	str += '<br />';
		    	str += d.charge_ratio;
		    	return str;
		    }},
		    {title: '充值金额',align:'center',width:120,templet:function(d){
		    	var str = '¥'+d.charge_money;
		    	str += '<br /><a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="detail">明细</a>';
		    	return str;
		    }},
		    {title: '成本',align:'center',width:120,templet:function(d){
		    	var str = '¥'+d.cost_money;
		    	str += '<br /><a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="cost">设置</a>';
		    	return str;
		    }},
		    {title: '操作',align:'center',width:120,templet:function(d){
		    	var str = '';
		    	str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
		    	str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return str;
		    }}
		]],
		page : true,
		limit : 20,
		height : 'full-260',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		switch(obj.event){
			case 'cost':
				layer.prompt({
					  title : '请输入成本金额',
					  offset : '200px',
					  formType: 0, 
					  value: field.cost_money,
					  maxlength: 10,
					},function(value,index,elem){
						layer.close(index);
						ajaxPost(U('setCostMoney'),{id:field.id,cost_money:value},'',function(){
							layOk('设置成功');
							setTimeout(function(){
								layLoad('数据加载中...');
								table.reload('table-block');
							},1400);
						});
					});
			break;
			case 'copy':
				layPage('复制推广链接【'+field.name+'】',field.copy_url,'800px','500px');
				break;
			case 'qrcode':
				layPage('生成二维码',field.qrcode_url,'600px','430px;');
				break;
			case 'delete':
				ajaxPost(U('delLink'),{id:field.id},'确定要删除该推广链接吗？',function(){
					layOk('操作成功');
					setTimeout(function(){
						layLoad('数据加载中...');
						table.reload('table-block');
					},1400);
				});
				break;
			case 'detail':
				layPage(field.name+'(明细数据)',field.detail_url,'70%','70%');
				break;
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});