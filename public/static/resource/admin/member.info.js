layui.use(['table','element'],function(){
		var table = layui.table,
		element = layui.element;
		
		//实例化充值记录
		table.render({
			elem : '#charge-table',
			url : chargeUrl,
			loading : true,
			cols : [[
			    {field: 'order_no', title: '订单号',align:'center',minWidth:200},
			    {title: '是否扣量',align:'center',minWidth:80,templet:function(d){
			    	var str = '否';
			    	if(parseInt(d.is_count) == 2){
			    		str = '<a class="layui-btn layui-btn-danger layui-btn-xs">扣</a>';
			    	}
			    	return str;
			    }},
			    {field: 'money', title: '充值金额',align:'center',minWidth:100},
			    {field: 'from_name', title: '订单来源',align:'center',minWidth:160},
			    {title: '状态',align:'center',width:120,templet:function(d){
			    	var str = '未知';
			    	switch(parseInt(d.status)){
				    	case 0:str = '已关闭';break;
						case 1:str = '待支付';break;
						case 3:str = '待支付';break;
						case 2:str = '已支付';break;
			    	}
			    	return str;
			    }},
			    {field: 'create_time', title: '下单时间',align:'center',minWidth:200}
			]],
			page : true,
			height : 'full',
			response: {statusCode:1}
		});
		
		//实例化未支付充值记录
		table.render({
			elem : '#charge-table1',
			url : chargeUrl+'&status=1',
			loading : true,
			cols : [[
				{field: 'order_no', title: '订单号',align:'center',minWidth:200},
				{field: 'money', title: '充值金额',align:'center',minWidth:100},
				{field: 'from_name', title: '订单来源',align:'center',minWidth:160},
				{title: '状态',align:'center',width:120,templet:function(d){
					var str = '未知';
					switch(parseInt(d.status)){
					case 0:str = '已关闭';break;
					case 1:str = '待支付';break;
					case 3:str = '待支付';break;
					case 2:str = '已支付';break;
					}
					return str;
				}},
				{field: 'create_time', title: '下单时间',align:'center',minWidth:200}
				]],
				page : true,
				height : 'full',
				response: {statusCode:1}
		});
		//实例化已支付充值记录
		table.render({
			elem : '#charge-table2',
			url : chargeUrl+'&status=2',
			loading : true,
			cols : [[
				{field: 'order_no', title: '订单号',align:'center',minWidth:200},
				{field: 'money', title: '充值金额',align:'center',minWidth:100},
				{field: 'from_name', title: '订单来源',align:'center',minWidth:160},
				{title: '状态',align:'center',width:120,templet:function(d){
					var str = '未知';
					switch(parseInt(d.status)){
					case 0:str = '已关闭';break;
					case 1:str = '待支付';break;
					case 3:str = '待支付';break;
					case 2:str = '已支付';break;
					}
					return str;
				}},
				{field: 'create_time', title: '下单时间',align:'center',minWidth:200}
				]],
				page : true,
				height : 'full',
				response: {statusCode:1}
		});
		
		//实例化阅读记录
		table.render({
			elem : '#read-table',
			url : readUrl,
			loading : true,
			cols : [[
			    {field: 'create_time', title: '阅读时间',align:'center',minWidth:200},
			    {field: 'name', title: '书籍名称',align:'center',minWidth:100},
			    {field: 'chapter_name', title: '最后阅读章节',align:'center',minWidth:160},
			]],
			page : false,
			height : 'full',
			response: {statusCode:1}
		});
		
		//实例化消费记录
		table.render({
			elem : '#consume-table',
			url : consumeUrl,
			loading : true,
			cols : [[
				{title: '序号',type:'numbers'},
			    {field: 'money', title: '记录书币',align:'center',minWidth:100},
			    {field: 'summary', title: '详细描述',align:'left',minWidth:260,templet:function(d){
			    	var str = '';
			    	str += d.title+'<br />'+d.summary;
			    	return str;
			    }},
			    {field: 'create_time', title: '记录时间',align:'center',minWidth:200}
			]],
			page : true,
			height : 'full',
			response: {statusCode:1}
		});
		
	});