layui.use(['jquery','layer','table'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
			{field: 'channel_name',title: '代理名称',align:'center',minWidth:140},
		    {title: '类型',align:'center',width:120,templet:function(d){
		    	var str = '未知';
		    	switch(parseInt(d.type)){
		    	case 1:
		    		str = '一级代理';
		    		break;
		    	case 2:
		    		str = '二级代理';
		    		break;
		    	}
		    	return str;
		    }},
		    {title: '是否阅读',align:'center',width:120,templet:function(d){
		    	var str = '未知';
		    	switch(parseInt(d.is_read)){
		    	case 1:
		    		str = '已读';
		    		break;
		    	case 2:
		    		str = '<font class="text-red">未读</font>';
		    		break;
		    	}
		    	return str;
		    }},
		]],
		page : false,
		height : 'full',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
});