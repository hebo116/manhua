layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form,
	laydate = layui.laydate;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
			{title: '申请来源',align:'left',minWidth:150,templet:function(d){
		    	var str = '';
		    	str += d.name;
		    	switch(parseInt(d.account_type)){
		    	case 1:
		    		str += '&nbsp;<a class="layui-btn layui-btn-xs layui-btn-danger">VIP</a>';
		    		str += '<br />';
			    	str += '账号备注：'+d.remark;
		    		break;
		    	case 2:
		    		str += '&nbsp;<a class="layui-btn layui-btn-xs layui-btn-primary">代理</a>';
		    		break;
		    	}
		    	return str;
		    }},
		    {title: '充值金额',align:'center',width:100,templet:function(d){
		    	return '¥'+d.charge_money;
		    }},
		    {field: 'charge_num', title: '充值单数',align:'center',width:100},
		    {title: '提现金额',align:'center',width:200,templet:function(d){
		    	var str = '¥'+d.money;
		    	if(d.detail_url){
		    		str += '&nbsp;<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="detail">明细</a>';
		    	}
		    	return str;
		    }},
		    {field: 'cur_date',title: '账单日期',align:'center',width:200},
		    {title: '提现账号信息',align:'left',width:260,templet:function(d){
		    	var str = '';
		    	str += d.bank_info.bank_user+'<br />';
		    	str += d.bank_info.bank_name+'<br />';
		    	str += d.bank_info.bank_no;
		    	return str;
		    }},
		    {field: 'pay_time', title: '支付时间',align:'center',width:180},
		    {field: 'opt_name', title: '操作员',align:'center',width:120},
		]],
		page : true,
		height : 'full-380',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	laydate.render({elem:'#between_time',type:'date',range:'~'});	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {sign:[field.sign]};
		switch(obj.event){
			case 'detail':
				layPage('提现明细',field.detail_url,'70%','60%');
				break;
		}
	});
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});