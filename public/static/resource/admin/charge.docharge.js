layui.use(['jquery','form'],function(){
	var $ = layui.jquery,
	form = layui.form;
	form.on('radio(type)',function(obj){
		var val = parseInt(obj.value);
		switch(val){
		case 1:
			$('.type2').addClass('layui-hide');
			$('.type1').removeClass('layui-hide');
			break;
		case 2:
			$('.type1').addClass('layui-hide');
			$('.type2').removeClass('layui-hide');
			break;
		}
	});
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			setTimeout(function(){
				location.href = U('index');
			},1000);
		});
	});
});