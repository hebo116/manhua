layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '封面',minWidth:100,align:'center',templet:function(d){
				var str = '<font class="text-red">暂无封面</font>';
				if(d.cover){
					str = '<img src="'+d.cover+'" style="width:50px;" />';
				}
				return str;
			}},
		    {field: 'name',title: '小说名称',align:'center',minWidth:260},
		    {title: '限免时间',align:'center',minWidth:220,templet:function(d){
		    	var str = d.start_date+'~'+d.end_date;
		    	switch(parseInt(d.free_status)){
		    	case 2:
		    		str += '&nbsp;<a class="layui-btn layui-btn-xs layui-btn-danger">未开始</a>';
		    		break;
		    	case 3:
		    		str += '&nbsp;<a class="layui-btn layui-btn-xs layui-btn-danger">已结束</a>';
		    		break;
		    	}
		    	return str;
		    }},
		    {title: '操作',align:'center',minWidth:400,templet:function(d){
		    	var $html = '';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
			    	$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		if(obj.event === 'delete'){
			ajaxPost(U('delFree'),{id:field.id},'确定要删除该限免小说吗？',function(){
				layOk('操作成功');
				setTimeout(function(){
					layLoad('数据加载中...');
					table.reload('table-block');
				},1400);
			});
		}
	});
	
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});