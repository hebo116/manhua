layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'title', title: '消息标题',align:'center',minWidth:200},
		    {title: '接收明细',align:'center',minWidth:200,templet:function(d){
		    	var str = d.channel_num+'&nbsp;';
		    	str += '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="list">明细</a>';
		    	return str;
		    }},
		    {field: 'create_time', title: '发布时间',align:'center',minWidth:200},
		    {title: '操作',align:'center',minWidth:180,templet:function(d){
		    	var str = '';
		    		str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
		    		str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return str;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		switch(obj.event){
			case 'delete':
				ajaxPost(U('delMessage'),data,'确认要删除该公告吗？',function(){
					layOk('操作成功');
					setTimeout(function(){
						layLoad('数据加载中...');
						table.reload('table-block');
					},1400);
				});
			break;
			case 'list':
				layPage('接收渠道列表',field.list_url,'600px','70%');
				break;
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});