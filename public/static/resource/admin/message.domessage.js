layui.use(['form','layedit'],function(){
	var form = layui.form, 
	layedit = layui.layedit;
	layedit.set({
        tool: [
            'html' , 'strong', 'italic', 'underline', 'del', 'addhr', '|', 'link', 'unlink'
            , '|', 'left', 'center', 'right'
        ]
        , height: '300px'
    });
	var textindex = layedit.build('content');
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		var content = layedit.getContent(textindex);
		data['content'] = content;
		ajaxPost('',data,'确定要发布吗？',function(d){
			layOk('发布成功');
			setTimeout(function(){
				location.href = $backUrl;
			},1000);
		});
	});
});