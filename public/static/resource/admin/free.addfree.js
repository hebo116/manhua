layui.use(['jquery','layer','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	form = layui.form,
	laydate = layui.laydate;
	laydate.render({elem:'#start_date',type:'date'});
	laydate.render({elem:'#end_date',type:'date'});
	var book_ids = [];
	$('#chooseBook').click(function(){
		layer.open({
			  type: 2,
			  title: '选择限免书籍',
			  closeBtn: 1,
			  shade: [0],
			  area: ['70%','70%'],
			  offset : '100px',
			  time: 0,
			  anim: 2,
			  scrollbar :false,
			  content: [U('selBook')+'?category='+$category,'yes'],
			  btn : ['确定选择','取消'],
			  yes : function(index,layero){
				  var childWindow = $(layero).find('iframe')[0].contentWindow;
				  var checkStatus = childWindow.layui.table.checkStatus('table-block');
				  if(checkStatus.data.length === 0){
					  layError('您尚未选择书籍');
					  return false;
				  }
				  var str;
				  $.each(checkStatus.data,function(i,item){
					  var book_id = parseInt(item.id);
					  if(book_ids.indexOf(book_id) === -1){
						  book_ids.push(book_id);
						  str = '';
						  str += '<div class="book_item">';
						  str += '<span class="removeBook" data-id="'+book_id+'">移除</span>';
						  str += '<img src="'+item.cover+'" />';
						  str += '<p>'+item.name+'</p>';
						  str += '</div>';
						  $('#book_box').append(str);
					  }
				  });
				  layer.close(index);
			  }
		});
	});
	
	$('body').on('click','.removeBook',function(){
		var id = parseInt($(this).attr('data-id'));
		if(id){
			var cur_key = book_ids.indexOf(id);
			if(cur_key > -1){
				book_ids.splice(cur_key,1);
			}
		}
		$(this).parent().remove();
	});
	
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		if(book_ids.length === 0){
			layMsg('您尚未选择限免书籍');
			return false;
		}
		data['book_ids'] = book_ids;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('新增成功'+d.num+'本书籍');
			setTimeout(function(){
				window.location.href = U('index');
			},1500);
		});
	});
});