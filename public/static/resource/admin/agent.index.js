layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'name', title: '代理名称',align:'center',minWidth:150},
		    {field: 'login_name', title: '代理账号',align:'center',minWidth:100},
		    {field: 'money', title: '账户余额',align:'center',minWidth:120},
		    {field: 'ratio', title: '佣金比例',align:'center',minWidth:200},
		    {field: 'charge_money', title: '累计充值',align:'center',minWidth:200},
		    {title: '代理状态',align:'center',minWidth:260,templet:function(d){
		    	var $html = d.status_name+'&nbsp;';
		    	var status = parseInt(d.status);
		    	switch(status){
			    	case 0:
			    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="pass">通过</a>';
			    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="fail">不通过</a>';
			    		break;
			    	case 1:
			    		$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">禁用</a>';
			    		break;
			    	case 2:
			    		$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="on">启用</a>';
			    		break;
		    	}
		    	return $html;
		    }},
		    {title: '操作',align:'center',minWidth:400,templet:function(d){
		    	var $html = '';
		    	var id = parseInt(d.id);
		    	var status = parseInt(d.status);
		    	if(status === 1 || status === 2){
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="location">进入后台</a>';
			    	$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="resetpwd">重置登录密码</a>';
			    	$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.child_url+'">下级代理</a>';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
			    	$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	}else{
		    		$html += '--';
		    	}
		    	
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		if(obj.event === 'location'){
			doLocation(field.id);
		}else{
			var data = {id:field.id,event:obj.event};
			var asked = '';
			switch(obj.event){
				case 'on':
					asked = '确定要启用该代理吗？';
					break;
				case 'off':
					asked = '确定要禁用该代理吗？';
					break;
				case 'delete':
					asked = '确定要将该删除该代理吗？';
					break;
				case 'resetpwd':
					asked = '确定要将该代理登录密码重置为123456吗？';
					break;
				case 'pass':
					asked = '确定要审核通过该代理吗？';
					break;
				case 'fail':
					asked = '确定要拒绝审核通过该代理吗？';
					break;
			}
			if(asked){
				ajaxPost(U('doAgentEvent'),data,asked,function(){
					layOk('操作成功');
					if(obj.event !== 'resetpwd'){
						setTimeout(function(){
							layLoad('数据加载中...');
							table.reload('table-block');
						},1400);
					}
				});
			}else{
				layError('该按钮未绑定事件');
			}
		}
	});
	
	function doLocation(id){
		var data = {id:id};
		ajaxPost(U('intoBackstage'),data,'',function(res){
			window.open(res.url,'_blank');
		});
	}
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});