/**
 * 轮播图页面配置
 */
layui.use(['jquery','form'],function(){
	var $ = layui.jquery,
	form = layui.form;
	var len = $('.imgbox-list').length;
	len = len ? (len-1) : 0;
	$('#addBox').click(function(){
		len ++;
		var html = '';
		html += '<div class="imgbox-list">';
		html += '<a class="layui-btn layui-btn-sm layui-btn-danger img-delete layui-hide" href="javascript:void(0);"><i class="layui-icon layui-icon-delete"></i>删除</a>';
		html += '<div class="child-item img-item" title="点击上传图片" onclick="cropImage(\'上传轮播图\',\'870x390\',this);">';
		html += '<img class="showimg" src="/static/common/images/435x195.png" />';
		html += '<input type="hidden" class="hideval" name="src['+len+']" value="" />';
		html += '</div>';
		html += '<div class="child-item input-item">';
		html += '<textarea name="link['+len+']" placeholder="请输入链接"></textarea>';
		html += '</div>';
		html += '</div>';
		$('#imgbox').append(html);
	});
	
	$('body').on('mouseover','.imgbox-list',function(){
		$(this).find('.img-delete').removeClass('layui-hide');
	});
	
	$('body').on('mouseout','.imgbox-list',function(){
		$(this).find('.img-delete').addClass('layui-hide');
	});
	
	$('body').on('click','.img-delete',function(){
		$(this).parent().remove();
	});
	
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
		});
	});
});