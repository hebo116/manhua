layui.use(['jquery','layer','table'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
			{field: 'name',title: '广告位置',align:'left',width:140},
		    {title: '广告图片',align:'center',minWidth:240,templet:function(d){
		    	var str = '';
		    	if(d.src){
		    		str += '<img src="'+d.src+'" style="width:200px !important;"/>';
		    	}else{
		    		str += '<font class="text-red">未配置</font>'
		    	}
		    	return str;
		    }},
		    {field: 'url', title: '跳转链接',align:'left',minWidth:200},
		    {title: '点击量',align:'center',width:120,templet:function(d){
		    	var str = d.click_num+'&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="clear">清空</a>';
		    	return str;
		    }},
		    {title: '广告状态',align:'center',width:140,templet:function(d){
		    	var str = '';
		    	switch( parseInt(d.status)){
			    	case 1:
			    		str += '启用&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="statusoff">禁用</a>';
			    		break;
			    	case 2:
			    		str += '禁用&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="statuson">启用</a>';
			    		break;
		    	}
		    	return str;
		    }},
		    {title: '操作',align:'center',width:100,templet:function(d){
		    	return '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
		    }}
		]],
		page : true,
		height : 'full-200',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		switch(obj.event){
			case 'statuson':
				doState('确定要启用该广告吗？',data);
				break;
			case 'statusoff':
				doState('确定要禁用该广告吗？',data);
				break;
			case 'clear':
				doState('确定要清空点击量？',data);
				break;
			default:
				layError('该按钮未绑定事件');
				break;
		}
	});
	
	function doState(asked,data){
		ajaxPost(U('doAdEvent'),data,asked,function(){
			layOk('操作成功');
			setTimeout(function(){
				layLoad('数据加载中...');
				table.reload('table-block');
			},1400);
		});
	}
});