layui.use(['jquery','layer','table'],function(){
	var layer = layui.layer,
	table = layui.table;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'name',title: '渠道代理',align:'center',minWidth:120},
		    {title: '统计时段',align:'center',minWidth:120,templet:function(d){
		    	return d.from_date+' '+d.from_h+'时';
		    }},
		    {title: '补发金额',align:'center',minWidth:120,templet:function(d){
		    	return '¥'+d.money;
		    }},
		    {field: 'to_date', title: '计入账单日期',align:'center',minWidth:160},
		]],
		page : false,
		height : 'full-50',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
});