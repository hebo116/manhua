layui.use(['jquery','form'],function(){
	var $ = layui.jquery,
	form = layui.form;
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			setTimeout(function(){
				window.location.href = U('index');
			},1000);
		});
	});
});