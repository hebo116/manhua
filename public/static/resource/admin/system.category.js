layui.use(['jquery','form'],function(){
	var $ = layui.jquery,
	form = layui.form;
	var len = $('.block-input').length;
	len = len ? (len-1) : 0;
	$('#addInput').click(function(){
		len++;
		var html = '';
		html += '<div class="block-input">';
		html += '<input type="text" name="category['+len+']" />';
		html += '<span class="delete"></span>';
		html += '</div>';
		$('.block-item-box').append(html);
	});
	$('body').on('click','.delete',function(){
		$(this).parent().remove();
	});
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
		});
	});
});