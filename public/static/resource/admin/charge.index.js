layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'money', title: '充值金额',align:'center',minWidth:120},
		    {title: '套餐内容',align:'center',minWidth:120,templet:function(d){
		    	var str = '';
		    	switch(parseInt(d.type)){
			    	case 1:
			    		str += '充值:'+d.content.coin+'书币';
			    		if(parseInt(d.content.send_coin) > 0){
			    			str += ',赠送:'+d.content.send_coin+'书币'; 
			    		}
			    		break;
			    	case 2:
			    		switch(parseInt(d.content.vip_type)){
			    		case 1:
			    			str += '日会员';
			    			break;
			    		case 2:
			    			str += '月会员';
			    			break;
			    		case 3:
			    			str += '季度会员';
			    			break;
			    		case 4:
			    			str += '半年度会员';
			    			break;
			    		case 5:
			    			str += '年度会员';
			    			break;
			    		case 6:
			    			str += '终身会员';
			    			break;
			    		}
			    		break;
		    	}
		    	return str;
		    }},
		    {title: '充值限制',align:'center',minWidth:120,templet:function(d){
		    	if(parseInt(d.pay_times) > 0){
		    		return '限冲'+d.pay_times+'次';
		    	}else{
		    		return '不限次数';
		    	}
		    }},
		    {title: '是否热门',align:'center',minWidth:160,templet:function(d){
		    	if(parseInt(d.is_hot) == 1){
		    		return '是';
		    	}else{
		    		return '否&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="sethot">设为热门</a>';
		    	}
		    }},
		    {title: '是否选中',align:'center',minWidth:160,templet:function(d){
		    	if(parseInt(d.is_check) == 1){
		    		return '是';
		    	}else{
		    		return '否&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="setcheck">默认选中</a>';
		    	}
		    }},
		    {title:'排序',minWidth:140,align:'center',templet:function(d){
            	var str = '';
            		str += '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="sortUp">向上排序</a>';
    				str += '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="sortDown">向下排序</a>';
            	return str;
            }},
		    {title: '状态',align:'center',minWidth:160,templet:function(d){
		    	if(parseInt(d.status) == 1){
		    		return '显示&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="off">隐藏</a>';
		    	}else{
		    		return '隐藏&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="on">显示</a>';
		    	}
		    }},
		    {title: '操作',align:'center',minWidth:120,templet:function(d){
		    	var str = '';
		    		str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
		    		str += '<a class="layui-btn layui-btn-normal layui-btn-xs layui-btn-danger" lay-event="delete">删除</a>';
		    	return str;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		switch(obj.event){
			case 'on':
				doState('确定要显示该套餐吗？',data);
				break;
			case 'off':
				doState('确定要隐藏该套餐吗？',data);
				break;
			case 'sethot':
				doState('确定要将该套餐设为热门吗？',data);
				break;
			case 'setcheck':
				doState('确定要将该套餐设为默认选中吗？',data);
				break;
			case 'delete':
				doState('删除后不可恢复，是否继续？',data);
				break;
			case 'sortUp':
			case 'sortDown':
				doState('',data);
				break;
			default:
				layError('该按钮未绑定事件');
				break;
		}
	});
	
	function doState(asked,data){
		ajaxPost(U('doChargeEvent'),data,asked,function(){
			layOk('操作成功');
			setTimeout(function(){
				layLoad('数据加载中...');
				table.reload('table-block');
			},1400);
		});
	}
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});