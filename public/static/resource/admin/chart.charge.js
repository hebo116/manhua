layui.use(['table','element'],function(){
	var table = layui.table,
	element = layui.element;
	var dataurl = U('charge');
	//实例化活动充值
	table.render({
		elem : '#book-table',
		url : dataurl,
		loading : true,
		cols : [[
			{type: 'numbers',title: '序号'},
		    {field: 'name', title: '书名',align:'center',minWidth:100},
		    {field: 'yesterday',title: '昨日充值',align:'center',minWidth:100},
		    {field: 'money', title: '总充值',align:'center',minWidth:200}
		]],
		page : true,
		height : 'full',
		response: {statusCode:1}
	});
	
});