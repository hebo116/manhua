layui.use(['jquery','form','upload'],function(){
	var $ = layui.jquery,
		form = layui.form,
		upload = layui.upload;
	upload.render({
		  elem: '#doUpload',
		  url: U('Upload/doUploadImg'),
		  accept : 'images',
		  exts : 'jpg|jpeg|png|gif',
		  size : 1024,
		  drag : false,
		  done : function(res){
			  if(res.code === 1){
				  var src = res.data.url;
				  $('#showImg').attr('src',src);
				  $('#hideVal').val(src);
			  }else{
				  layError(res.msg);
			  }
		  },
		  error : function(){
			  layError('上传失败,请重试');
		  }
	});
	upload.render({
		elem: '#doUpload2',
		url: U('Upload/doUploadImg'),
		accept : 'images',
		exts : 'jpg|jpeg|png|gif',
		size : 1024,
		drag : false,
		done : function(res){
			if(res.code === 1){
				var src = res.data.url;
				$('#showImg2').attr('src',src);
				$('#hideVal2').val(src);
			}else{
				layError(res.msg);
			}
		},
		error : function(){
			layError('上传失败,请重试');
		}
	});
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			$("#SiteName",window.parent.document).html(data.name);
		});
	});
});