layui.use(['jquery','table','element'],function(){
		var $ = layui.jquery, 
		table = layui.table,
		element = layui.element;
		var is_refresh = true;
		getTodayMsg();
		$('.refresh-span').click(function(){
			if(is_refresh){
				is_refresh = false;
			}else{
				is_refresh = true;
				getTodayMsg();
			}
		});
		
		function getTodayMsg(){
			if(is_refresh){
				var sec = parseInt($('#refreshSec').text());
				if(sec === 0){
					ajaxPost('',{},'',function(d){
						console.log(d);
						var str = '';
						str += '<h4>今日充值</h4>';
						str += '<p class="block-numbers">¥'+d.total_money+'</p>';
						str += '<div class="layui-row">';
						str += '<div class="layui-col-md6">';
						str += '<h4 class="sub-title">非VIP充值</h4>';
						str += '<p class="sub-numbers">¥'+d.n_money+'</p>';
						str += '<p>已支付：<font class="text-red">'+d.n_pay+'</font>笔</p>';
						str += '<p>未支付：<font class="text-red">'+d.n_notpay+'</font>笔</p>';
						str += '<p>完成率：<font class="text-red">'+d.n_rate+'%</font></p>';
						str += '</div>';
						str += '<div class="layui-col-md6">';
						str += '<h4 class="sub-title">VIP充值</h4>';
						str += '<p class="sub-numbers">¥'+d.p_money+'</p>';
						str += '<p>已支付：<font class="text-red">'+d.p_pay+'</font>笔</p>';
						str += '<p>未支付：<font class="text-red">'+d.p_notpay+'</font>笔</p>';
						str += '<p>完成率：<font class="text-red">'+d.p_rate+'%</font></p>';
						str += '</div>';
						str += '</div>';
						str += '<div class="layui-row">';
						str += '<hr>';
						str += '<div class="layui-col-md6">普通充值：<font class="text-red">'+d.type1_money+'</font></div>';
						str += '<div class="layui-col-md6">活动充值：<font class="text-red">'+d.type2_money+'</font></div>';
						str += '</div>';
						$('#todayContent').html(str);
					});
					sec = 60;
				}else{
					sec--;
				}
				$('#refreshSec').text(sec);
				setTimeout(function(){
					getTodayMsg();
				},999);
			}
		}
	});