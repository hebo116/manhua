layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	var dataurl = window.location.href;
	renderTable(dataurl);
	function renderTable(url,where){
		if(!where){
			where = {};
		}
		table.render({
			elem : '#table-block',
			url : dataurl,
			where : where,
			loading : true,
			cols : [[
				{
					title: '封面',
					minWidth:100,
					align:'center',
					templet:function(d){
					var str = '<font class="text-red">暂无封面</font>';
					if(d.cover){
						str = '<img src="'+d.cover+'" style="width:50px;" />';
					}
					return str;
				}},
			    {
			    	title: '视频标题',
					align:'center',
					minWidth:200,
					templet:function(d){
					var str = d.name + "&nbsp;";
					str += '<a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="copy" title="点击复制链接"><i class="layui-icon layui-icon-link"></i></a>';
					return str
			    }},
			    {
					field: "free_str",
					title: "是否收费",
					align: "center",
					minWidth: 120
				},
			    {
					field: "read_num",
					title: "播放次数",
					align: "center",
					minWidth: 120
			    },
			    {
					title: "状态",
					align: "center",
					minWidth: 200,
					templet: function(d) {
						var str = d.status_name;
						var g = parseInt(d.status);
						switch (g) {
							case 1:
								str += '&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">下架</a>';
								break;
							case 2:
								str += '&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="on">上架</a>';
								break
						}
						return str
				}},
			    {
					title: "操作",
					align: "center",
					minWidth: 400,
					templet: function(d) {
						var str = "";
						str += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="play">播放</a>';
						str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="' + d.do_url + '">编辑</a>';
						str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
						return str
					}
				}
			]],
			page : true,
			height : 'full-420',
			response: {statusCode:1}, 
			id : 'table-block',
		    done:function (res) {
		    	layer.closeAll();
		    }
		});
	}
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		switch(obj.event){
			case 'on':
				doState({id:field.id,event:obj.event},'确定要上架该视频吗？');
				break;
			case 'off':
				doState({id:field.id,event:obj.event},'确定要下架该视频吗？');
				break;
			case 'delete':
				doState({id:field.id,event:obj.event},'删除后不可恢复，确定要删除该视频吗？');
				break;
			case 'spread':
				createLink(field.spread_url);
				break;
			case 'copy':
				layPage('复制视频链接【'+field.name+'】',field.copy_url,'800px','500px');
				break;
			case "play":
				layPage("播放【" + field.name + "】", field.play_url, "800px", "500px");
				break
		}
	});
	
	$('body').on('click','#refresh',function(){
		ajaxPost(U('refreshCache'),{},'',function(){
			layOk('更新成功');
		});
	});
	
	function doState(data,asked){
		ajaxPost(U('doVideoEvent'),data,asked,function(){
			layOk('操作成功');
			setTimeout(function(){
				layLoad('数据加载中...');
				table.reload('table-block');
			},1400);
		});
	}
	
	function createLink(url){
		layer.open({
			  type: 2,
			  title: '生成推广链接',
			  closeBtn: 1,
			  shade: [0],
			  area: ['800px','630px'],
			  offset : '100px',
			  time: 0,
			  anim: 2,
			  scrollbar :false,
			  content: [url,'yes'],
			  btn : ['生成推广链接'],
			  yes : function(index,layero){
				  var iframeWindow = $(layero).find('iframe')[0].contentWindow,
		            submitID = 'pageSubmit',
		            submit = layero.find('iframe').contents().find('#'+ submitID);
		            iframeWindow.layui.form.on('submit('+ submitID +')', function(obj){
		              var data = obj.field;
		              ajaxPost(url,data,'确定要生成推广链接吗？',function(res){
	            		  layOk('生成成功,请前往推广链接列表查看');
	            		  layer.close(index);
		              });
		            });  
		            submit.trigger('click');
			  }
		});
	}
	
	$('.search-item').click(function(){
		if($(this).hasClass('layui-btn-normal')){
			return false;
		}
		$(this).addClass('layui-btn-normal').removeClass('layui-btn-primary');
		$(this).siblings().removeClass('layui-btn-normal').addClass('layui-btn-primary');
		var where = getWhere();
		console.log(where);
		layLoad('数据加载中...');
		renderTable(dataurl,where);
	});
	
	function getWhere(){
		var data = {};
		$('.search-block').each(function(){
			var val = $(this).find('.search-item.layui-btn-normal').attr('data-val');
			if(val){
				var name = $(this).attr('data-name');
				data[name] = val;
			}
		});
		return data;
	}
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			url : dataurl,
			where : where,
			page: {curr:1}
		});
	});
});