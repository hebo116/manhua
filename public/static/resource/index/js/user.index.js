//复制
    var clipboard = new ClipboardJS('#copywb');
    var clipboard = new ClipboardJS('#copysina');
    var clipboard = new ClipboardJS('#copyline');
    var clipboard = new ClipboardJS('#copyqq');
    var clipboard = new ClipboardJS('#copywechat');
    
    $(".list_type li").click(function () {
        $(this).addClass("chooseList_red_color_themes");
        $(this).siblings().removeClass("chooseList_red_color_themes")
    });
    
    $('.popup_bg').on('click',function(){
    	$('#contact').animate({
            "bottom":"-10rem"
        });
    	$('#line_pop').hide().addClass('scaleBoxto0').removeClass("scaleBoxto1");
    	$('#wechat_pop').hide().addClass("scaleBoxto0").removeClass('scaleBoxto1');
    	$('#qq_pop').hide().addClass("scaleBoxto0").removeClass('scaleBoxto1');
    	$('#wb').hide().addClass("scaleBoxto0").removeClass('scaleBoxto1');
    	$('#sina').hide().addClass("scaleBoxto0").removeClass('scaleBoxto1');
    	$('.popup_bg').fadeOut();
    });
    
    // 展示微博下拉
    $('#showWb').on('change', function () {
        if ($(this).is(':checked')) {
            $('#close').animate({
                "height": "3.04rem"
            });
        } else {
            $('#close').animate({
                "height": 0
            });
        }
    })
    // 展示邮箱信息
    $('#showWbpop').on("click", function () {
        $('.popup_bg').fadeIn();
        $('#wb').show().addClass("scaleBoxto1").removeClass('scaleBoxto0');
    });
    // 展示邮箱提示
    $('#copywb').on('click', function () {
        $('#wb').addClass("scaleBoxto0").removeClass('scaleBoxto1');
        $('#wbTips').show().addClass("scaleBoxto1").removeClass('scaleBoxto0');
        setTimeout(function () {
            $('#wb').hide();
        }, 90);
    });
    // 关闭邮箱提示
    $('#wbTipsClose').on("click", function () {
        $('#wbTips').addClass("scaleBoxto0").removeClass('scaleBoxto1');
        setTimeout(function () {
            $('#wbTips').hide();
            $('.popup_bg').fadeOut();
        }, 90);
    });
    // 展示微博信息
    $('#showSinapop').on("click", function () {
        $('.popup_bg').fadeIn();
        $('#sina').show().addClass("scaleBoxto1").removeClass('scaleBoxto0');
    });
    // 展示微博提示
    $('#copysina').on('click', function () {
        $('#sina').addClass("scaleBoxto0").removeClass('scaleBoxto1');
        $('#sinaTips').show().addClass("scaleBoxto1").removeClass('scaleBoxto0');
        setTimeout(function () {
            $('#sina').hide();
        }, 90);
    });
    // 关闭微博提示
    $('#sinaTipsClose').on("click", function () {
        $('#sinaTips').addClass("scaleBoxto0").removeClass('scaleBoxto1');
        setTimeout(function () {
            $('#sinaTips').hide();
            $('.popup_bg').fadeOut();
        }, 90);
    });

    //点击联系客服按钮
    $('#contact_btn').on("click", function () {
        $('.popup_bg').fadeIn();
        $('#contact').animate({
            "bottom": 0
        });
    });

    // line弹窗
    $('#line').on("click", function () {
        $('#contact').animate({
            "bottom":"-10rem"
        });
        $('#line_pop').show().addClass("scaleBoxto1").removeClass('scaleBoxto0');
    })
    // 关闭line弹窗
    $("#copyline").on("click",function(){
        $('#line_pop').addClass("scaleBoxto0").removeClass('scaleBoxto1');
        setTimeout(function () {
            $('#line_pop').hide();
            $('.popup_bg').fadeOut();
        }, 90);
    });

    // qq弹窗
    $('#qq').on("click", function () {
        $('#contact').animate({
            "bottom":"-10rem"
        });
        $('#qq_pop').show().addClass("scaleBoxto1").removeClass('scaleBoxto0');
    });
    // 关闭qq弹窗
    $("#copyqq").on("click",function(){
        $('#qq_pop').addClass("scaleBoxto0").removeClass('scaleBoxto1');
        setTimeout(function () {
            $('#qq_pop').hide();
            $('.popup_bg').fadeOut();
        }, 90);
    });

     // wechat弹窗
     $('#wechat').on("click", function () {
        $('#contact').animate({
            "bottom":"-10rem"
        });
        $('#wechat_pop').show().addClass("scaleBoxto1").removeClass('scaleBoxto0');
    });
    // 关闭qq弹窗
    $("#copywechat").on("click",function(){
        $('#wechat_pop').addClass("scaleBoxto0").removeClass('scaleBoxto1');
        setTimeout(function () {
            $('#wechat_pop').hide();
            $('.popup_bg').fadeOut();
        }, 90);
    });