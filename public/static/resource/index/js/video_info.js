var player;
getComments();
getSameVideos();
function getComments() {
    var a = U("Com/getComments");
    if (a) {
        ajaxPost(a, {
                pid: video_id,
                ptype: 4
            },
            function(b) {
                if (b.list) {
                    var c = "";
                    $.each(b.list,
                        function(d, e) {
                            c += "<li>";
                            c += '<div class="evaluationImg">';
                            c += '<img src="' + e.headimgurl + '" alt="">';
                            c += "</div>";
                            c += '<div class="evaluationFont">';
                            c += "<div>";
                            c += "<h4>" + e.nickname + "</h4>";
                            c += "<span>2019-09-27</span>";
                            c += "</div>";
                            c += "<p>" + e.content + "</p>";
                            c += "</div>";
                            c += "</li>"
                        });
                    $(".evaluationList>ul").html(c)
                } else {
                    $(".evaluationList>ul").html('<li style="text-align:center;font-size:1.3rem;">暂无评论</li>')
                }
                $("#commentNum").text("(" + b.count + ")")
            })
    }
}
function getSameVideos() {
    var a = U("getSameVideos");
    ajaxPost(a, {
            video_id: video_id
        },
        function(c) {
            if (c.list) {
                var b = U("info");
                var d = "";
                $.each(c.list,
                    function(e, f) {
                        d += "<li>";
                        d += '<a href="' + b + "?video_id=" + f.id + '">';
                        d += "<div>";
                        d += '<img src="' + f.cover + '">';
                        d += "</div>";
                        d += "<h4>" + f.name + "</h4>";
                        d += "</a>";
                        d += "</li>"
                    });
                if (d) {
                    $(".choicenessCount>ul").html(d);
                    $(".evaluation").show()
                }
            }
        })
}
function doPlay(b) {
    if (!player) {
        var a = U("checkPlay");
        postRes(a, {
                video_id: video_id
            },
            function(d) {
                switch (d.flag) {
                    case 0:
                        var e = {
                            container: "#video",
                            variable: "player",
                            autoplay: true,
                            video: {
                                file: d.url,
                                type: "video/mp4"
                            }
                        };
                        player = new ckplayer(e);
                        $(b).html("<em>立即播放</em>");
                        player.videoPlay();
                        player.addListener("play", c);
                    function c() {
                        $(b).html("<em>暂停播放</em>")
                    }
                        player.addListener("pause", f);
                    function f() {
                        $(b).html("<em>立即播放</em>")
                    }
                        $(".videoName").css("margin", "0 0 0 0");
                        break;
                    case 1:
                        layer.open({
                            content:
                            d.msg,
                            btn: ["确定", "取消"],
                            yes: function(g) {
                                layer.close(g);
                                window.location.href = d.url
                            }
                        });
                        return false;
                        break;
                    case 2:
                        layer.open({
                            content:
                                "您的书币余额不足，是否立即充值?",
                            btn: ["确定", "取消"],
                            yes: function(g) {
                                layer.close(g);
                                window.location.href = d.url
                            }
                        });
                        return false;
                        break
                }
            })
    } else {
        player.playOrPause()
    }
}
function rewardLayer() {
    layer.open({
        content: $(".ds_proup").html(),
        shadeClose: false,
        className: "layerPadding",
        style: "background-color:#fff; color:#fff; border:none;"
    })
}
function chooseRewardMoney(a) {
    $(a).addClass("active").siblings().removeClass("active")
}
function doReward() {
    var b = $(".layerPadding .reward_money .active").attr("data-val");
    var c = {
        money: b,
        relation_id: book_id,
        relation_type: book_type
    };
    var a = U("Charge/doReward");
    if (a) {
        ajaxPost(a, c,
            function(d) {
                window.location.href = d.url
            })
    }
}
$(function() {
    c();
    $(window).resize(function() {
        c()
    });
    function c() {
        var d = $("body").width();
        $(".footerBook,.video").width(d)
    }
    var b = 0;
    function a() {
        var g = window.innerHeight;
        var e = $(window).scrollTop();
        var f = $(document).height();
        var d = [g, e, f];
        return d
    }
    a();
    $(window).scroll(function() {
        var d = a();
        var g = d[0];
        var e = d[1];
        var f = d[2];
        if (e < 20) {
            $(".video").removeClass("active")
        } else {
            if (e > b) {
                $(".video").addClass("active")
            }
        }
        b = e
    })
});