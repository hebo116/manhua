var countdown = 0;
    $('.common_input').on('focus', function () {
        $(this).next().removeClass('scale_enlarge');
        $(this).next().addClass('scale_small');
        $(this).parent().addClass('red_border_color_themes');
    });
    $('.common_input').on('blur', function () {
    	var val = $(this).val();
    	if(val.length === 0){
    		$(this).next().removeClass('scale_small');
            $(this).next().addClass('scale_enlarge');
            $(this).parent().removeClass('red_border_color_themes');
    	}
    });
    
    $('#showPhoneBox').addClass('red_border_color_themes');
    $('#showPhoneBox label').addClass('scale_small').removeClass('scale_enlarge');
	
    //获取验证码
    function getCode(){
    	var code = $('#code').val();
    	var reg = /^[\d]{6}$/;
    	if(!reg.test(code)){
  		    return false;
  	    }
    	return code;
    }
    
    //获取手机号
    function getPhone(){
    	var phone = $('#phone').val();
    	var reg = /^1[3456789]\d{9}$/;
  	    if(!reg.test(phone)){
  		    countdown = 0;
  		    return false;
  	    }
  	    return phone;
    }
    
    
    
    
    function sendRegCode(obj){
    	if(countdown !== 0){
    		return false;
    	}
    	countdown = 120
    	var phone = getPhone();
    	if(!phone){
    		layMsg('手机号格式有误,请确认');
    		return false;
    	}
	   	  $.ajax({
	   		  type : 'post',
	   		  url : U('Com/sendRegCode'),
	   		  data : {phone:phone},
	   		  dataType : 'json',
	   		  success : function(res){
	   			  if(res.code == 1){
	   					settime(obj);
	   			  }else{
	   				  countdown = 0;
	   				  layMsg(res.msg);
	   			  }
	   		  },
	   		  error : function(){
	   			  countdown = 0;
	   			  layMsg('网络错误,请稍后再试');
	   		  }
	   	  });
    }
    
    function sendLoginCode(obj){
    	if(countdown !== 0){
    		return false;
    	}
    	countdown = 120
    	$.ajax({
    		type : 'post',
    		url : U('Com/sendLoginCode'),
    		dataType : 'json',
    		success : function(res){
    			if(res.code == 1){
    				settime(obj);
    			}else{
    				countdown = 0;
    				layMsg(res.msg);
    			}
    		},
    		error : function(){
    			countdown = 0;
    			layMsg('网络错误,请稍后再试');
    		}
    	});
    }

    function settime(obj) { 
    	  if (countdown == 0) { 
    		  $(obj).text("重新获取"); 
    		  return;
    	  } else { 
    		  $(obj).text(countdown + "s"); 
    		  countdown--; 
    	  } 
    	  setTimeout(function() { 
    	  		settime(obj); 
    	  },1000) 
    }
    
    function doRegister(){
    	var user = $('#user').val();
    	if(user.length === 0){
    		layMsg('请输入用户名');
    		return false;
    	}
    	var code = $('#code').val();
    	var pwd = $('#pwd').val();
    	if(pwd.length === 0){
    		layMsg('请输入密码');
    		return false;
    	}
    	var data = {username:user,invite_code:code,password:pwd};
    	ajaxPost('',data,function(d){
    		layMsg('注册成功');
    		setTimeout(function(){
    			window.location.href = d.url;
    		},1000);
    	});
    }
    
    function loginByUsername(){
    	var user = $('#user').val();
    	if(user.length === 0){
    		layMsg('请输入用户名');
    		return false;
    	}
    	var pwd = $('#pwd').val();
    	if(pwd.length === 0){
    		layMsg('请输入密码');
    		return false;
    	}
    	var data = {username:user,password:pwd};
    	ajaxPost('',data,function(d){
    		layMsg('登录成功');
    		setTimeout(function(){
    			window.location.href = d.url;
    		},1000);
    	});
    }
    
    //修改手机号
    function doChangePwd(){
    	var pwd = $('#pwd').val();
    	if(pwd.length === 0){
    		layMsg('请输入新密码');
    		return false;
    	}
    	if(pwd.length < 6){
    		layMsg('请输入至少6位新密码');
    		return false;
    	}
    	var repwd = $('#repwd').val();
    	if(pwd !== repwd){
    		layMsg('两次输入密码不一致');
    		return false;
    	}
    	var data = {pwd:pwd,repwd:repwd};
    	ajaxPost('',data,function(d){
    		layMsg(d.msg);
    		setTimeout(function(){
    			window.location.href = d.url;
    		},1000);
    	})
    }
    
    //解绑提交
    function doUnBindPhone(){
    	var code = getCode();
    	if(!code){
    		layMsg('请输入正确格式的验证码');
    		return false;
    	}
    	var data = {code:code};
    	ajaxPost('',data,function(d){
    		layMsg(d.msg);
    		setTimeout(function(){
    			window.location.href = d.url;
    		},1000);
    	});
    }
    
    //绑定手机提交
    function doPhoneSubmit(){
    	var phone = getPhone();
    	if(!phone){
    		layMsg('请输入正确格式的手机号');
    		return false;
    	}
    	var code = getCode();
    	if(!code){
    		layMsg('请输入正确格式的验证码');
    		return false;
    	}
    	var data = {phone:phone,code:code};
    	ajaxPost('',data,function(d){
    		layMsg(d.msg);
    		setTimeout(function(){
    			window.location.href = d.url;
    		},1000);
    	});
    }