$('.date').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        var number = parseInt($(this).attr('id'));
        $("#line").animate({
            left: (number - 1) / 4 * 100 + "%",
        });
        var showId = 'rank-ul-'+number;
    	$('.rank-ul').hide();
    	$('#'+showId).show();
    });

    var scoll = {
            lastScroH: 0,
            isAdd: false,
            navHeight: $('.nav').height()
        }
    $(window).scroll(function () {
        var scroH = $(document).scrollTop();
        if (scroH > scoll.lastScroH) { //下滑
            if (!scoll.isAdd) {
                $(".header").animate({
                    top: "-" + scoll.navHeight + "px",
                });
            }
            scoll.isAdd = true
        } else {
            if (scoll.isAdd) {
                $(".header").animate({
                    top: 0,
                });
            }
            scoll.isAdd = false
        }
        //重置上次高度
        setTimeout(function () {
            scoll.lastScroH = scroH
        }, 0);
    })