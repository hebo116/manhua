var is_end=false,page=1;
getData();
$(window).scroll(function() {
	var scrollTop = $(this).scrollTop();
	var scrollHeight = $(document).height();
	var windowHeight = $(this).height();
	if (scrollTop + windowHeight == scrollHeight) {
		if(!is_end){
   			getData();
		}
	}
});
function getData(){
	if(!is_end){
		is_end = true;
		$('#loadMore').text('数据加载中...').show();
		ajaxPost('',{page:page},function(list){
			if(list){
				is_end = false;
				$('#loadMore').hide();
				var str = createHtml(list);
				$('#msgList').append(str);
				page++;
			}else{
				if(page === 1){
					$('#loadMore').text('暂无消息');
				}else{
					$('#loadMore').text('别扯了，已经到底了^v^');
				}
			}
		});
	}
}
    function createHtml(list){
    	var str = '';
    	$.each(list,function(i,item){
    		str += '<li>';
    		str += '<img class="icon" src="/static/resource/index/images/news.png">';
    		str += '<div class="right">';
    		str += '<p class="type">系统公告</p>';
    		str += '<p class="time">'+item.create_time+'</p>';
    		str += '<p class="title">'+item.title+'</p>';
    		str += '<p class="main">'+item.content+'</p>';
    		str += '</div>';
    		str += '</li>';
    	});
    	return str;
    }