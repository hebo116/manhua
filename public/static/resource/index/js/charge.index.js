$('input[name="charge_id"]').click(function(){
	var money = $(this).attr('data-money');
	$('.pay_recharge').text('立即支付'+money+'元');
	doChargeSub();
});
function doChargeSub(){
	var charge_id = parseInt($('input[name="charge_id"]:checked').val());
	if(isNaN(charge_id)){
		layMsg('您尚未选择充值套餐');
		return false;
	}
	var pay_type = parseInt($('input[name="pay_type"]:checked').val());
	if(isNaN(pay_type)){
		layMsg('您尚未选择充值方式');
		return false;
	}
	var data = {charge_id:charge_id,pay_type:pay_type};
	ajaxPost('',data,function(d){
		window.location.href = d.url;
	});
}