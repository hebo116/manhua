$('.date').on('click', function () {
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
        var number = $(this).attr('id');
        var showId = 'show-ul-'+number;
        $('.show-ul').hide();
        $('#'+showId).show();
        $("#line_today").animate({
            left: (number - 1) / 7 * 100 + "%",
        });
    });

    var scoll = {
            lastScroH: 0,
            isAdd: false,
            navHeight: $('.nav').height()
        }
    $(window).scroll(function () {
        var scroH = $(document).scrollTop();
        if (scroH > scoll.lastScroH) { //下滑
            if (!scoll.isAdd) {
                $(".header").animate({
                    top: "-" + scoll.navHeight + "px",
                });
            }
            scoll.isAdd = true
        } else {
            if (scoll.isAdd) {
                $(".header").animate({
                    top: 0,
                });
            }
            scoll.isAdd = false
        }
        //重置上次高度
        setTimeout(function () {
            scoll.lastScroH = scroH
        }, 0);
    })