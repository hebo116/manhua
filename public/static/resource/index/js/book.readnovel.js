$(function () {
    monitor();
    doSetPage();
    //点击选背景色
    $('.background_A').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
        var $background =$(this).css("background-color");
        $.cookie('read_page_color',$background);
        $('.fictionCount,.read_main,.fixedScrool,.fontCount>p').css('background-color',$background);
    });

    //点击A+ A- 字体变大
    var font = 1,size = 0.5,line = 0.8;
    $('.font_A').click(function(){
        var index =$('.font_A').index($(this));
        if(index == 0){
            font++;
            if(font>4){
                font = 4;
            }else{
                size += 0.1;
                line += 0.1;
            }
        }else{
            font--;
            if(font < 1){
                font = 1;
            }else{
                size = size - 0.1;
                line = line - 0.1;
            }
        }
        $.cookie('read_page_size',size);
        $.cookie('read_page_line',line);
        $('.fontCount>p').css('font-size',size+'rem');
        $('.fontCount>p').css('line-height',line+'rem');
    })

    //滑动事件
    var winSTbefore = 0;//用于装触发scroll事件的上一个scrollTop
    function monitor() {
        var winH = window.innerHeight;    //获取浏览器窗口高度
        var winST = $(window).scrollTop();  //获取scrollTop
        var docH = $(document).height();  //获取文档高度
        var $fixedScrool= $('.fixedScrool').innerHeight(); //获取头部固定高度
        var arr = [winH, winST, docH,$fixedScrool];
        return arr;
    }

});

function doSetPage(){
    var $background = $.cookie('read_page_color');
    if($background){
        $('.fictionCount,.fixedScrool,.read_main,.fontCount>p').css('background-color',$background);
        $('.background_A').each(function(){
            var cbg = $(this).css('background-color');
            if(cbg === $background){
                $(this).addClass('active');
                return false;
            }
        });
    }
    var $size = $.cookie('read_page_size');
    var $line = $.cookie('read_page_line');
    if($size){
        $('.fontCount>p').css('font-size',$size+'rem');
    }
    if($line){
        $('.fontCount>p').css('line-height',$line+'rem');
    }
}

