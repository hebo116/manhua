var total_page = $('.content-ul').length,cur_page = 1,sort_type=1;
	total_page = total_page > 0 ? total_page : 1;
	checkPage();
	function checkPage(){
		$('.pageBtn').addClass('no');
		if(cur_page < total_page){
			$('#nextBtn').removeClass('no');
		}
		if(cur_page > 1){
			$('#prevBtn').removeClass('no');
		}
	}
	
	$('#prevBtn').click(function(){
		if($(this).hasClass('no')){
			return false;
		}
		page--;
		page = page < 1 ? 1 : page;
		checkPage();
		var showId = 'block_'+page;
		$('.content-ul').hide();
		$('#'+showId).show();
	});
	$('#nextBtn').click(function(){
		if($(this).hasClass('no')){
			return false;
		}
		page++;
		page = page > total_page ? total_page : page;
		checkPage();
		var showId = 'block_'+page;
		$('.content-ul').hide();
		$('#'+showId).show();
		closeChooseDirectory
	});
	
	$('.dir-item').click(function(){
		var number = parseInt($(this).attr('data-number'));
		if(number === cur_page){
			return false;
		}
		page = number;
		checkPage();
		var showId = 'block_'+page;
		$('.content-ul').hide();
		$('#'+showId).show();
		closeChooseDirectory();
	});
	
	$('#doSort').click(function(){
		sort_type = sort_type === 1 ? 2 : 1;
		var sort_str = sort_type === 1 ? '倒序' : '正序';
		$('#sortStr').text(sort_str);
		$('.sort_title').each(function(){
			var title_str = $(this).text();
			var arr = title_str.split('~');
			var new_arr = arr.reverse();
			var new_str = new_arr.join('~');
			$(this).text(new_str);
		});
		$('.content-ul').each(function(){
			var obj = $(this);
			var lis = obj.find('li');
			var total = lis.length;
			if(total){
				lis.each(function(i){
					obj.append(lis.eq((total-1-i)));
				});
			}
		});
	});
	
    var scoll = {
        lastScroH: 0,
        isAdd: false,
        navHeight: $('.nav').height()
    }
    $(window).scroll(function () {
        var scroH = $(document).scrollTop();
        if (scroH > scoll.lastScroH) { //下滑
            if (!scoll.isAdd) {
                $(".header").animate({
                    top: "-" + scoll.navHeight + "px",
                });
            }
            scoll.isAdd = true
        } else {
            if (scoll.isAdd) {
                $(".header").animate({
                    top: 0,
                });
            }
            scoll.isAdd = false
        }
        //重置上次高度
        setTimeout(function () {
            scoll.lastScroH = scroH
        }, 0)
    })

    // 显示选择章节
    $('.nav_right').on("click", function () {
        $('.choose_popup_bg').fadeIn();
        $('.choose_popup_main').show().addClass('scaleBoxto1');
        setTimeout(function () {
            $('.choose_popup_main').removeClass('scaleBoxto1');
        }, 100)
    })

    // 关闭章节选择
    $('.choose_popup_bg').on("click", closeChooseDirectory);
    // 关闭章节选择
    function closeChooseDirectory() {
        $('.choose_popup_bg').fadeOut();
        $('.choose_popup_main').addClass('scaleBoxto0');
        setTimeout(function () {
            $('.choose_popup_main').hide().removeClass('scaleBoxto1');
        }, 100)
    }

    //全屏移动
    // 全屏左移动画
    $('#show_my').on("click", function () {
        $('#directory_box').animate({
            "right": "6.8533rem"
        });
        $('#mongolia').show();
    })

    $('#mongolia').on("click", function () {
        $('#directory_box').animate({
            "right": 0
        });
        $('#mongolia').hide();
    })