function delRecord(book_id,type,obj){
		var data = {book_id:book_id,type:type};
		ajaxPost(U('com/delRecord'),data,function(){
			$('.succs_pop').fadeIn();
	        setTimeout(function(){
	            $('.succs_pop').fadeOut();
	            window.location.reload();
	        },1500);
		});
	}
	
	function toRead(url){
		var flag = $('#status').is(':checked');
		if(!flag){
			window.location.href = url;	
		}
	}
	
	function clearRecord(type){
		ajaxPost(U('com/clearRecord'),{type:type},function(){
			$('.succs_pop').fadeIn();
	        setTimeout(function(){
	            $('.succs_pop').fadeOut();
	            window.location.reload();
	        },1500);
		});
	}
	
    $("#status").on("change", function () {
        if ($(this).is(":checked")) {
            $(this).text("完成")
            $('#clear').fadeIn();
            $('.delete').fadeIn();
        } else {
            $(this).text("編輯")
            $('#clear').fadeOut();
            $('.delete').fadeOut();
        }
    })