var is_end=false,page=1;
getData();
$(window).scroll(function() {
	var scrollTop = $(this).scrollTop();
	var scrollHeight = $(document).height();
	var windowHeight = $(this).height();
	if (scrollTop + windowHeight == scrollHeight) {
		if(!is_end){
   			getData();
		}
	}
});
function getData(){
	if(is_end === false){
		is_end = true;
		ajaxPost('',{page:page},function(list){
			if(list){
				$.each(list,function(i,item){
					var dateId = 'date-'+item.month;
					var obj = $('#'+dateId);
					var str = '';
					str += '<div class="item">';
					str += '<div class="left">';
					str += '<p class="week">'+item.week+'</p>';
					str += '<p class="mouth">'+item.date+'</p>';
					str += '</div>';
					str += '<div class="center">';
					str += '<p class="center_top">'+item.title+'</p>';
					str += '<p class="center_bottom">'+item.summary+'</p>';
					str += '</div>';
					str += '<p class="record">'+item.money+'書幣</p>';
					str += '</div>';
					if(obj.length > 0){
						obj.append(str);
					}else{
						$('#main-box').append('<div id="'+dateId+'"><p class="date">'+item.month+'</p>'+str+'</div>');
					}
				});
				if(list.length >= 20){
					page++;
					is_end = false;
				}
			}
		});
	}
}