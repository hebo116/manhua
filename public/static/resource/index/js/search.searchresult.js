$('#clear_input').on("click", function () {
        $('#search_input').val("");
    });
    searchData();
    function searchData(){
    	var keyword = $('#search_input').val();
    	if(keyword.length > 0){
    		ajaxPost('',{keyword:keyword},function(list){
    			if(list){
    				var str = '';
    				str += '<ul class="main">';
    				$.each(list,function(i,item){
	    				str += '<li class="item_1_bar item_1_bar_line" onclick="javascript:toInfo('+item.id+');">';
	    				str += '<div class="img_box">';
	    				str += '<img class="cover" src="'+item.cover+'">';
	    				if(parseInt(item.is_alone) == 1){
	    					str += '<p class="icon_only"></p>';
	    				}
	    				if(parseInt(item.over_type) === 1){
	    					str += '<span class="status_serialization status_icon_short"></span>';
	    				}else{
	    					str += '<span class="status_end status_icon_short"></span>';
	    				}
	    				str += '</div>';
	    				str += '<div class="right">';
	    				str += '<h2 class="title red_color_themes">'+item.name+'</h2>';
	    				str += '<p class="content">'+item.summary+'</p>';
	    				str += '<div class="info">';
	    				str += '<span>作者：'+item.author+'</span>';
	    				str += '<div class="read">';
	    				str += '<img src="/static/resource/index/images/eyes.png">';
	    				str += '<span>'+item.hot_num+'</span>';
	    				str += '</div>';
	    				str += '<div class="start">';
	    				str += '<img src="/static/resource/index/images/star.png">';
	    				str += '<span>'+item.star+'</span>';
	    				str += '</div>';
	    				str += '</div>';
	    				str += '</div>';
	    				str += '</li>';
    				});
    				str += '</ul>';
    				$('.not_book').hide();
    				$('.resultBox').html(str);
    			}else{
    				$('.not_book').show();
    			}
    		});
    	}
    }
    
    $(document).keyup(function(event){
		  if(event.keyCode == 13){
			  searchData();
		  }
	});