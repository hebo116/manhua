var page1 = 1,page5 = 1;
getType1Data();
getType5Data();
        $('#clear_input').on("click", function () {
            $('#search_input').val("");
        });
        $(document).keyup(function(event){
			  if(event.keyCode ==13){
			    var keyword = $('#search_input').val();
			    if(keyword.length > 0){
			    	searchData(keyword);
			    }
			  }
		});
        
        function getType1Data(){
        	var data = {type:1,page:page1,category:$category};
        	ajaxPost(U('book/getTypeData'),data,function(list){
        		if(list){
        			if(page1 === 1){
        				$('#type1Box').show();
        			}
        			var str = createType1Html(list);
        			$('#type1Box>ul').html(str);
        			page1++;
        		}else{
        			if(page1 === 1){
        				$('#type1Box').remove();
        			}else{
        				layMsg('已经到最后一页了');
        			}
        		}
        	});
        }
        function getType5Data(){
        	var data = {type:5,page:page1,category:$category};
        	ajaxPost(U('book/getTypeData'),data,function(list){
        		if(list){
        			if(page5 === 1){
        				$('#type5Box').show();
        			}
        			var str = createType5Html(list);
        			$('#type5Box>ul').html(str);
        			page5++;
        		}else{
        			if(page5 === 1){
        				$('#type5Box').remove();
        			}else{
        				layMsg('已经到最后一页了');
        			}
        		}
        	});
        }
        function createType1Html(list){
        	var str = '';
        	$.each(list,function(i,item){
        		str += '<li class="item_4" onclick="javascript:toInfo('+item.id+');">';
        		str += '<div class="img_box">';
        		str += '<img class="cover" src="'+item.cover+'">';
        		if(parseInt(item.over_type) === 1){
        			str += '<p class="status_serialization status_icon_short"></p>';
        		}else{
        			str += '<p class="status_end status_icon_short"></p>';
        		}
        		str += '</div>';
        		str += '<h4 class="red_color_themes">'+item.name+'</h4>';
        		str += '<div class="info">';
        		str += '<div class="start">';
        		str += '<img src="/static/resource/index/images/star.png">';
        		str += '<span>'+item.star+'</span>';
        		str += '</div>';
        		str += '</div>';
        		str += '</li>';
        	});
        	return str;
        }
        
        function createType5Html(list){
        	var str = '';
        	$.each(list,function(i,item){
        		str += '<li class="item_1_bar item_1_bar_not_line" onclick="javascript:toInfo('+item.id+');">';
        		str += '<div class="img_box">';
        		str += '<img class="cover" src="'+item.cover+'">';
        		if(parseInt(item.over_type) === 1){
        			str += '<p class="status_serialization status_icon_short"></p>';
        		}else{
        			str += '<p class="status_end status_icon_short"></p>';
        		}
        		str += '</div>';
        		str += '<div class="right">';
        		str += '<h2 class="title red_color_themes">'+item.name+'</h2>';
        		str += '<p class="content">'+item.summary+'</p>';
        		str += '<div class="info">';
        		str += '<span>作者：'+item.author+'</span>';
        		str += '<div class="read">';
        		str += '<img src="/static/resource/index/images/eyes.png">';
        		str += '<span>'+item.hot_num+'</span>';
        		str += '</div>';
        		str += '<div class="start">';
        		str += '<img src="/static/resource/index/images/star.png">';
        		str += '<span>'+item.star+'</span>';
        		str += '</div>';
        		str += '</div>';
        		str += '</div>';
        		str += '</li>';
        	});
        	return str;
        }
        
        function searchData(keyword){
        	var url = U('searchResult')+'?keyword='+keyword;
        	window.location.href = url;
        }
        function clearSearch(obj){
        	ajaxPost(U('clearRecord'),{},function(){
        		layMsg('清理成功');
        		$('.searchDiv').html('<p class="no_history">暫無搜索記錄</p>');
        	});
        }
        function delSearch(keyword,obj){
        	ajaxPost(U('delRecord'),{keyword:keyword},function(){
        		layMsg('清理成功');
        		$(obj).parent().remove();
        		var len = $('.li-item').length;
        		len = len ? len : 0;
        		if(!len){
        			$('.searchDiv').html('<p class="no_history">暫無搜索記錄</p>');
        		}
        	});
        }
        $('#showClearBtn').on("change", function () {
            if ($(this).is(':checked')) {
                $('.clear_history').animate({
                    height: '1.2rem'
                })
            } else {
                $('.clear_history').animate({
                    height: 0
                })
            }
        });