var page = 1,is_end=false;
getData();
$(window).scroll(function() {
	var scrollTop = $(this).scrollTop();
	var scrollHeight = $(document).height();
	var windowHeight = $(this).height();
	var thisNum = scrollTop + windowHeight + 100;
	if (thisNum >= scrollHeight) {
		if(!is_end){
   			getData();
		}
	}
});
function getData(){
	if(!is_end){
		is_end = true;
		$('#loadMore').text('数据加载中...').show();
		ajaxPost('',{type:type,page:page,category:category},function(list){
			if(list){
				page++;
				is_end = false;
				$('#loadMore').hide();
				var str = createHtml(list);
				$('#bookList').append(str);
			}else{
				$('#loadMore').text('别扯了，已经到底了^v^');
			}
		});
	}
}
    function createHtml(list){
    	var str = '';
    	$.each(list,function(i,item){
    		str += '';
    		str += '<li class="item_1_bar item_1_bar_line" onclick="javascript:toInfo('+item.id+');">';
    		str += '<div class="img_box">';
    		str += '<img class="cover" src="'+item.cover+'">';
    		if(parseInt(item.is_alone) === 1){
    			str += '<p class="icon_only"></p>';
    		}
    		if(parseInt(item.over_type) === 1){
				str += '<span class="status_serialization status_icon_short"></span>';
			}else{
				str += '<span class="status_end status_icon_short"></span>';
			}
    		str += '</div>';
    		str += '<div class="right">';
    		str += '<h2 class="title red_color_themes">'+item.name+'</h2>';
    		str += '<p class="content">'+item.summary+'</p>';
    		str += '<div class="info">';
    		str += '<span>作者：'+item.author+'</span>';
    		str += '<div class="read">';
    		str += '<img src="/static/resource/index/images/eyes.png">';
    		str += '<span>'+item.hot_num+'</span>';
    		str += '</div>';
    		str += '<div class="start">';
    		str += '<img src="/static/resource/index/images/star.png">';
    		str += '<span>'+item.star+'</span>';
    		str += '</div>';
    		str += '</div>';
    		str += '</div>';
    		str += '</li>';
    	});
    	return str;
    }