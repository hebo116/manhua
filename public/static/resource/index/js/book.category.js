var type=1,page=1,is_end=false;
	getData();
    $(".type_list li").click(function () {
    	var data_type = parseInt($(this).attr('data-type'));
    	if(data_type === type){
    		return false;
    	}
    	page = 1;
    	type = data_type;
    	is_end = false;
    	getData();
        $(this).addClass("chooseList_red_color_themes");
        $(this).siblings().removeClass("chooseList_red_color_themes");
    });
    
    $(window).scroll(function() {
    	var scrollTop = $(this).scrollTop();
    	var scrollHeight = $(document).height();
    	var windowHeight = $(this).height();
    	if (scrollTop + windowHeight == scrollHeight) {
    		if(!is_end){
       			getData();
    		}
    	}
    });
    function getData(){
    	if(!is_end){
    		is_end = true;
    		$('#loadMore').text('数据加载中...').show();
    		ajaxPost('',{type:type,page:page},function(list){
    			if(list){
    				is_end = false;
    				$('#loadMore').hide();
    				var str = createHtml(list);
    				if(page === 1){
    					$('#bookList').html(str);
    				}else{
    					$('#bookList').append(str);
    				}
    				page++;
    			}else{
    				if(page === 1){
    					$('#loadMore').hide();
    					$('#bookList').html('');
    				}else{
    					$('#loadMore').text('别扯了，已经到底了^v^');
    				}
    			}
    		});
    	}
    }
        function createHtml(list){
        	var str = '';
        	$.each(list,function(i,item){
        		str += '<li class="item_1_bar item_1_bar_line" onclick="javascript:toInfo('+item.id+');">';
        		str += '<div class="img_box">';
        		str += '<img class="cover" src="'+item.cover+'">';
        		if(parseInt(item.is_alone) === 1){
        			str += '<p class="icon_only"></p>';
        		}
        		if(parseInt(item.over_type) === 1){
    				str += '<span class="status_serialization status_icon_short"></span>';
    			}else{
    				str += '<span class="status_end status_icon_short"></span>';
    			}
        		str += '</div>';
        		str += '<div class="right">';
        		str += '<h2 class="title red_color_themes">'+item.name+'</h2>';
        		str += '<p class="content">'+item.summary+'</p>';
        		str += '<div class="info">';
        		str += '<span>作者：'+item.author+'</span>';
        		str += '<div class="read">';
        		str += '<img src="/static/resource/index/images/eyes.png">';
        		str += '<span>'+item.hot_num+'</span>';
        		str += '</div>';
        		str += '<div class="start">';
        		str += '<img src="/static/resource/index/images/star.png">';
        		str += '<span>'+item.star+'</span>';
        		str += '</div>';
        		str += '</div>';
        		str += '</div>';
        		str += '</li>';
        	});
        	return str;
        }