//复制
var clipboard = new ClipboardJS('#copy');


//点击右下角按钮显示更多信息
$('#fab').on("click", function () {
    $('.more').show();
    $('.more_bg').animate({
        "opacity": "0.5"
    });
    $('.more_main').animate({
        "bottom": 0
    });
})
// 关闭显示更多
$('.more_bg').on("click", function () {
    $('.more_bg').animate({
        "opacity": "0"
    });
    $('.more_main').animate({
        "bottom": "-10rem"
    });
    setTimeout(function () {
        $('.more').hide();
    }, 200);
});

function doSubBook(book_id,obj){
	var type = 2;
    if ($(obj).is(":checked")) {
    	type = 1;
    }
    var data = {book_id:book_id,type:type};
    ajaxPost(U('Com/doSubBook'),data,function(){
    	showSubRes(type);
    });
}

function showSubRes(type){
	switch(type){
	case 1:
		 $('.tips_popup p').text("訂閱成功");
	        $('.tips_popup').fadeIn();
	        setTimeout(function () {
	            $('.tips_popup').fadeOut();
	        }, 1500);
		break;
	case 2:
		$('.tips_popup p').text("取消訂閱成功");
        $('.tips_popup').fadeIn();
        setTimeout(function () {
            $('.tips_popup').fadeOut();
        }, 1500);
		break;
	}
}

$('.share').on('click', function () {
    $('.copy_box').show().addClass("scaleBoxto1").removeClass('scaleBoxto0');
});

$('#copy').on('click', function () {
    $('.copy_box').addClass("scaleBoxto0").removeClass('scaleBoxto1');
    setTimeout(function () {
        $('.copy_box').hide();
        $('.copy_succ_box').fadeIn();
        setTimeout(function () {
            $('.copy_succ_box').fadeOut();
        }, 1500)
    }, 90)
});

// 全屏左移动画
$('#show_my').on("click", function () {
    $('#detail_box').animate({
        "right": "6.8533rem"
    });
    $('#fab').animate({
        "margin-right": "2.6533rem"
    });
    $('#mongolia').show();
});

$('#mongolia').on("click", function () {
    $('#detail_box').animate({
        "right": 0
    });
    $('#fab').animate({
        "margin-right": "-4.2rem"
    });
    $('#mongolia').hide();
});

//订阅追更
$('#doCollect').on("click", function () {
	var flag = $(this).attr('data-flag');
	if(flag !== 'yes'){
		return false;
	}
    $('.subscribe_popup').css({
        "z-index": 999
    });
    $('.subscribe_popup').animate({
        opacity: 1
    });
    $('.subscribe_box').addClass('scaleBoxto1');
    setTimeout(function () {
        $('.subscribe_box').removeClass('scaleBoxto1');
    }, 120);
});

$('.subscribe_cancel').on('click', function () {
    $('.subscribe_box').addClass('scaleBoxto0');
    setTimeout(function () {
        $('.subscribe_popup').css({
            "z-index": -10
        });
        $('.subscribe_popup').animate({
            opacity: 0.5
        });
    }, 90);
    setTimeout(function () {
        $('.subscribe_box').removeClass('scaleBoxto0')
    }, 120);

});
function doCollect(book_id){
	ajaxPost(U('Com/doCollect'),{book_id:book_id},function(){
		$('.subscribe_box').addClass('scaleBoxto0');
	    $('#doCollect').addClass('subscribed').text("已加入書架");
	    $('#doCollect').attr('data-flag','no');
	    setTimeout(function () {
	        $('.subscribe_popup').css({
	            "z-index": -1
	        });
	        $('.subscribe_popup').animate({
	            opacity: 0.5
	        });
	        setTimeout(function () {
	            $('.tips_popup p').text("加入成功");
	            $('.tips_popup').fadeIn();
	            setTimeout(function () {
	                $('.tips_popup').fadeOut();
	            }, 1500);
	        }, 100);
	    }, 90);
	    setTimeout(function () {
	        $('.subscribe_box').removeClass('scaleBoxto0')
	    }, 120);
	});
}


// nav 显示隐藏动画
var scoll = {
    lastScroH: 0,
    isAdd: false,
    navHeight: $('.nav').height()
}
$(window).scroll(function () {
    var scroH = $(document).scrollTop();
    if (scroH > scoll.lastScroH) { //下滑
        if (!scoll.isAdd) {
            $(".header").animate({
                top: "-" + scoll.navHeight + "px",
            });
        }
        scoll.isAdd = true
    } else {
        if (scoll.isAdd) {
            $(".header").animate({
                top: 0,
            });
        };
        scoll.isAdd = false;
    };
    //重置上次高度
    setTimeout(function () {
        scoll.lastScroH = scroH;
    }, 0);
});