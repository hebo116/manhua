//复制
    var clipboard = new ClipboardJS('#copy');
    
    function doBuyNumber(book_id,number){
    	var data = {book_id:book_id,number:number};
    	ajaxPost(U('duBuyNumber'),data,function(){
    		window.location.reload();
    	});
    }
    function doSubBook(book_id,obj){
    	var type = 2;
        if ($(obj).is(":checked")) {
        	type = 1;
        }
        var data = {book_id:book_id,type:type};
        ajaxPost(U('Com/doSubBook'),data,function(){
        	showSubRes(type);
        });
    }

    function showSubRes(type){
    	switch(type){
    	case 1:
    		 $('.sub_popup p').text("訂閱成功");
    	        $('.sub_popup').fadeIn();
    	        setTimeout(function () {
    	            $('.sub_popup').fadeOut();
    	        }, 1500);
    		break;
    	case 2:
    		$('.sub_popup p').text("取消訂閱成功");
            $('.sub_popup').fadeIn();
            setTimeout(function () {
                $('.sub_popup').fadeOut();
            }, 1500);
    		break;
    	}
    }

    // 点击主屏幕
    $('#read_main img,p').on("click", function () {
        $('#read_main_mongolia').fadeIn()
        $(".header").animate({
            top: 0,
        });
        $('.bottom_bar').animate({
            "bottom": 0
        });
    })

    $('#read_main_mongolia').on("click", function () {
        $('#read_main_mongolia').fadeOut()
        $('.bottom_bar').animate({
            "bottom": "-10rem"
        });
    });

    $('#share').on("click", function () {
        $('.more').show();
        $('.more_bg').animate({
            "opacity": "0.5"
        });
        $('.more_main').animate({
            "bottom": 0
        });
        $('#read_main_mongolia').fadeOut()
        $('.bottom_bar').css({
            "bottom": "-10rem"
        });
    });



    //显示更多信息
    $('#show_info').on("click", function () {
        $('.more').show();
        $('.more_bg').animate({
            "opacity": "0.5"
        });
        $('.more_main').animate({
            "bottom": 0
        });
    });


    // 关闭显示更多
    $('.more_bg').on("click", function () {
        $('.more_bg').animate({
            "opacity": "0"
        });
        $('.more_main').animate({
            "bottom": "-10rem"
        });
        setTimeout(function () {
            $('.more').hide();
        }, 200);
    });


    $('.share').on('click', function () {
        $('.copy_box').show().addClass("scaleBoxto1").removeClass('scaleBoxto0');
    });

    $('#copy').on('click', function () {
        $('.copy_box').addClass("scaleBoxto0").removeClass('scaleBoxto1');
        setTimeout(function () {
            $('.copy_box').hide();
            $('.copy_succ_box').fadeIn();
            setTimeout(function () {
                $('.copy_succ_box').fadeOut();
            }, 1500)
        }, 90)
    });

    // 全屏右移动画
    $('#list').on("click", function () {
        allMoveRight()
        $('.bottom_bar').css({
            "bottom": "-10rem"
        })
        $('#read_main_mongolia').fadeOut()
    });

    $('#mongolia').on("click", recoveryAllMoveRight)

    // 全屏右移动画
    function allMoveRight() {
        $('#read_box').animate({
            "left": "6.8533rem"
        });
        $('#mongolia').show();
    }
    // 恢复全屏移动
    function recoveryAllMoveRight() {
        $('#read_box').animate({
            "left": 0
        });
        $('#mongolia').hide();
    }
    // nav 显示隐藏动画
    var scoll = {
        lastScroH: 0,
        isAdd: false,
        navHeight: $('.nav').height()
    }
    $(window).scroll(function () {
        var scroH = $(document).scrollTop();
        if (scroH > scoll.lastScroH) { //下滑
            if (!scoll.isAdd) {
                $('.header').css('top',"-"+scoll.navHeight + "px");
            }
            scoll.isAdd = true
        } else {
            if (scoll.isAdd) {
            	$('.header').css('top',0);
            };
            scoll.isAdd = false;
        };
        //重置上次高度
        setTimeout(function () {
            scoll.lastScroH = scroH;
        }, 0);
        
        var scrollTop = $(this).scrollTop();
    	var scrollHeight = $(document).height();
    	var windowHeight = $(this).height();
    	if (scrollTop + windowHeight + 50 >= scrollHeight) {
    		$('#read_main_mongolia').fadeIn()
            $('.header').css('top',0);
    		$('.bottom_bar').css('bottom',0);
    	}
    });