    function getData(type,category,obj){
    	var page = parseInt($(obj).attr('data-page'));
    	page++;
    	var data = {type:type,page:page,category:category};
    	ajaxPost('/index/book/getTypeData',data,function(list){
    		if(list){
    			var str = '';
    			switch(parseInt(type)){
    			case 1:
    				str = createItem3Html(list);
    				break;
    			case 5:
    				str = createItem1Html(list);
    				break;
    			}
    			$(obj).parent().find('.main').html(str);
    			$(obj).attr('data-page',page);
    		}else{
    			layMsg('已经到最后一页了');
    		}
    	});
    }

    function createItem1Html(list){
    	var str = '';
    	$.each(list,function(i,item){
    		str += '<li class="item_1" onclick="javascript:toInfo('+item.id+');">';
    		str += '<div class="img_box">';
    		str += '<img class="cover" src="'+item.detail_img+'">';
    		str += '<div class="status">';
    		if(parseInt(item.over_type) === 1){
    			str += '<p class="status_serialization status_icon_short"></p>';
    		}else{
    			str += '<p class="status_end status_icon_short"></p>';
    		}
    		str += '</div>';
    		str += '<h4>'+item.name+'</h4>';
    		str += '<div class="info">';
    		str += '<div class="read">';
    		str += '<img src="/static/resource/index//images/eyes.png">';
    		str += '<span>'+item.hot_num+'</span>';
    		str += '</div>';
    		str += '<div class="start">';
    		str += '<img src="/static/resource/index/images/star.png">';
    		str += '<span>'+item.star+'</span>';
    		str += '</div>';
    		str += '</div>';
    		str += '</div>';
    		str += '<p class="introduce">'+item.summary+'</p>';
    		str += '</li>';
    	});
    	return str;
    }
    
    function createItem3Html(list){
    	var str = '';
    	$.each(list,function(i,item){
    		str += '<li class="item_3" onclick="javascript:toInfo('+item.id+');">';
    		str += '<div class="img_box">';
    		str += '<img class="cover" src="'+item.cover+'">';
    		if(parseInt(item.over_type) === 1){
    			str += '<p class="status_serialization"></p>';
    		}else{
    			str += '<p class="status_end"></p>';
    		}
    		str += '</div>';
    		str += '<h4 class="red_color_themes">'+item.name+'</h4>';
    		str += '<div class="info">';
    		str += '<div class="start">';
    		str += '<img src="/static/resource/index/images/star.png">';
    		str += '<span>'+item.star+'</span>';
    		str += '</div>';
    		str += '<div class="read">';
    		str += '<img src="/static/resource/index//images/eyes.png">';
    		str += '<span>'+item.hot_num+'</span>';
    		str += '</div>';
    		str += '</div>';
    		str += '</li>';
    	});
    	return str;
    }
    
    
    
    $(".list_type li").click(function () {
    	var index = $(this).index();
    	var showId = 'rank-ul-'+(index+1);
    	$('.rank-ul').hide();
    	$('#'+showId).show();
        $(this).addClass("chooseList_red_color_themes");
        $(this).siblings().removeClass("chooseList_red_color_themes");
    });