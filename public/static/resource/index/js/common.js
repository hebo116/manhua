showDesktopMsg();
//构建url
function U($uri){
	var arr = $uri.split('/');
	var len = arr.length;
	var url = '';
	var urlPath = window.location.pathname;
	var tmps = urlPath.split('/');
	switch(len){
		case 3:
			url = '/'+arr[0]+'/'+arr[1]+'/'+arr[2]+'.html';
			break;
		case 2:
			url = '/'+tmps[1]+'/'+arr[0]+'/'+arr[1]+'.html';
			break;
		case 1:
			url = '/'+tmps[1]+'/'+tmps[2]+'/'+$uri+'.html';
			break;
	}
	return url;
}

function showDesktopMsg(){
	var is_desktop = $.cookie('is_desktop');
	is_desktop = is_desktop ? is_desktop : 'no';
	if(is_desktop === 'no'){
		if(/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
			$('#showDesktop').show();
			$.cookie('is_desktop','yes');
		}
	}
}

function showDeskTopChild(){
	$('#desktopChild').show();
}

//消息弹出层
function layMsg(msg){
	layer.open({
	    content: msg,
	    skin: 'msg',
	    time: 2
	 });
}

//检测广告跳转
function jumpAd(id){
	$.ajax({
		type : 'post',
		url : '/index/com/checkAd',
		data : {id:id},
		dataType : 'json',
		success : function(d){
			if(d.code === 1){
				window.location.href = d.data.url;
			}
		}
	})
}

//ajax提交
function ajaxPost(url,data,callback,is_sync){
	is_sync = is_sync !== false ? true : false;
	var loadindex;
	$.ajax({
		type : 'post',
		url : url,
		async : is_sync,
		data : data,
		dataType : 'json',
		beforeSend : function(){
			loadindex = layer.open({type: 2,shade : 'background-color: rgba(255,255,255,.5)',shadeClose:false});
		},
		success : function(res){
			switch(res.code){
			case 1:
				callback(res.data);
				break;
			case 2:
				layMsg(res.msg);
				break;
			case 3:
				layer.open({
					content: res.msg,
					btn: ['确定', '取消'],
					yes: function(index){
						layer.close(index);
						var location_url = res.data ? res.data.url : '/index/Login/index';
						window.location.href = location_url;
					}
				});
				break;
			case 4:
				layer.open({
			        content: res.msg
			        , shadeClose: false
			        ,className :'chaseLayer'
			        ,style: 'background-color:#fff;color:#fff; border:none;border-radius:20px;width:80%;padding:0;margin:0;'
			    });
				break;
			}
		},
		complete: function(){
			layer.close(loadindex);
		},
		error : function(){
			layer.close(loadindex);
		}
	});
}

function doChangeTheme(color){
	var data = {color:color};
	ajaxPost('',data,function(){
		window.location.reload();
	});
}

function toUserIndex(){
	window.location.href = '/index/user/index.html';
}

//去登录
function toLogin(){
	window.location.href = '/index/login/index.html';
}
//退出登录
function toLoginOut(){
	window.location.href = '/index/user/logOut.html';
}

//去阅读
function toRead(book_id,number){
	window.location.href = '/index/book/read.html?book_id='+book_id+'&number='+number;
}

//书籍详情页
function toInfo(book_id){
	window.location.href = '/index/book/info.html?book_id='+book_id;
}
//视频详情页
function toInfos(video_id){
	window.location.href = '/index/video/info.html?video_id='+video_id;
}


