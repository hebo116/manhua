import router from './router'
import store from './store'
import '@/assets/css/reset.css';
import $local from 'storejs';
import "./assets/lib/flex"
import './assets/scss/mixin.scss';
import './assets/scss/reset.scss';
import './assets/scss/themes.scss';
import './assets/scss/icon.scss';
let Themes = $local.get("Themes") ? $local.get("Themes") : "change_red"
import(`./assets/scss/${Themes}.scss`)

import Video from 'video.js'

import 'video.js/dist/video-js.css'
Vue.prototype.$video = Video;
import 'vant/lib/index.css';

import Toast from '@/components/toast'
Vue.use(Toast);
import Vant from 'vant'
import 'vant/lib/index.css'
Vue.use(Vant);

Vue.config.productionTip = false
let vm = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
export default vm
