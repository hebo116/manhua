import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/css/reset.css';
import $local from 'storejs';
import "./assets/lib/flex"
import './assets/scss/mixin.scss';
import './assets/scss/reset.scss';
import './assets/scss/themes.scss';
import './assets/scss/icon.scss';
let Themes = $local.get("Themes") ? $local.get("Themes") : "change_red"
import(`./assets/scss/${Themes}.scss`)
import Toast from '@/components/toast'

import 'video.js/dist/video-js.css'


Vue.use(Toast);

Vue.prototype.$mainPage=[];
Vue.prototype.$mainPageTop=[];
Vue.prototype.$mainPageHeight=[];


Vue.config.productionTip = false
let vm = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

router.afterEach(function (to) {
    if (window.ga) {
        window.ga('set', 'page', to.fullPath) // 你可能想根据请求参数添加其他参数，可以修改这里的 to.fullPath
        window.ga('send', 'pageview')
    }
})





export default vm
