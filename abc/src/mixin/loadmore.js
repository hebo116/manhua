import { getScrollTop, getClientHeight, getScrollHeight,setScrollTop,setScrollHeight } from "@/utils/common";
export const loadmore = {
    data() {
        return {
            allload: false,
            isload: false,
            loadText: "数据加载中...",
            list: [],
            self:{},
            path:""
        }
    },
    created() {
        this.self=this;
        this.path=this.$route.name;
        this.$mainPage[this.$route.name]=this;
        this.$mainPageTop[this.path]=0;
        this.$mainPageHeight[this.path]=0;
        window.addEventListener("scroll",e=>{
            this.$mainPageTop[this.path]=getScrollTop();
            this.$mainPageHeight[this.path]=getScrollHeight();
            if (getScrollTop() + getClientHeight() + 100 > getScrollHeight()) {
                this.self.loadmore()
            };
        })
    },
    methods: {
        _changeStatus(res) {
            if (this.sendData.size !== res.length) {
                this.allload = true
                this.loadText = "别扯了，已经到底了^v^"
            }
            this.isload = false

        },
        loadmore() {
            if (!this.self.isload && !this.self.allload) {
                if(this.self.path!="video") {

                    this.self.isload = true
                    this.self.sendData.page++
                    this.self.self._getList()
                }else{
                    this.self.isload = true
                    this.self.videoData.page++
                    this.self._getList()
                }
            }
        }
    },
    watch: {
        $route(to, from) {
            if(this.$mainPage[to.name]){
                this.self = this.$mainPage[to.name];
                this.path=to.name;
            }
        }
    }
}