import { getScrollTop, getClientHeight, getScrollHeight } from "@/utils/common";
export const videoloadmore = {
    data() {
        return {
            allload: false,
            isload: false,
            loadText: "数据加载中...",
            list: [],
            self:{},
            path:"ssss"
        }
    },
    created() {
        this.self=this;
        this.$mainPage[this.$route.name]=this;
        window.addEventListener("scroll",e=>{
            if (getScrollTop() + getClientHeight() + 100 > getScrollHeight()) {
                console.log("should score",this.path);
                this.self.videoloadmore()
            };
        })
    },
    methods: {
        _changeStatus(res) {
            if (this.videoData.size !== res.length) {
                this.allload = true
                this.loadText = "别扯了，已经到底了^v^"
            }
            this.isload = false

        },
        videoloadmore() {
            if (!this.self.isload && !this.self.allload) {
                this.self.isload = true
                this.self.videoData.page++
                this.self._getList()
            }
        }
    },

}