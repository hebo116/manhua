

// import MD5 from "js-md5"
// import SHA1 from "sha1"

export const autoUrl = function () {
    let appId = "wx00a55cc6a065e6be"
    //授权后重定向的回调链接地址， 请使用 urlEncode 对链接进行处理
    let redirect_uri = encodeURIComponent(window.location.href)
    //重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
    let state = "login"
    let url = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appId}&redirect_uri=${redirect_uri}&response_type=code&scope=snsapi_userinfo&state=${state}#wechat_redirect`
    return url
}


//获取滚动条当前的位置
export const getScrollTop = function () {
    var scrollTop = 0;
    if (document.documentElement && document.documentElement.scrollTop) {
        scrollTop = document.documentElement.scrollTop;
    } else if (document.body) {
        scrollTop = document.body.scrollTop;
    }
    return scrollTop;
}

export const setScrollTop=function (scrollTop){
    if (document.documentElement && document.documentElement.scrollTop) {
        document.documentElement.scrollTop=scrollTop;
    } else if (document.body) {
        document.body.scrollTop=scrollTop;
    }
}

//获取当前可视范围的高度  
export const getClientHeight = function () {
    var clientHeight = 0;
    if(document.body.clientHeight && document.documentElement.clientHeight) {
        clientHeight = Math.min(document.body.clientHeight, document.documentElement.clientHeight);
    } else {
        clientHeight = Math.max(document.body.clientHeight, document.documentElement.clientHeight);
    }
    return clientHeight;

}


//获取文档完整的高度 
export const getScrollHeight = function () {
    return Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
}

export const setScrollHeight=function (scrollHeight){
    document.body.scrollHeight=scrollHeight;
    document.documentElement.scrollHeight=scrollHeight;
}