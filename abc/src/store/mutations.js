
export default {
  TYPE: (state, data) => {
    state.TYPE = data
  },
  userInfo: (state, data) => {
    state.userInfo = data
  },
  refreshUserInfo:  (state, status) => {
    state.refreshUserInfo = status
  },
}
