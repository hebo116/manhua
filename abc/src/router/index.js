import Vue from 'vue'
import VueRouter from 'vue-router'
/**
 * 重写路由的push方法
 */
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
    return routerPush.call(this, location).catch(error => error)
}

Vue.use(VueRouter)

const routes = [
    {
        path: "/",
        redirect: "/index"
    },
    {
        path: '/index',
        name: 'index',
        component: () => import('../views/index/index.vue'),
        meta: {
            keepAlive: true
        }
    },
    {
        path: '/video',
        name: 'video',
        component: () => import('../views/video/index.vue'),
        meta: {
            keepAlive: true
        }
    },
    {
        path: '/videoDetail',
        name: 'videoDetail',
        component: () => import('../views/videoDetail/index.vue')

    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/login/index.vue')
    }, 
    {
        path: '/change',
        name: 'change',
        component: () => import('../views/change/index.vue')
    }, 
    {
        path: '/retrieve',
        name: 'retrieve',
        component: () => import('../views/retrieve/index.vue')
    },
    {
        path: '/modifyPwd',
        name: 'modifyPwd',
        component: () => import('../views/modifyPwd/index.vue')
    },
    {
        path: '/userlogin',
        name: 'userlogin',
        component: () => import('../views/userlogin/index.vue')
    },
    {
        path: '/chapter',
        name: 'chapter',
        component: () => import('../views/chapter/index.vue')
    },
    {
        path: '/automatic',
        name: 'automatic',
        component: () => import('../views/automatic/index.vue')
    },
    {
        path: '/bindPhone',
        name: 'bindPhone',
        component: () => import('../views/bindPhone/index.vue')
    },
    {
        path: '/cartoon',
        name: 'cartoon',
        component: () => import('../views/cartoon/index.vue'),
        meta: {
            keepAlive: true
        }
    },
    {
        path: '/classification',
        name: 'classification',
        component: () => import('../views/classification/index.vue'),
        meta: {
            keepAlive: true
        }
    },
    {
        path: '/consumption',
        name: 'consumption',
        component: () => import('../views/consumption/index.vue')
    },
    {
        path: '/detail',
        name: 'detail',
        component: () => import('../views/detail/index.vue')
    },
    {
        path: '/my',
        name: 'my',
        component: () => import('../views/my/index.vue')
    },
    {
        path: '/news',
        name: 'news',
        component: () => import('../views/news/index.vue')
    },
    {
        path: '/ranking',
        name: 'ranking',
        component: () => import('../views/ranking/index.vue'),
        meta: {
            keepAlive: true
        }
    },
    {
        path: '/more',
        name: 'more',
        component: () => import('../views/more/index.vue')
    },
    {
        path: '/read',
        name: 'read',
        component: () => import('../views/read/index.vue')
    },
    {
        path: '/readHistory',
        name: 'readHistory',
        component: () => import('../views/readHistory/index.vue')
    },
    {
        path: '/search',
        name: 'search',
        component: () => import('../views/search/index.vue')
    },
    {
        path: '/searchResult',
        name: 'searchResult',
        component: () => import('../views/searchResult/index.vue')
    },
    {
        path: '/setting',
        name: 'setting',
        component: () => import('../views/setting/index.vue')
    },
    {
        path: '/theme',
        name: 'theme',
        component: () => import('../views/theme/index.vue')
    },
    {
        path: '/today',
        name: 'today',
        component: () => import('../views/today/index.vue')
    },
    {
        path: '/recharge',
        name: 'recharge',
        component: () => import('../views/recharge/index.vue')
    },



]

const router = new VueRouter({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes,
    // scrollBehavior (to, from, savedPosition) {
    //     if (savedPosition) {
    //       return savedPosition
    //     } else {
    //       return {
    //         x: 0,
    //         y: 0
    //       }
    //     }
    //   }
    scrollBehavior () {
        return { x: 0, y: this.app.$mainPageTop[this.app.$route.name] }
    }
})

export default router
