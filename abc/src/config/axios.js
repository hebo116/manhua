/**
 * 通用常量
 */

// 请求成功与失败的状态码
export const SUCCESS_STATUS_CODE = 1
export const ERR_STATUS_CODE = 2

// 请求失败错误提示
export const REQ_ERR_TIP = '出错啦！请刷新重试!'

// 请求超时时间(ms)
export const REQ_TIME_OUT = 10000