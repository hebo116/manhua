/**
 * 区分环境
 */
const isProduction = process.env.NODE_ENV === 'production' 

const  DEFAULT = {
  //请求api
//   api: isProduction ? window.location.host : 'http://192.168.2.200:8080',
  api: 'https://api.qqcmh.click',
  //vuex是否debug
  debug: !isProduction
}

export default DEFAULT
