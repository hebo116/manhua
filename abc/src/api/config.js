
// 请求成功与失败的状态码
export const SUCCESS_STATUS_CODE = 1
export const ERR_STATUS_CODE = 2
export const ERR_TOKEN_INVALID = 1001 //token  失效
export const ERR_NOT_LOGIN = 1002 //未登录

// 请求失败错误提示
export const REQ_ERR_TIP = '出错啦！请刷新重试或联系系统管理员'

// 请求超时时间(ms)
export const REQ_TIME_OUT = 10000
