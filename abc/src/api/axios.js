/**
 * 封装请求方法
 */
import Vue from "vue"
import axios from 'axios'
import DEFAULT from '@/config/evn'
import local from 'storejs'
import Router from '@/router/index'
import vm from "../main"

import {
    SUCCESS_STATUS_CODE,
    ERR_STATUS_CODE,
    ERR_TOKEN_INVALID,
    ERR_NOT_LOGIN,
    REQ_ERR_TIP,
    REQ_TIME_OUT
} from './config'

import { autoUrl } from "@/utils/common"
import MD5 from "js-md5"

const fetch = options => {
    return new Promise((resolve, reject) => {
        const instance = axios.create({
            baseURL: DEFAULT.api,
            timeout: REQ_TIME_OUT
        })
        let token = local.get("token")

        if(window.android){
            instance.defaults.headers["plat"]="android";
        }

        if (token) {
            instance.defaults.headers['usertoken'] = local.get("token")
        } else {
            Router.push({ name: 'login' })
        }
        instance(options)
            .then(response => {
                const res = response.data
                /**
                 * 根据返回状态执行不同操作
                 */
                if(res.code == undefined){
                    vm.$toast("返回格式不对")
                }
                if (res.code !== SUCCESS_STATUS_CODE) {
                    if (res.code === ERR_STATUS_CODE) {
                        vm.$toast(res.msg)
                        reject(res)
                    } else if (//未登录或者token失效
                        res.code == 3
                    ) {
                        local.remove("token")
                        Router.push({ name: 'login' })
                        //未登录或者token失效
                    } else {
                        alert(res)
                        reject(res)
                    }
                } else {
                    resolve(res.data)
                }
            })
            .catch(error => {
                if (error.response) {
                    console.log('error.response')
                    console.log(error.response);
                } else if (error.request) {
                    console.log(error.request)
                    console.log('error.request')
                    if(error.request.readyState == 4 && error.request.status == 0){
                        //我在这里重新请求
                        vm.$toast("请求超时")
                    }
                } else {
                    console.log('Error', error.message);
                }
                console.log(error.config);

                reject(error.data)
            })
    })
}

export default {
    get(url, params = {}, str = '') {
        return fetch({
            method: 'get',
            url: `${url}${str}`,
            params
        })
    },

    post(url, data = {}, str = '') {
        return fetch({
            method: 'post',
            url: `${url}${str}`,
            data
        })
    },

    delete(url, str, params = {}) {
        return fetch({
            method: 'delete',
            url: `${url}${str}`,
            params
        })
    },

    put(url, str, data = {}) {
        return fetch({
            method: 'put',
            url: `${url}${str}`,
            data
        })
    },

    patch(url, str, data = {}) {
        return fetch({
            method: 'patch',
            url: `${url}${str}`,
            data
        })
    }
}
