
import ajax from "@/api/axios";

//随机生成一组用户名和密码
export const RandUser = params => ajax.get('/api/user/randUser', params)
//注册
export const Register = params => ajax.post('/api/user/register', params)


//获取首页导航
export const IndexGetNav = params => ajax.get('/api/index/get_nav', params)
//获取轮播图地址
export const IndexGetBanner = params => ajax.get('/api/index/get_banner', params)
//获取书籍列表
// book类型(1:精品，2:最新，3:限免书籍，4：推荐书籍，5：猜你喜欢书籍)
export const GetBookList = params => ajax.get('/api/index/get_book_list', params)
// 获取书籍排行榜(与首页更多列表通用)
export const GetRankList = params => ajax.get('/api/index/get_rank_book', params)

//获取视频列表
export const GetVideoList = params => ajax.get('/api/video/get_video_list',params)

//获取视频详情
export const GetVideoInfo = params => ajax.get('/api/video/get_video_info',params)

//获取猜你喜欢视频列表
export const GetVideoLikeList = params => ajax.get('/api/video/get_like_video_list',params)


//获取广告（首页更多列表通用）
export const GetAd = params => ajax.get('/api/index/get_ad', params)

//获取广告（首页更多列表通用）
export const GetBookInfo = params => ajax.get('/api/book/info', params)

//获取漫画分类
export const GetBookCategory = params => ajax.get('/api/book/get_category', params)
// 获取小说分类
export const GetNovelCategory = params => ajax.get('/api/novel/get_category', params)


// 获取消息列表
export const GetUserMessage = params => ajax.get('/api/user/message', params)
// 未读消息数量
export const GetUserMessageNum = params => ajax.get('/api/user/messageNum', params)

// 获取用户信息
export const GetUserInfo = params => ajax.get('/api/user/getUserInfo', params)
// 获取联系方式
export const GetContact = params => ajax.get('/api/user/contact', params)
// 获取消费记录
export const GetBill = params => ajax.get('/api/user/bill', params)



//登录
export const SignIn = params => ajax.post('/api/user/sign_in', params)

//书籍收藏(加入书架)
export const DoCollect = params => ajax.get('/api/book/doCollect', params)

//设置书籍自动订阅
export const SetAuto = params => ajax.get('/api/user/setAuto', params)

//设置书籍自动订阅
export const GetChapter = params => ajax.get('/api/book/chapter', params)

//获取充值数据
export const GetChargeList = params => ajax.get('/api/charge/getChargeList', params)
//获取选择支付方式
export const GetPayList = params => ajax.get('/api/charge/getPayList', params)
//获取选择支付方式
export const DoCharge = params => ajax.post('/api/charge/doCharge', params)

//书架
export const GetBookCollect = params => ajax.get('/api/book/collect', params)
//阅读历史
export const GetReadHistoryt = params => ajax.get('/api/book/readHistory', params)
// 清理历史记录
export const ClearRecord = params => ajax.get('/api/user/clearRecord', params)
//删除指定记录
export const DelRecord = params => ajax.post('/api/book/delRecord', params)
//删除指定记录
export const DelUserRecord = params => ajax.post('/api/user/delRecord', params)

//自动订阅列表
export const GetAutoList = params => ajax.post('/api/user/autoList', params)
//书籍正文(漫画、小说通用)
export const GetBookRead = params => ajax.get('/api/book/read', params)

//书籍正文(漫画、小说通用)
export const DoBuyNumber = params => ajax.get('/api/book/duBuyNumber', params)


//搜索内容
export const SearchResult = params => ajax.get('/api/search/searchResult', params)
//搜索历史
export const SearchHistory = params => ajax.get('/api/search/history', params)
//删除搜索历史
export const DelSearchRecord = params => ajax.get('/api/search/delRecord', params)
//清除搜索历史
export const ClaearSearchRecord = params => ajax.get('/api/search/clearRecord', params)

//修改密码
export const ChangePwd = params => ajax.post('/api/user/changePwd', params)
//赠送书币
export const LoginSendMoney = params => ajax.post('/api/user/loginSendMoney', params)


//找回用户帐号
export const FindUser = params => ajax.get('/api/user/findUser', params)

//找回用户帐号
export const CutUser = params => ajax.post('/api/user/cutUser', params)

//找回用户帐号
export const TodayRefresh = params => ajax.get('/api/book/refresh', params)



//获取分类类型
export const GetCategoryList = params => ajax.get('/api/book/categoryList', params)
//获取书籍分类列表
export const GetCategoryBookList = params => ajax.get('/api/book/categoryBookList', params)

export const GetDownload = params => ajax.get('/api/index/getDownloadUrl', params);























