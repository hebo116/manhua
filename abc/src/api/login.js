/**
 * 登陆请求
 */
import {ajax} from "api/axios";

export const getCode = (options,config) => {
	return ajax.get('/getCode',options,config)
}

export const sendCode = (options,config) => {
	return ajax.get('/sendCode',options,config)
}