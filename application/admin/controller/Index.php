<?php
namespace app\admin\controller;

use site\myCache;
use site\myDb;
use site\myValidate;
use app\common\model\mdLogin;
use app\common\model\mdConsole;

class Index extends Common
{
    
    //后台首页
    public function index(){
        $siteMsg = getMyConfig('website');
        $site_name = '后台管理';
        if($siteMsg && isset($siteMsg['name']) && $siteMsg['name']){
            $site_name = $siteMsg['name'];
        }
        $cur = mdLogin::getCache();
        $variable = [
            'cur' => $cur,
            'site_name' => $site_name
        ];
        return $this->fetch('common@index',$variable);
    }
    
    //控制台
    public function console(){
        $number = mdConsole::getNumbersData();
        $charge_rank = mdConsole::getChargeRank([['a.status','=',2]]);
        $variable = [
            'number' => $number,
            'charge_rank' => $charge_rank,
        ];
        return $this->fetch('common@console',$variable);
    }
    
    //获取用户趋势图
    public function getUserChartData(){
    	$data = mdConsole::getUserChartData();
        res_api($data);
    }
    
    //修改个人信息
    public function userinfo(){
        global $loginId;
        if($this->request->isAjax()){
        	$rules = ['name' =>  ["require|max:20",["require"=>"请输入用户名称",'max'=>'用户名称最多支持20个字符']]];
            $name = myValidate::getData($rules);
            $re = mdConsole::setField('Manage',[['id','=',$loginId]], 'name', $name);
            if($re){
                cache('ADMIN_USER_'.$loginId,null);
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
        	$cur = myDb::getById('Manage',$loginId);
            $login_msg = mdLogin::getLastLoginMsg($cur['login_name']);
            if($login_msg){
                $cur = array_merge($cur,$login_msg);
            }
            $variable = [
            	'cur' => $cur,
            	'js' => getJs('index.userinfo')
            ];
            return $this->fetch('common@userinfo',$variable);
        }
    }
    
    //修改密码
    public function password(){
        if($this->request->isAjax()){
            global $loginId;
            $re = mdLogin::changePwd($loginId);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
        	
        	return $this->fetch('common@password');
        }
    }
    
    //退出登录
    public function logOut(){
        mdLogin::clearCache();
        $url = my_url('Login/index');
        echo "<script language=\"javascript\">window.open('".$url."','_top');</script>";
        exit;
    }

}
