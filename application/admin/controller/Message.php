<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use app\common\model\mdMessage;


class Message extends Common{
    
    //渠道公告
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'default' => [],
                'like' => 'keyword:a.title'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdMessage::getAdminMsgList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$v['channel_num'] = $v['channel_num'] ? : 0;
                    $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                    $v['do_url'] = my_url('doMessage',['id'=>$v['id']]);
                    $v['list_url'] = my_url('channel',['message_id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
            return $this->fetch('common@index');
        }
    }
    
    //新增公告
    public function addMessage(){
        if($this->request->isAjax()){
            $rules = mdMessage::getRules();
            $data = myValidate::getData($rules);
            mdMessage::doneMessage($data);
        }else{
            $field = 'id,title,type,content';
            $cur = myDb::buildArr($field);
            $variable = [
            	'cur' => $cur,
            	'backUrl' => url('index')
            ];
            return $this->fetch('common@doMessage',$variable);
        }
    }
    
    //编辑公告
    public function doMessage(){
        if($this->request->isAjax()){
        	$rules = mdMessage::getRules();
        	$data = myValidate::getData($rules);
        	mdMessage::doneMessage($data); 
        }else{
            $id = myHttp::getId('公告');
            $cur = myDb::getById('Message',$id);
            if(!$cur){
                res_api('公告不存在');
            }
            $cur['content'] = getBlockContent($id,'message'); 
            $variable = [
            	'cur' => $cur,
            	'backUrl' => url('index')
            ];
            return $this->fetch('common@doMessage',$variable);
        }
    }
    
    //删除公告
    public function delMessage(){
        $id = myHttp::postId('公告');
        $flag = mdMessage::delMessage($id);
        if($flag){
            res_api();
        }else{
            res_api('删除失败');
        }
    }
    
    //接收渠道列表
    public function channel(){
    	if($this->request->isAjax()){
    		$message_id = $this->request->get('message_id');
    		if($message_id){
    			$pages = myHttp::getPageParams();
    			$res = mdMessage::getChannelList($message_id, $pages);
    			res_table($res['data'],$res['count']);
    		}else{
    			res_table([]);
    		}
    	}else{
    		
    		return $this->fetch('common@channel');
    	}
    }
    
}