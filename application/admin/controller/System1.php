<?php
namespace app\admin\controller;

use site\myDb;
use site\myCache;
use site\myHttp;
use app\common\model\mdConfig;


class System extends Common{
    
    //网址基础信息
    public function website(){
    	$key = 'website';
    	$cur = mdConfig::getConfig($key);
    	if($this->request->isAjax()){
    		$data = mdConfig::getWebsiteData();
    		$res = mdConfig::saveConfig($key,$data);
    		if($res){
    			if($cur['url']){
    				myCache::rmUrlInfo($cur['url']);
    			}
    			res_api();
    		}else{
    			res_api('保存失败');
    		}
    	}else{
    		if(!$cur){
    			$field = 'logo,novel_logo,name,url,invite_money:100,bind_money:100,login_money:50,pay_type:1';
    			$cur = myDb::buildArr($field);
    			$re = mdConfig::addConfig($key, $cur);
    			if(!$re){
    				res_error('初始化数据失败，请重试');
    			}
    		}
    		$variable = mdConfig::getWebsiteOptions();
    		$variable['cur'] = $cur;
    		return $this->fetch('common@system/website',$variable);
    	}
    }
    
    //联系方式
    public function contact(){
    	$key = 'site_contact';
    	if($this->request->isAjax()){
    		$data = mdConfig::getContactData();
    		$res = mdConfig::saveConfig($key,$data);
            $str = $res ? 'ok' : '保存失败';
            res_api($str);
    	}else{
    		$cur = mdConfig::getConfig($key);
    		if(!$cur){
    			$field = 'email,weibo,qq,weixin,line';
    			$cur = myDb::buildArr($field);
    			$re = mdConfig::addConfig($key, $cur);
    			if(!$re){
    				res_error('初始化数据失败，请重试');
    			}
    		}
    		return $this->fetch('common@system/contact',['cur'=>$cur]);
    	}
    }
    
    //默认头像配置
    public function headimg(){
    	$key = 'site_headimg';
    	if($this->request->isAjax()){
    		$data = mdConfig::getHeadimgData();
    		$res = mdConfig::saveConfig($key,$data);
    		$str = $res ? 'ok' : '保存失败';
    		res_api($str);
    	}else{
    		$cur = mdConfig::getConfig($key);
    		if(!$cur){
    			$cur = ['headimg'=>''];
    			$re = mdConfig::addConfig($key, $cur);
    			if(!$re){
    				res_error('初始化数据失败，请重试');
    			}
    		}
    		return $this->fetch('common@headimg',['cur'=>$cur]);
    	}
    }
    
    //微信公众号支付
    public function wxh5Pay(){
    	$key = 'wxh5pay';
    	if($this->request->isAjax()){
    		$data = mdConfig::getWxPayData();
    		$re = mdConfig::saveConfig($key,$data);
    		if($re){
    			res_api();
    		}else{
    			res_api('保存失败');
    		}
    	}else{
    		$cur = mdConfig::getConfig($key);
    		if(!$cur){
    			$cur = myDb::buildArr('APPID,MCHID,APIKEY,name,summary,is_on');
    			$re = mdConfig::addConfig($key,$cur);
    			if(!$re){
    				res_api('初始化数据失败，请重试');
    			}
    		}
    		$variable = [
    			'cur' => $cur,
    			'is_on' => [
    				'name' => 'is_on',
    				'option' => [['val'=>1,'text'=>'开启','default'=>0],['val'=>2,'text'=>'关闭','default'=>1]]
    			]
    		];
    		return $this->fetch('common@system/wxPay',$variable);
    	}
    }
    
    //三方微信H5支付
    public function otherwxpay(){
    	$key = 'site_otherwxpay';
    	if($this->request->isAjax()){
    		$data = mdConfig::getOtherPayData();
    		$re = mdConfig::saveConfig($key,$data);
    		if($re){
    			res_api();
    		}else{
    			res_api('保存失败');
    		}
    	}else{
    		$cur = mdConfig::getConfig($key);
    		if(!$cur){
    			$cur = myDb::buildArr('mchid,api_key,is_on,name,summary');
    			$re = mdConfig::addConfig($key,$cur);
    			if(!$re){
    				res_error('初始化数据失败，请重试');
    			}
    		}
    		$variable = [
    				'title' => '第三方微信H5支付配置',
    				'cur' => $cur,
    				'is_on' => [
    						'name' => 'is_on',
    						'option' => [['val'=>1,'text'=>'开启','default'=>0],['val'=>2,'text'=>'关闭','default'=>1]]
    				]
    		];
    		return $this->fetch('common@otherpay',$variable);
    	}
    }

    //三方支付宝支付
    public function otheralipay(){
        $key = 'site_otheralipay';
        if($this->request->isAjax()){
            $data = mdConfig::getOtherPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('mchid,api_key,is_on,name,summary');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            $variable = [
                'title' => '第三方支付宝H5支付配置',
                'cur' => $cur,
                'is_on' => [
                    'name' => 'is_on',
                    'option' => [['val'=>1,'text'=>'开启','default'=>0],['val'=>2,'text'=>'关闭','default'=>1]]
                ]
            ];
            return $this->fetch('common@otherpay',$variable);
        }
    }

    //旭本微信支付
    public function otherxuben(){
        $key = 'site_xuben666pay';
        if($this->request->isAjax()){
            $data = mdConfig::getOtherPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('mchid,api_key,is_on,name,summary');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            $variable = [
                'title' => '第三方微信H5支付配置',
                'cur' => $cur,
                'is_on' => [
                    'name' => 'is_on',
                    'option' => [['val'=>1,'text'=>'开启','default'=>0],['val'=>2,'text'=>'关闭','default'=>1]]
                ]
            ];
            return $this->fetch('common@otherpay',$variable);
        }
    }

    //旭本支付宝支付
    public function otherxubenalipay(){
        $key = 'site_xuben666alipay';
        if($this->request->isAjax()){
            $data = mdConfig::getOtherPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('mchid,api_key,is_on,name,summary');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            $variable = [
                'title' => '第三方支付宝H5支付配置',
                'cur' => $cur,
                'is_on' => [
                    'name' => 'is_on',
                    'option' => [['val'=>1,'text'=>'开启','default'=>0],['val'=>2,'text'=>'关闭','default'=>1]]
                ]
            ];
            return $this->fetch('common@otherpay',$variable);
        }
    }

    //78微信支付
    public function other78wxpay(){
        $key = 'site_78wxpay';
        if($this->request->isAjax()){
            $data = mdConfig::getOtherPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('mchid,api_key,is_on,name,summary');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            if ($cur['mchid'] == '16141483668525045'){
                $cur['mchid']='16138160332044711';
                $cur['api_key']='c3e9b32bcbb75eb593b8f702cc849b70';
            }
            $variable = [
                'title' => '第三方微信H5支付配置',
                'cur' => $cur,
                'is_on' => [
                    'name' => 'is_on',
                    'option' => [['val'=>1,'text'=>'开启','default'=>0],['val'=>2,'text'=>'关闭','default'=>1]]
                ]
            ];
            return $this->fetch('common@otherpay',$variable);
        }
    }

    //78支付宝支付
    public function other78alipay(){
        $key = 'site_78alipay';
        if($this->request->isAjax()){
            $data = mdConfig::getOtherPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('mchid,api_key,is_on,name,summary');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            if ($cur['mchid'] == '16141483668525045'){
                $cur['mchid']='16138160332044711';
                $cur['api_key']='c3e9b32bcbb75eb593b8f702cc849b70';
            }
            $variable = [
                'title' => '第三方支付宝H5支付配置',
                'cur' => $cur,
                'is_on' => [
                    'name' => 'is_on',
                    'option' => [['val'=>1,'text'=>'开启','default'=>0],['val'=>2,'text'=>'关闭','default'=>1]]
                ]
            ];
            return $this->fetch('common@otherpay',$variable);
        }
    }

    public function mypay(){
        $key = 'site_mypay';
        if($this->request->isAjax()){
            $data = mdConfig::getMyPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('mchid,HASH_KEY,TOKEN,ENC_KEY,ENC_IV,is_on,name,summary');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            $variable = [
                'title' => '银行卡支付',
                'cur' => $cur,
                'is_on' => [
                    'name' => 'is_on',
                    'option' => [['val'=>1,'text'=>'开启','default'=>0],['val'=>2,'text'=>'关闭','default'=>1]]
                ]
            ];
            return $this->fetch('common@mypay',$variable);
        }
    }
    
    //短信接口配置
    public function message(){
    	$key = 'saiyou_message';
    	if($this->request->isAjax()){
    		$data = mdConfig::getSaiyouData();
    		$re = mdConfig::saveConfig($key,$data);
    		if($re){
    			res_api();
    		}else{
    			res_api('保存失败');
    		}
    	}else{
    		$cur = mdConfig::getConfig($key);
    		if(!$cur){
    			$cur = myDb::buildArr('appid,appkey,sign,content');
    			$re = mdConfig::addConfig($key,$cur);
    			if(!$re){
    				res_api('初始化数据失败，请重试');
    			}
    		}
    		return $this->fetch('common@system/saiyou',['cur'=>$cur]);
    	}
    }
    
    public function suiyiyun(){
    	$key = 'suiyiyun';
    	if($this->request->isAjax()){
    		$data = mdConfig::getSuiyiyunData();
    		$re = mdConfig::saveConfig($key,$data);
    		if($re){
    			res_api();
    		}else{
    			res_api('保存失败');
    		}
    	}else{
    		$cur = mdConfig::getConfig($key);
    		if(!$cur){
    			$cur = ['url'=>''];
    			$re = mdConfig::addConfig($key,$cur);
    			if(!$re){
    				res_api('初始化数据失败，请重试');
    			}
    		}
    		return $this->fetch('common@system/suiyiyun',['cur'=>$cur]);
    	}
    }
    
    //轮播图片配置
    public function banners(){
    	$key = 'index_banner';
    	if($this->request->isAjax()){
    		$data = mdConfig::getBannerData();
    		$res = mdConfig::saveConfig($key,$data);
    		if($res){
    			$html = $this->fetch('common@block/banners',['list'=>$data]);
    			saveBlock($html,$key,'other');
    			res_api();
    		}else{
    			res_api('保存失败');
    		}
    	}else{
    		$cur = mdConfig::getConfig($key);
    		if($cur === false){
    			$cur = [];
    			$re = mdConfig::addConfig($key, $cur);
    			if(!$re){
    				res_api('初始化数据失败，请重试');
    			}
    		}
    		return $this->fetch('common@system/banners',['cur'=>$cur,'title'=>'首页轮播图配置']);
    	}
    }
    
    //类型配置
    public function category(){
    	$key = 'cartoon_category';
    	if($this->request->isAjax()){
    		$data = mdConfig::getCategoryData();
    		$res = mdConfig::saveConfig($key,$data);
    		if($res){
    			cache($key,$data);
    			res_api();
    		}else{
    			res_api('保存失败');
    		}
    	}else{
    		$cur = mdConfig::getConfig($key);
    		if($cur === false){
    			$cur = [];
    			$re = mdConfig::addConfig($key, $cur);
    			if(!$re){
    				res_api('初始化数据失败，请重试');
    			}
    		}
    		$variable = [
    			'cur' => $cur,
    			'title' => '漫画类型配置',
    		];
    		return $this->fetch('common@system/category',$variable);
    	}
    }

    //小说轮播图片配置
    public function novelBanners(){
        $key = 'novel_index_banner';
        if($this->request->isAjax()){
            $data = mdConfig::getBannerData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                $html = $this->fetch('common@block/banners',['list'=>$data]);
                saveBlock($html,$key,'novel');
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/novel_banners',['cur'=>$cur,'title'=>'小说首页轮播图配置']);
        }
    }

    //小说类型配置
    public function novelCategory(){
        $key = 'novel_category';
        if($this->request->isAjax()){
            $data = mdConfig::getCategoryData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                cache($key,$data);
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            $variable = [
                'cur' => $cur,
                'title' => '小说类型配置',
            ];
            return $this->fetch('common@system/novel_category',$variable);
        }
    }

    //网站板块管理
    public function block(){
        $key = 'web_block';
        $defaultData = [
            ['name'=>'漫画','key'=>'cartoon','url'=>'/index/index/index.html','is_on'=>1],
            ['name'=>'小说','key'=>'novel','url'=>'/index/Novel/index.html','is_on'=>1],
        ];
        if($this->request->isAjax()){
            $post = myHttp::getData('novel,cartoon','post');
            foreach ($defaultData as &$v){
                if(!$post[$v['key']]){
                    $v['is_on'] = 0;
                }
            }
            $re = mdConfig::saveConfig($key,$defaultData);
            if($re){
                cache($key,$defaultData);
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = $defaultData;
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            $this->assign('cur',$cur);
            return $this->fetch('common@system/block');
        }
    }
}
