<?php
namespace app\admin\controller;

use app\common\model\mdChart;

class Chart extends Common{
    
    //充值统计
    public function charge(){
        if($this->request->isAjax()){
            $data = mdChart::getChargeList();
            $data = $data ? : [];
            res_table($data);
        }else{
        	$data = mdChart::getChargeData();
            return $this->fetch('common@chart/charge',['data'=>$data]);
        }
    }
    
    //用户统计
    public function member(){
    	
        $data = mdChart::getMemberCount();
        return $this->fetch('common@chart/member',$data);
    }
    
    //订单统计
    public function order(){
    	if($this->request->isAjax()){
    		$data = mdChart::getTodayOrderMsg();
    		res_table($data);
    	}else{
    		$data = mdChart::getOrderCount();
    		return $this->fetch('common@chart/order',$data);
    	}
    }
    
    //活跃度统计
    public function active(){
    	$end_time = strtotime('today');
    	$start_date = date('Y-m-d',$end_time - 30 * 86400);
    	$end_date = date('Y-m-d',$end_time);
    	if($this->request->isAjax()){
    		$between_time = $this->request->post('between_time');
    		if($between_time){
    			$arr = explode('~', $between_time);
    			if(count($arr) == 2){
    				$input_start_date = date('Y-m-d',strtotime(trim($arr[0])));
    				$input_end_date = date('Y-m-d',strtotime(trim($arr[1])));
    				if($input_start_date <= $input_end_date){
    					$start_date = $input_start_date;
    					$end_date = $input_end_date;
    				}
    			}
    		}
    		$data = mdChart::getActiveList($start_date, $end_date);
    		res_api($data);
    	}else{
    		$between_time = $start_date.' ~ '.$end_date;
    		return $this->fetch('common@active',['between_time'=>$between_time]);
    	}
    }
    
}