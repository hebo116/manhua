<?php
namespace app\admin\controller;

use app\common\model\mdNovel;
use QL\QueryList;
use think\Db;
use site\myHttp;
use site\myCache;
use OSS\OssClient;
use OSS\Core\OssException;
use site\myPush;

class Book{

    // 0 6 * * * php /www/wwwroot/book/public/index.php /admin/Book/doStartYruan > /dev/null

	private $channel = '';
	
	private $ossClient;


	public function __construct(){
//		if($_SERVER['HTTP_HOST'] !== 'localhost'){
//			//exit();
//		}
	}
	
	//开始更新书籍

	//添加日志
	private function addLog($book_id,$chapter,$number){
		$data = [
			'book_id' => $book_id,
			'chapter' => $chapter,
			'chapter_num' => $number,
			'create_time' => time()
		];
		Db::name('BookCatLog')->insert($data);
	}
	
	//清除书籍缓存
	private function clearCache($book_id,$isOverType=false){
		myCache::rmBookChapterList($book_id);
		myCache::rmBookChapterNum($book_id);
		myCache::rmBookHotNum($book_id);
		myCache::rmUpdateBookIds();
		myCache::rmNewBookIds(2);
		if($isOverType){
			myCache::doBook($book_id, 'over_type', 2);
		}
	}
	
	//获取小说封面
	private function getBookCover($img_url){
		$url = '';
        if($img_url){
            $path = '/uploads/img/'.date('Ymd');
            if(!is_dir(MYPUBLIC.$path)){
                mkdir(MYPUBLIC.$path,0777,true);
            }
            if(!is_dir(MYPUBLIC.$path)){
                self::resMsg('图片目录生成失败');
            }

            $coverinfo = pathinfo($img_url);
            if($coverinfo && isset($coverinfo['extension'])){
                $name = md5($img_url).'.'.$coverinfo['extension'];
                $filename = $path.'/'.$name;
                $imgcontent = self::getImgContent($img_url);
                if($imgcontent){
                    $myfile = fopen(MYPUBLIC.$filename,"w");
                    fwrite($myfile,$imgcontent);
                    fclose($myfile);
                    $config = getMyConfig('suiyiyun');
                    $suiyiyun = $config ? $config['url'] : '';
                    $url = $suiyiyun.ltrim($filename,'.');
                }
            }
        }
		return $url;
	}
	
	//获取图像内容
	private function getImgContent($img_url,$referer=''){
		$header = array('User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:45.0) Gecko/20100101 Firefox/45.0', 'Accept-Language: zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3', 'Accept-Encoding: gzip, deflate', );
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $img_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		if($referer){
			curl_setopt($curl, CURLOPT_REFERER,$referer);
		}
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip');
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		$data = curl_exec($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		return $data;
	}

    //开始更新书籍
    public function doStartYruan(){
        set_time_limit(0);
        $repeat_num = 0;
        $bookIds = self::getYranBookIds();
        //echo __DIR__."\n";die;
        //dump($bookIds);die;
        if($bookIds){
            $time = time();
            foreach ($bookIds as $v){
                //$push_book_id = 0;
                $cur = Db::name('BookCat')->where('sync_id',$v['bookid'])->find();
                if($cur && $cur['is_end'] == 1){
                    $cacheBook = myCache::getBook($cur['book_id']);
                    if($cacheBook && $cacheBook['over_type'] == 1){
                        $fre = Db::name('Book')->where('id',$cacheBook['id'])->setField('over_type',2);
                        if($fre !== false){
                            myCache::doBook($cacheBook['id'], 'over_type', 2);
                        }
                    }
                    $cabook = myCache::getBook($cur['book_id']);
                    echo "《".$cabook['name']."》已经完结,无需继续更新<br>";
                    continue;
                }
                $bookInfo = self::getYranBookInfo($v['bookid']);
                //dump($bookInfo);die;
                if($bookInfo){
                    if(!$cur){
                        $bookname = $bookInfo['bookname'];
                        $repeat = Db::name('Book')->where('name',$bookname)->value('id');
                        if($repeat){
                            $bookname = $bookname.'/'.$bookInfo['authorname'];
                            $repeat2 = Db::name('Book')->where('name',$bookname)->value('id');
                            if($repeat2){
                                $repeat_num++;
                                continue;
                            }
                        }
                        $data = [
                            'type' => 2,
                            'name' => $bookname,
                            'author' => $bookInfo['authorname'],
                            'cover' => self::getBookCover($bookInfo['bookpic']),//todo:
                            'summary' => $bookInfo['intro'],
                            'category' => ','.$bookInfo['bookType'].',',
                            'over_type' => $bookInfo['state'],
                            'free_type' => 2,
                            'free_chapter' => 15,
                            'money' => 28,
                            'text_tag'=>[],
                            'star'=>mt_rand(800,999)/100,
                            'update_date'=>date('Y-m-d'),
                            'create_time' => $time
                        ];
                        Db::startTrans();
                        $flag = false;
                        $book_id = Db::name('Book')->insertGetId($data);
                        if($book_id){
                            $cur = [
                                'book_id' => $book_id,
                                'sync_id' => $v['bookid'],
                                'last_num' => 0
                            ];
                            $re = Db::name('BookCat')->insertGetId($cur);
                            if($re){
                                $cur['id'] = $re;
                                $flag = true;
                            }
                        }
                        if($flag){
                            //echo "《".$bookInfo['bookname']."》添加成功<br/>";
                            Db::commit();
                        }else{
                            Db::rollback();
                            continue;
                        }
                    }
                }
                if($cur){
                    if($cur['last_num'] < $bookInfo['chaptercount']){
                        $chapter = self::getYruanBookChapter($v['bookid']);
                        if($chapter){
                            $cur_num = $cur['last_num'] + 1;
                            foreach ($chapter as $cv){
                                if($cur_num == $cv['num']){
                                    $content = self::getYruanChapterText($v['bookid'], $cv['chapterid']);
                                    if($content){
                                        $html = "<p>".$content;
                                        $html = preg_replace('/\n|\r\n|<br\/>/','</p><p>',$html);
                                        $chapterData = [
                                            'book_id' => $cur['book_id'],
                                            'name' => $cv['chaptername'],
                                            'number' => $cur_num,
                                            'create_time' => $time
                                        ];
                                        Db::startTrans();
                                        $flag = false;
                                        $isOverType = false;
                                        $chapter_id = Db::name('BookChapter')->insertGetId($chapterData);
                                        if($chapter_id){
                                            $catData = ['last_num'=>$cur_num];
                                            if($cur_num == $bookInfo['chaptercount'] && $bookInfo['state'] == 2){
                                                $catData['is_end'] = 1;
                                            }
                                            $res = Db::name('BookCat')->where('id',$cur['id'])->update($catData);
                                            if($res){
                                                $doRes = true;
                                                if(isset($catData['is_end']) && $catData['is_end'] == 1){
                                                    $doRes = false;
                                                    $bres = Db::name('Book')->where('id',$cur['book_id'])->setField('over_type',2);
                                                    if($bres !== false){
                                                        $isOverType = true;
                                                        $doRes = true;
                                                    }
                                                }
                                                if($doRes){
                                                    $block_res = saveBlock($html,$cur_num,'book/'.$cur['book_id']);
                                                    if($block_res){
                                                        $flag = true;
                                                    }
                                                }
                                            }
                                        }
                                        if($flag){
                                            Db::commit();
                                            //echo "[".$cv['chaptername']."] 添加成功<br/>";
                                            //$push_book_id = $cur['book_id'];
                                            self::addLog($cur['book_id'], $chapterData['name'], $cur_num);
                                            self::clearCache($cur['book_id'],$isOverType);
                                            $cur_num += 1;
                                        }else{
                                            Db::rollback();
                                            break 1;
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        if($bookInfo['state'] == 2 && $cur['last_num'] == $bookInfo['chaptercount']){
                            Db::startTrans();
                            $flag = false;
                            $re = Db::name('BookCat')->where('id',$cur['id'])->setField('is_end',1);
                            if($re){
                                $res = Db::name('Book')->where('id',$cur['book_id'])->setField('over_type',2);
                                if($res){
                                    $flag = true;
                                }
                            }
                            if($flag){
                                Db::commit();
                                myCache::doBook($cur['book_id'], 'over_type', 2);
                            }else{
                                Db::rollback();
                            }
                        }
                    }
                }
                echo "《".$bookInfo['bookname']."》更新完毕！<br/>";
            }
        }
        exit;
    }

    //-------TOPTOON漫画站同步https://www.toptoon.net/--------------

    //获取TOPTOON书籍id
    public function getTopBookIds(){
	    //$url = "https://www.toptoon.net/comic/epList/80828_1";
	    $url = "https://www.toptoon.net/comic/epList/80870";
	    $html = file_get_contents($url);
	    var_dump($html);
    }

    //获取TOPTOON书籍详情
    public function getTopBookInfo(){
        //$url = "https://www.toptoon.net/comic/epList/".$bookid;
        $url = "http://wap.cloudready.site/cartoon/1729/indexs/sectionInfo/26594?token=3a5b95c909522b5b0c6b53692bf7506d";
        $bookInfo = [];
        $html = file_get_contents($url);
        $bool = preg_match_all('/<img data-original="(.*?)" width="100%" class="lazy" \/>/ism',$html,$match);

        dump($match);die;

        //书籍id
        $bool = preg_match('/<meta property="og:novel:read_url" content="http:\/\/www.yruan.com\/article\/(.*?).html"\/>/i',$html,$bookid);
        if ($bool){
            $bookInfo['bookid'] = $bookid[1];
        }
        //书籍名称
        $bool = preg_match('/<meta property="og:novel:book_name" content="(.*?)"\/>/i',$html,$bookname);
        if ($bool){
            $bookInfo['bookname'] = $bookname[1];
        }

        //作者
        $bool = preg_match('/<meta property="og:novel:author" content="(.*?)"\/>/i',$html,$authorname);
        if ($bool){
            $bookInfo['authorname'] = $authorname[1];
        }

        //更新状态 1连载中 2已完结
        $bool = preg_match('/<meta property="og:novel:status" content="(.*?)"\/>/i',$html,$state);
        if ($bool){
            if ($state[1] == '连载中'){
                $bookInfo['state'] = 1;
            }else{
                $bookInfo['state'] = 2;
            }
        }

        //分类名称
        $bool = preg_match('/<meta property="og:novel:category" content="(.*?)"\/>/i',$html,$bookType);
        if ($bool){
            $bookInfo['bookType'] = $bookType[1];
        }

        //章节总数
        $num = preg_match_all('/<dd><a href=".*?" title=".*?<\/a><\/dd>/im',$html,$match);
        if ($num){
            $bookInfo['chaptercount'] = $num;
        }else{
            $bookInfo['chaptercount'] = 0;
        }

        //书籍封面
        $bool = preg_match('/<img width="120" height="150" src="(.*?)"(.*?)\/>/i',$html,$bookpic);
        if ($bool){
            $bookInfo['bookpic'] = 'http://www.yruan.com/'.$bookpic[1];
        }

        //书籍描述
        $bool = preg_match('/<meta property="og:description" content="(.*?)"\/>/i',$html,$intro);
        if ($bool){
            $intro = str_replace('<br/>','',$intro[1]);
            //var_dump($intro);die;
            if(mb_strlen($intro)>=50){
                $bookInfo['intro'] = mb_substr($intro,0,50)."......";
            }else{
                $bookInfo['intro'] = $intro;
            }
        }

        //更新日期
        $bool = preg_match('/<meta property="og:novel:update_time" content="(.*?)"\/>/i',$html,$update_time);
        if ($bool){
            $bookInfo['update_time'] = $update_time[1];
        }

        //echo "<pre>";
        //var_dump($bookpic);

        return $bookInfo;
    }

    //获取TOPTOON章节列表
    public function getTopBookChapter($bookid){
        $url = $url = "https://www.toptoon.net/comic/epList/".$bookid;;
        $html = file_get_contents("compress.zlib://".$url);
        $chapter = [];
        $bool = preg_match_all('/<dd><a href="\/article\/(.*?)\/(.*?).html" title="(.*?)">.*?<\/a><\/dd>/ism',$html,$match);
        if ($bool){
            echo "<pre>";
            $chapter = [];
            foreach ($match[2] as $k=>$chapterid){
                $chapter[$k]['chapterid'] = $chapterid;
                $chapter[$k]['chaptername'] = $match[3][$k];
                $chapter[$k]['num'] = $k+1;
                $chapter[$k]['bookid'] = $match[1][0];
                $chapter[$k]['red_url'] = "http://www.yruan.com/article/".$match[1][0]."/".$chapterid.".html";
            }
        }

        //echo "<pre>";
        //var_dump($chapter);
        return $chapter;
    }

    //获取TOPTOON章节内容
    public function getTopChapterText($bookid,$chapterid){
        $red_url = "https://www.toptoon.net/comic/epView/".$bookid."/".$chapterid;
        $html = file_get_contents("compress.zlib://".$red_url);
        $text = '';
        $bool = preg_match('/<div id="content">([\S\s]*?)<\/div>/i',$html,$match);
        if ($bool){
            $text = str_replace(['<!--go-->','<!--over-->'],'',$match[1]);
        }
        return $text;
    }

    //curl获取TOPTOON网页内容
    public function my_file_get_contents($url){
        $arr = array(
            "cookie: language=tw; viewModeSaving=1; userKeyToken=83565bb13e4bb36845be1274bc4f2975_1616420175; service_key=d4855234b665da158e87d11dc408f7d8; __lt__cid=cd2bfcc7-7ff2-45d9-a992-46969b285220; _fbp=fb.1.1616420177957.756104015; _ga=GA1.2.1043653395.1616420178; comicEpListSort=asc; _gid=GA1.2.908194339.1616565836; isLoginUsed=1; notOpenGoGiftboxAlert=1; userRecentTop10ListToLS=1; timeUTC=0; __lt__sid=67fab45e-803cf1f6; saveId=xulin66008189%40gmail.com; net_session=vGp93cagAJZDTYawBVV3nhw5Or8xyk5TlZcoYlK1TfiY%2F2bYoluHin9zXvJ793zkqXm5p%2FkR2FPt0cZ%2Fyz2pootR9vuCRFTrCqq9ltijaACaWqDKjicMN1eXSZbHUdCLZY8CiIBfiybwX6TLbL1hq9rVGGgDDJDHAOytuEFU5QHZW5PiiDsGm6eH6Ap%2BWT7ETh42PBFfO8lJ3OTzN2rCa7cTto43yfVdabIEDmbFfSi0Cg44iOZ1q3L0C3dgQKKM80awciGl7%2BR2OzIqBdee5cYODtFQPChGx2OL2P8EQXHGerdHOmuK1b9uBFLHnaiCQz6RUwhQcX%2FmZXPVGyvKBEfnkQLGcJZkUwbGqrL4pDNgpGbGSBgyl0rQL9I33Uc6q4eCg%2BlCkcEvc8Pq5L8G0r%2FZhjc2NF%2BYfkDK022XDvc7dDh5nQr17AyB5eHUuXohTIVyeZuH%2BLuQa8t7jwTVyXIFXuIugA9YN4ZyE52hmm%2F21QXO7s3mnmUijV3nwebfGnZeS%2F2S6RaKbOeT0KSjfq%2FphOrbzsfEdghNEEjtIRI7%2BEEsFhPbtQsLnIIF9F60j30Az%2BKQ1%2BVa6BwwwNLAWgREKSrTkbX7itKNOPBQs5RkWm4r7wpcOF%2BWp5DmgREvfc16e943f0384c55b376c01cdd6c3e6987282c16; userGender=1; adultStatus=1; notOpenGiftNotice=1; userKey=D0818219317B0ED6B46F27DEF699DF8992AC67305A30EEC21D9A7481EC61A7DD; epListAccessPath=%2Fweekly%2F4; userPayCount=14; userCoinInfo=%7B%22userCoinWeb%22%3A%22126%22%2C%22userCoinSilver%22%3A%220%22%2C%22userCoinBronze%22%3A%220%22%2C%22userCoinCoupon%22%3A0%2C%22userCoinBronzeDDay%22%3A0%2C%22userCoin%22%3A126%7D; userLastActionDt=2021-03-25+16%3A03%3A49; hotTimeGiveInfo=%7B%22hotTimeSet%22%3A1616659429%7D; _gat_gtag_UA_63738880_1=1"
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, 10000);
        // 如果是请求https时，要打开下面两个ssl安全校验
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//表示string输出，0为直接输出；
        curl_setopt($ch,CURLOPT_HEADER,0);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$arr);
        $html_source = curl_exec($ch);
        curl_close($ch);
        return $html_source;
    }

    //-----------------------渔网漫画同步-----------------------------------
    //获取渔网漫画书籍列表
    public function getYuwangBookIds(){
	    //
    }

}



