<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdAgent;

class Agent extends Common{
    
    //代理列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'eq' => 'status:a.status',
                'like' => 'keyword:a.name',
            	'default' => [['a.status','between',[0,3]],['a.type','=',1]]
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdAgent::getList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$v['charge_money'] = $v['charge_money'] ? : 0;
                	$v['money'] = myCache::getChannelAmount($v['id']);
                    $v['status_name'] = mdAgent::getStatusName($v['status']);
                    $v['do_url'] = my_url('doAgent',['id'=>$v['id']]);
                    $v['child_url'] = my_url('child',['id'=>$v['id']]);
                    $v['ratio'] .= '%';
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
            return $this->fetch('common@agent/index',['js'=>getJs('agent.index')]);
        }
    }
    
    //新增代理
    public function addAgent(){
        if($this->request->isAjax()){
            $rules = mdAgent::getRules();
            $data = myValidate::getData($rules);
            $data['type'] = 1;
            $data['status'] = 1;
            mdAgent::doneAgent($data);
        }else{
            $field = 'id,name,login_name,status,url,deduct_min:0,deduct_num:0,wefare_days:30,ratio:90,bank_user,bank_name,bank_no';
            $cur = myDb::buildArr($field);
            $variable = mdAgent::getAgentOptions();
            $variable['ratio'] = 100;
            $variable['cur'] = $cur;
            $variable['backUrl'] = url('index');
            return $this->fetch('common@doAgent',$variable);
        }
    }
    
    //编辑代理
    public function doAgent(){
        if($this->request->isAjax()){
        	$rules = mdAgent::getRules(0);
        	$data = myValidate::getData($rules);
        	mdAgent::doneAgent($data);
        }else{
            $id = myHttp::getId('代理');
            $cur = myDb::getById('Channel', $id);
            if(!$cur){
                res_api('渠道不存在');
            }
            $variable = mdAgent::getAgentOptions();
            $variable['ratio'] = 100;
            $variable['cur'] = $cur;
            $variable['backUrl'] = url('index');
            return $this->fetch('common@doAgent',$variable);
        }
    }
    
    //子代理列表
    public function child(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['a.status','between',[0,3]],['a.type','=',2]],
                'eq' => 'status:a.status,id:a.parent_id',
                'like' => 'keyword:a.name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdAgent::getList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$v['charge_money'] = $v['charge_money'] ? : 0;
                	$v['money'] = myCache::getChannelAmount($v['id']);
                    $v['status_name'] = mdAgent::getStatusName($v['status']);
                    $v['do_url'] = my_url('doChild',['id'=>$v['id']]);
                    $v['ratio'] .= '%';
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            $parent_id = myHttp::getId('代理');
            $cur = myDb::getById('Channel',$parent_id);
            if(!$cur){
                res_api('渠道不存在');
            }
            return $this->fetch('common@child',['back_url'=>url('index')]);
        }
    }
    
    //编辑子代理
    public function doChild(){
    	if($this->request->isAjax()){
    		$rules = mdAgent::getRules(0);
    		$data = myValidate::getData($rules);
    		mdAgent::doneAgent($data);
    	}else{
    		$id = myHttp::getId('代理');
    		$cur = myDb::getById('Channel', $id);
    		if(!$cur){
    			res_api('代理不存在');
    		}
    		if($cur['type'] != 2 || !$cur['parent_id']){
    			res_api('代理信息异常');
    		}
    		$variable = mdAgent::getAgentOptions();
    		$parent = myDb::getById('Channle', $cur['parent_id'],'id,ratio');
    		$variable['cur'] = $cur;
    		$variable['ratio'] = $parent['ratio'];
    		$variable['backUrl'] = my_url('child',['id'=>$cur['parent_id']]);
    		return $this->fetch('common@doAgent',$variable);
    	}
    }
    
    
    //处理代理状态
    public function doAgentEvent(){
    	$rules = mdAgent::getEventRules();
    	$data = myValidate::getData($rules);
        $cur = myDb::getById('Channel',$data['id'],'id,url');
        if(!$cur){
            res_api('代理信息异常');
        }
        $key = 'status';
        switch ($data['event']){
            case 'on':
                $value = 1;
                break;
            case 'off':
                $value = 2;
                break;
            case 'delete':
                $value = 4;
                break;
            case 'pass':
                $value = 1;
                break;
            case 'fail':
                $value = 3;
                break;
            case 'resetpwd':
                $key = 'password';
                $value = createPwd(123456);
                break;
            default:
                res_api('未指定该事件');
                break;
        }
        if($value === 4){
        	$saveData = ['status'=>4,'login_name'=>'','url'=>''];
        	$re = myDb::save('Channel',[['id','=',$cur['id']]],$saveData);
        }else{
        	$re = myDb::setField('Channel', [['id','=',$cur['id']]],$key, $value);
        }
        if($re){
            if($key === 'status'){
            	myCache::rmChannel($cur['id']);
                if($cur['url']){
                    myCache::rmUrlInfo($cur['url']);
                }
            }
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    //进入代理后台
    public function intoBackstage(){
        $id = myHttp::postId('代理');
        $url = mdAgent::intoBackstage($id);
        res_api(['url'=>$url]);
    }
    
}