<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use app\common\model\mdBook;
use site\myCache;

class Free extends Common{
	
	public function index(){
	    $category = input('category',1,'intval');
		if($this->request->isAjax()){
			$config = [
				'default' => [['b.status','=',1],['b.type','=',$category]],
				'like' => 'keyword:b.name'
			];
			$pages = myHttp::getPageParams();
			$where = mySearch::getWhere($config);
			$res = mdBook::getFreeBookList($where, $pages);
			if($res['data']){
				$date = date('Y-m-d');
				foreach ($res['data'] as &$v){
					if($date >= $v['start_date'] && $date <= $v['end_date']){
						$free_status = 1;
					}else{
						if($v['end_date'] > $date){
							$free_status = 2;
						}else{
							$free_status = 3;
						}
					}
					$v['free_status'] = $free_status;
					$v['do_url'] = my_url('doFree',['id'=>$v['id'],'category'=>$category]);
				}
			}
			res_table($res['data'],$res['count']);
		}else{

			return $this->fetch('common@free/index',['category'=>$category]);
		}
	}

	//新增限免书籍
	public function addFree(){
		if($this->request->isAjax()){
			$rules = [
				'start_date' => ['require|date',['require'=>'请选择开始日期','date'=>'日期格式不正确']],
				'end_date' => ['require|date',['require'=>'请结束开始日期','date'=>'日期格式不正确']],
				'book_ids' => ['require|array',['require'=>'请选择书籍','array'=>'书籍格式不规范']],
			];
			$date = date('Y-m-d');
			$data = myValidate::getData($rules);
			$bookIds = $data['book_ids'];
			unset($data['book_ids']);
			$num = 0;
			foreach ($bookIds as $v){
				if(is_numeric($v)){
					$cur_id = myDb::getValue('BookFree', [['book_id','=',$v]], 'id');
					if($cur_id){
						myDb::save('BookFree', [['id','=',$cur_id]], $data);
					}else{
						$data['book_id'] = $v;
						$re = myDb::add('BookFree', $data);
						if($re){
							$num++;
						}
					}
				}
			}

			if($num && $date >= $data['start_date'] && $date <= $data['end_date']){
				myCache::rmFreeBookIds();
			}
			res_api(['num'=>$num]);
		}else{
			$category = input('category',1,'intval');
			return $this->fetch('common@free/addFree',['category'=>$category]);
		}
	}
	
	//选择书籍
	public function selBook(){
		if($this->request->isAjax()){
			$config = [
				'like' => 'keyword:name',
				'default' => [['status','=',1],['free_type','=',2]],
			];
			$where = mySearch::getWhere($config);
            $category = input('category',1,'intval');
            $where[] = ['type','=',$category];
			$list = myDb::getLimitList('Book', $where,'id,name,cover,free_chapter,money,hot_num',20000);
			foreach ($list as &$v){
                $v['cover'] = handleImg($v['cover']);
            }
            res_table($list);
		}else{
			return $this->fetch('common@free/selBook');
		}
	}
	
	//编辑限免书籍
	public function doFree(){
	    $category = input('category',1,'intval');
		if($this->request->isAjax()){
			$rules = [
				'start_date' => ['require|date',['require'=>'请选择开始日期','date'=>'日期格式不正确']],
				'end_date' => ['require|date',['require'=>'请结束开始日期','date'=>'日期格式不正确']],
				'id' => ['require|number|gt:0',['require'=>'参数错误','number'=>'参数错误','gt'=>'参数错误']],
			];
			$data = myValidate::getData($rules);
			$date = date('Y-m-d');
			if($data['start_date'] > $data['end_date']){
				res_api('开始时间不能大于结束时间');
			}
			$re = myDb::saveIdData('BookFree', $data);
			if($re){
				myCache::rmFreeBookIds();
				res_api();
			}else{
				res_api('更新失败');
			}
		}else{
			$id = myHttp::getId('限免书籍');
			$cur = myDb::getById('BookFree', $id);
			$cur['book'] = myCache::getBook($cur['book_id']);
			return $this->fetch('common@free/doFree',['cur'=>$cur,'category'=>$category]);
		}
	}
	
	//删除限免书籍
	public function delFree(){
		$id = myHttp::postId('限免书籍');
		$re = myDb::delById('BookFree', $id);
		if($re){
			myCache::rmFreeBookIds();
			res_api();
		}else{
			res_api('删除失败');
		}
	}
}