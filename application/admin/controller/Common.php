<?php
namespace app\admin\controller;

use think\Controller;
use app\common\model\mdLogin;

class Common extends Controller{
    
    //初始化
    public function __construct(){
        parent::__construct();
        global $loginId;
        $loginId = mdLogin::getCache('id');
        if(!$loginId){
            $url = my_url('Login/index');
            if($this->request->isAjax()){
                res_api('登录已失效');
            }else{
                echo "<script language=\"javascript\">window.open('".$url."','_top');</script>";
                exit;
            }
        }
    }
    
    
    public function _empty(){
    	res_error('页面不存在');
    }
}