<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use app\common\model\mdBook;
use app\common\model\mdSpread;


class Novel extends Common{
	
    //小说列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['status','between',[1,3]],['type','=',2]],
                'eq' => 'status,over_type,free_type,is_god',
                'like' => 'keyword:name,category'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdBook::getBookPageList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$free_time = '';
                	if(myCache::checkBookFree($v['id'])){
                		$free_time = '未知';
                		$freeItem = myDb::getCur('BookFree', [['book_id','=',$v['id']]],'start_date,end_date');
                		if($freeItem){
                			$free_time = $freeItem['start_date'].'~'.$freeItem['end_date'];
                		}
                	}
                	$v['free_time'] = $free_time;
                	$v['total_chapter'] = myCache::getBookChapterNum($v['id']);
                	$v['is_free'] = myCache::checkBookFree($v['id']);
                    $v['do_url'] = my_url('doBook',['id'=>$v['id']]);
                    $v['chapter_url'] = my_url('chapter',['book_id'=>$v['id']]);
                    $v['spread_url'] = my_url('Spread/createLink',['book_id'=>$v['id']]);
                    $v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
        	$category = getMyConfig('novel_category');
        	$keyword = $this->request->get('keyword');
        	$variable = ['js'=>getJs('novel.index','admin',['date'=>'20200226']),'category'=>$category,'keyword'=>$keyword];
        	return $this->fetch('common@index',$variable);
        }
    }
    
    //复制链接
    public function copyLink(){
        $id = myHttp::getId('小说');
        $book = myDb::getById('Book',$id,'id,name');
        if(!$book){
            res_api('书籍参数错误');
        }
        $url = mdSpread::getSpreadUrl();
        $short_url = '/Index/Book/info.html?category=2&book_id='.$id;
        $short_url = '/index.html#/detail?book_id='.$id;
        $data = [
            'notice' => '温馨提示 : 相对链接只能应用到页面跳转链接中，如轮播图链接等，渠道用户点击后不会跳转到总站',
            'links' => [
                ['title'=>'相对链接','val'=>$short_url],
                ['title'=>'绝对链接','val'=>$url.$short_url]
            ]
        ];
        return $this->fetch('common@public/copyLink',$data);
    }
    
    //新增小说
    public function addBook(){
        if($this->request->isAjax()){
        	$data = mdBook::getData(1);
        	$data['type'] = 2;
            mdBook::doneBook($data);
        }else{
        	$star = (9).'.'.mt_rand(0,99);
            $field = 'id,name,cover,detail_img,author,summary,money:28,status,free_type:2,over_type,free_chapter:15,book_tag,star:'.$star;
            $field .= ',hot_num:0,category';
            $option = mdBook::getOptions();
            $option['cur'] = myDb::buildArr($field);
            $option['category'] = getMyConfig('novel_category');
            $option['title'] = '新增小说';
            return $this->fetch('common@doCartoon',$option);
        }
    }
    
    //编辑小说
    public function doBook(){
        if($this->request->isAjax()){
        	$data = mdBook::getData(0);
            mdBook::doneBook($data);
        }else{
            $id = myHttp::getId('小说');
            $cur = myDb::getById('Book',$id);
            if(!$cur){
                res_error('书籍不存在');
            }
            $book_tag = [];
            if($cur['is_beast'] == 1){
            	$book_tag[] = 1; 
            }
            if($cur['is_recommend'] == 1){
            	$book_tag[] = 2;
            }
            if($cur['is_alone'] == 1){
            	$book_tag[] = 3;
            }
            if($cur['is_vip'] == 1){
            	$book_tag[] = 4;
            }
            if($book_tag){
            	$cur['book_tag'] = implode(',', $book_tag);
            }else{
            	$cur['book_tag'] = '';
            }
            $cur['text_tag'] = $cur['text_tag'] ? json_decode($cur['text_tag']) : [];
            $option = mdBook::getOptions();
            $option['category'] = getMyConfig('novel_category');
            $option['cur'] = $cur;
            $option['title'] = '更新小说信息';
            return $this->fetch('common@doCartoon',$option);
        }
    }
    
    //处理小说事件
    public function doBookEvent(){
       $flag = mdBook::doBookEvent();
       if($flag){
       		res_api();
       }else{
       		res_api('操作失败,请重试');
       }
    }
    
    //分集列表
    public function chapter(){
        $book_id = myHttp::getId('小说','book_id');
        if($this->request->isAjax()){
            $config = [
                'eq' => 'number',
                'like' => 'keyword:name',
            	'default' => [['book_id','=',$book_id]]
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdBook::getChapterPageList($where,$pages);
            if($res['data']){
            	$book = myCache::getBook($book_id);
            	if($book){
            		foreach ($res['data'] as &$v){
            			$v['is_free'] = 1;
            			if($book['free_type'] == 2){
            				if($v['number'] > $book['free_chapter']){
            					$v['is_free'] = 2;
            					if(!$v['money']){
            						$v['money'] = $book['money'];
            					}
            				}
            			}
            		}
            	}
            }
            res_table($res['data'],$res['count']);
        }else{
            
        	return $this->fetch('common@novel/chapter',['book_id'=>$book_id]);
        }
    }

    //新增章节
    public function addChapter(){
        if($this->request->isAjax()){
            $data = mdBook::getChapterData(1);
            mdBook::doneChapter($data);
        }else{
            $book_id = myHttp::getId('章节','book_id');
            $field = 'id,name,number,content';
            $option = [];
            $option['cur'] = myDb::buildArr($field);
            $option['book_id'] = $book_id;
            return $this->fetch('common@doChapter',$option);
        }
    }

    //编辑章节
    public function doChapter(){
        if($this->request->isAjax()){
            $data = mdBook::getChapterData();
            mdBook::doneChapter($data);
        }else{
            $id = myHttp::getId('章节','id');
            $cur = myDb::getById('BookChapter', $id,'id,name,book_id,number');
            if(!$cur){
                res_api('章节不存在');
            }
            $cur['content'] = getBlockContent($cur['number'],'book/'.$cur['book_id']);
            $option = [];
            $option['cur'] = $cur;
            $option['book_id'] = $cur['book_id'];
            return $this->fetch('common@doChapter', $option);
        }
    }
    
    //解析zip文件
    public function doDecodeZip(){
        set_time_limit(3600);
        mdBook::doBookNovel();
        res_api();
    }
    
    //修改章节封面
    public function changeCover(){
    	$data = mdBook::getChapterCoverData();
    	$cur = myDb::getById('BookChapter', $data['id']);
    	if(!$cur){
    		res_api('章节数据异常');
    	}
    	$re = myDb::saveIdData('BookChapter', $data);
    	if($re){
    		myCache::rmBookChapterList($cur['book_id']);
    		res_api('ok');
    	}else{
    		res_api('保存失败,请重试');
    	}
    }
    
    //查看章节
    public function showInfo(){
        $id = myHttp::getId('小说章节');
        $cur = myDb::getById('BookChapter', $id,'id,book_id,number');
        if(!$cur){
            res_api('章节不存在');
        }
        $cur['content'] = getBlockContent($cur['number'],'book/'.$cur['book_id']);
        return $this->fetch('common@showInfo',['cur'=>$cur]);
    }
    
    //章节检测
    public function checkChapter(){
        $book_id = myHttp::postId('小说','book_id');
        $error = mdBook::checkChapter($book_id);
        if($error){
            res_api($error);
        }else{
            res_api('ok',1,0);
        }
    }
    
    //设置书币数量
    public function setChapterMoney(){
    	$data = mdBook::getChapterMoneyData();
    	$table = 'BookChapter';
    	$cur = myDb::getById($table,$data['id'],'id,book_id,money');
    	if(!$cur){
    		res_api('章节信息异常');
    	}
    	if($cur['money'] == $data['money']){
    		res_api();
    	}
    	$res = myDb::saveIdData($table, $data);
    	if($res){
    		myCache::rmBookChapterList($cur['book_id']);
    		res_api();
    	}else{
    		res_api('设置失败');
    	}
    }
    
    //删除章节
    public function delChapter(){
        $re = mdBook::delChapter();
        if($re){
            res_api();
        }else{
            res_api('删除失败');
        }
    }
    
    //清空所有章节
    public function delAllChapter(){
        $book_id = myHttp::postId('小说','book_id');
        $re = mdBook::delAllChapter($book_id);
        if($re){
            res_api();
        }else{
            res_api('删除失败');
        }
    }
    
    //刷新缓存
    public function refreshCache(){
        $block_category = 2;//小说
    	$beastBook = mdBook::getCacheBookData(1,6,0,$block_category);
    	$html = $this->fetch('common@block/bookItem3',['list'=>$beastBook,'title'=>'大家都在看','type'=>1,'is_more'=>1,'is_load'=>1,'category'=>$block_category]);
    	saveBlock($html,'index_content_beast','novel');

    	myCache::rmUpdateBookIds();
    	
    	$newBook = mdBook::getCacheBookData(2,3,0,$block_category);
    	$html = $this->fetch('common@block/bookItem3',['list'=>$newBook,'title'=>'每日上新','type'=>2,'is_more'=>1,'is_load'=>2,'category'=>$block_category]);
    	saveBlock($html,'index_content_news','novel');
    	
    	$freeBook = mdBook::getCacheBookData(3,6,0,$block_category);
    	$html = $this->fetch('common@block/bookItem1',['list'=>$freeBook,'title'=>'每周限免','type'=>3,'is_load'=>2,'category'=>$block_category]);
    	saveBlock($html,'index_content_free','novel');
    	
    	$recommendBook = mdBook::getCacheBookData(4,6,0,$block_category);
    	$html = $this->fetch('common@block/bookItem3',['list'=>$recommendBook,'title'=>'重磅推薦','type'=>4,'is_more'=>1,'is_load'=>2,'category'=>$block_category]);
    	saveBlock($html,'index_content_recommend','novel');
    	
    	$allBook = mdBook::getCacheBookData(5,6,0,$block_category);
    	$html = $this->fetch('common@block/bookItem1',['list'=>$allBook,'title'=>'猜你喜歡','type'=>5,'is_load'=>1,'category'=>$block_category]);
    	saveBlock($html,'index_content_all','novel');
    	
    	$rankBook = mdBook::getRankBookData($block_category);
    	$html = $this->fetch('common@block/indexrank',['list'=>$rankBook,'category'=>$block_category]);
    	saveBlock($html,'index_content_rank','novel');
    	$html = $this->fetch('common@block/rankall',['list'=>$rankBook,'category'=>$block_category]);
    	saveBlock($html,'book_rank_all','novel');
        res_api();
    }
}