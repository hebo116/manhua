<?php
namespace app\admin\controller;

use site\myHttp;
use site\myDb;
use site\myValidate;
use site\myCache;

class Ad extends Common{
	
	//广告列表
	public function index(){
		if($this->request->isAjax()){
			$list = myDb::getList('Ad', [],'*',['id'=>'asc']);
			if($list){
				foreach ($list as &$v){
					$v['do_url'] = my_url('doAd',['id'=>$v['id']]);
				}
			}
			res_table($list);
		}else{
			
			return $this->fetch('common@ad/index');
		}
	}
	
	//编辑广告
	public function doAd(){
		if($this->request->isAjax()){
			$rules = [
				'id' =>  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
				'url' => ['require',['require'=>'请输入广告链接','url'=>'广告链接格式不规范']],
				'src' => ['require|url',['require'=>'请上传广告图片','url'=>'广告图片格式不规范']],
				'status' => ['require|in:1,2',['require'=>'请选择广告状态','in'=>'未指定该状态']]
			];
			$data = myValidate::getData($rules);
			$re = myDb::saveIdData('Ad', $data);
			if($re){
				myCache::rmAdList();
				res_api();
			}else{
				res_api('更新失败');
			}
		}else{
			$id = myHttp::getId('广告');
			$cur = myDb::getById('Ad', $id);
			if(!$cur){
				res_error('广告异常');
			}
			$variable = [
				'cur' => $cur,
				'status' => [
					'name' => 'status',
					'option' => [['val'=>1,'text'=>'是','default'=>0],['val'=>2,'text'=>'否','default'=>1]]
				],
			];
			return $this->fetch('common@doAd',$variable);
		}
	}
	
	//处理广告事件
	public function doAdEvent(){
		$rules = [
			'id' =>  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
			'event' => ["require|in:statuson,statusoff,clear",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']],
		];
		$data = myValidate::getData($rules);
		$cur = myDb::getById('Ad', $data['id']);
		if(!$cur){
			res_api('广告信息异常');
		}
		$is_cache = false;
		if($data['event'] === 'clear'){
			$is_cache = true;
			$re = myDb::setField('Ad', [['id','=',$data['id']]], 'click_num', 0);
		}else{
			$status = 2;
			if($data['event'] === 'statuson'){
				if(!$cur['src']){
					res_api('该广告尚未配置图片');
				}
				$status = 1;
			}
			$re = myDb::setField('Ad', [['id','=',$data['id']]], 'status', 1);
		}
		if($re){
			if($is_cache){
				myCache::rmAdList();
			}
			res_api();
		}else{
			res_api('设置失败');
		}
	}
}