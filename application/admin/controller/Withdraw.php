<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdWithdraw;

class Withdraw extends Common{
    
    //提现申请列表
    public function index(){
    	$status = myValidate::getData(['status'=>['in:0,1',['in'=>'非法访问']]],'get');
    	$status = $status ? : 0;
        if($this->request->isAjax()){
        	$config = [
        		'between' => 'between_time:cur_date%date',
        		'default' => [['to_channel_id','=',0],['status','=',$status]]
        	];
        	$where = mySearch::getWhere($config);
        	switch ($status){
        		case 0:
        			$list = mdWithdraw::getWaitList($where);
        			res_table($list);
        			break;
        		case 1:
        			$pages = myHttp::getPageParams();
        			$res = mdWithdraw::getPassList($where, $pages);
        			res_table($res['data'],$res['count']);
        			break;
        	}
        	res_api('非法访问');
        }else{
        	switch ($status){
        		case 0:
        			$js = getJs('withdraw.wait','admin',['date'=>'20200310']);
        			break;
        		case 1:
        			$js = getJs('withdraw.pass','admin',['date'=>'20200310']);
        			break;
        	}
        	$cur = mdWithdraw::getChildCountData();
        	return $this->fetch('common@withdraw/index',['cur'=>$cur,'status'=>$status,'js'=>$js]);
        }
    }
    
    //获取导出数据
    public function exportData(){
    	$where = [['to_channel_id','=',0],['status','=',0]];
    	$data = mdWithdraw::getExportData($where);
    	res_table($data);
    }
    
    //明细
    public function detail(){
    	$rules = ['sign' => ['require|alphaNum|length:32',['require'=>'明细参数异常','alphaNum'=>'明细参数格式不规范','length'=>'明细参数格式不规范']]];
    	$sign = myValidate::getData($rules,'get');
    	$field = 'id,channel_id,money,charge_money,charge_num,create_time';
    	$list = myDb::getList('Withdraw', [['sign','=',$sign]],'id,channel_id,money,charge_money,charge_num,create_time');
    	if($list){
    		foreach ($list as &$v){
    			$v['name'] = '未知';
    			$channel = myCache::getChannel($v['channel_id']);
    			if($channel){
    				$v['name'] = $channel['name'];
    			}
    			$v['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
    		}
    	}
    	return $this->fetch('common@withdraw/detail',['list'=>$list]);
    }
    
    //处理提现申请
    public function doWithdraw(){
    	$rules = ['ids' => ['require|array',['require'=>'账单参数错误','array'=>'账单格式不规范']]];
    	$ids = myValidate::getData($rules);
    	$num = 0;
    	foreach ($ids as $v){
    		$re = mdWithdraw::doPass($v);
    		if($re){
    			$num++;
    		}
    	}
    	res_api(['num'=>$num]);
    }
}
