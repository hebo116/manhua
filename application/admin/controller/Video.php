<?php
namespace app\admin\controller;
use app\common\model\mdConfig;
use app\common\model\mdSpread;
use app\common\model\mVideo;
use site\myCache;
use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;

class Video extends Common{
    
    //视频列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['status','between',[1,2]],['free_type','between',[1,2]]],
                'eq' => 'status,free_type',
                'like' => 'keyword:name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mVideo::getPageList('Video',$where, '*', $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                    $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                    $v['status_name'] = $v['status'] == 1 ? '正常' : '已下架';
                    $v['do_url'] = my_url('doVideo',['id'=>$v['id']]);
                    $v['customer_url'] = my_url('Message/addTask',['video_id'=>$v['id']]);
                    $v['play_url'] = my_url('doPlay',['id'=>$v['id']]);
                    $v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
                    $v['free_str'] = '免费';
                    if($v['free_type'] == 2){
                        $v['free_str'] = '收费:'.$v['money'].'书币';
                    }
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            $keyword = $this->request->get('keyword');               //'20200226'
            $variable = ['js'=>getJs('video.index','admin',['date'=>time()]),'keyword'=>$keyword];
            return $this->fetch('common@index',$variable);
        }
    }
    
    //复制链接
    public function copyLink(){
        $id = myHttp::getId('视频');
        $cur = myDb::getById('Video',$id,'id,name');
        if(!$cur){
            res_api('书籍参数错误');
        }
        $url = mdSpread::getSpreadUrl();
        $short_url = '/Index/Video/info.html?video_id='.$id;
        //$short_url ='/index.html#/info?video_id='.$id;
        $data = [
            'notice' => '温馨提示 : 相对链接只能应用到页面跳转链接中，如轮播图链接等，渠道用户点击后不会跳转到总站',
            'links' => [
                ['title'=>'相对链接','val'=>$short_url],
                ['title'=>'绝对链接','val'=>$url.$short_url]
            ]
        ];
        return $this->fetch('common@public/copyLink',$data);
    }
    
    //新增视频
    public function addVideo(){
        if($this->request->isAjax()){
            $rules = mVideo::$rules;
            unset($rules['id']);
            unset($rules['event']);
            unset($rules['area']);
            unset($rules['cate']);
            $data = myValidate::getData($rules);
            if(isset($data['category'])){
                $data['category'] = ','.implode(',', $data['category']).',';
            }
            if(isset($data['area'])){
                $data['area'] = ','.implode(',', $data['area']).',';
            }
            $data['create_time'] = time();
            $re = myDb::add('Video', $data);
            if($re){
                res_api();
            }else{
                res_api('新增失败，请重试');
            }
        }else{
            $field = 'id,name,cover,summary,money:158,status,free_type:2,url,file_key,is_hot';
            $field .= ',hot_num:0,share_title,share_desc,sort_num:0,area,category';
            $option = mVideo::getVideoRadioList();
            $option['cur'] = myDb::buildArr($field);
            $option['category'] = getMyConfig('video_category');
            $option['area'] = getMyConfig('video_area');
            $option['backUrl'] = my_url('index');
            $this->assign($option);
            $this->assign('js',getJs('video.dovideo','admin',['date'=>time()]));
            return $this->fetch('common@doVideo');
        }
    }
    
    //编辑视频
    public function doVideo(){
        if($this->request->isAjax()){
            $rules = mVideo::$rules;
            unset($rules['event']);
            unset($rules['area']);
            unset($rules['cate']);
            $data = myValidate::getData($rules);
            if(isset($data['category'])){
                $data['category'] = implode(',', $data['category']);
            }
            if(isset($data['area'])){
                $data['area'] = ','.implode(',', $data['area']).',';
            }
            $re = myDb::saveIdData('Video', $data);
            if($re){
            	cache('video_'.$data['id'],null);
                res_api();
            }else{
                res_api('编辑失败，请重试');
            }
        }else{
            $id = myHttp::getId('视频');
            $cur = myDb::getById('Video',$id);
            if(!$cur){
                res_api('视频不存在');
            }
            $option = mVideo::getVideoRadioList();
            $option['category'] = getMyConfig('video_category');
            $option['area'] = getMyConfig('video_area');
            $option['cur'] = $cur;
            $option['backUrl'] = my_url('index');
            $this->assign($option);
            $this->assign('js',getJs('video.dovideo','admin',['date'=>time()]));
            return $this->fetch('common@doVideo');
        }
    }
    
    //处理视频事件
    public function doVideoEvent(){
        $rules = [
            'id' =>  ["require|number|gt:0",['require'=>'主键参数错误','number'=>'主键参数不规范',"gt"=>"主键参数不规范"]],
            'event' => ["require|in:on,off,delete",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
        ];
        $data = myValidate::getData($rules);
        if(in_array($data['event'], ['on','off'])){
            switch ($data['event']){
                case 'on':
                    $status = 1;
                    break;
                case 'off':
                    $status = 2;
                    break;
            }
            $re = myDb::setField('Video', [['id','=',$data['id']]], 'status', $status);
            if($re){
                res_api();
            }else{
                res_api('操作失败,请重试');
            }
        }else{
            if($data['event'] === 'delete'){
                $re = mVideo::delete($data['id']);
                if($re){
                    res_api();
                }else{
                    res_api('操作失败,请重试');
                }
            }
        }
    }
    
    //处理视频播放
    public function doPlay(){
        $id = myHttp::getId('视频');
        $cur = myDb::getById('Video',$id,'id,url');
        if(!$cur){
            res_api('视频不存在');
        }
        if(!$cur['url']){
            res_api('视频链接未配置');
        }
        return $this->fetch('common@doPlay',['cur'=>$cur,'js'=>getJs('video.doplay','admin',['date'=>time()])]);
        
    }
    
    //发布区域配置
    public function area(){
        $key = 'video_area';
        if($this->request->isAjax()){
            $data = myValidate::getData(mConfig::$icon_rules, 'area');
            $res = mConfig::saveConfig($key,$data);
            if($res){
                if($data){
                    cache($key,$data,3600);
                }else{
                    cache($key,null);
                }
                res_return();
            }else{
                res_return('保存失败');
            }
        }else{
            $cur = mConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mConfig::addConfig($key, $cur);
                if(!$re){
                    res_return('初始化数据失败，请重试');
                }
            }
            $variable = [
                'title' => '视频发布区域配置',
                'cur' => $cur,
                'backUrl' => my_url('index')
            ];
            $this->assign($variable);
            return $this->fetch('public/area');
        }
    }
    
    //轮播图片配置
    public function banners(){
        $key = 'video_banner';
        if($this->request->isAjax()){
            $data = myValidate::getData(mConfig::$icon_rules, 'src,link');
            $config = [];
            if($data['src']){
                $num = 0;
                foreach ($data['src'] as $k=>$v){
                    $num++;
                    $one = ['src'=>$v];
                    if(!$one['src']){
                        res_return('第'.$num.'张轮播图片未上传');
                    }
                    $one['link'] = $data['link'][$k];
                    $config[] = $one;
                }
            }
            $res = mConfig::saveConfig($key,$config);
            if($res){
                $this->assign('list',$config);
                $html = $this->fetch('block/banners');
                saveBlock($html,$key,'other');
                res_return();
            }else{
                res_return('保存失败');
            }
        }else{
            $cur = mConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mConfig::addConfig($key, $cur);
                if(!$re){
                    res_return('初始化数据失败，请重试');
                }
            }
            $variable = [
                'title' => '视频轮播图配置',
                'cur' => $cur,
                'backUrl' => my_url('index')
            ];
            $this->assign($variable);
            return $this->fetch('public/banners');
        }
    }
    
    //类型配置
    public function category(){
        $key = 'video_category';
        if($this->request->isAjax()){
            $data = mdConfig::getCategoryData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                cache($key,$data);
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            $variable = [
                'cur' => $cur,
                'title' => '视频类型配置',
            ];
            return $this->fetch('common@system/category',$variable);
        }
    }
    
    //底部导航
    public function footer(){
        $key = 'video_footer';
        if($this->request->isAjax()){
            $data = myValidate::getData(mConfig::$icon_rules, 'src,text,link');
            $config = [];
            if($data['src']){
                $num = 0;
                foreach ($data['src'] as $k=>$v){
                    $num++;
                    $one = ['src'=>$v];
                    if(!$one['src']){
                        res_return('第'.$num.'张图标未上传');
                    }
                    $one['link'] = $data['link'][$k];
                    $one['text'] = $data['text'][$k];
                    $config[] = $one;
                }
                if($num > 5){
                    res_return('最多上传5个底部导航');
                }
            }
            $res = mConfig::saveConfig($key,$config);
            if($res){
            	$this->assign('list',$config);
            	$html = $this->fetch('block/footer');
            	saveBlock($html,'video_footer','other');
                res_return();
            }else{
                res_return('保存失败');
            }
        }else{
            $cur = mConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mConfig::addConfig($key,$cur);
                if(!$re){
                    res_return('初始化数据失败，请重试');
                }
            }
            $variable = [
                'title' => '视频底部导航配置',
                'cur' => $cur,
                'backUrl' => my_url('index')
            ];
            $this->assign($variable);
            return $this->fetch('public/icon');
        }
    }
    
    //菜单导航
    public function nav(){
        $key = 'video_nav';
        if($this->request->isAjax()){
            $data = myValidate::getData(mConfig::$icon_rules, 'text,link');
            $config = [];
            if($data['text']){
                $num = 0;
                foreach ($data['text'] as $k=>$v){
                    $num++;
                    $one = ['text'=>$v];
                    if(!$one['text']){
                        res_return('第'.$num.'条标题未填写');
                    }
                    $one['link'] = $data['link'][$k];
                    $config[] = $one;
                }
                if($num > 5){
                    res_return('最多上传5个菜单导航');
                }
            }
            $res = mConfig::saveConfig($key,$config);
            if($res){
                $this->assign('list',$config);
                $html = $this->fetch('block/videoNav');
                saveBlock($html,$key,'other');
                res_return();
            }else{
                res_return('保存失败');
            }
        }else{
            $cur = mConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mConfig::addConfig($key,$cur);
                if(!$re){
                    res_return('初始化数据失败，请重试');
                }
            }
            $variable = [
                'title' => '视频导航菜单配置',
                'cur' => $cur,
                'backUrl' => my_url('index')
            ];
            $this->assign($variable);
            return $this->fetch();
        }
    }
    
    //刷新缓存
    public function refreshCache(){
        //$variable = mVideo::getBlockData();
        //$this->assign($variable);
        //$html = $this->fetch('block/videoContent');
        //saveBlock($html, 'video_content','other');


        //清理视频缓存
        myCache::clearVideoAll();


        res_api();
    }
}