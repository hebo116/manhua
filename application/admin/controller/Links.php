<?php
namespace app\admin\controller;

use site\myCache;
use site\myLinks;
use app\common\model\mdSpread;

class Links extends Common{
	
	//获取链接
	public function index(){
		global $loginId;
		$list = myLinks::getAll();
		$url = mdSpread::getSpreadUrl($loginId);
		$code = myCache::getAgentCode($loginId);
		foreach ($list as &$v){
			foreach ($v['links'] as &$val){
				$val['long_url'] = $url.$val['url'];
			}
		}
		return $this->fetch('common@links/index',['list'=>$list]);
	}
}