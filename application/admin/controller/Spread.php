<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use Endroid\QrCode\QrCode;
use app\common\model\mdSpread;
use site\myCache;

class Spread extends Common{
    
    //推广链接列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['a.status','=',1],['a.channel_id','=',0]],
                'like' => 'keyword:a.name,book_name:a.book_name',
            	'between' => 'between_time:a.create_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdSpread::getSpreadList($where, $pages);
            res_table($res['data'],$res['count']);
        }else{
            
            $get = myHttp::getData('from','get');
            $this->assign('from',$get['from']);
            return $this->fetch('common@spread/index');
        }
    }
    
    //明细
    public function detail(){
    	$sid = myHttp::getId('推广','sid');
    	if($this->request->isAjax()){
    		$where = [['sid','=',$sid]];
    		$pages = myHttp::getPageParams();
    		$res = myDb::getPageList('SpreadChart',$where,'*',$pages);
    		if($res['data']){
    			foreach ($res['data'] as &$v){
    				$v['charge_ratio'] = '0%';
    				if($v['charge_num']){
    					if($v['charge_num'] > $v['total_charge_num']){
    						$v['charge_ratio'] = '100%';
    					}else{
    						$v['charge_ratio'] = (round($v['charge_num']/$v['total_charge_num'],2)*100).'%';
    					}
    				}
    			}
    		}
    		res_table($res['data'],$res['count']);
    	}else{
    		$time = time();
    		$H = date('Y-m-d H',$time);
    		$max_time = strtotime($H.':00:00')+3600;
    		$sec = $max_time - $time;
    		return $this->fetch('common@spread/detail',['sec'=>$sec]);
    	}
    }
    
    
    //生成推广链接
    public function createLink(){
        if($this->request->isAjax()){
        	$rules = mdSpread::getRules();
        	$data = myValidate::getData($rules);
        	$data['channel_id'] = 0;
            mdSpread::doneSpread($data);
        }else{
            $book_id = myHttp::getId('书籍','book_id');
            $book = myCache::getBook($book_id);
            if(!$book){
            	res_error('书籍不存在');
            }
            $options = mdSpread::getOptions($book['id']);
            $field = 'id,name,chapter_number:1,is_sub:1,number:5,cost_money:0,remark';
            $options['cur'] = myDb::buildArr($field);
            $options['book'] = $book;
            $options['is_layer'] = 'yes';
            return $this->fetch('common@spread/createLink',$options);
        }
    }
    
    //编辑链接
    public function doLink(){
        if($this->request->isAjax()){
        	$rules = mdSpread::getRules(0);
        	$data = myValidate::getData($rules);
        	mdSpread::doneSpread($data);
        }else{
            $id = myHttp::getId('推广链接');
            $cur = myDb::getById('Spread',$id);
            if(!$cur){
                res_error('推广链接参数异常');
            }
            $book = myCache::getBook($cur['book_id']);
            if(!$book){
            	res_error('书籍不存在');
            }
            $options = mdSpread::getOptions($cur['book_id']);
            $options['cur'] = $cur;
            $options['book'] = $book;
            $options['is_layer'] = 'no';
            return $this->fetch('common@spread/createLink',$options);
        }
    }
    
    //删除链接
    public function delLink(){
        $id = myHttp::postId('推广链接');
        $re = myDb::setField('Spread',[['id','=',$id]],'status',2);
        if($re !== false){
            res_api();
        }else{
            res_api('删除失败');
        }
    }
    
    //复制链接
    public function copyLink(){
        $id = myHttp::getId('推广链接');
        $cur = myDb::getById('Spread',$id,'id,url,short_url,status');
        if(empty($cur)){
            res_api('推广链接不存在');
        }
        if($cur['status'] != 1){
            res_api('该链接已删除');
        }
        $data = [
            'notice'=>'温馨提示 : 短链接为长链接转化而来，长链接和短链接实际跳转到同一地址',
            'links' => [
                ['title' => '短链接','val' => $cur['short_url']],
                ['title' => '长链接','val' => $cur['url']],
            ]
        ];
        return $this->fetch('common@public/copyLink',$data);
    }
    
    //生成二维码
    public function qrcode(){
        $id = myHttp::getId('推广链接');
        mdSpread::createQrcode($id);
        $variable = ['path'=>'/uploads/qrcode/'.$id.'/'];
        return $this->fetch('common@spread/qrcode',$variable);
    }
    
    //设置成本金额
    public function setCostMoney(){
    	$data = mdSpread::getCostMoneyData();
    	$re = myDb::saveIdData('Spread', $data);
    	$str = $re ? 'ok' : '设置失败';
    	res_api($str);
    }
    
}