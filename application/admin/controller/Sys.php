<?php
namespace app\admin\controller;

use think\Db;
use site\myValidate;
use app\common\model\mdPlan;
use site\myHttp;
use site\myCache;

class Sys extends Common{
	
	//任务记录
	public function index(){
		$time = time();
		$cur_date = date('Y-m-d',$time);
		if($this->request->isAjax()){
			$rules = ['create_date'=>['date',['date'=>'补录数据异常']]];
			$pdate = myValidate::getData($rules,'get');
			if($pdate && $pdate != $cur_date){
				$date = $pdate;
				$h = 23;
				$end_time = strtotime($pdate.' 23:59:59');
			}else{
				$time = time();
				$date = date('Y-m-d',$time);
				$h = date('H',$time);
				$end_time = strtotime($date.' '.$h.':00:00');
				$end_time -= 7200;
			}
			$start_time = strtotime($date);
			$data = [];
			$list = Db::name('PlanRecord')->where('create_date',$date)->order('create_date','desc')->order('hour','desc')->limit(25)->select();
			while ($end_time >= $start_time){
				if(($end_time+3600) < $time){
					$tmpH = date('G',$end_time);
					$tmp_date = date('Y-m-d',$end_time);
					$key = $tmp_date.'~'.$tmpH;
					$data[$key] = ['hour'=>$tmpH,'create_date'=>$tmp_date,'create_time'=>'','status'=>2];
				}
				$end_time -= 3600;
			}
			if($list){
				foreach ($list as $v){
					$cK = $v['create_date'].'~'.$v['hour'];
					if(isset($data[$cK])){
						$data[$cK]['status'] = 1;
						$data[$cK]['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
						$remark = $v['remark'] ? json_decode($v['remark'],true) : '';
						$data[$cK]['is_remark'] = $remark ? 'yes' : 'no';
						$data[$cK]['remark_url'] = $remark ? my_url('remark',['id'=>$v['id']]) : '';
					}
				}
			}
			$data = array_values($data);
			res_table($data);
		}else{
			
			return $this->fetch('common@sys/index',['create_date'=>$cur_date]);
		}
	}
	
	//补发明细
	public function remark(){
		$id = myHttp::getId('日志');
		if($this->request->isAjax()){
			$remark = Db::name('PlanRecord')->where('id',$id)->value('remark');
			$remark = $remark ? json_decode($remark,true) : [];
			if($remark){
				foreach ($remark as &$v){
					$channel = myCache::getChannel($v['channel_id']);
					$v['name'] = $channel ? $channel['name'] : '';
				}
			}
			res_table($remark);
		}else{
			
			return $this->fetch('common@remark');
		}
	}
	
	//再次执行
	public function doAddPlan(){
		$rules = [
			'hour' => ['require|between:0,23',['require'=>'缺少补录数据','between'=>'补录数据异常']],
			'create_date' => ['require|date',['require'=>'缺少补录数据','date'=>'补录数据异常']]
		];
		$data = myValidate::getData($rules);
		$start_time = strtotime($data['create_date'].' '.$data['hour'].':00:00');
		$end_time = strtotime($data['create_date'].' '.$data['hour'].':59:59');
		$res = mdPlan::doSysCount($start_time, $end_time);
		$str = $res ? 'ok' : '执行失败';
		res_api($str);
	}
}