<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use app\common\model\mdMessage;
use site\myValidate;

class Notice extends Common{
	
	//用户消息
	public function index(){
		if($this->request->isAjax()){
			$config = [
				'default' => [],
				'like' => 'keyword:title'
			];
			$pages = myHttp::getPageParams();
			$where = mySearch::getWhere($config);
			$res = myDb::getPageList('Notice', $where, '*', $pages);
			if($res['data']){
				foreach ($res['data'] as &$v){
					$v['content'] = str_replace("\n", '<br />', $v['content']);
					$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
					$v['do_url'] = my_url('doNotice',['id'=>$v['id']]);
				}
			}
			res_table($res['data'],$res['count']);
		}else{
			
			return $this->fetch('common@index');
		}
	}
	
	//新增公告
	public function addNotice(){
		if($this->request->isAjax()){
			$rules = mdMessage::getRules();
			$data = myValidate::getData($rules);
			$data['create_time'] = time();
			$re = myDb::add('Notice', $data);
			if($re){
				res_api();
			}else{
				res_api('新增失败');
			}
		}else{
			$field = 'id,title,type,content';
			$cur = myDb::buildArr($field);
			$variable = [
				'cur' => $cur,
				'backUrl' => url('index')
			];
			return $this->fetch('common@doNotice',$variable);
		}
	}
	
	//编辑公告
	public function doNotice(){
		if($this->request->isAjax()){
			$rules = mdMessage::getRules();
			$data = myValidate::getData($rules);
			$re = myDb::saveIdData('Notice', $data);
			if($re){
				res_api();
			}else{
				res_api('更新失败');
			}
		}else{
			$id = myHttp::getId('公告');
			$cur = myDb::getById('Notice',$id);
			if(!$cur){
				res_api('公告不存在');
			}
			$variable = [
				'cur' => $cur,
				'backUrl' => url('index')
			];
			return $this->fetch('common@doNotice',$variable);
		}
	}
	
	//删除公告
	public function delNotice(){
		$id = myHttp::postId('公告');
		$flag = myDb::delById('Notice', $id);
		if($flag){
			res_api();
		}else{
			res_api('删除失败');
		}
	}
	
}