<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use think\Db;
use site\myCache;
use think\Controller;
use app\common\model\mdPlan;
use app\common\model\mdBook;
use think\Env;
use Zxing\Qrcode\Decoder\ECB;

class Plan extends Controller
{

    /**
     * #统计用户信息 凌晨5点
     * 0 5 * * * /usr/bin/curl localhost/admin/Plan/member.html
     * #统计订单信息 凌晨5点10分
     * 10 5 * * * /usr/bin/curl localhost/admin/Plan/order.html
     * #设置vip超时时间 每分钟
     * * * * * /usr/bin/curl localhost/admin/Plan/setViptime.html
     * #更新站点缓存 每天5点20
     * 20 5 * * * /usr/bin/curl localhost/admin/Plan/refreshCache.html
     * #自动结算 每日凌晨3点
     * 0 3 * * * /usr/bin/curl localhost/admin/Plan/doAutoWithdraw
     * #统计系统数据 每小时59分
     * 59 * * * * /usr/bin/curl localhost/admin/Plan/doSysCount
     */

    public function __construct()
    {
        parent::__construct();
        $res = false;
//		if('localhost' === $_SERVER['HTTP_HOST']){
//			$res = true;
//		}
        if (!$res) {
            //echo '';exit;
        }
    }

    //统计用户信息，凌晨5点执行
    public function member()
    {
        set_time_limit(1800);
        $start_time = strtotime('yesterday');
        $end_time = $start_time + 86399;
        $create_date = date('Y-m-d', $start_time);
        $repeat = Db::name('TaskMember')->where('create_date', '=', $create_date)->value('id');
        if ($repeat) {
            exit;
        }
        $data = [];
        $list = Db::name('Member')->where('create_time', 'between', [$start_time, $end_time])->field('id,channel_id,agent_id,is_charge')->select();
        if ($list) {
            foreach ($list as $v) {
                $temp = ['create_date' => $create_date, 'channel_id' => 0, 'add_num' => 1, 'charge_money' => 0, 'charge_nums' => 0];
                if (!isset($data[0])) {
                    $data[0] = $temp;
                } else {
                    $data[0]['add_num'] += 1;
                }
                $channel_id = $v['channel_id'];
                $agent_id = $v['agent_id'];
                if ($channel_id) {
                    if (!isset($data[$channel_id])) {
                        $data[$channel_id] = $temp;
                        $data[$channel_id]['channel_id'] = $channel_id;
                    } else {
                        $data[$channel_id]['add_num'] += 1;
                    }
                }
                if ($agent_id) {
                    if (!isset($data[$agent_id])) {
                        $data[$agent_id] = $temp;
                        $data[$agent_id]['channel_id'] = $agent_id;
                    } else {
                        $data[$agent_id]['add_num'] += 1;
                    }
                }
                if ($v['is_charge'] == 1) {
                    $order = Db::name('Order')
                        ->where('uid', '=', $v['id'])
                        ->where('status', '=', 2)
                        ->where('create_time', 'between', [$start_time, $end_time])
                        ->field('id,uid,channel_id,agent_id,money,is_count')
                        ->select();
                    if ($order) {
                        $channel_num = $agent_num = 0;
                        $data[0]['charge_nums'] += 1;
                        foreach ($order as $val) {
                            $data[0]['charge_money'] += $val['money'];
                            if ($channel_id > 0 && $channel_id == $val['channel_id'] && $val['is_count'] == 1) {
                                $channel_num = 1;
                                $data[$channel_id]['charge_money'] += $val['money'];
                            }
                            if ($agent_id > 0 && $agent_id == $val['agent_id'] && $val['is_count'] == 1) {
                                $agent_num = 1;
                                $data[$agent_id]['charge_money'] += $val['money'];
                            }
                        }
                        if ($channel_num) {
                            $data[$channel_id]['charge_nums'] += $channel_num;
                        }
                        if ($agent_num) {
                            $data[$agent_id]['charge_nums'] += $agent_num;
                        }
                    }
                }
            }
        }
        if ($data) {
            Db::name('TaskMember')->insertAll($data);
        }

    }

    //统计订单信息，凌晨5点10分执行
    public function order()
    {
        set_time_limit(1800);
        $start_time = strtotime('yesterday');
        $end_time = $start_time + 86399;
        $create_date = date('Y-m-d', $start_time);
        $repeat = Db::name('TaskOrder')->where('create_date', '=', $create_date)->value('id');
        if ($repeat) {
            exit;
        }
        $data = [];
        $list = Db::name('Order')->where('create_time', 'between', [$start_time, $end_time])->field('id,type,channel_id,agent_id,package,uid,status,money,is_count')->select();
        if ($list) {
            foreach ($list as $v) {
                $temp = ['create_date' => $create_date, 'channel_id' => 0, 'n_pay' => 0, 'n_notpay' => 0, 'n_money' => 0, 'n_user' => 0, 'n_rate' => 0, 'p_pay' => 0, 'p_notpay' => 0, 'p_money' => 0, 'p_user' => 0, 'p_rate' => 0, 'total_money' => 0, 'type1_money' => 0, 'type2_money' => 0, 'puids' => [], 'nuids' => []];
                if (!isset($data[0])) {
                    $data[0] = $temp;
                }
                $channel_id = $v['channel_id'];
                if ($channel_id) {
                    if (!isset($data[$channel_id])) {
                        $data[$channel_id] = $temp;
                        $data[$channel_id]['channel_id'] = $channel_id;
                    }
                }
                $agent_id = $v['agent_id'];
                if ($agent_id) {
                    if (!isset($data[$agent_id])) {
                        $data[$agent_id] = $temp;
                        $data[$agent_id]['channel_id'] = $agent_id;
                    }
                }
                if ($v['status'] == 2) {
                    if ($v['package'] > 0) {
                        $data[0]['p_pay'] += 1;
                        $data[0]['p_money'] += $v['money'];
                        if (!in_array($v['uid'], $data[0]['puids'])) {
                            $data[0]['p_user'] += 1;
                            $data[0]['puids'][] = $v['uid'];
                        }
                        if ($channel_id && $v['is_count'] == 1) {
                            $data[$channel_id]['p_pay'] += 1;
                            $data[$channel_id]['p_money'] += $v['money'];
                            if (!in_array($v['uid'], $data[$channel_id]['puids'])) {
                                $data[$channel_id]['p_user'] += 1;
                                $data[$channel_id]['puids'][] = $v['uid'];
                            }
                        }
                        if ($agent_id && $v['is_count'] == 1) {
                            $data[$agent_id]['p_pay'] += 1;
                            $data[$agent_id]['p_money'] += $v['money'];
                            if (!in_array($v['uid'], $data[$agent_id]['puids'])) {
                                $data[$agent_id]['p_user'] += 1;
                                $data[$agent_id]['puids'][] = $v['uid'];
                            }
                        }
                    } else {
                        $data[0]['n_pay'] += 1;
                        $data[0]['n_money'] += $v['money'];
                        if (!in_array($v['uid'], $data[0]['nuids'])) {
                            $data[0]['n_user'] += 1;
                            $data[0]['nuids'][] = $v['uid'];
                        }
                        if ($channel_id && $v['is_count'] == 1) {
                            $data[$channel_id]['n_pay'] += 1;
                            $data[$channel_id]['n_money'] += $v['money'];
                            if (!in_array($v['uid'], $data[$channel_id]['nuids'])) {
                                $data[$channel_id]['n_user'] += 1;
                                $data[$channel_id]['nuids'][] = $v['uid'];
                            }
                        }
                        if ($agent_id && $v['is_count'] == 1) {
                            $data[$agent_id]['n_pay'] += 1;
                            $data[$agent_id]['n_money'] += $v['money'];
                            if (!in_array($v['uid'], $data[$agent_id]['nuids'])) {
                                $data[$agent_id]['n_user'] += 1;
                                $data[$agent_id]['nuids'][] = $v['uid'];
                            }
                        }
                    }
                    $type_key = in_array($v['type'], [1, 2]) ? 'type' . $v['type'] . '_money' : 'type1_money';
                    $data[0][$type_key] += $v['money'];
                    $data[0]['total_money'] += $v['money'];
                    if ($channel_id && $v['is_count'] == 1) {
                        $data[$channel_id]['total_money'] += $v['money'];
                        $data[$channel_id][$type_key] += $v['money'];
                    }
                    if ($agent_id && $v['is_count'] == 1) {
                        $data[$agent_id]['total_money'] += $v['money'];
                        $data[$agent_id][$type_key] += $v['money'];
                    }
                } else {
                    if ($v['package'] > 0) {
                        $data[0]['p_notpay'] += 1;
                        if ($channel_id) {
                            $data[$channel_id]['p_notpay'] += 1;
                        }
                        if ($agent_id) {
                            $data[$agent_id]['p_notpay'] += 1;
                        }
                    } else {
                        $data[0]['n_notpay'] += 1;
                        if ($channel_id) {
                            $data[$channel_id]['n_notpay'] += 1;
                        }
                        if ($agent_id) {
                            $data[$agent_id]['n_notpay'] += 1;
                        }
                    }
                }
            }
        }
        if ($data) {
            foreach ($data as &$val) {
                unset($val['puids']);
                unset($val['nuids']);
                if ($val['n_notpay'] || $val['n_pay']) {
                    $val['n_rate'] = round($val['n_pay'] / ($val['n_pay'] + $val['n_notpay']), 2) * 100;
                }
                if ($val['p_notpay'] || $val['p_pay']) {
                    $val['p_rate'] = round($val['p_pay'] / ($val['p_pay'] + $val['p_notpay']), 2) * 100;
                }
            }
            Db::name('TaskOrder')->insertAll($data);
        }
    }

    //vip超时设置
    public function setVipTime()
    {
        $time = time();
        $over_ids = Db::name('Member')->where('viptime', '>', 1)->where('viptime', '<', $time)->limit(100)->column('id');
        if (!empty($over_ids)) {
            Db::name('Member')->where('id', 'in', $over_ids)->setField('viptime', 0);
            foreach ($over_ids as $v) {
                $key = 'member_info_' . $v;
                cache($key, null);
            }
        }
    }

    //刷新站点缓存
    public function refreshCache()
    {
        $block_category = 1;//漫画
        $beastBook = mdBook::getCacheBookData(1, 6);
        $html = $this->fetch('common@block/bookItem3', ['list' => $beastBook, 'title' => '大家都在看', 'type' => 1, 'is_more' => 1, 'is_load' => 1, 'category' => $block_category]);
        saveBlock($html, 'index_content_beast', 'other');

        $newBook = mdBook::getCacheBookData(2, 3);
        $html = $this->fetch('common@block/bookItem3', ['list' => $newBook, 'title' => '每日上新', 'type' => 2, 'is_more' => 1, 'is_load' => 2, 'category' => $block_category]);
        saveBlock($html, 'index_content_news', 'other');

        $freeBook = mdBook::getCacheBookData(3, 6);
        $html = $this->fetch('common@block/bookItem1', ['list' => $freeBook, 'title' => '每周限免', 'type' => 3, 'is_load' => 2, 'category' => $block_category]);
        saveBlock($html, 'index_content_free', 'other');

        $recommendBook = mdBook::getCacheBookData(4, 6);
        $html = $this->fetch('common@block/bookItem3', ['list' => $recommendBook, 'title' => '重磅推薦', 'type' => 4, 'is_more' => 1, 'is_load' => 2, 'category' => $block_category]);
        saveBlock($html, 'index_content_recommend', 'other');

        $allBook = mdBook::getCacheBookData(5, 6);
        $html = $this->fetch('common@block/bookItem1', ['list' => $allBook, 'title' => '猜你喜歡', 'type' => 5, 'is_load' => 1, 'category' => $block_category]);
        saveBlock($html, 'index_content_all', 'other');

        $rankBook = mdBook::getRankBookData();
        $html = $this->fetch('common@block/indexrank', ['list' => $rankBook, 'category' => $block_category]);
        saveBlock($html, 'index_content_rank', 'other');
        $html = $this->fetch('common@block/rankall', ['list' => $rankBook, 'category' => $block_category]);
        saveBlock($html, 'book_rank_all', 'other');

        $block_category = 2;//小说
        $beastBook = mdBook::getCacheBookData(1, 6, 0, $block_category);
        $html = $this->fetch('common@block/bookItem3', ['list' => $beastBook, 'title' => '大家都在看', 'type' => 1, 'is_more' => 1, 'is_load' => 1, 'category' => $block_category]);
        saveBlock($html, 'index_content_beast', 'novel');

        $newBook = mdBook::getCacheBookData(2, 3, 0, $block_category);
        $html = $this->fetch('common@block/bookItem3', ['list' => $newBook, 'title' => '每日上新', 'type' => 2, 'is_more' => 1, 'is_load' => 2, 'category' => $block_category]);
        saveBlock($html, 'index_content_news', 'novel');

        $freeBook = mdBook::getCacheBookData(3, 6, 0, $block_category);
        $html = $this->fetch('common@block/bookItem1', ['list' => $freeBook, 'title' => '每周限免', 'type' => 3, 'is_load' => 2, 'category' => $block_category]);
        saveBlock($html, 'index_content_free', 'novel');

        $recommendBook = mdBook::getCacheBookData(4, 6, 0, $block_category);
        $html = $this->fetch('common@block/bookItem3', ['list' => $recommendBook, 'title' => '重磅推薦', 'type' => 4, 'is_more' => 1, 'is_load' => 2, 'category' => $block_category]);
        saveBlock($html, 'index_content_recommend', 'novel');

        $allBook = mdBook::getCacheBookData(5, 6, 0, $block_category);
        $html = $this->fetch('common@block/bookItem1', ['list' => $allBook, 'title' => '猜你喜歡', 'type' => 5, 'is_load' => 1, 'category' => $block_category]);
        saveBlock($html, 'index_content_all', 'novel');

        $rankBook = mdBook::getRankBookData($block_category);
        $html = $this->fetch('common@block/indexrank', ['list' => $rankBook, 'category' => $block_category]);
        saveBlock($html, 'index_content_rank', 'novel');
        $html = $this->fetch('common@block/rankall', ['list' => $rankBook, 'category' => $block_category]);
        saveBlock($html, 'book_rank_all', 'novel');
    }

    //自动结算
    public function doAutoWithdraw()
    {
        $date = date('Y-m-d');
        $list = Db::name('ChannelChart')->where('create_date', '<', $date)->where('status', 2)->where('channel_id', '>', 0)->field('id,channel_id,charge_money,charge_num,income_money,create_date')->select();
        if ($list) {
            $accounts = [];
            foreach ($list as $v) {
                $data = [];
                if ($v['income_money']) {
                    $channel_id = $v['channel_id'];
                    $channel = myCache::getChannel($channel_id);
                    if ($channel) {
                        $data = [
                            'channel_id' => $channel_id,
                            'to_channel_id' => $channel['parent_id'],
                            'money' => $v['income_money'],
                            'charge_money' => $v['charge_money'],
                            'charge_num' => $v['charge_num'],
                            'chart_id' => $v['id'],
                            'cur_date' => $v['create_date'],
                            'create_time' => time()
                        ];
                        $ak = 'agent_' . $channel_id;
                        if (!isset($accounts[$ak])) {
                            if ($channel['bank_no']) {
                                $accounts[$ak] = [
                                    'bank_no' => $channel['bank_no'],
                                    'bank_name' => $channel['bank_name'],
                                    'bank_user' => $channel['bank_user']
                                ];
                            }
                        }
                        if (!array_key_exists($ak, $accounts)) {
                            continue;
                        }
                        $data['bank_info'] = json_encode($accounts[$ak]);
                        Db::startTrans();
                        $flag = false;
                        $re = Db::name('ChannelChart')->where('id', $v['id'])->setField('status', 1);
                        if ($re) {
                            $res = Db::name('Withdraw')->insert($data);
                            if ($res) {
                                $chartData = [
                                    'withdraw_total' => Db::raw('withdraw_total+' . $data['money']),
                                    'withdraw_wait' => Db::raw('withdraw_wait+' . $data['money'])
                                ];
                                $chartRes = Db::name('ChannelMoney')->where('channel_id', $v['channel_id'])->update($chartData);
                                if ($chartRes) {
                                    $flag = true;
                                }
                            }
                        }
                        if ($flag) {
                            myCache::rmChannelAmount($channel_id);
                            Db::commit();
                        } else {
                            Db::rollback();
                        }
                    }
                } else {
                    Db::name('ChannelChart')->where('id', $v['id'])->setField('status', 1);
                }
            }
        }
        die('ok');
    }


    //执行结算统计
    public function doSysCount()
    {
        $time = time();
        $hour = date('H', $time);
        $date = date('Y-m-d', $time);
        $tmpDate = $date . ' ' . $hour . ':00:00';
        $end_time = strtotime($tmpDate) - 1;
        $start_time = $end_time - 3599;
        mdPlan::doSysCount($start_time, $end_time);
    }

    //采集歪歪漫画章节内容
    public function gather()
    {
        set_time_limit(1800);
        $book_id = input('book_id');
        //章节列表url
        $chapter_list_url = 'https://6mmh.com/home/api/chapter_list/tp/' . $book_id . '-1-1-10000';
        $data = myHttp::doGet($chapter_list_url);
        $chapter_list = isset($data['result']['list']) ? $data['result']['list'] : [];
        $chapter_list_ids = [];
        foreach ($chapter_list as $chapter) {
            $chapter_list_ids[] = $chapter['id'];
        }
        if (count($chapter_list_ids) == 0) {
            echo '没有这本书';
            die;
        }
        //查询更新对照表
        $sync = Db::name('BookCatYy')->where(['sync_id' => $book_id])->find();
        if ($sync && $sync['last_num'] >= count($chapter_list_ids)) {
            echo '已经是最新章节';
            die;
        }
        $last_num = isset($sync['last_num']) ? $sync['last_num'] : 0;

        foreach ($chapter_list_ids as $k => $id) {
            if ($k + 1 <= $last_num) {
                continue;
            }
            //章节url
            $chapter_url = 'https://6mmh.com/home/book/capter/id/' . $id;
            $content = myHttp::doGet($chapter_url, ["cookie: PHPSESSID=ld2t1r8g8oe5qplbnjr18keb4k; Hm_lvt_104b314cf29580d630696ae975988231=1658643520; goback=%2Fhome%2Fuser%2Findex.html; reglimit=1658649566; 20220724setinfo=1; user-day-notice=20220724; goback_t=1658650726; userid=833022; password=1eb14ce2c9d5d9becefff39c622c2cd6; username=18381689136; 20220725setinfo=1; day-once-notice=202207252; Hm_lpvt_104b314cf29580d630696ae975988231=1658693297"], 'string');
            //获取漫画名
            $preg = '/<span class=\"name\">(.*?)<\/span>/ism';
            preg_match_all($preg, $content, $match);
            $name = $match[1][0];

            $path = env('root_path');
            $path .= 'content/' . $name . '_' . $book_id . '/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            $number = $k + 1;
            //更新漫画更新对照表
            if ($sync) {
                Db::name('BookCatYy')->where(['sync_id' => $book_id])->update(['last_num' => $number]);
            } else {
                Db::name('BookCatYy')->insert(['book_name' => $name, 'sync_id' => $book_id, 'last_num' => $number]);
                $sync = true;
            }

            file_put_contents($path . $number . '.html', $content);
        }
    }

    //整理章节图片1
    public function dealtPic()
    {
        set_time_limit(3600);
        //列出content_tmp/tmpx年x月x日下的书籍目录名称
        $path = ENV('root_path');
        $put_path = $path . 'content_tmp/chapter20220725/';//图片写入路径
        $book_source_path = $path . 'content_tmp/tmp20220725/';//书籍源文件路径
        $book_dirs = scandir($book_source_path, 1);
        foreach ($book_dirs as $key => $dir_name) {
            if ($dir_name == '.' || $dir_name == '..') {
                unset($book_dirs[$key]);
                continue;
            }
            //列出书籍下的所有章节文件名称
            $book_path = $book_source_path . $dir_name;
            $chapter_files = scandir($book_path, 1);
            foreach ($chapter_files as $k => $chapter_file) {
                if ($chapter_file == '.' || $chapter_file == '..') {
                    unset($chapter_files[$k]);
                    continue;
                }
                $chapter_path = $book_path . '/' . $chapter_file;//章节文件路径
                $html = file_get_contents($chapter_path);//html源文件
                //正则匹配章节内容图片
                $preg = '/<div class=\"reader-cartoon-image loaded lazy-load img-loading\">(.*?)<\/div>/ism';
                preg_match_all($preg, $html, $match);
                //echo $chapter_path;
                if (isset($match[1]) && !empty($match[1])) {
                    //判断书籍的章节目录是否存在
                    $chapter_dir = str_replace('.html', '', $chapter_file);
                    $new_put_path = $put_path . $dir_name . '/' . $chapter_dir;
                    if (!is_dir($new_put_path)) {
                        mkdir($new_put_path, 0777, true);
                    }
                    echo "----------更新" . $dir_name . '第' . $chapter_dir . "话------------\n";
                    //判断$new_put_path中的图片数量是否等于抓取的图片数量
                    if (is_dir($new_put_path) && (count(scandir($new_put_path, 1)) - 2) >= count($match[1])) {
                        continue;
                    }
                    //匹配出漫画图片网络地址
                    foreach ($match[1] as $ks => $html2) {
                        $preg2 = '/<img onerror=\"backjpg\(this\)\" data-jpg=\"(.*?)"/ism';
                        preg_match_all($preg2, $html2, $match2);
                        //网络图片保存到本地
                        //判断图片是否存在
                        if (is_file($new_put_path . '/' . ($ks + 1) . '.jpg')) {
                            continue;
                        }
                        $img_data = myHttp::doGetHTTPS($match2[1][0], ["cookie: PHPSESSID=ld2t1r8g8oe5qplbnjr18keb4k; Hm_lvt_104b314cf29580d630696ae975988231=1658643520; goback=%2Fhome%2Fuser%2Findex.html; reglimit=1658649566; 20220724setinfo=1; user-day-notice=20220724; goback_t=1658650726; userid=833022; password=1eb14ce2c9d5d9becefff39c622c2cd6; username=18381689136; 20220725setinfo=1; day-once-notice=202207252; Hm_lpvt_104b314cf29580d630696ae975988231=1658693297"], 'string');
                        file_put_contents($new_put_path . '/' . ($ks + 1) . '.jpg', $img_data);

                        //$img_data = file_get_contents($match2[1][0]);
                        //file_put_contents($new_put_path.'/'.($ks+1).'.jpg',$img_data);
                    }
                }
            }
        }
    }

    //整理章节图片(接收参数xxxx年xx月xx日；如：20220726)
    public function dealtPicParam()
    {
        $date = input('date');
        set_time_limit(36000);
        //列出content_tmp/tmpx年x月x日下的书籍目录名称
        $path = ENV('root_path');
        $put_path = $path . 'content_tmp/chapter' . $date . '/';//图片写入路径
        $book_source_path = $path . 'content_tmp/tmp' . $date . '/';//书籍源文件路径
        $book_dirs = scandir($book_source_path, 1);
        foreach ($book_dirs as $key => $dir_name) {
            if ($dir_name == '.' || $dir_name == '..') {
                unset($book_dirs[$key]);
                continue;
            }
            //列出书籍下的所有章节文件名称
            $book_path = $book_source_path . $dir_name;
            $chapter_files = scandir($book_path, 1);
            foreach ($chapter_files as $kv => $vv) {
                $chapter_files[$kv] = str_replace('.html', '', $vv);
            }
            asort($chapter_files);
            foreach ($chapter_files as $k => $chapter_file) {
                if ($chapter_file == '.' || $chapter_file == '..') {
                    unset($chapter_files[$k]);
                    continue;
                }
                $chapter_path = $book_path . '/' . $chapter_file . '.html';//章节文件路径
                $html = file_get_contents($chapter_path);//html源文件
                //正则匹配章节内容图片
                $preg = '/<div class=\"reader-cartoon-image loaded lazy-load img-loading\">(.*?)<\/div>/ism';
                preg_match_all($preg, $html, $match);
                //echo $chapter_path;
                if (isset($match[1]) && !empty($match[1])) {
                    //判断书籍的章节目录是否存在
                    $chapter_dir = str_replace('.html', '', $chapter_file);
                    $new_put_path = $put_path . $dir_name . '/' . $chapter_dir;
                    if (!is_dir($new_put_path)) {
                        mkdir($new_put_path, 0777, true);
                    }
                    echo "----------更新" . $dir_name . '第' . $chapter_dir . "话------------\n";
                    //判断$new_put_path中的图片数量是否等于抓取的图片数量
                    if (is_dir($new_put_path) && (count(scandir($new_put_path, 1)) - 2) >= count($match[1])) {
                        continue;
                    }
                    //匹配出漫画图片网络地址
                    foreach ($match[1] as $ks => $html2) {
                        $preg2 = '/<img onerror=\"backjpg\(this\)\" data-jpg=\"(.*?)"/ism';
                        preg_match_all($preg2, $html2, $match2);
                        //网络图片保存到本地
                        //判断图片是否存在
                        if (is_file($new_put_path . '/' . ($ks + 1) . '.jpg')) {
                            continue;
                        }
                        if ($match2[1][0] == 'https://yy.26mh.top/imagesw/waimanwu1.png') {
                            $match2[1][0] = 'https://yy.26mh.top/imagesw/ds/book/4711/16-97010/173_7BEC44A9962E0638FD3A0976203F31E8.jpg';
                        }
                        if ($match2[1][0] == 'https://toon-g.toomics.com/toon-g/DcjBCoAgDADQP2qbU6aH6FOGWARROtIO/X2944PHzpZXKK2OrY4ODkmQidXu/ALjHw4VWckphchJJJBPPvrpsH3pI1824wc=') {
                            $match2[1][0] = 'https://yy.26mh.top/imagesw/wm/books/4670/108237/20200331073425a455zbko2gk.jpg';
                        }

                        $header = [
                            //'Content-Type: application/json',
                            'Accept: */*',
                            'Connection: Keep-Alive',
                            "cookie: PHPSESSID=ld2t1r8g8oe5qplbnjr18keb4k; Hm_lvt_104b314cf29580d630696ae975988231=1658643520; goback=%2Fhome%2Fuser%2Findex.html; reglimit=1658649566; 20220724setinfo=1; user-day-notice=20220724; goback_t=1658650726; userid=833022; password=1eb14ce2c9d5d9becefff39c622c2cd6; username=18381689136; 20220725setinfo=1; day-once-notice=202207252; Hm_lpvt_104b314cf29580d630696ae975988231=1658693297",
                            "referer: https://wmh6.com/home/book/capter/id/29242",
                        ];

                        $img_data = myHttp::doGet($match2[1][0], $header, 'string');
                        file_put_contents($new_put_path . '/' . ($ks + 1) . '.jpg', $img_data);

                        /*
                        $stream_opts = ["ssl" => ["verify_peer"=>false, "verify_peer_name"=>false,]];
                        $img_data = file_get_contents($match2[1][0],false,stream_context_create($stream_opts));
                        file_put_contents($new_put_path.'/'.($ks+1).'.jpg',$img_data);
                        */
                        echo ">>>>>>>---" . ($ks + 1) . '.jpg' . "写入成功--------\n";
                    }
                }
            }
        }
    }

    //抓取YY文章章节图片路径
    public function dealPicYY()
    {
        $date = input('date');
        set_time_limit(36000);
        //列出content_tmp/tmpx年x月x日下的书籍目录名称
        $path = ENV('root_path');
        $book_source_path = $path . 'content_yy/tmp' . $date;//书籍源文件路径
        $book_dirs = scandir($book_source_path, 1);
        /*
        $book_dirs = [
            //'韩道修_432',
            //'重考生_516',
            //'重罪之岛_346',
            //'逐步靠近_347',
            //'继母_297',
            //'海之恋_464',
            //'大声说爱我_344',
            '大叔_322',
            '匠人_467',
            'Missing9_446',
            ];
        */
        foreach ($book_dirs as $key => $dir_name) {
            if ($dir_name == '.' || $dir_name == '..') {
                unset($book_dirs[$key]);
                continue;
            }
            $sub = strripos($dir_name, '_');
            $book_name = substr($dir_name, 0, $sub);
            $db = Db::connect('mysql://root:H[n9utxHW9GYFGnR@158.247.203.240:3306/newbook#utf8');
            $book_id = $db->table('sy_book')->where(['name' => $book_name])->value('id');
            //列出源书籍目录下的章节列表
            $source_book_path = $book_source_path . '/' . $dir_name;
            $chapter_files = scandir($source_book_path, 1);
            foreach ($chapter_files as $kv => $file) {
                if ($file == '.' || $file == '..') {
                    unset($chapter_files[$kv]);
                    continue;
                }
                $chapter_files[$kv] = str_replace('.html', '', $file);
            }
            asort($chapter_files);
            foreach ($chapter_files as $kk => $chapter_file) {
                $chapter_path = $source_book_path . '/' . $chapter_file . '.html';//章节文件路径
                $html = file_get_contents($chapter_path);//html源文件
                //正则匹配章节内容图片
                $preg = '/<div class=\"reader-cartoon-image loaded lazy-load img-loading\">(.*?)<\/div>/ism';
                preg_match_all($preg, $html, $match);
                if (isset($match[1]) && !empty($match[1])) {
                    //匹配出漫画图片网络地址
                    $yy_files = [];
                    foreach ($match[1] as $ks => $html2) {
                        $preg2 = '/<img onerror=\"backjpg\(this\)\" data-jpg=\"(.*?)"/ism';
                        preg_match_all($preg2, $html2, $match2);
                        $yy_files[] = $match2[1][0];
                    }
                    //$db = Db::connect('mysql://newbook:Wnhjzfcri5wErtHC@141.164.38.163:3306/newbook#utf8');
                    $db = Db::connect('mysql://root:H[n9utxHW9GYFGnR@158.247.203.240:3306/newbook#utf8');
                    $res = $db->table('sy_book_chapter')
                        ->where(['book_id' => $book_id, 'number' => $chapter_file])
                        ->update(['yy_files' => json_encode($yy_files)]);
                    echo "-----------" . $dir_name . "第" . $chapter_file . "话抓取完成------------\n";
                }
            }
        }

    }

    //新增文章(抓取yy文章信息)
    public function addBook()
    {
        $id = input('id');
        //$url = 'D:\book.html';
        $url = 'https://6mmh.com/home/book/index/id/'.$id;
        $html = file_get_contents($url);
        //curl-header头
        $header = [
            //'Content-Type: application/json',
            'Accept: */*',
            'Connection: Keep-Alive',
            "cookie: PHPSESSID=ld2t1r8g8oe5qplbnjr18keb4k; Hm_lvt_104b314cf29580d630696ae975988231=1658643520; goback=%2Fhome%2Fuser%2Findex.html; reglimit=1658649566; 20220724setinfo=1; user-day-notice=20220724; goback_t=1658650726; userid=833022; password=1eb14ce2c9d5d9becefff39c622c2cd6; username=18381689136; 20220725setinfo=1; day-once-notice=202207252; Hm_lpvt_104b314cf29580d630696ae975988231=1658693297",
            "referer: https://wmh6.com/home/book/capter/id/29242",
        ];

        //获取书籍名称
        $preg = '/<div class=\"title\">(.*?)<\/div>/ism';
        preg_match_all($preg, $html, $match);
        $book_name = $match[1][0];

        //连载or完结
        preg_match_all('/<span class="status" data-status="serialized">/ism', $html, $match_serialized);
        preg_match_all('/<span class="status" data-status="finish">/ism', $html, $match_finish);
        $over_type = 2;
        if (count($match_serialized[0]) >= 3) {
            $over_type = 1;
        }
        if (count($match_finish[0]) >= 3) {
            $over_type = 2;
        }

        //$db = Db::connect('mysql://newbook:Wnhjzfcri5wErtHC@141.164.38.163:3306/newbook#utf8');
        $db = Db::connect('mysql://root:H[n9utxHW9GYFGnR@158.247.203.240:3306/newbook#utf8');
        if ($db->table('sy_book')->where(['name' => $book_name])->value('name')) {
            echo "-----" . $book_name . "已存在------\n";
            die;
        }

        //作者
        $preg = '/href="\/home\/index\/search\/k\/(.*?)">/ism';
        preg_match_all($preg, $html, $match);
        $author = $match[1][0];
        //类型
        $preg = '/<small><label>类型: <\/label>(.*?)<\/small>/ism';
        preg_match_all($preg, $html, $match);
        $cate = ',' . $match[1][0] . ',';
        //简介
        $preg = '/<p class="book-desc">(.*?)<\/p>/ism';
        preg_match_all($preg, $html, $match);
        $summary = $match[1][0];
        //todo:封面图
        //http://comic.op1234.xyz/uploads/img/20220726/6eb228c148717ff516be6069401ea565.jpg
        $path = ENV('root_path') . 'public/uploads/img/' . date('Ymd');
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $preg = '/<img alt="" webp-src="(.*?)">/ism';
        preg_match_all($preg, $html, $match);

        //$img_data = file_get_contents($match[1][0]);
        $img_data = myHttp::doGet($match[1][0], $header, 'string');

        $put_name = md5(microtime() . mt_rand(10000, 99999)) . '.jpg';
        $new_path = $path . '/' . $put_name;
        file_put_contents($new_path, $img_data);
        $cover = 'https://api.qqcmh.click/uploads/img/' . date('Ymd') . '/' . $put_name;
        //todo:详情图
        //<img webp-src="https://yy.26mh.top/imagesw/xm/88129/cover/ht_16331721335543.jpg" alt="">
        $preg = '/<img webp-src="(.*?)" alt="">/ism';
        preg_match_all($preg, $html, $match);
        //$img_data = file_get_contents($match[1][0]);
        $img_data = myHttp::doGet($match[1][0], $header, 'string');
        $put_name = md5(microtime() . mt_rand(10000, 99999)) . '.jpg';
        $new_path = $path . '/' . $put_name;
        file_put_contents($new_path, $img_data);
        $detail_img = 'https://api.qqcmh.click/uploads/img/' . date('Ymd') . '/' . $put_name;

        $data = [
            'type' => 1,
            'name' => $book_name,
            'author' => $author,
            'cover' => $cover,
            'detail_img' => $detail_img,
            'summary' => $summary,
            'category' => $cate,
            'free_type' => 2,
            'over_type' => $over_type,
            'free_chapter' => 2,
            'money' => 48,
            'hot_num' => mt_rand(101111, 101555),
            'star' => bcdiv(mt_rand(881, 999), 100, 2),
            'status' => 2,
            'update_date' => date('Y-m-d'),
            'create_time' => time()
        ];
        $db->table('sy_book')->insert($data);
        echo "-----" . $book_name . "发布成功-----\n";
    }

    public function test(){
        $db = Db::connect('mysql://root:H[n9utxHW9GYFGnR@158.247.203.240:3306/newbook#utf8');
        dump($db->table('sy_ad')->where('id',1)->value("name"));
    }

    public function downEmoji(){
        $url = "https://aic333.com/subtlechat/user/getFixedEmotionsList";
        $path = ENV('root_path');
        $save_path = $path.'emoji';

        $header = [
            //'Content-Type: application/json',
            'Accept: */*',
            'Connection: Keep-Alive',
            "cookie: PHPSESSID=ld2t1r8g8oe5qplbnjr18keb4k; Hm_lvt_104b314cf29580d630696ae975988231=1658643520; goback=%2Fhome%2Fuser%2Findex.html; reglimit=1658649566; 20220724setinfo=1; user-day-notice=20220724; goback_t=1658650726; userid=833022; password=1eb14ce2c9d5d9becefff39c622c2cd6; username=18381689136; 20220725setinfo=1; day-once-notice=202207252; Hm_lpvt_104b314cf29580d630696ae975988231=1658693297",
            //"referer: https://wmh6.com/home/book/capter/id/29242",
        ];

        $data = myHttp::doGet($url)['obj'];

        foreach ($data as $k=>$v){
            $dir = $save_path.'/'.$v['part_id'];
            if (!is_dir($dir)){
                mkdir($dir,0777,true);
            }

            if (is_file($dir . '/' . $v['id'] . '.gif')) {
                continue;
            }

            $img_data = myHttp::doGet($v['emotion_profile'], $header, 'string');
            file_put_contents($dir . '/' . $v['id'] . '.gif', $img_data);
        }

    }
}