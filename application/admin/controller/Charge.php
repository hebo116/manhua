<?php
namespace app\admin\controller;

use site\myValidate;
use app\common\model\mdCharge;
use site\myDb;
use site\myHttp;
use site\myCache;

class Charge extends Common{
	
	//充值档位配置
	public function index(){
		if($this->request->isAjax()){
			$status = myValidate::getData(['status'=>['in:1,2',['in'=>'未指定该状态']]],'get');
			$where = [];
			if($status){
				$where[] = ['status','=',$status];
			}
			$list = myDb::getList('Charge', $where,'*',['sort_num'=>'desc']);
			if($list){
				foreach ($list as &$v){
					$v['content'] = json_decode($v['content'],true);
					$v['do_url'] = my_url('doCharge',['id'=>$v['id']]);
				}
			}
			res_table($list);
		}else{
			
			return $this->fetch('common@charge/index');
		}
	}
	
	//新增套餐
	public function addCharge(){
		if($this->request->isAjax()){
			$data = mdCharge::getData();
			$res = mdCharge::doneCharge($data);
			if($res){
				res_api();
			}else{
				res_api('新增失败');
			}
		}else{
			$field = 'id,money,type:1,coin,send_coin,vip_type,status,pay_times:0';
			$cur = myDb::buildArr($field);
			$options = mdCharge::getOptions();
			$options['cur'] = $cur;
			return $this->fetch('common@charge/doCharge',$options);
		}
	}
	
	//更新套餐
	public function doCharge(){
		if($this->request->isAjax()){
			$data = mdCharge::getData(0);
			$res = mdCharge::doneCharge($data);
			if($res){
				res_api();
			}else{
				res_api('更新失败');
			}
		}else{
			$id = myHttp::getId('套餐');
			$cur = myDb::getById('Charge', $id);
			if(!$cur){
				res_error('套餐不存在');
			}
			$content = json_decode($cur['content'],true);
			$cur = array_merge($cur,$content);
			unset($cur['content']);
			$options = mdCharge::getOptions();
			$options['cur'] = $cur;
			return $this->fetch('common@charge/doCharge',$options);
		}
	}
	
	//处理充值套餐事件
	public function doChargeEvent(){
		$data = mdCharge::getEventData();
		$cur = myDb::getById('Charge', $data['id'],'id,money');
		if(!$cur){
			res_api('非法操作');
		}
		switch ($data['event']){
			case 'on':
			case 'off':
				$value = $data['event'] === 'on' ? 1 : 2;
				$re = myDb::setField('Charge', [['id','=',$cur['id']]], 'status', $value);
				break;
			case 'sethot':
				$re = mdCharge::setField($cur['id'],'is_hot');
				break;
			case 'setcheck':
				$re = mdCharge::setField($cur['id'],'is_check');
				break;
			case 'delete':
				$re = myDb::delById('Charge',$cur['id']);
				break;
			case 'sortUp':
			case 'sortDown':
				$re = mdCharge::doSort($data);
				break;
		}
		if($re){
			myCache::rmSiteCharge();
			res_api();
		}else{
			res_api('操作失败,请重试');
		}
	}
}