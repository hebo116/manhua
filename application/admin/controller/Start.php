<?php
namespace app\admin\controller;

use think\Db;
use think\Controller;

class Start extends Controller{
	
	//初始化数据
	public function clearData(){
        die;
		if(true){
			$list = [
					['name'=>'book'],
					['name'=>'book_chapter'],
					['name'=>'book_free'],
					['name'=>'book_sub'],
					['name'=>'channel'],
					['name'=>'channel_chart'],
					['name'=>'channel_money','data'=>[
						'channel_id' => 0
					]],
					['name'=>'charge'],
					['name'=>'code'],
					['name'=>'config'],
					['name'=>'login_log'],
					['name'=>'manage','data'=>[
							'id' => 1,
							'role_id' => 0,
							'name' => '超级管理员',
							'login_name' => 'manage',
							'password' => createPwd(123456),
							'status' => 1,
							'create_time' => time()
					]],
					['name'=>'member'],
					['name'=>'member_charge'],
					['name'=>'member_collect'],
					['name'=>'member_log'],
					['name'=>'member_share'],
					['name'=>'member_theme'],
					['name'=>'message'],
					['name'=>'message_read'],
					['name'=>'notice'],
					['name'=>'order'],
					['name'=>'plan_record'],
					['name'=>'read_history'],
					['name'=>'search_record'],
					['name'=>'spread'],
					['name'=>'spread_chart'],
					['name'=>'spread_view'],
					['name'=>'task_member'],
					['name'=>'task_message'],
					['name'=>'task_order'],
					['name'=>'withdraw'],
			];
			$prefix = 'sy_';
			foreach ($list as $v){
				$table = $prefix.$v['name'];
				$sql = 'TRUNCATE TABLE '.$table;
				$re = Db::execute($sql);
				if($re !== false){
					if(isset($v['data'])){
						Db::name($v['name'])->insert($v['data']);
					}
				}
			}
		}
		echo 'ok';
		exit;
	}
}
