<?php

use site\myCache;

//创建底部导航
function createFooterHtml($flag=0){
	global $loginId;
	$last_str = $loginId ? '賬號' : '註冊送書幣';
	$str = '';
	$list = [
        ['id'=>1,'name'=>'首页','url'=>'/'],
        ['id'=>5,'name'=>'视频','url'=>url('video/index')],
		['id'=>2,'name'=>'分類','url'=>url('book/category')],
		['id'=>3,'name'=>'我的漫畫','url'=>url('book/collect')],
		['id'=>4,'name'=>$last_str,'url'=>url('user/index')]
	];
	$img_path = '/static/resource/index/images/';
	$str .= '<div class="footer">';
	foreach ($list as $v){
		$class = ($flag == $v['id']) ? 'footer_font red_color_themes' : 'footer_font';
		$url = ($flag == $v['id']) ? 'javascript:void(0);' : $v['url'];
		$str .= '<a href="'.$url.'" class="footer_item">';
		$img_name = $flag == $v['id'] ? 'footer_'.$v['id'].'_a.png' : 'footer_'.$v['id'].'.png';
		$str .= '<img src="'.$img_path.$img_name.'">';
		$str .= '<span class="'.$class.'">'.$v['name'].'</span>';
		$str .= '</a>';
	}
	$str .= '</div>';
	return $str;
}

//创建小说底部导航
function createNovelFooterHtml($flag=0){
    global $loginId;
    $last_str = $loginId ? '賬號' : '註冊送書幣';
    $str = '';
    $list = [
        ['id'=>1,'name'=>'首页','url'=>url('novel/index')],
        ['id'=>2,'name'=>'分類','url'=>url('novel/category')],
        ['id'=>5,'name'=>'我的小説','url'=>url('novel/collect')],
        ['id'=>4,'name'=>$last_str,'url'=>url('user/index')]
    ];
    $img_path = '/static/resource/index/images/';
    $str .= '<div class="footer">';
    foreach ($list as $v){
        $class = ($flag == $v['id']) ? 'footer_font red_color_themes' : 'footer_font';
        $url = ($flag == $v['id']) ? 'javascript:void(0);' : $v['url'];
        $str .= '<a href="'.$url.'" class="footer_item">';
        $img_name = $flag == $v['id'] ? 'footer_'.$v['id'].'_a.png' : 'footer_'.$v['id'].'.png';
        $str .= '<img src="'.$img_path.$img_name.'">';
        $str .= '<span class="'.$class.'">'.$v['name'].'</span>';
        $str .= '</a>';
    }
    $str .= '</div>';
    return $str;
}

//获取广告html
function getAd($id){
	$str = '';
	$ad = myCache::getAdList();
	if($ad && isset($ad[$id])){
		$cur = $ad[$id];
		if($cur){
			$str = '<a class="adverb" href="javascript:void(0);" onclick="javascript:jumpAd('.$id.');"><img src="'.$cur['src'].'" /></a>';
		}
	}
	return $str;
}

function addZero($number){
	if(is_numeric($number) && $number > 0){
		return str_pad($number,6,"0",STR_PAD_LEFT);
	}
	return $number;
}
	