<?php
namespace app\index\controller;

use antPay\antPay;
use qibaPay\qibaPay;
use think\Db;
use site\myDb;
use site\myCache;
use weixin\wxpay;
use think\Controller;
use otherPay\otherPay;
use app\common\model\mdMember;
use wuyisanPay\wuyisanPay;
use xubenPay\xubenPay;

class Notify extends Controller{

    private $pay_type = 1;

    //微信支付异步回调
    public function wxh5pay(){
        $this->pay_type = 1;
        $postStr = file_get_contents('php://input');
        libxml_disable_entity_loader(true);
        $result = json_decode(json_encode(simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        if(isset($result['sign'])){
            $sign = $result['sign'];
            unset($result['sign']);
            wxpay::$config = getMyConfig('wxh5pay');
            $create_sign = wxpay::getSign($result);
            if($create_sign == $sign){
                if($result['result_code'] == 'SUCCESS'){
                    self::doChargeOrder($result['out_trade_no'],$result['transaction_id']);
                }
            }
            self::resFail();
        }
    }

    //三方微信支付异步通知
    public function otherwxpay(){
        $this->pay_type = 2;
        $data = $this->request->get();
        $config = getMyConfig('site_otherwxpay');
        if($config){
            otherPay::$config = $config;
            $res = otherPay::checkSign($data);
            if($res){
                self::doChargeOrder($data['sdcustomno'],$data['sd51no']);
            }
        }
        self::resFail();
    }

    //三方支付宝支付异步通知
    public function otheralipay(){
        $this->pay_type = 2;
        $data = $this->request->get();
        $config = getMyConfig('site_otheralipay');
        if($config){
            otherPay::$config = $config;
            $res = otherPay::checkSign($data);
            if($res){
                self::doChargeOrder($data['sdcustomno'],$data['sd51no']);
            }
        }
        self::resFail();
    }

    //三方-旭本支付微信支付通知
    public function xuben666pay(){
        $this->pay_type = 3;
        $data = $this->request->get();
        file_put_contents('./2.txt',json_encode($data).PHP_EOL,FILE_APPEND);
        $da = file_get_contents('php://input');
        file_put_contents('./2-1.txt',json_encode($da).PHP_EOL,FILE_APPEND);
        $config = getMyConfig('site_xuben666pay');
        if($config){
            xubenPay::$config = $config;
            $res = xubenPay::checkSign($data,$config['api_key']);
            if($res){
                self::doChargeOrder($data['orderid'],$data['transaction_id']);
            }
        }
        self::resFail();
    }

    //三方-旭本支付支付宝支付通知
    public function xuben666alipay(){
        $this->pay_type = 3;
        $data = $this->request->get();
        file_put_contents('./1.txt',json_encode($data).PHP_EOL,FILE_APPEND);
        $da = file_get_contents('php://input');
        file_put_contents('./1-1.txt',json_encode($da).PHP_EOL,FILE_APPEND);
        $config = getMyConfig('site_xuben666alipay');
        if($config){
            xubenPay::$config = $config;
            $res = xubenPay::checkSign($data,$config['api_key']);
            if($res){
                self::doChargeOrder($data['orderid'],$data['transaction_id']);
            }
        }
        self::resFail();
    }

    //78支付异步通知
    public function qibapay(){
        $this->pay_type = 2;
        $data = $this->request->get();
        //file_put_contents("./6.txt",json_encode($data).PHP_EOL,FILE_APPEND);
        $config = getMyConfig('site_78wxpay');
        if($config){
            qibaPay::$config = $config;
            $res = qibaPay::checkSign($data);
            if($res){
                $cur = [
                    'table' => 'order',
                    'pay_money' => $data['ordermoney'],
                    'order_id' => $data['mark'],
                    'out_trade_no' => $data['sdcustomno']
                ];
                self::doChargeOrderwx($cur['out_trade_no']);
            }
        }
        self::resFail();
    }

    public function qibaalipay(){
        $this->pay_type = 2;
        $data = $this->request->get();
        //file_put_contents("./7.txt",json_encode($data).PHP_EOL,FILE_APPEND);
        $config = getMyConfig('site_78alipay');
        if($config){
            qibaPay::$config = $config;
            $res = qibaPay::checkSign($data);
            if($res){
                $cur = [
                    'table' => 'order',
                    'pay_money' => $data['ordermoney'],
                    'order_id' => $data['mark'],
                    'out_trade_no' => $data['sdcustomno']
                ];
                self::doChargeOrder($cur['out_trade_no']);
            }
        }
        self::resFail();
    }

    //蚂蚁支付微信原生异步回调
    public function wxantpay(){
        $this->pay_type = 4;
        $data = $this->request->param();
        file_put_contents("./8039.txt",json_encode($data).date('Y-m-d H:i:s').PHP_EOL,FILE_APPEND);
        $config = getMyConfig('site_wxantpay');
        if($config){
            antPay::$config = $config;
            $res = antPay::checkSign($data);
            if($res){
                $cur = [
                    'table' => 'order',
                    'pay_money' => $data['amount'],
                    'order_id' => $data['param1'],
                    'out_trade_no' => $data['mchOrderNo']
                ];
                self::doChargeOrderwx($cur['out_trade_no']);
            }else{
                self::resFail();
            }
        }else{
            self::resFail();
        }
    }

    //蚂蚁支付支付宝H5异步回调
    public function alih5antpay(){
        $this->pay_type = 4;
        $data = $this->request->param();
        file_put_contents("./8007.txt",json_encode($data).date('Y-m-d H:i:s').PHP_EOL,FILE_APPEND);
        $config = getMyConfig('site_alih5antpay');
        if($config){
            antPay::$config = $config;
            $res = antPay::checkSign($data);
            if($res){
                $cur = [
                    'table' => 'order',
                    'pay_money' => $data['amount'],
                    'order_id' => $data['param1'],
                    'out_trade_no' => $data['mchOrderNo']
                ];
                self::doChargeOrderwx($cur['out_trade_no']);
            }else{
                self::resFail();
            }
        }else{
            self::resFail();
        }
    }

    //513支付回调
    public function wuyisanpay(){
        $this->pay_type = 4;
        //$data = file_get_contents('php://input');
        $data = $this->request->param();
        file_put_contents("./wuyisan.txt",json_encode($data).PHP_EOL,FILE_APPEND);
        $config = getMyConfig('site_513pay');
        if($config){
            wuyisanPay::$config = $config;
            $res = wuyisanPay::checkSign($data);
            if($res){
                $cur = [
                    'table' => 'order',
                    'pay_money' => $data['fxfee'],
                    'order_id' => $data['fxattch'],
                    'out_trade_no' => $data['fxddh']
                ];
                self::doChargeOrder($cur['out_trade_no']);
            }
        }
        self::resFail();
    }


    //处理订单状态
    private function doChargeOrderwx($order_no,$third_order_no=''){
        $order = Db::name('Order')->where('order_no',$order_no)->field('id,channel_id,agent_id,order_no,uid,money,send_money,package,spread_id,status,count_info,charge_id,book_id')->find();
        if($order['status'] != 1){
            self::resOk();
        }
        $user = myCache::getMember($order['uid']);
        $data = [
            'status' => 2,
            'is_count' => 1,
            'pay_time' => time()
        ];
        $member_charge_type = 1;
        if($order['agent_id']){
            $data['is_count'] = self::getIsCount($order['agent_id']);
        }else{
            if($order['channel_id']){
                $data['is_count'] = self::getIsCount($order['channel_id']);
            }
        }
        $mdata = ['is_charge'=>1];
        if($order['package']){
            $member_charge_type = 2;
            $mdata['viptime'] = self::getVipTime($order['package'],$user['viptime']);
        }elseif ($order['send_money'] > 0){
            $mdata['money'] = Db::raw('money+'.$order['send_money']);
            $mdata['total_money'] = Db::raw('total_money+'.$order['send_money']);
        }
        Db::startTrans();
        $flag = false;
        $re = true;
        if($data['is_count'] == 1){
            $mcInfo = myCache::getMemberChargeId($order['uid']);
            $mcData = ['charge_num'=>Db::raw('charge_num+1'),'charge_money'=>Db::raw('charge_money+'.$order['money'])];
            $re = Db::name('MemberCharge')->where('uid',$mcInfo['uid'])->update($mcData);
            $mcInfo['charge_num'] = $data['charge_num'] = $mcInfo['charge_num'] + 1;
            $mcInfo['charge_money'] = $data['charge_money'] = $mcInfo['charge_money'] + $order['money'];
        }
        if($re){
            $data['third_order_no'] = $third_order_no;
            $res = Db::name('Order')->where('id','=',$order['id'])->update($data);
            if($res){
                $ms = Db::name('Member')->where('id','=',$user['id'])->update($mdata);
                if($ms){
                    $flag = true;
                    $count_info = json_decode($order['count_info'],true);
                    if($count_info){
                        $moneyData = ['real_charge_num'=>Db::raw('real_charge_num+1')];
                        $flag = false;
                        $moneyRes = true;
                        foreach ($count_info as $cv){
                            if($data['is_count'] == 1){
                                $moneyData['charge_num'] = Db::raw('charge_num+1');
                                $moneyData['income_money'] = Db::raw('income_money+'.$cv['money']);
                                $moneyData['charge_money'] = Db::raw('charge_money+'.$order['money']);
                            }
                            $cres = Db::name('ChannelMoney')->where('channel_id',$cv['id'])->update($moneyData);
                            if(!$cres){
                                $moneyRes = false;
                                break;
                            }
                        }
                        if($moneyRes){
                            $flag = true;
                        }
                    }
                }
            }
        }
        if($flag){
            Db::commit();
            if($data['is_count'] == 1){
                myCache::getMemberChargeId($order['uid'],$mcInfo);
            }
            switch ($member_charge_type){
                case 1:
                    myCache::doMemberMoney($user['id'], $order['send_money']);
                    mdMember::addMemberLog($order['uid'],'书币充值','充值书币到账',$order['send_money'],6);
                    break;
                case 2:
                    myCache::updateMember($user['id'], 'viptime', $mdata['viptime']);
                    break;
            }
            myCache::incChargeNum($order['uid'], $order['charge_id']);

            self::resOk();
        }else{
            Db::rollback();
            self::resFail();
        }
    }

    private function doChargeOrder($order_no,$third_order_no=''){
        $order = Db::name('Order')->where('order_no',$order_no)->field('id,channel_id,agent_id,order_no,uid,money,send_money,package,spread_id,status,count_info,charge_id,book_id')->find();
        if($order['status'] != 1){
            self::resOk();
        }
        $user = myCache::getMember($order['uid']);
        $data = [
            'status' => 2,
            'is_count' => 1,
            'pay_time' => time()
        ];
        $member_charge_type = 1;
        if($order['agent_id']){
            $data['is_count'] = self::getIsCount($order['agent_id']);
        }else{
            if($order['channel_id']){
                $data['is_count'] = self::getIsCount($order['channel_id']);
            }
        }

        $mdata = ['is_charge'=>1];
        if($order['package']){
            $member_charge_type = 2;
            $mdata['viptime'] = self::getVipTime($order['package'],$user['viptime']);
        }elseif ($order['send_money'] > 0){
            $mdata['money'] = Db::raw('money+'.$order['send_money']);
            $mdata['total_money'] = Db::raw('total_money+'.$order['send_money']);
        }
        Db::startTrans();
        $flag = false;
        $re = true;
        if($data['is_count'] == 1){
            $mcInfo = myCache::getMemberChargeId($order['uid']);
            $mcData = ['charge_num'=>Db::raw('charge_num+1'),'charge_money'=>Db::raw('charge_money+'.$order['money'])];
            $re = Db::name('MemberCharge')->where('uid',$mcInfo['uid'])->update($mcData);
            $mcInfo['charge_num'] = $data['charge_num'] = $mcInfo['charge_num'] + 1;
            $mcInfo['charge_money'] = $data['charge_money'] = $mcInfo['charge_money'] + $order['money'];
        }
        if($re){
            $data['third_order_no'] = $third_order_no;
            $res = Db::name('Order')->where('id','=',$order['id'])->update($data);
            if($res){
                $ms = Db::name('Member')->where('id','=',$user['id'])->update($mdata);
                if($ms){
                    $flag = true;
                    $count_info = json_decode($order['count_info'],true);
                    if($count_info){
                        $moneyData = ['real_charge_num'=>Db::raw('real_charge_num+1')];
                        $flag = false;
                        $moneyRes = true;
                        foreach ($count_info as $cv){
                            if($data['is_count'] == 1){
                                $moneyData['charge_num'] = Db::raw('charge_num+1');
                                $moneyData['income_money'] = Db::raw('income_money+'.$cv['money']);
                                $moneyData['charge_money'] = Db::raw('charge_money+'.$order['money']);
                            }
                            $cres = Db::name('ChannelMoney')->where('channel_id',$cv['id'])->update($moneyData);
                            if(!$cres){
                                $moneyRes = false;
                                break;
                            }
                        }
                        if($moneyRes){
                            $flag = true;
                        }
                    }
                }
            }
        }
        if($flag){
            Db::commit();
            if($data['is_count'] == 1){
                myCache::getMemberChargeId($order['uid'],$mcInfo);
            }
            switch ($member_charge_type){
                case 1:
                    myCache::doMemberMoney($user['id'], $order['send_money']);
                    mdMember::addMemberLog($order['uid'],'书币充值','充值书币到账',$order['send_money'],6);
                    break;
                case 2:
                    myCache::updateMember($user['id'], 'viptime', $mdata['viptime']);
                    break;
            }
            myCache::incChargeNum($order['uid'], $order['charge_id']);

            self::resOk();
        }else{
            Db::rollback();
            self::resFail();
        }
    }

    //检测渠道是否应该扣量
    private function getIsCount($channel_id){
        $isCount = 1;
        $channel = myCache::getChannel($channel_id);
        if($channel && $channel['deduct_num'] > 0){
            $info = myDb::getCur('ChannelMoney',[['channel_id','=',$channel_id]],'real_charge_num,charge_num');
            if($info){
                $diff = $info['real_charge_num'] - $channel['deduct_min'];
                if($diff > 0){
                    if($channel['deduct_num'] == 1){
                        $isCount = 2;
                    }else{
                        $week = ceil($diff/$channel['deduct_num']);
                        $deductCount = $info['real_charge_num'] - $info['charge_num'];
                        if($week > $deductCount){
                            $last = $diff%$channel['deduct_num'];
                            if(($last + 1) == $channel['deduct_num']){
                                $isCount = 2;
                            }else{
                                $rand = mt_rand(0,100);
                                if($rand < 50){
                                    $isCount = 2;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $isCount;
    }

    //返回失败状态
    private function resFail(){
        echo 'fail';
        exit;
    }

    //返回成功状态
    private function resOk(){
        switch ($this->pay_type){
            case 1:
                $str = '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
                break;
            case 2:
                $str = '<result>1</result>';
                break;
            case 3:
                $str = 'OK';
                break;
            case 4:
                $str = 'success';
                break;
        }
        echo $str;
        exit;
    }

    //获取vip包月时间
    private function getVipTime($package,$viptime){
        $time = time();
        if($viptime){
            if($viptime == 1){
                return 1;
            }else{
                if($viptime > $time){
                    $time = $viptime;
                }
            }
        }
        switch ($package){
            case 1:
                $time += 86400;
                break;
            case 2:
                $time += 86400*30;
                break;
            case 3:
                $time += 86400*90;
                break;
            case 4:
                $time += 86400*180;
                break;
            case 5:
                $time += 86400*365;
                break;
            case 6:
                $time = 1;
                break;
        }
        return $time;
    }
}