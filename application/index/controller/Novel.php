<?php
namespace app\index\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdBook;
use app\common\model\mdMember;

class Novel extends Common{

    //小说首页
    public function index()
    {
        $site = getMyConfig('website');
        $logo = $site ? $site['logo'] : '';
        $block = getMyConfig('web_block');
        foreach ($block as $key=>$val){
            if($val['key'] != 'cartoon' && $val['is_on']==1){
                $_logo = $site && isset($site[$val['key'].'_logo']) ? $site[$val['key'].'_logo'] : '';
                $block[$key]['logo'] = $_logo ?: 'https://nc-bk.oss-cn-hongkong.aliyuncs.com/images/20200406/4b485471c8338f3d6eb18a048ecf604c.png';
            }
        }
        return $this->fetch('index',['logo'=>$logo,'web_block'=>$block]);
    }
	
	//书籍分类
	public function category(){
		if($this->request->isAjax()){
			$rules = [
				'type'=>['between:1,5',['in'=>'参数错误']],
				'page' => ['require|number|gt:0',['require'=>'参数错误','number'=>'参数错误','gt'=>'参数错误']]
			];
			$post = myValidate::getData($rules);
			$type = $post['type'] ? : 1;
			$list = mdBook::getNovelCategoryList($type, $post['page']);
			$list = $list ? : '';
			res_table($list);
		}else{
			return $this->fetch('category');
		}
	}
	

    //书籍详情
    public function info(){
    	global $loginId;
    	$rules = [
    		'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
    		'invite_code' => ['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]
    	];
    	$get = myValidate::getData($rules,'get');
    	if($get['invite_code']){
    		$invite_uid = myCache::getUidByCode($get['invite_code']);
    		if($invite_uid){
    			if(!$loginId){
    				$session_invite_code = '';
    				if(session('?INDEX_INVITE_CODE')){
    					$session_invite_code = session('INDEX_INVITE_CODE');
    				}
    				if($session_invite_code !== $get['invite_code']){
    					session('INDEX_INVITE_CODE',$get['invite_code']);
    					mdMember::addShareMoney($invite_uid,30);
    				}
    			}
    		}
    	}
        $book = myCache::getBook($get['book_id']);
        if(empty($book)){
            res_error('书籍不存在');
        }
        if(!in_array($book['status'], [1,2])){
        	res_error('书籍已下架');
        }
        $read_number = 1;
        if($loginId){
        	$readCache = myCache::getReadCache($loginId);
        	$rkey = 'book_'.$book['id'];
        	if($readCache && isset($readCache[$rkey])){
        		$read_number = $readCache[$rkey]['last_number'];
        	}
        }
        mdBook::addHot($book['id']);
        $book['hot_num'] = myCache::getBookHotNum($book['id']);
        $book['chapter_num'] = myCache::getBookChapterNum($book['id']);
        if($book['category']){
        	$book['category'] = trim($book['category'],',');
        }
        $chapter_list = myCache::getBookChapterList($book['id']);
        $chapter = [];
        if($chapter_list){
        	reset($chapter_list);
        	$end = end($chapter_list);
        	$chapter['name'] = $end['name'];
        	$chapter['number'] = $end['number'];
        	$chapter['create_time'] = date('Y-m-d H:i',$end['create_time']);
        }
        $book['end_chapter'] = $chapter;
        $is_collect = $is_sub = 'no';
        $user = [];
        $share_url = 'http://'.$_SERVER['HTTP_HOST'].'/index/book/info.html?book_id='.$book['id'];
        if($loginId){
        	$member = myCache::getMember($loginId);
        	$collectIds = myCache::getCollectBookIds($loginId);
        	if($collectIds && in_array($book['id'], $collectIds)){
        		$is_collect = 'yes';
        	}
        	$subIds = myCache::getSubBookIds($loginId);
        	if($subIds && in_array($book['id'], $subIds)){
        		$is_sub = 'yes';
        	}
        	$user['username'] = $member['username'];
        	$user['viptime'] = '';
        	if($member['viptime']){
        		if($member['viptime'] == 1){
        			$user['viptime'] = 'VIP：永久有效';
        		}else{
        			$user['viptime'] = 'VIP：'.date('Y-m-d',$member['viptime']).'到期';
        		}
        	}
        	$user['headimg'] = $member['headimgurl'];
        	$user['money'] = myCache::getMemberMoney($loginId);
        	$share_url .= '&invite_code='.$member['invite_code'];
        }
        $likes = mdBook::getCacheBookData(5,3);
        $variable = [
        	'user' => $user,
        	'book' => $book,
        	'likes' => $likes,
        	'is_sub' => $is_sub,
        	'share_url' => $share_url,
        	'is_collect' => $is_collect,
        	'read_number' => $read_number,
        ];
        return $this->fetch('info',$variable);
    }
    
    //章节信息
    public function chapter(){
    	global $loginId;
    	$book_id = myHttp::getId('书籍','book_id');
    	$book = myCache::getBook($book_id);
    	if(!$book){
    		res_error('书籍不存在');
    	}
    	if(!in_array($book['status'], [1,2])){
    		res_error('书籍已下架');
    	}
    	$num = 0;
    	$temp = [];
    	$is_free = myCache::checkBookFree($book['id']);
    	$cache = myCache::getBookChapterList($book['id']);
    	if($cache){
    		$list = self::getReadChapterList($cache, $book, $is_free);
    		$list = array_chunk($list, 50);
    		foreach ($list as $k=>$v){
    			$length = count($v);
    			$title  = ($num+1).'~'.($num+$length);
    			$is_check = $num === 0 ? 'yes' : 'no';
    			$temp[] = ['title'=>$title,'is_check'=> $is_check,'id'=>'block_'.($k+1),'list'=>$v];
    			$num += $length;
    		}
    	}
    	$user = [];
    	if($loginId){
    		$member = myCache::getMember($loginId);
    		$user['username'] = $member['username'];
    		$user['viptime'] = '';
    		if($member['viptime']){
    			if($member['viptime'] == 1){
    				$user['viptime'] = 'VIP：永久有效';
    			}else{
    				$user['viptime'] = 'VIP：'.date('Y-m-d',$member['viptime']).'到期';
    			}
    		}
    		$user['headimg'] = $member['headimgurl'];
    		$user['money'] = myCache::getMemberMoney($loginId);
    	}
    	$variable = ['book_id'=>$book['id'],'list'=>$temp,'total'=>$num,'user'=>$user];
    	return $this->fetch('chapter',$variable);
    }
    
    //书籍正文
    public function read(){
    	global $loginId;
    	$kc_code = myValidate::getData(['kc_code'=>['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]],'get');
    	if($kc_code){
    		$codeInfo = myCache::getKcCodeInfo($kc_code);
    		if($codeInfo && $codeInfo['type'] == 1){
    			$spread = myCache::getSpread($codeInfo['sid']);
    			if(!$spread){
    				res_error('该链接已失效');
    			}
    			session('INDEX_SPREAD_ID',$spread['id']);
    			myDb::add('SpreadView',['sid'=>$spread['id'],'create_time'=>time()]);
    			$get = [
    				'book_id' => $spread['book_id'],
    				'number' => $spread['chapter_number']
    			];
    			$get['number'] = self::getReadNumber($get['book_id'],$get['number']);
    		}else{
    			res_error('非法访问');
    		}
    	}else{
    		$rules = [
    			'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
    			'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
    		];
    		$get = myValidate::getData($rules,'get');
    	}
    	$book = myCache::getBook($get['book_id']);
    	if(!$book){
    		res_error('书籍不存在');
    	}
    	if(!in_array($book['status'], [1,2])){
    		res_error('书籍已下架');
    	}
    	$totalChapter = myCache::getBookChapterNum($book['id']);
    	if($totalChapter == 0){
    		res_error('该书籍尚无可读章节');
    	}
    	$list = myCache::getBookChapterList($book['id']);
    	$number = ($get['number'] > $totalChapter) ? $totalChapter : $get['number'];
    	if(!isset($list[$number])){
    		res_error('该章节不存在');
    	}
    	$next_number = (($number + 1) > $totalChapter) ? 0 : ($number + 1);
    	$is_free = myCache::checkBookFree($book['id']);
    	$read_type = 1;;
    	$chapterList = self::getReadChapterList($list, $book, $is_free);
    	$chapter = $chapterList[$number];
    	$is_sub = 'no';
    	$share_url = 'http://'.$_SERVER['HTTP_HOST'].'/index/book/info.html?book_id='.$book['id'];
    	$user_money = 0;
    	if(!$loginId){
    		if(!$is_free){
    			$login_res = self::checkIsLogin($book, $number);
    			if($login_res){
    				$read_type = 2;
    			}
    		}
    	}else{
    		$member = myCache::getMember($loginId);
    		$share_url .= '&invite_code='.$member['invite_code'];
    		$subIds = myCache::getSubBookIds($loginId);
    		if($subIds && in_array($book['id'], $subIds)){
    			$is_sub = 'yes';
    		}
    		$isAdd = false;
    		$is_read = myCache::checkIsRead($loginId, $book['id'], $number);
    		if(!$is_read){
    			$isAdd = true;
    			if(!$is_free){
    				if($book['free_type'] == 2 && $number > $book['free_chapter']){
    					$is_money = true;;
    					if($member['viptime']){
    						if($member['viptime'] == 1 || $member['viptime'] > time()){
    							$is_money = false;
    							$res = mdBook::addReadhistory($book,$chapter,$loginId);
    							if($res){
    								$isAdd = false;
    							}
    						}
    					}
    					if($is_money){
    						$isAdd = false;
    						if($is_sub){
    							$money = myCache::getMemberMoney($loginId);
    							if($money >= $chapter['money']){
    								$res = mdBook::addReadhistory($book, $chapter, $loginId,$chapter['money']);
    								if(!$res){
    									res_error('扣费失败');
    								}
    							}else{
    								$read_type = 4;
    							}
    						}else{
    							$read_type = 3;
    						}
    					}
    				}
    			}
    		}
    		if($isAdd){
    			mdBook::addReadhistory($book,$chapter,$loginId);
    		}
    		$user_money = myCache::getMemberMoney($loginId);
    	}
    	$variable = [
    		'book' => $book,
    		'is_sub' => $is_sub,
    		'dir' => 'book/'.$book['id'],
    		'chapter' => $chapter,
    		'read_type' => $read_type,
    		'share_url' => $share_url,
    		'user_money' => $user_money,
    		'number' => ['current'=>$number,'prev'=>($number-1),'next'=>$next_number]
    	];
    	if($read_type == 1){
    		mdBook::addChapterRead($chapter['id']);
    	}
    	return $this->fetch('read',$variable);
    }
    
    //整理章节列表
    private function getReadChapterList($chapterList,$book,$is_free){
    	global $loginId;
    	if($is_free || $book['free_type'] == 1){
    		foreach ($chapterList as &$cv){
    			$cv['money'] = 0;
    		}
    	}else{
    		$read_numbers = [];
    		if($loginId){
    			$readCache = myCache::getReadCache($loginId);
    			$lkey = 'book_'.$book['id'];
    			if(isset($readCache[$lkey])){
    				$read_numbers = $readCache[$lkey]['numbers'];
    			}
    		}
    		foreach ($chapterList as &$cv){
    			if($cv['number'] > $book['free_chapter']){
    				if(!$cv['money']){
    					$cv['money'] = $book['money'];
    				}
    				if($cv['money']){
    					if($read_numbers && in_array($cv['number'], $read_numbers)){
    						$cv['money'] = -1;
    					}
    				}
    			}
    		}
    	}
    	return $chapterList;
    }
    
    //检测是否强制登录
    private function checkIsLogin($book,$number){
    	$res = false;
    	if($book['free_type'] == 2){
    		if($number > $book['free_chapter']){
    			$res = true;
    		}else{
    			$spread_id = session('INDEX_SPREAD_ID');
    			if($spread_id){
    				$spread = myCache::getSpread($spread_id);
    				if($spread){
    					if($number >= $spread['number']){
    						if($spread['is_sub'] == 1){
    							$res = true;
    						}
    					}
    				}
    			}
    		}
    	}
    	return $res;
    }
    
    //获取阅读章节
    private function getReadNumber($book_id,$number=0){
    	$number = $number ? : 1;
    	if(checkIsWeixin()){
    		global $loginId;
    		$cache = myCache::getReadCache($loginId);
    		$key = 'book_'.$book_id;
    		if(isset($cache[$key])){
    			$number = $cache[$key]['last_number'];
    		}
    	}
    	return $number;
    }
    
    //购买章节
    public function duBuyNumber(){
    	parent::checkLogin();
    	$rules = [
    		'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
    		'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
    	];
    	$post = myValidate::getData($rules);
    	$book = myCache::getBook($post['book_id']);
    	if(!$book){
    		res_api('书籍信息异常');
    	}
    	global $loginId;
    	$member = myCache::getMember($loginId);
    	$is_read = myCache::checkIsRead($member['id'], $book['id'], $post['number']);
    	if(!$is_read){
    		$list = myCache::getBookChapterList($book['id']);
    		if(!isset($list[$post['number']])){
    			res_api('该章节不存在');
    		}
    		$chapter = $list[$post['number']];
    		$is_free = myCache::checkBookFree($book['id']);
    		if(!$is_free){
    			$is_money = true;;
    			if($member['viptime']){
    				if($member['viptime'] == 1 || $member['viptime'] > time()){
    					$is_money = false;
    					mdBook::addReadhistory($book,$chapter,$loginId);
    				}
    			}
    			if($is_money){
    				$money = $book['money'];
    				if($chapter['money']){
    					$money = $chapter['money'];
    				}
    				$userMoney = myCache::getMemberMoney($member['id']);
    				if($userMoney >= $money){
    					$res = mdBook::addReadhistory($book, $chapter, $member['id'],$money);
    					if(!$res){
    						res_api('购买失败，请重试');
    					}
    				}else{
    					res_api('您的书币余额不足，是否立即充值',3,['url'=>my_url('Charge/index',['book_id'=>$book['id']])]);
    				}
    			}
    		}else{
    			mdBook::addReadhistory($book, $chapter, $member['id']);
    		}
    	}
    	res_api();
    }
    
    //检查章节是否可读
    public function checkChapter(){
    	global $loginId;
    	$rules = [
    		'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
    		'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
    	];
    	$post = myValidate::getData($rules);
    	$book = myCache::getBook($post['book_id']);
    	if(!$book){
    		res_api('书籍不存在');
    	}
    	if(!in_array($book['status'], [1,2])){
    		res_api('书籍已下架');
    	}
    	$totalChapter = myCache::getBookChapterNum($book['id']);
    	if($totalChapter == 0){
    		res_api('该书籍尚无可读章节');
    	}
    	if($post['number'] > $totalChapter){
    		res_api('已经是最后一章了');
    	}
    	$chapterList = myCache::getBookChapterList($book['id']);
    	if(!isset($chapterList[$post['number']])){
    		res_api('该章节不存在');
    	}
    	$chapter = $chapterList[$post['number']];
    	$spread_id = session('INDEX_SPREAD_ID');
    	if($spread_id){
    		$spread = myCache::getSpread($spread_id);
    		if($spread){
    			if($post['number'] >= $spread['number']){
    				if($spread['is_sub'] == 1){
    					if(!$loginId){
    						res_api('您尚未登录，是否立即登录',3);
    					}
    				}
    			}
    		}
    	}
    	$is_free = myCache::checkBookFree($book['id']);
    	if($loginId){
    		if($book['free_type'] == 2 && $post['number'] > $book['free_chapter']){
    			$is_read = myCache::checkIsRead($loginId, $book['id'], $post['number']);
    			if(!$is_read){
    				if(!$is_free){
    					$member = myCache::getMember($loginId);
    					$is_money = true;
    					if($member['viptime'] > 0){
    						$is_money = false;
    						if($member['viptime'] != 1){
    							if(time() > $member['viptime']){
    								$is_money = true;
    							}
    						}
    					}
    					if($is_money){
    						$money = myCache::getMemberMoney($loginId);
    						$book_money = $chapter['money'] ? $chapter['money'] : $book['money'];
    						if($money < $book_money){
    							res_api('您的书币余额不足，是否立即充值',3,['url'=>my_url('Charge/index',['book_id'=>$book['id']])]);
    						}
    					}
    				}
    			}
    		}
    	}else{
    		if(!$is_free){
    			if($book['free_type'] == 2 && $post['number'] > $book['free_chapter']){
    				res_api('您尚未登录，是否立即登录',3);
    			}
    		}
    	}
    	res_api(['url'=>my_url('read',['book_id'=>$book['id'],'number'=>$post['number']])]);
    }
    
    //今日更新
    public function refresh(){
    	$updateIds = myCache::getUpdateBookIds();
    	$start_time = strtotime(date('Y-m-d',strtotime('-6 days')));
    	$list = [];
    	$today_time =  strtotime('today');
    	$num = 0;
    	while ($start_time <= $today_time){
    		$num++;
    		$name = '';
    		$bookList = [];
    		$date = date('Y-m-d',$start_time);
    		if($start_time == $today_time){
    			$name = '今日';
    		}else{
    			$week = date('N',$start_time);
    			switch ($week){
    				case 1:$name = '周一';break;
    				case 2:$name = '周二';break;
    				case 3:$name = '周三';break;
    				case 4:$name = '周四';break;
    				case 5:$name = '周五';break;
    				case 6:$name = '周六';break;
    				case 7:$name = '周日';break;
    			}
    		}
    		if(isset($updateIds[$date])){
    			foreach ($updateIds[$date] as $v){
    				$book = mdBook::getIndexBookInfo($v);
    				if($book){
    					$bookList[] = $book;
    				}
    			}
    		}
    		$list[] = ['name'=>$name,'list'=>$bookList,'num'=>$num];
    		$start_time += 86400;
    	}
    	return $this->fetch('refresh',['list'=>$list]);
    }
    
    //排行
    public function rank(){
    	
    	return $this->fetch('rank');
    }
    
    //阅读历史
    public function readHistory(){
    	global $loginId;
    	$variable = mdMember::getMyReadHistory($loginId);
    	return $this->fetch('collect',$variable);
    }
    
    //书架
    public function collect(){
    	global $loginId;
    	$variable = mdMember::getMyCollect($loginId);
    	return $this->fetch('collect',$variable);
    }
    
    //按类型获取书籍
    public function getTypeData(){
    	$rules = [
    		'type' => ['require|in:1,5',['require'=>'请求参数错误','in'=>'未指定该请求参数']],
    		'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数格式不规范','gt'=>'分页参数格式不规范']]
    	];
    	$data = myValidate::getData($rules);
    	$page = $data['page'] - 1;
    	$list = mdBook::getCacheBookData($data['type'],6,$page);
    	$list = $list ? : 0;
    	res_table($list);
    }
    
}