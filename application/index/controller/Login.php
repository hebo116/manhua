<?php
namespace app\index\controller;

use site\myDb;
use site\myMsg;
use site\myCache;
use site\myValidate;
use app\common\model\mdMember;

class Login extends Common{
    
    //登录
    public function index(){
    	if($this->request->isAjax()){
	    	$rules = [
	    		'username' => ['require|alphaDash|length:4,12',['require'=>'请输入用户名','alphaDash'=>'用户名仅支持字母数字','length'=>'用户名长度仅支持6-12位']],
	    		'password' => ['require|length:6,16',['require'=>'请输入密码','length'=>'密码长度为6-16位字符']],
	    	];
	    	$post = myValidate::getData($rules);
	    	$where = [['username|phone','=',$post['username']],['password','=',createPwd($post['password'])]];
	    	$user = myDb::getCur('Member', $where);
	    	if(!$user){
	    		res_api('用户名或者密码输入错误');
	    	}
	    	if($user['status'] != 1){
	    		res_api('您的账号已被禁用，请联系客服');
	    	}
	    	cookie('INDEX_LOGIN_ID',$user['id']);
	    	res_api(['url'=>url('user/index')]);
    	}else{
    		
    		return $this->fetch();
    	}
    }
    
    //短信登录
    public function codeLogin(){
    	if($this->request->isAjax()){
    		$post = mdMember::getPhoneData();
    		$flag = myMsg::check($post['phone'], $post['code']);
    		if(is_string($flag)){
    			res_api($flag);
    		}
    		$uid = myCache::getUidByPhone($post['phone']);
    		if($uid){
    			session('INDEX_LOGIN_ID',$uid);
    			res_api(['msg'=>'登录成功','url'=>url('user/index')]);
    		}else{
    			$user_id = myDb::getValue('Member', [['username','=',$post['phone']]],'id');
    			if($user_id){
    				res_api('该手机号已注册用户，请使用账号密码登录');
    			}
    			$re = mdMember::regByPhone($post['phone']);
    			if($re){
    				res_api(['msg'=>'注册成功','url'=>url('user/index')]);
    			}else{
    				res_api('注册失败');
    			}
    		}
    	}else{
    		
    		return $this->fetch('codeLogin');
    	}
    }
    
    //注册
    public function register(){
    	if($this->request->isAjax()){
    		$rules = [
    			'username' => ['require|alphaDash|length:4,12',['require'=>'请输入用户名','alphaDash'=>'用户名仅支持字母数字','length'=>'用户名长度仅支持6-12位']],
    			'password' => ['require|length:6,16',['require'=>'请输入密码','length'=>'密码长度为6-16位字符']],
    		];
    		$post = myValidate::getData($rules);
    		$repeat = myDb::getValue('Member',[['username','=',$post['username']]], 'id');
    		if($repeat){
    			res_api('该用户名已被注册');
    		}
    		$intive_code = session('?INDEX_INVITE_CODE') ? session('INDEX_INVITE_CODE') : '';
    		$re = mdMember::regByUsername($post['username'], $post['password'],$intive_code);
    		if($re){
    			res_api(['url'=>url('user/index')]);
    		}else{
    			res_api('注册失败');
    		}
    	}else{
    		$nickname = mdMember::getNickname();
    		$password = 123456;
    		return $this->fetch('register',['nickname'=>$nickname,'password'=>$password]);
    	}
    }
    
    
}