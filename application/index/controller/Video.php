<?php
namespace app\index\controller;
use site\myCache;
use site\myDb;
use site\myHttp;
use weixin\wx;

class Video extends Common{
    
    //视频首页
    public function index(){
        $site = getMyConfig('website');
        $logo = $site ? $site['logo'] : '';
        $block = getMyConfig('web_block');
        foreach ($block as $key=>$val){
            if($val['key'] != 'cartoon' && $val['is_on']==1){
                $_logo = $site && isset($site[$val['key'].'_logo']) ? $site[$val['key'].'_logo'] : '';
                $block[$key]['logo'] = $_logo ?: 'https://nc-bk.oss-cn-hongkong.aliyuncs.com/images/20200406/4b485471c8338f3d6eb18a048ecf604c.png';
            }
        }
        $cur = myDb::getList('Video',['status'=>1],'id,name,cover,author,source,summary,category,hot_num',['hot_num'=>'desc']);
        return $this->fetch('index',['logo'=>$logo,'web_block'=>$block,'cur'=>$cur,'time'=>time()]);
    }
    
    //视频详情页
    public function info(){
    	$video_id = myHttp::getId('视频','video_id');
    	$video = myCache::getVideoCache($video_id);
    	if(!$video){
    		res_api('视频不存在');
    	}
    	$is_read = 'no';
    	global $loginId;
    	if($loginId){
    		$read = myDb::getCur('PlayHistory',[['uid','=',$loginId],['video_id','=',$video_id]]);
    		if($read){
    			$is_read = 'yes';
    		}
    	}
    	$jsConfig = $share_data = '';
    	$reward = getMyConfig('reward');
    	$variable = [
    		'cur' => $video,
    		'reward' => $reward,
    		'is_read' => $is_read,
    		'site_title' => getMyConfig('website')['name'],
    		'jsConfig' => json_encode($jsConfig,JSON_UNESCAPED_UNICODE),
    		'share_data' => json_encode($share_data,JSON_UNESCAPED_UNICODE)
    	];
    	myDb::setIncById('Video',$video['id'],'hot_num');
    	$this->assign($variable);
    	return $this->fetch();
    }
    
    //检查视频播放
    public function checkPlay(){
    	$video_id = myRequest::postId('视频','video_id');
    	$video = myCache::getVideoCache($video_id);
    	if(!$video){
    		res_return('视频不存在');
    	}
    	if($video['free_type'] == 2){
    		global $loginId;
    		if(!$loginId){
    			res_return(['flag'=>1,'msg'=>'您尚未登陆,是否立即登陆?','url'=>my_url('Login/index')]);
    		}
    		$member = myCache::getUserCache($loginId);
    		if(!$member){
    			iMember::clearLogin();
    			res_return(['flag'=>1,'msg'=>'您尚未登陆,是否立即登陆?','url'=>my_url('Login/index')]);
    		}
    		$read = iVideo::getCur('PlayHistory',[['uid','=',$loginId],['video_id','=',$video_id]]);
    		if(!$read){
    			$is_money = true;
    			if($member['viptime'] > 0){
    				$is_money = false;
    				if($member['viptime'] != 1){
    					if(time() > $member['viptime']){
    						$is_money = true;
    					}
    				}
    			}
    			if($is_money){
    				if($member['money'] < $video['money']){
    					res_return(['flag'=>2,'url'=>my_url('Charge/index',['video_id'=>$video_id])]);
    				}
    				$re = iVideo::costMoney($video, $member);
    				if(!$re){
    					res_return('扣除书币失败，请重试');
    				}
    			}else{
    				iVideo::addReadhistory($video_id, $loginId);
    			}
    		}
    	}
    	res_return(['flag'=>0,'url'=>$video['url']]);
    }
    
    //影库
    public function category(){
    	if($this->request->isAjax()){
    		$config = [
    				'default' => [['status','=',1]],
    				'eq' => 'free_type',
    				'like' => 'category',
    				'rules' => ['free_type'=>'in:1,2']
    		];
    		$where = mySearch::getWhere($config,'post');
    		$post = myRequest::post('page');
    		$page =  (is_numeric($post['page']) && $post['page'] > 0) ? $post['page'] : 1;
    		$list = iVideo::getCategoryList($where, $page);
    		$list = $list ? : 0;
    		res_return('ok',$list);
    	}else{
    		$option = iVideo::getCategoryOption();
    		$option['site_title'] = $this->site_title;
    		$this->assign($option);
    		return $this->fetch();
    	}
    }
    
    //更多列表
    public function more(){
    	if($this->request->isAjax()){
    		$post = myRequest::post('area,page,is_hot');
    		$list = 0;
    		$where = [['status','=',1]];
    		if(in_array($post['is_hot'],[1,2])){
    			$where[] = ['is_hot','=',$post['is_hot']];
    		}else{
    			if($post['area']){
    				$where[] = ['area','like','%,'.$post['area'].',%'];
    			}else{
    				res_return('ok',$list);
    			}
    		}
    		$page = $post['page']>=1 ? $post['page'] : 1;
    		$list = iVideo::getMoreList($where,$page);
    		$list = $list ? : 0;
    		res_return('ok',$list);
    	}else{
    		$get = myRequest::get('area,is_hot');
    		if($get['is_hot'] == 1){
    			$get['page_title'] = '热门推荐';
    		}else{
    			$title = $get['area'];
    			$get['page_title'] = $title;
    		}
    		$get['site_title'] = $this->site_title;
    		$this->assign('cur',$get);
    		return $this->fetch();
    	}
    }
    
    //获取猜你喜欢书籍
    public function getSameVideos(){
    	$video_id = myRequest::postId('视频','video_id');
    	$video = myCache::getVideoCache($video_id);
    	$data = 0;
    	if($video){
    		$category = $video['category'];
    		$where = [['category','=',$category],['status','=',1],['id','<>',$video_id]];
    		$list = iVideo::getLimitVideos($where, 6);
    		$data = $list ? $list : 0;
    	}
    	res_return(['list'=>$data]);
    }
}