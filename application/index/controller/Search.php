<?php
namespace app\index\controller;

use think\Db;
use site\myCache;

class Search extends Common{
	
	//搜索页面
	public function index(){
		global $loginId;
		$category = input('category',1,'intval');
		$history = $loginId ? myCache::getUserSearch($loginId) : '';
		$variable = ['history' => $history ? : ''];
        $variable['category'] = $category;
		return $this->fetch('search/index',$variable);
	}
	
	//搜索结果
	public function searchResult(){
		if($this->request->isAjax()){
			$keyword = $this->request->post('keyword');
			$list = [];
			if($keyword){
				global $loginId;
				$where = [['status','=',1],['name|author','like','%'.$keyword.'%']];
				$field = 'id,name,author,cover,summary,hot_num,over_type,is_alone,star';
				$list = Db::name('Book')->where('status',1)->where('name|author','like','%'.$keyword.'%')->field($field)->order('hot_num','desc')->limit(20)->select();
				foreach ($list as &$v){
					$v['cover'] = $v['cover'] ? : '';
				}
				if($loginId){
					myCache::addUserSearch($loginId, $keyword);
				}
			}
			$list = $list ? : 0;
			res_table($list);
		}else{
			$keyword = $this->request->get('keyword');
			return $this->fetch('searchResult',['keyword'=>$keyword]);
		}
	}
	
	//删除指定搜索记录
	public function delRecord(){
		global $loginId;
		if($loginId){
			$keyword = $this->request->post('keyword');
			if($keyword){
				myCache::rmUserSearch($loginId,$keyword);
			}
		}
		res_api();
	}
	
	//删除全部
	public function clearRecord(){
		global $loginId;
		if($loginId){
			myCache::rmUserAllSearch($loginId);
		}
		res_api();
	}
	
}