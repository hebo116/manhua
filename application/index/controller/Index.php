<?php
namespace app\index\controller;

class Index extends Common{
    
    //首页
    public function index(){
        $site = getMyConfig('website');
        header('Location:'.'http://'.$site['url']);
        die;
        $logo = $site ? $site['logo'] : '';
        $block = getMyConfig('web_block');
        foreach ($block as $key=>$val){
            if($val['key'] != 'cartoon' && $val['is_on']==1){
                $_logo = $site && isset($site[$val['key'].'_logo']) ? $site[$val['key'].'_logo'] : '';
                $block[$key]['logo'] = $_logo ?: 'https://nc-bk.oss-cn-hongkong.aliyuncs.com/images/20200406/4b485471c8338f3d6eb18a048ecf604c.png';
            }
        }
    	return $this->fetch('index',['logo'=>$logo,'web_block'=>$block]);
    }
    
}