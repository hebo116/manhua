<?php
namespace app\index\controller;

use weixin\wx;
use site\myDb;
use site\myHttp;
use weixin\wxpay;
use think\Controller;
use otherPay\otherPay;


class Pay extends Controller{
	
	//微信h5支付
	public function h5pay(){
		if(!$this->request->isMobile()){
			res_error('请在手机上打开该链接进行支付');
		}
		$website = getMyConfig('website');
		if($_SERVER['HTTP_HOST'] !== $website['url']){
			res_error('非法访问');
		}
		$get = myHttp::getData('order_no,back_url','get');
		if(!$get['order_no']){
			res_error('订单参数异常');
		}
		$field = 'id,order_no,money,status,pay_type';
		$where = [['order_no','=',$get['order_no']],['status','=',1]];
		$order = myDb::getCur('Order',$where,$field);
		if(!$order){
			res_error('订单数据异常');
		}
		if($order['pay_type'] != 1){
			res_error('支付方式有误');
		}
		$config = getMyConfig('wxh5pay');
		if(!$config){
			res_error('未配置支付参数');
		}
		if($config['is_on'] != 1){
			res_api('该支付方式已关闭');
		}
		wx::$config = $config;
		wxpay::$config = $config;
		$params = [
			'body' => '书币充值',
			'out_trade_no' => $order['order_no'],
			'total_fee' => $order['money'],
			'trade_type' => 'MWEB',
			'notify_url' => 'http://'.$website['url'].'/index/Notify/wxh5pay.html'
		];
		$payurl = wxpay::getPayMsg($params);
		if(!$payurl){
			res_error('拉起微信支付失败');
		}
		$this->redirect($payurl);
	}

	///三方支付支付
	public function otherwxpay(){
		$website = getMyConfig('website');
		if($_SERVER['HTTP_HOST'] !== $website['url']){
			res_error('非法访问');
		}
		$get = myHttp::getData('order_no,back_url','get');
		if(!$get['order_no']){
			res_error('订单参数异常');
		}
		$config = getMyConfig('site_otherwxpay');
		if(!$config){
			res_api('未配置支付参数');
		}
		if($config['is_on'] != 1){
			res_api('该支付方式已关闭');
		}
		$field = 'id,order_no,money,status,pay_type';
		$where = [['order_no','=',$get['order_no']],['status','=',1]];
		$order = myDb::getCur('Order', $where,$field);
		if(!$order){
			res_error('订单数据异常');
		}
		if($order['status'] != 1){
			res_error('该订单已支付');
		}
		if($order['pay_type'] != 2){
			res_error('支付方式异常');
		}
		otherPay::$config = $config;
		$noticeurl = 'http://'.$website['url'].'/index/Notify/otherwxpay.html';
		$param = [
				'sdcustomno' => $order['order_no'],
				'orderAmount' => $order['money']*100,
				'noticeurl' => $noticeurl,
				'backurl' => $get['back_url'],
				'mark' => $order['id']
		];
		$form = otherPay::getPayForm($param,'wxh5');
		echo $form;
		exit;
	}
	
	//三方支付支付
	public function otheralipay(){
		$website = getMyConfig('website');
		if($_SERVER['HTTP_HOST'] !== $website['url']){
			res_error('非法访问');
		}
		$get = myHttp::getData('order_no,back_url','get');
		if(!$get['order_no']){
			res_error('订单参数异常');
		}
		$config = getMyConfig('site_otheralipay');
		if(!$config){
			res_api('未配置支付参数');
		}
		if($config['is_on'] != 1){
			res_api('该支付方式已关闭');
		}
		$field = 'id,order_no,money,status,pay_type';
		$where = [['order_no','=',$get['order_no']],['status','=',1]];
		$order = myDb::getCur('Order', $where,$field);
		if(!$order){
			res_error('订单数据异常');
		}
		if($order['status'] != 1){
			res_error('该订单已支付');
		}
		if($order['pay_type'] != 3){
			res_error('支付方式异常');
		}
        if(checkIsWeixin()){
            return $this->fetch('charge/notice',['cur'=>['order_no'=>$order['order_no'],'money'=>$order['money']]]);
        }

		otherPay::$config = $config;
		$noticeurl = 'http://'.$website['url'].'/index/Notify/otheralipay.html';
		$param = [
			'sdcustomno' => $order['order_no'],
			'orderAmount' => $order['money']*100,
			'noticeurl' => $noticeurl,
			'backurl' => $get['back_url'],
			'mark' => $order['id']
		];
		$form = otherPay::getPayForm($param,'alipayh5');
		echo $form;
		exit;
	}
}