<?php
namespace app\index\controller;

use think\Db;
use site\myDb;
use site\myMsg;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdBook;
use app\common\model\mdMember;

class User extends Common{
	
	//构造函数
	public function __construct(){
		parent::__construct();
		parent::checkLogin();
	}
    
    //个人中心
    public function index(){
        global $loginId;
        $cur = myCache::getMember($loginId);
        $cur['money'] = myCache::getMemberMoney($loginId);
        if($cur['phone']){
        	$cur['phone'] = substr_replace($cur['phone'], '****', 3,4);
        }
        $contact = getMyConfig('site_contact');
        if(!$contact){
        	$contact = myDb::buildArr('email,weibo,weixin,qq,line');
        	$contact['line_hide'] = 'yes';
        	$contact['contact_hide'] = 'yes';
        }else{
        	$contact['line_hide'] = 'no';
        	$contact['contact_hide'] = 'no';
        	if(!$contact['email'] && !$contact['weibo']){
        		$contact['line_hide'] = 'yes';
        	}
        	if(!$contact['qq'] && !$contact['weixin'] && !$contact['line']){
        		$contact['contact_hide'] = 'yes';
        	}
        }
        $msg = Db::name('Notice a')
        ->join('notice_read b','a.id=b.msg_id and b.uid='.$loginId,'left')
        ->field('a.id,IFNULL(b.id,0) as read_id')
        ->having('read_id=0')
        ->select();
        $msg_num = $msg ? count($msg) : 0;
        $variable = ['cur' => $cur,'contact' => $contact,'msg_num'=>$msg_num];
    	return $this->fetch('index',$variable);
    }
    
    //签到
    public function sign(){
    	global $loginId;
    	$member = myCache::getMember($loginId);
    	if(!$member){
    		res_error('用户信息异常');
    	}
    	$config = getMyConfig('site_sign');
    	if($config['is_sign'] != 1){
    		res_error('签到功能未启用');
    	}
    	$signInfo = myCache::getUserSignInfo($loginId);
    	if($this->request->isAjax()){
    		if($signInfo['cur_sign'] === 'yes'){
    			res_api();
    		}
    		$data = [
    			'uid' => $member['id'],
    			'date' => date('Ymd'),
    			'create_time' => time()
    		];
    		foreach ($signInfo['list'] as &$v){
    			if($v['is_sign'] === 'no'){
    				$data['money'] = $v['money'];
    				$data['days'] = $v['day'];
    				$v['is_sign'] = 'yes';
    				break;
    			}
    		}
    		if(!isset($data['money'])){
    			res_api('签到参数有误');
    		}
    		$re = mdMember::doSign($data, $loginId);
    		if($re){
    			$signInfo['add_coin'] = intval($data['money']);
    			$signInfo['cur_sign'] = 'yes';
    			$key = md5($data['date'].'_signuser_'.$loginId);
    			cache($key,$signInfo,86400);
    			mdMember::addChargeLog($loginId, '+'.$data['money'].' 书币', '签到送书币');
    			res_api($signInfo);
    		}else{
    			res_api('签到失败');
    		}
    	}else{
    		$variable = ['cur'=>$signInfo];
    		return $this->fetch('sign',$variable);
    	}
    }
    
    //消费记录
    public function consume(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$page = myHttp::postId('分页','page');
    		$pages = ['page'=>$page,'limit'=>20];
    		$list = myDb::getOnlyPageList('MemberConsume', [['uid','=',$loginId]],'*',$pages);
    		if($list){
    			$temp = [];
    			foreach ($list as &$v){
    				if(isset($temp[$v['book_id']])){
    					$one = $temp[$v['book_id']];
    				}else{
    					$book = myCache::getBook($v['book_id']);
    					if($book){
    						$one = ['cover'=>$book['cover'],'name'=>$book['name']];
    					}else{
    						$one = ['cover'=>'/static/templet/default/cover.png','name'=>'未知'];
    					}
    					$temp[$v['book_id']] = $one;
    				}
    				$v['name'] = $one['name'];
    				$v['cover'] = $one['cover'];
    				$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
    			}
    		}else{
    			$list = '';
    		}
    		res_table($list);
    	}else{
    		
    		return $this->fetch();
    	}
    }
    
    //充值记录
    public function charge(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$page = myHttp::postId('分页','page');
    		$pages = ['page'=>$page,'limit'=>20];
    		$list = myDb::getOnlyPageList('MemberChargeLog', [['uid','=',$loginId]],'id,money,summary,create_time',$pages);
    		if($list){
    			$temp = [];
    			foreach ($list as &$v){
    				$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
    			}
    		}else{
    			$list = '';
    		}
    		res_table($list);
    	}else{
    		
    		return $this->fetch();
    	}
    }
    
    //消息中心
    public function message(){
    	if($this->request->isAjax()){
    		$page = myHttp::postId('分页','page');
    		$list = mdMember::getMyMsg($page);
    		res_table($list);
    	}else{
    		global $loginId;
    		$msg = Db::name('Notice a')
    		->join('notice_read b','a.id=b.msg_id and b.uid='.$loginId,'left')
    		->field('a.id,IFNULL(b.id,0) as read_id')
    		->having('read_id=0')
    		->select();
    		if($msg){
    			foreach ($msg as $v){
    				Db::name('NoticeRead')->insert(['msg_id'=>$v['id'],'uid'=>$loginId]);
    			}
    		}
    		return $this->fetch();
    	}
    }
    
    //自动订阅
    public function auto(){
    	$list = [];
    	global $loginId;
    	$ids = myCache::getSubBookIds($loginId);
    	if($ids){
    		foreach ($ids as $v){
    			$book = mdBook::getIndexBookInfo($v);
    			if($book){
    				$list[] = $book;
    			}
    		}
    	}
    	return $this->fetch('auto',['list'=>$list]);
    }
    
    //我的账单
    public function log(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$page = myHttp::postId('分页','page');
    		$list = mdMember::getMemberLog($loginId, $page);
    		res_table($list);
    	}else{
    		
    		return $this->fetch();
    	}
    }
    
    //安全设置
    public function safe(){
    	global $loginId;
    	$cur = myCache::getMember($loginId);
    	if($cur['phone']){
    		$cur['phone'] = substr_replace($cur['phone'],'****', 3,4);
    	}
    	return $this->fetch('',['cur'=>$cur]);
    }
    
    //修改密码
    public function changePwd(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$rules = [
    			"pwd" => ["require|length:6,16",["require"=>"请输入新密码","length"=>"请输入6到16位新密码"]],
    			"repwd"  => ["require|confirm:pwd",["require"=>"请再次输入新密码","confirm"=>"两次输入密码不一致"]],
    		];
    		$data = myValidate::getData($rules);
    		$password = createPwd($data['pwd']);
    		$re = myDb::setField('Member', [['id','=',$loginId]], 'password',$password);
    		if($re){
    			res_api(['msg'=>'修改成功','url'=>url('safe')]);
    		}else{
    			res_api('修改失败');
    		}
    	}else{
    		
    		return $this->fetch('changePwd');
    	}
    }
    
    //绑定手机号
    public function BindPhone(){
    	if($this->request->isAjax()){
    		$data = mdMember::getPhoneData();
    		myMsg::check($data['phone'], $data['code']);
    		$re = mdMember::doBindPhone($data['phone']);
    		if($re){
    			res_api(['msg'=>'绑定成功','url'=>url('safe')]);
    		}else{
    			res_api('绑定失败');
    		}
    	}else{
    		$variable = ['type' => 1];
    		return $this->fetch('bind',$variable);
    	}
    }
    
    //解绑手机号
    public function unBind(){
    	global $loginId;
    	$user = myCache::getMember($loginId);
    	if($this->request->isAjax()){
    		$rules = ['code' => ['require|number|length:6',['require'=>'请输入验证码','number'=>'验证码为6位数字','length'=>'验证码为6位数字']]];
    		$code = myValidate::getData($rules);
    		if(!$user['phone']){
    			res_api('您尚未绑定手机号');
    		}
    		myMsg::check($user['phone'], $code);
    		$re = mdMember::doUnBindPhone($user['phone']);
    		if($re){
    			res_api(['msg'=>'解绑成功','url'=>url('safe')]);
    		}else{
    			res_api('解绑失败');
    		}
    	}else{
    		$variable = [
    			'type' => 2,
    			'username' => $user['username'],
    			'encode_phone' => substr_replace($user['phone'], '****', 3,4)
    		];
    		return $this->fetch('bind',$variable);
    	}
    }
    
    //设置自动登录
    public function setAutoLogin(){
    	global $loginId;
    	$rules = ['auto_login' => ['require|in:1,2',['require'=>'参数错误','in'=>'未指定该参数']]];
    	$autoLogin = myValidate::getData($rules);
    	$re = myDb::setField('Member', [['id','=',$loginId]], 'auto_login', $autoLogin);
    	if($re){
    		myCache::updateMember($loginId, 'auto_login',$autoLogin);
    		res_api();
    	}else{
    		res_api('设置失败');
    	}
    }
    
    //更换主题
    public function theme(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$color = $this->request->post('color');
    		$arr = ['red','green','blue','pink','yellow','orange','gray','black','violet'];
    		if(!in_array($color, $arr)){
    			res_api('未指定该主题');
    		}
    		$curColor = myCache::getMemberTheme($loginId);
    		if($curColor){
    			$re = myDb::setField('MemberTheme', [['uid','=',$loginId]], 'color', $color);
    		}else{
    			$re = myDb::add('MemberTheme',['uid'=>$loginId,'color'=>$color]);
    		}
    		if($re){
    			myCache::setMemberTheme($loginId, $color);
    			res_api();
    		}else{
    			res_api('设置失败');
    		}
    	}else{
    		
    		return $this->fetch();
    	}
    }
    
    //退出登录
    public function logOut(){
    	//cookie('INDEX_LOGIN_ID',null);
    	$this->redirect('/index/login/index');
    }
   
}