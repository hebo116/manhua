<?php
namespace app\index\controller;

use weixin\wx;
use site\myDb;
use site\myHttp;
use site\myCache;
use think\Controller;
use app\common\model\mdMember;

class Open extends Controller{
    
    /**
     * 公众号消息与事件接收URL
     */
    public function callback(){
        $get = myHttp::getData('echostr','get');
        $config = myCache::getWeixinInfo();
        wx::$config = $config;
        if($get['echostr']){
            $res = wx::valid();
            $str = $res ? $get['echostr'] : '';
            echo $str;
        }else{
            $this->handler();
        }
    }
    
    public function logs(){
    	if($this->request->get('is_clean') == 1){
    		cache('sys_log',null);
    		echo 'ok';
    	}else{
    		$log = cache('sys_log');
    		my_print($log);
    	}
    }
    
    /**
     * 分类处理消息
     */
    public function handler(){
        $postStr = file_get_contents('php://input');
        libxml_disable_entity_loader(true);
        $message = json_decode(json_encode(simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        if(isset($message['MsgType'])){
            wx::$object = $message;
            switch($message['MsgType']){
                case 'text':
                    $this->textHandler();
                    break;
                case 'event':
                    $this->eventHandler();
                    break;
                default:
                    echo '';
                    break;
            }
        }else{
            echo '';
        }
        exit;
    }
    
    /**
     * 处理回复发送过来的事件信息
     */
    private function eventHandler(){
        //解析参数
        switch (wx::$object['Event']){
            //用户关注
            case 'subscribe':
                if(isset(wx::$object['EventKey']) && wx::$object['EventKey']){
                	self::scanQrcode();
                }else{
                	self::doSubscribe();
                }
                break;
            case 'SCAN':
            	self::scanQrcode();
            	break;
                //用户取消关注
            case 'unsubscribe':
                mdMember::unsubscribeMember(wx::$object['FromUserName']);
                echo '';
                break;
                //用户点击事件推菜单
            case 'CLICK':
                self::getClickStr();
                break;
            default:
                echo '';
                break;
        }
        exit;
    }
    
    //扫码登陆
    private function scanQrcode(){
    	$info = wx::getUserInfo(wx::$object['FromUserName']);
    	if($info){
    		$code = ltrim(wx::$object['EventKey'],'qrscene_');
    		$flag = mdMember::subscribeByQrcode($info,$code);
    		if($flag){
    			$str = self::doHistoryReply('扫码登录成功');
    			wx::responseText($str);
    		}else{
    			wx::responseText('扫码登录失败');
    		}
    	}else{
    		wx::responseText('获取信息失败，扫码登陆失败');
    	}
    }
    
    //直接关注
    private function doSubscribe(){
    	$info = wx::getUserInfo(wx::$object['FromUserName']);
    	if($info){
    		$is_first = mdMember::subscribeMember($info);
    		switch ($is_first){
    			case 1:
    				$uid = myCache::getUidByOpenId(wx::$object['FromUserName']);
    				$is_read = myDb::getValue('ReadHistory', [['uid','=',$uid],['type','between',[1,2]]], 'id');
    				if($is_read){
    					$str = self::doHistoryReply('尊敬的用户,您已关注成功!');
    					wx::responseText($str);
    				}else{
    					self::doAttentionBack();
    				}
    				break;
    			case 2:
    				$str = self::doHistoryReply('尊敬的用户,您已关注成功!');
    				wx::responseText($str);
    				break;
    			default:
    				wx::responseText('注册失败');
    				break;
    		}
    	}else{
    		wx::responseText('获取微信用户公开信息失败');
    	}
    }
    
    /**
     * 处理菜单事件推
     */
    private function getClickStr(){
        $keyword = isset(wx::$object['EventKey']) ? wx::$object['EventKey'] : '';
        if($keyword){
            if($keyword === 'sign'){
                self::doSign();
            }else{
            	$config = myCache::getUrlInfo();
                $where = [['cate','=',2],['status','=',1],['channel_id','=',$config['channel_id']],['keyword','=',$keyword]];
                $cur = myDb::getCur('WxReply', $where,'id,type,content,material');
                if($cur){
                    if($cur['type'] == 1){
                        wx::responseText($cur['content']);
                    }else{
                        wx::responseNews(json_decode($cur['material'],true));
                    }
                }
            }
        }
        wx::responseText('未定义该菜单');
    }
    
    /**
     * 处理签到回复
     */
    private function doSign(){
        $config = getMyConfig('site_sign');
        if(isset($config['is_sign']) && $config['is_sign'] == 1){
        	$signConfig = $config['sign_config'];
            $res = mdMember::doMenuSign(wx::$object['FromUserName'],$signConfig);
            $str = $res['str'];
            if($res['flag'] !== 3){
            	if($res['flag'] === 1){
            		$str = self::doHistoryReply($str);
            	}
            	$urlInfo = myCache::getUrlInfo();
            	$activityId = myCache::getLastActivityId();
            	if($activityId){
            		$activity = myCache::getActivity($activityId);
            		if($activity){
            			$code = '';
            			if($urlInfo['wx_id']){
            				$delIds = myCache::getActivityDelIds($urlInfo['wx_id']);
            				if(!$delIds || !in_array($activityId, $delIds)){
            					$code = myCache::getActivityCode($activityId,$urlInfo['wx_id']);
            				}
            			}else{
            				$code = myCache::getActivityCode($activityId);
            			}
            			if($code){
            				$str .= "\n\n";
            				$str .= '<a href="http://'.$urlInfo['url'].'/index/Activity/index.html?kc_code='.$code.'">'.$activity['name'].'</a>';
            			}
            		}
            	}
            }
            wx::responseText($str);
        }else{
            wx::responseText('签到功能已关闭');
        }
    }
    
    /**
     * 首次关注回复
     */
    private function doAttentionBack(){
    	$config = myCache::getUrlInfo();
        $where = [['cate','=',2],['status','=',1],['channel_id','=',$config['channel_id']],['keyword','=','attention']];
        $cur = myDb::getCur('WxReply', $where,'id,type,content,material');
        if($cur){
            if($cur['type'] == 1){
                wx::responseText($cur['content']);
            }else{
                wx::responseNews(json_decode($cur['material'],true));
            }
        }
        wx::responseText('欢迎关注!');
    }
    
    /**
     * 回复阅读记录
     * @param string $first_str
     */
    private function doHistoryReply($first_str){
    	$uid = myCache::getUidByOpenId(wx::$object['FromUserName']);
    	$urlData = myCache::getUrlInfo();
    	$str = mdMember::getReadStr($uid, $urlData['url']);
    	$first_str .= $str;
    	$first_str .= "\n\n";
    	$first_str .= '为方便下次阅读，请置顶公众号';
    	if(!$str){
    		$first_str .= "\n\n";
    		$href = '➢ <a href="http://'.$urlData['url'].'">进入书城</a>';
    		$first_str .= $href;
    	}
    	return $first_str;
    }
    
    /**
     * 处理发送过来的文字消息
     */
    private function textHandler(){
    	$config = myCache::getUrlInfo();
    	$where = [['status','=',1],['cate','=',1],['channel_id','=',$config['channel_id']],['keyword','=',wx::$object['Content']]];
    	$cur = myDb::getCur('WxReply',$where,'id,type,content,material');
        if($cur){
            if($cur['type'] == 1){
                wx::responseText($cur['content']);
            }else{
                wx::responseNews(json_decode($cur['material'],true));
            }
        }else{
            self::searchBook(); 
        }
    }
    
    /**
     * 搜索推荐小说并发送,当只匹配到一本小说时推送图文消息
     */
    private function searchBook(){
    	$path = '/index/Book/info.html';
    	$urlinfo = myCache::getUrlInfo();
    	$url = $urlinfo['is_location'] == 1 ? $urlinfo['location_url'] : $urlinfo['url'];
    	$where = [['status','=',1],['name','like','%'.wx::$object['Content'].'%']];
    	$books = myDb::getOnlyPageList('Book', $where, 'id,name,cover,summary', ['page'=>1,'limit'=>6]);
    	if(!empty($books)){
    		if(count($books) > 1){
    			$html = "我们找到以下书籍；";
    			$html.="\n\n";
    			foreach ($books as $v){
    				$link = "http://".$url.$path."?book_id=".$v['id'];
    				$a = '➢  <a href="'.$link.'">'.$v['name'].'</a>';
    				$html .= $a;
    				$html .= "\n\n";
    			}
    			wx::responseText($html);
    		}else{
    			$data = [
    				[
    					'title'=>$books[0]['name'],
    					'picurl' => $books[0]['cover'],
    					'url' => 'http://'.$url.$path.'?book_id='.$books[0]['id'],
    					'description' => $books[0]['summary']
    				]
    			];
    			wx::responseNews($data);
    		}
    	}else{
    		$str = '未找到匹配的内容';
    		$str .= "\n\n";
    		$str .= '请回复小说/漫画书名';
    		$str .= "\n\n";
    		$str .= '我们帮您查找其他内容哦......';
    		wx::responseText($str);
    	}
    }
    
}