<?php
namespace app\index\controller;

use site\myDb;
use site\myMsg;
use site\myHttp;
use site\myCache;
use site\myValidate;

class Com extends Common{
	
	//检测广告跳转
	public function checkAd(){
		$url = '/';
		$id = myHttp::postId('广告');
		$ad = myCache::getAdList();
		if($ad && isset($ad[$id])){
			$cur = $ad[$id];
			if($cur){
				$url = $cur['url'];
				myDb::setIncById('Ad', $id, 'click_num');
			}
		}
		res_api(['url'=>$url]);
	}
	
	//处理订阅书籍
	public function doSubBook(){
		parent::checkLogin();
		global $loginId;
		$rules = [
			'type'=>['require|in:1,2',['require'=>'参数错误','in'=>'未指定该参数']],
			'book_id'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']]
		];
		$subIds = myCache::getSubBookIds($loginId);
		$post = myValidate::getData($rules);
		switch ($post['type']){
			case 1:
				if($subIds && in_array($post['book_id'], $subIds)){
					res_api();
				}
				$re = myDb::add('BookSub', ['uid'=>$loginId,'book_id'=>$post['book_id']]);
				break;
			case 2:
				if(!$subIds || !in_array($post['book_id'], $subIds)){
					res_api();
				}
				$re = myDb::delByWhere('BookSub', [['uid','=',$loginId],['book_id','=',$post['book_id']]]);
				break;
		}
		if($re){
			myCache::rmSubBookIds($loginId);
			res_api();
		}else{
			res_api('操作失败');
		}
	}
	
	//书籍收藏
	public function doCollect(){
		parent::checkLogin();
		global $loginId;
		if(!$loginId){
			res_api('您尚未登陆,是否立即登录？',3);
		}
		$rules = ['book_id'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']]];
		$book_id = myValidate::getData($rules);
		$ids = myCache::getCollectBookIds($loginId);
		if($ids && in_array($book_id, $ids)){
			res_api();
		}
		$data = [
			'uid' => $loginId,
			'book_id' => $book_id,
			'create_time' => time()
		];
		$re = myDb::add('MemberCollect', $data);
		if($re){
			myCache::rmCollectBookIds($loginId);
			res_api();
		}else{
			res_api('收藏失败');
		}
	}
	
	
	//清空所有记录
	public function clearRecord(){
		parent::checkLogin();
		global $loginId;
		$rules = ['type'=>['require|in:1,2',['require'=>'参数错误','in'=>'未指定该参数']]];
		$type = myValidate::getData($rules);
		$where = [['uid','=',$loginId]];
		if($type == 1){
			$res = myDb::delByWhere('MemberCollect',$where);
			if($res){
				myCache::rmCollectBookIds($loginId);
			}
		}else{
			$where[] = ['status','=',1];
			$res = myDb::setField('ReadHistory', $where, 'status', 2);
			if($res){
				myCache::rmReadCache($loginId);
			}
		}
		if($res){
			res_api();
		}else{
			res_api('删除失败');
		}
	}
	
	//删除指定记录
	public function delRecord(){
		parent::checkLogin();
		global $loginId;
		$rules = [
			'type' => ['require|in:1,2',['require'=>'参数错误','in'=>'未指定该参数']],
			'book_id' => ['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数不规范','gt'=>'书籍参数不规范']]
		];
		$post = myValidate::getData($rules);
		$where = [['uid','=',$loginId],['book_id','=',$post['book_id']]];
		if($post['type'] == 1){
			$res = myDb::delByWhere('MemberCollect', $where);
			if($res){
				myCache::rmCollectBookIds($loginId);
			}
		}else{
			$where[] = ['status','=',1];
			$res = myDb::setField('ReadHistory', $where, 'status', 2);
			if($res){
				myCache::rmReadCache($loginId);
			}
		}
		if($res){
			res_api();
		}else{
			res_api('删除失败');
		}
	}
	
	//发送短信验证码
	public function sendRegCode(){
		$rules = ['phone' =>  ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']]];
		$phone = myValidate::getData($rules);
		myMsg::saiyouSend($phone);
	}
	
	//发送短信验证码
	public function sendLoginCode(){
		parent::checkLogin();
		global $loginId;
		$user = myCache::getMember($loginId);
		if(!$user['phone']){
			res_api('您尚未绑定手机号');
		}
		myMsg::saiyouSend($user['phone']);
	}
}