<?php
namespace app\index\controller;

use site\myCache;
use site\myValidate;
use think\Controller;
use app\common\model\mdMember;
use site\myDb;

class Common extends Controller{
	
	private $is_login_msg = 'yes';
    
    //初始化程序
    public function __construct(){
        parent::__construct();
        //$this->redirect(request()->domain().'/index.html',[],301);
        //$url= request()->domain().'/index.html';
        //header("location:".$url);
        //die;
        $server_url = $_SERVER['HTTP_HOST'];
        if(!$server_url){
        	res_error('非法访问');
        }
        $urlData = myCache::getUrlInfo($server_url);
        if(!$this->request->isAjax()){
        	$kc_code = myValidate::getData(['kc_code'=>['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]],'get');
        	if($kc_code){
        		session('INDEX_KC_CODE',$kc_code);
        	}
        }
        global $loginId;
        $loginId = cookie('INDEX_LOGIN_ID');
        $loginId = $loginId ? : 0;
        $theme = 'red';
        if(!$this->request->isAjax()){
        	if($loginId){
        		self::checkIsLogin($loginId);
        		self::saveActive($loginId);
        	}
        	$this->assign('page_title',$urlData['name']);
        	if($loginId){
        		$theme = myCache::getMemberTheme($loginId);
        		$theme = $theme ? : 'red';
        	}
        	$this->assign('theme',$theme);
        	$this->assign('is_login_msg',$this->is_login_msg);
        }
    }
    
    //发送未登录奖励
    private function checkIsLogin($uid){
    	$res = myCache::checkIsMsg($uid);
    	if(!$res){
    		$where = [['uid','=',$uid],['type','=',2],['create_date','=',date('Y-m-d')]];
    		$isSend = myDb::getValue('MemberLog',$where,'id');
    		if(!$isSend){
    			$config = getMyConfig('website');
    			$money = $config && $config['login_money'] ? $config['login_money'] : 0;
    			if($money){
    				$res = mdMember::addLoginMoney($uid, $money);
    				if($res){
    					myCache::checkIsMsg($uid,1);
    					$this->assign('login_money',$money);
    					$this->is_login_msg = 'no';
    				}
    			}
    		}else{
    			myCache::checkIsMsg($uid,1);
    		}
    	}
    }
    
    //检查登录
    protected function checkLogin(){
    	global $loginId;
    	if(!$loginId){
    		if($this->request->isAjax()){
    			res_api('您尚未登录,是否立即登录',3);
    		}else{
    			$this->redirect('login/index');
    		}
    	}
    }
    
    //记录活动信息
    private function saveActive($uid){
    	$date = date('Y-m-d');
    	$key = 'user_active_'.$date.$uid;
    	if(!cache('?'.$key)){
    		$data = [
    			'uid' => $uid,
    			'create_date' => $date
    		];
    		$re = myDb::add('MemberActive', $data);
    		if($re){
    			cache($key,1,86400);
    		}
    	}
    }
    
    public function _empty(){
        
        res_error('页面不存在');
    }
    
}