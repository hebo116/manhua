<?php
namespace app\index\controller;

use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdOrder;
use site\myDb;

class Charge extends Common{
	
	public function __construct(){
		parent::__construct();
		parent::checkLogin();
	}
	
    //充值首页
    public function index(){
    	if($this->request->isAjax()){
    		self::doCharge();
    	}else{
    		global $loginId;
    		$cur = myCache::getMember($loginId);
    		$cur['money'] = myCache::getMemberMoney($loginId);
    		$get = myHttp::getData('book_id','get');
    		$charge = myCache::getSiteCharge();
    		$coins = $vips = [];
    		$check_money = 0;
    		foreach ($charge as $k=>$v){
    			if($v['status'] != 1){
    				unset($charge[$k]);
    				continue;
    			}
    			if($v['pay_times']){
    				$charge_num = myCache::getChargeNum($loginId, $v['id']);
    				if($charge_num >= $v['pay_times']){
    					unset($charge[$k]);
    					continue;
    				}
    			}
    			if($v['type'] == 1){
    				$coins[] = $v;
    			}else{
    				$vips[] = $v;
    			}
    			if($v['is_check'] == 1){
    				$check_money = $v['money'];
    			}
    		}
    		$variable = [
    			'get' => $get,
    			'cur' => $cur,
    			'coins' => $coins,
    			'vips' => $vips,
    			'check_money' => $check_money,
    			'paylist' => self::getPayList()
    		];
    		return $this->fetch('index',$variable);
    	}
    }
    
    //获取选择支付方式
    private function getPayList(){
    	$list = [
    		'wxh5pay'			=>	['id'=>1,'is_check'=>2,'icon'=>'weixinpay.png'],
    		'site_otherwxpay'	=>	['id'=>2,'is_check'=>2,'icon'=>'weixinpay.png'],
    		'site_otheralipay'	=>	['id'=>3,'is_check'=>2,'icon'=>'alipay.png']
    	];
    	if(!checkIsWeixin()){
    		unset($list['wxh5pay']);
    		unset($list['site_otherwxpay']);
    	}
    	$is_check = false;
    	$config = getMyConfig('website');
    	foreach ($list as $key=>&$val){
    		$payConfig = getMyConfig($key);
    		if($payConfig && $payConfig['is_on'] == 1){
    			$val['name'] = $payConfig['name'];
    			$val['summary'] = $payConfig['summary'];
    			if($config['pay_type'] == $val['id']){
    				$val['is_check'] = 1;
    				$is_check = true;
    			}
    		}else{
    			unset($list[$key]);
    		}
    	}
    	if($list){
    		$list = array_values($list);
    		if(!$is_check){
    			$list[0]['is_check'] = 1;
    		}
    	}
    	return $list;
    }
    
    //普通充值
    private function doCharge(){
    	global $loginId;
    	$book_id = myValidate::getData(['book_id' => ['number|gt:0',['number'=>'书籍参数错误','gt'=>'书籍参数错误']]],'get');
    	$rules = [
    		'pay_type' => ['require|between:1,3',['require'=>'请选择支付方式','between'=>'未指定该支付方式']],
    		'charge_id' => ['require|number|gt:0',['require'=>'充值信息异常','number'=>'充值信息异常','gt'=>'充值信息异常']]
    	];
    	$post = myValidate::getData($rules);
    	$chargeList = myCache::getSiteCharge();
    	if(!$chargeList || !isset($chargeList[$post['charge_id']])){
    		res_api('未开启该支付方式');
    	}
    	$charge = $chargeList[$post['charge_id']];
    	$time = time();
    	if($charge['pay_times']){
    		$chargeNum = myCache::getChargeNum($loginId, $post['charge_id']);
    		if($chargeNum >= $charge['pay_times']){
    			res_api('该套餐仅限充值'.$charge['pay_times'].'次');
    		}
    	}
    	$member = myCache::getMember($loginId);
        $data = mdOrder::createOrderInfo($member);
        $data['charge_id'] = $post['charge_id'];
        $data['money'] = $charge['money'] - 1;
        if($charge['type'] == 1){
        	$data['send_money'] = $charge['content']['coin'];
        	if($charge['content']['send_coin']){
        		$data['send_money'] += $charge['content']['send_coin'];
        	}
        }else{
        	$data['package'] = $charge['content']['vip_type'];
        }
        $data['type'] = 1;
        $data['create_date'] = date('Ymd',$time);
        $data['create_time'] = $time;
        $data['order_no'] = mdOrder::createOrderNo();
        $config = getMyConfig('website');
        $data['pay_type'] = $post['pay_type'];
        if($book_id){
        	$data['book_id'] = $book_id;
        }
        if($member['spread_id']){
        	$data['spread_id'] = $member['spread_id'];
        }
        $count_info = [];
        if($data['channel_id']){
        	$curInfo =  mdOrder::getCountInfo($data['channel_id'], $data['money']);
        	if($curInfo){
        		$count_info['channel'] = $curInfo;
        	}
        }
        if($data['agent_id']){
        	$curInfo = mdOrder::getCountInfo($data['agent_id'], $data['money']);
        	if($curInfo){
        		$count_info['agent'] = $curInfo;
        	}
        }
        $data['count_info'] = json_encode($count_info);
        $res = myDb::add('Order', $data);

        if($res){
        	$back_url = 'http://'.$_SERVER['HTTP_HOST'].'/index/User/index.html';
        	$pay_url = mdOrder::createPayUrl($data['order_no'], $data['pay_type'],$back_url);
        	res_api(['url'=>$pay_url]);
        }else{
        	res_api('创建订单失败');
        }
    }
    
    
    //活动充值
    public function doActivityCharge(){
        global $loginId;
        $member = myCache::getMember($loginId);
        $activity_id = myHttp::postId('活动','activity_id');
        $activity = myCache::getActivity($activity_id);
        if(!$activity){
            res_api('该活动不存在');
        }
        if($activity['is_first'] == 1){
        	$acUids = myCache::getActivityUids($activity_id);
        	if(in_array($loginId, $acUids)){
        		res_api('该活动仅限充值一次');
        	}
        }
        $time = time();
        if($activity['start_time'] > $time){
            res_api('该活动尚未开始');
        }
        if($activity['end_time'] < $time){
        	res_api('该活动已结束');
        }
        if(floatval($activity['money']) <= 0){
        	res_api('充值金额异常');
        }
        $data = mdOrder::createOrderInfo($member);
        $data['type'] = 2;
        $data['create_time'] = $time;
        $data['order_no'] = mdOrder::createOrderNo();
        $data['ptype'] = 0;
        $data['pid'] = $activity['id'];
        $data['money'] = $activity['money'] - 1;
        $data['send_money'] = $activity['send_money'];
        if(floatval($data['money']) <= 0){
        	res_api('充值金额异常');
        }
        if($member['spread_id']){
        	$data['spread_id'] = $member['spread_id'];
        }
        $config = getMyConfig('website');
        $data['pay_type'] = isset($config['pay_type']) && in_array($config['pay_type'], [1,2,3]) ? $config['pay_type'] : 1;
        $count_info = [];
        if($data['channel_id']){
        	$curInfo =  mdOrder::getCountInfo($data['channel_id'], $data['money']);
        	if($curInfo){
        		$count_info['channel'] = $curInfo;
        	}
        }
        if($data['agent_id']){
        	$curInfo = mdOrder::getCountInfo($data['agent_id'], $data['money']);
        	if($curInfo){
        		$count_info['agent'] = $curInfo;
        	}
        }
        $data['count_info'] = json_encode($count_info);
        $res = myDb::add('Order', $data);
        if($res){
        	$back_url = 'http://'.$_SERVER['HTTP_HOST'].'/index/User/index.html';
        	$pay_url = mdOrder::createPayUrl($data['order_no'],$data['pay_type'],$back_url);
        	res_api(['url'=>$pay_url]);
        }else{
            res_api('创建订单失败');
        }
    }

    public function notice(){
        return $this->fetch('notice',['cur'=>['money'=>111,'order_no'=> 12121212]]);
    }
    
}