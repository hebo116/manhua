<?php
namespace app\agent\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use app\common\model\mdBook;
use app\common\model\mdSpread;
use site\myCache;


class Cartoon extends Common{
    
    //漫画列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
            	'default' => [['status','=',1]],
            	'eq' => 'over_type,free_type,is_god',
            	'like' => 'keyword:name,category'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdBook::getBookPageList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$free_time = '';
                	if(myCache::checkBookFree($v['id'])){
                		$free_time = '未知';
                		$freeItem = myDb::getCur('BookFree', [['book_id','=',$v['id']]],'start_date,end_date');
                		if($freeItem){
                			$free_time = $freeItem['start_date'].'~'.$freeItem['end_date'];
                		}
                	}
                	$v['free_time'] = $free_time;
                	$v['total_chapter'] = myCache::getBookChapterNum($v['id']);
                    $v['spread_url'] = my_url('Spread/createLink',['book_id'=>$v['id']]);
                    $v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
        	$category = getMyConfig('cartoon_category');
        	$keyword = $this->request->get('keyword');
        	$variable = ['category'=>$category,'keyword'=>$keyword];
        	return $this->fetch('common@index-agent',$variable);
        }
    }
    
    //复制链接
    public function copyLink(){
    	global $loginId;
        $id = myHttp::getId('漫画');
        $book = myDb::getById('Book',$id,'id,name');
        if(!$book){
            res_api('书籍参数错误');
        }
        $code = myCache::getAgentCode($loginId);
        if(!$code){
        	res_api('代理数据异常');
        }
        $url = mdSpread::getSpreadUrl($loginId);
        $url .= '/Index/Book/info.html?kc_code='.$code.'&book_id='.$id;
        $data = ['links' => [['title'=>'漫画链接','val'=>$url]]];
        return $this->fetch('common@public/copyLink',$data);
    }
    
}