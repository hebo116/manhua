<?php
namespace app\agent\controller;

use site\myDb;
use site\myCache;
use site\myValidate;
use app\common\model\mdLogin;
use app\common\model\mdMessage;
use app\common\model\mdConsole;

class Index extends Common
{
    
    //后台首页
    public function index(){
    	global $loginId;
        $cur = myCache::getChannel($loginId);
        $variable = [
        	'cur' => $cur,
            'site_name' => $cur['name']
        ];
        return $this->fetch('common@index/index-agent',$variable);
    }
    
    //控制台
    public function console(){
    	global $loginId;
        $number = mdConsole::getAgentNumbersData();
        $key = parent::getCurKeyId();
        $charge_rank = mdConsole::getChargeRank([['a.status','=',2],['a.is_count','=',1],['a.'.$key,'=',$loginId]]);
        $variable = [
            'number' => $number,
            'charge_rank' => $charge_rank,
        ];
        return $this->fetch('common@index/console-agent',$variable);
    }
    
    //检查是否有未读公告
    public function getReadMessage(){
        $res = mdMessage::checkMessage();
        if($res['id'] > 0){
            $res['url'] = my_url('Message/showInfo',['id'=>$res['id']]);
        }
        res_api($res);
    }
    
    //获取用户趋势图
    public function getUserChartData(){
    	global $loginId;
        $data = mdConsole::getUserChartData($loginId);
        res_api($data);
    }
    
    
    //基础信息
    public function userinfo(){
        global $loginId;
        if($this->request->isAjax()){
        	$rules = ['name' =>  ["require|max:20",["require"=>"请输入用户名称",'max'=>'用户名称最多支持20个字符']]];
        	$name = myValidate::getData($rules);
            $re = myDb::setField('Channel',[['id','=',$loginId]], 'name', $name);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = myCache::getChannel($loginId);
            $login_msg = mdLogin::getLastLoginMsg($cur['login_name'],2);
            if($login_msg){
                $cur = array_merge($cur,$login_msg);
            }
            $variable = [
            	'cur' => $cur,
            	'js' => getJs('index.userinfo','channel')
            ];
            return $this->fetch('common@index/userinfo',$variable);
        }
    }
    
    //修改密码
    public function password(){
        if($this->request->isAjax()){
            global $loginId;
            $rules = [
            	"old_pwd" => ["require|length:6,16",["require"=>"请输入原密码","length"=>"请输入6到16位原密码"]],
            	"new_pwd" => ["require|length:6,16",["require"=>"请输入新密码","length"=>"请输入6到16位新密码"]],
            	"re_pwd"  => ["require|confirm:new_pwd",["require"=>"请再次输入新密码","confirm"=>"两次输入密码不一致"]],
            ];
            $data = myValidate::getData($rules);
            $cur = myDb::getById('Channel', $loginId,'id,password');
            if(createPwd($data['old_pwd']) !== $cur['password']){
                res_api('原密码输入不正确');
            }
            $password = createPwd($data['new_pwd']);
            $re = myDb::setField('Channel',[['id','=',$loginId]],'password', $password);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            
        	return $this->fetch('common@index/password');
        }
    }
    
    //退出登录
    public function logOut(){
        session('AGENT_LOGIN_ID',null);
        $url = my_url('Login/index');
        echo "<script language=\"javascript\">window.open('".$url."','_top');</script>";
        exit;
    }

}
