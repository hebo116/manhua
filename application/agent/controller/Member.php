<?php
namespace app\agent\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use app\common\model\mdMember;

class Member extends Common{
    
    //用户列表
    public function index(){
        if($this->request->isAjax()){
            global $loginId;
            $key = parent::getCurKeyId();
            $config = [
                'default' => [['status','between',[1,2]],[$key,'=',$loginId]],
                'eq' => 'status,subscribe',
                'like' => 'keyword:username'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $field = 'id,username,headimgurl,phone,money,status,create_time';
            $res = myDb::getPageList('Member', $where, $field, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                    $v['info_url'] = my_url('Member/info',['id'=>$v['id']]);
                    $v['status_name'] = ($v['status'] == 1) ? '正常' : '禁用';
                    $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                    $v['phone'] = $v['phone'] ? $v['phone'] : '未绑定';
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
            return $this->fetch('common@member/index',['js'=>getJs('member.index','agent')]);
        }
    }
    
    //用户详情
    public function info(){
    	global $loginId;
        $id = myHttp::getId('用户');
        $cur = myDb::getById('Member',$id,'id,username,headimgurl,money,total_money,viptime,create_time');
        if(empty($cur)){
            res_api('用户信息异常');
        }
        $count = mdMember::getMemberCountMsg($id,$loginId);
        $cur['charge_money'] = $count['charge'];
        $cur['consume_money'] = $count['consume'];
        $variable = [
            'cur' => $cur,
            'url' => [
            	'charge' => my_url('getRecordList',['uid'=>$id,'type'=>1]),
            	'read' => my_url('getRecordList',['uid'=>$id,'type'=>2]),
            	'consume' => my_url('getRecordList',['uid'=>$id,'type'=>3])
            ],
        	'js' => getJs('member.info','agent',['date'=>'20200301'])
        ];
        return $this->fetch('common@member/info',$variable);
    }
    
    //获取各种记录列表
    public function getRecordList(){
    	global $loginId;
    	$rules = ['type'=>['require|between:1,5',['require'=>'请选择查看类型','between'=>'未指定该查看类型']]];
    	$type = myValidate::getData($rules,'get');
    	$pages = myHttp::getPageParams();
        $key = parent::getCurKeyId();
        global $loginId;
        switch ($type){
            case 1:
                $config = [
                    'default' => [[$key,'=',$loginId],['is_count','=',1]],
                    'eq' => 'uid,status'
                ];
                $where = mySearch::getWhere($config);
                $res = mdMember::getChargeOrder($where,$pages);
                break;
            case 2:
            	$uid = $this->request->get('uid');
            	$res = mdMember::getReadHistory($uid);
                break;
            case 3:
                $config = ['eq' => 'uid'];
                $where = mySearch::getWhere($config);
                $res = mdMember::getConsumeList($where,$pages);
                break;
            default:
                res_api('请求数据异常');
                break;
        }
        res_table($res['data'],$res['count']);
    }
    
}