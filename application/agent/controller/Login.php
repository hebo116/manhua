<?php
namespace app\agent\controller;

use site\myValidate;
use think\Controller;
use think\captcha\Captcha;
use app\common\model\mdLogin;

class Login extends Controller{
    
    //登录页面
    public function index(){
        if($this->request->isAjax()){
        	$rules = mdLogin::getLoginRules();
        	$data = myValidate::getData($rules);
        	$captcha = new Captcha();
        	if(!$captcha->check($data['verify_code'])){
        		res_api('验证码输入错误');
        	}
        	mdLogin::channelLogin($data['login_name'], $data['password'],2);
            $url = my_url('Index/index');
            res_api(['url'=>$url]);
        }else{
        	$web = getMyConfig('website');
        	$title = (isset($web['name']) && $web['name']) ? $web['name'] : '凯驰网络';
        	$variable = [
        		'site' => '代理',
        		'title' => $title
        	];
        	return $this->fetch('common@login/index',$variable);
        }
    }
    
    //验证码
    public function verify(){
    	$config =    [
    		'fontSize'    =>    30,
    		'length'      =>    4,
    		'codeSet' 	  => 	'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789'
    	];
    	$captcha = new Captcha($config);
    	return $captcha->entry();
    }
}