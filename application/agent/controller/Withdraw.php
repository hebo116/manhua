<?php
namespace app\agent\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use app\common\model\mdAgent;
use app\common\model\mdWithdraw;

class Withdraw extends Common{
    
    //我的结算
    public function index(){
    	global $loginId;
        if($this->request->isAjax()){
        	$config = [
        		'default' => [['channel_id','=',$loginId],['status','between',[0,1]]],
        		'eq' => 'status:status',
        		'between' => 'between_time:cur_date%date'
        	];
        	$pages = myHttp::getPageParams();
        	$where = mySearch::getWhere($config);
        	$res = myDb::getPageList('Withdraw', $where, '*', $pages);
        	if($res['data']){
        		$opts = [];
        		foreach ($res['data'] as &$v){
        			$v['bank_info'] = json_decode($v['bank_info'],true);
        			if($v['status'] == 1){
        				$v['opt_name'] = '未知';
        				$v['pay_time'] = $v['pay_time'] ? date('Y-m-d H:i:s',$v['pay_time']) : '--';
        				if($v['opt_id']){
        					if($v['to_channel_id'] > 0){
        						$opt = myCache::getChannel($v['opt_id']);
        						if($opt){
        							$v['opt_name'] = $opt['name'];
        						}
        					}else{
        						if(!array_key_exists($v['opt_id'], $opts)){
        							$opts[$v['opt_id']] = myDb::getValue('Manage',[['id','=',$v['opt_id']]],'name');
        						}
        						if($opts[$v['opt_id']]){
        							$v['opt_name'] = $opts[$v['opt_id']];
        						}
        					}
        				}
        			}
        			$v['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
        		}
        	}
            res_table($res['data'],$res['count']);
        }else{
        	$cur = mdWithdraw::getMyCountData($loginId);
            return $this->fetch('common@withdraw/mine',['cur'=>$cur]);
        }
    }
    
    //设置账号
    public function setAccount(){
        global $loginId;
        if($this->request->isAjax()){
        	$data = mdAgent::getAccountData();
        	$re = myDb::save('Channel',[['id','=',$loginId]], $data);
            if($re){
            	myCache::rmChannel($loginId);
                res_api();
            }else{
                res_api('设置失败');
            }
        }else{
            $cur = myCache::getChannel($loginId);
            $variable = ['cur' => $cur];
            return $this->fetch('common@public/setAccount',$variable);
        }
    }
    
}
