<?php
namespace app\agent\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdAgent;
use app\common\model\mdWithdraw;

class Agent extends Common{
    
    //初始化检测是否总站一级代理
    public function __construct(){
        parent::__construct();
        global $loginId;
        $channel = myCache::getChannel($loginId);
        if($channel['type'] != 1){
            res_api('您无权访问该页面');
        }
    }
    
    //代理列表
    public function index(){
        if($this->request->isAjax()){
            global $loginId;
            $config = [
                'default' => [['status','between',[0,3]],['type','=',2],['parent_id','=',$loginId]],
                'eq' => 'status',
                'like' => 'keyword:name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdAgent::getList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$v['status_name'] = mdAgent::getStatusName($v['status']);
                    $v['money'] = myCache::getChannelAmount($v['id']);
                    $v['do_url'] = my_url('doAgent',['id'=>$v['id']]);
                    $v['ratio'] .= '%';
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
        	return $this->fetch('common@agent/index',['js'=>getJs('agent.index','agent')]);
        }
    }
    
    //新增代理
    public function addAgent(){
    	global $loginId;
        if($this->request->isAjax()){
        	$rules = mdAgent::getRules();
            $data = myValidate::getData($rules);
            $data['parent_id'] = $loginId;
            $data['type'] = 2;
            $data['status'] = 0;
            mdAgent::doneAgent($data);
        }else{
            $channel = myCache::getChannel($loginId);
            $field = 'id,name,login_name,url,ratio:80,bank_user,bank_name,bank_no';
            $cur = myDb::buildArr($field);
            $variable = ['cur'=>$cur,'backUrl'=>my_url('index'),'ratio'=>$channel['ratio']];
            return $this->fetch('common@doAgent-agent',$variable);
        }
    }
    
    //编辑代理
    public function doAgent(){
        if($this->request->isAjax()){
        	$rules = mdAgent::getRules(0);
            $data = myValidate::getData($rules);
            mdAgent::doneAgent($data);
        }else{
            global $loginId;
            $channel = myCache::getChannel($loginId);
            $id = myHttp::getId('代理');
            $cur = myDb::getById('Channel', $id);
            if(!$cur){
                res_api('渠道不存在');
            }
            $variable = ['cur'=>$cur,'backUrl'=>my_url('index'),'ratio'=>$channel['ratio']];
            return $this->fetch('common@doAgent-agent',$variable);
        }
    }
    
    //处理代理状态
    public function doAgentEvent(){
    	$rules = mdAgent::getEventRules();
        $data = myValidate::getData($rules);
        $cur = myDb::getById('Channel',$data['id'],'id,url');
        if(!$cur){
            res_api('代理信息异常');
        }
        $key = 'status';
        switch ($data['event']){
            case 'on':
                $value = 1;
                break;
            case 'off':
                $value = 2;
                break;
            case 'delete':
                $value = 4;
                break;
            case 'resetpwd':
                $key = 'password';
                $value = createPwd(123456);
                break;
            default:
                res_api('未指定该事件');
                break;
        }
        if($value === 4){
        	$saveData = ['status'=>4,'login_name'=>'','url'=>''];
        	$re = myDb::save('Channel',[['id','=',$cur['id']]],$saveData);
        }else{
        	$re = myDb::setField('Channel', [['id','=',$cur['id']]],$key, $value);
        }
        if($re){
            if($key === 'status'){
                if($cur['url']){
                    myCache::rmUrlInfo($cur['url']);
                }
                myCache::rmChannel($data['id']);
            }
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    //提现申请列表
    public function withdraw(){
    	global $loginId;
    	$status = myValidate::getData(['status'=>['in:0,1',['in'=>'非法访问']]],'get');
    	$status = $status ? : 0;
    	if($this->request->isAjax()){
    		$config = [
    			'between' => 'between_time:cur_date%date',
    			'default' => [['to_channel_id','=',$loginId],['status','=',$status]]
    		];
    		$where = mySearch::getWhere($config);
    		switch ($status){
    			case 0:
    				$list = mdWithdraw::getWaitList($where);
    				res_table($list);
    				break;
    			case 1:
    				$pages = myHttp::getPageParams();
    				$res = mdWithdraw::getPassList($where, $pages,true);
    				res_table($res['data'],$res['count']);
    				break;
    		}
    		res_api('非法访问');
    	}else{
    		switch ($status){
    			case 0:
    				$js = getJs('withdraw.wait','admin',['date'=>'20200310']);
    				break;
    			case 1:
    				$js = getJs('withdraw.pass','admin',['date'=>'20200310']);
    				break;
    		}
    		$cur = mdWithdraw::getChildCountData($loginId);
    		return $this->fetch('common@withdraw/index-agent',['cur'=>$cur,'status'=>$status,'js'=>$js]);
    	}
    }
    
    //获取导出数据
    public function exportData(){
    	global $loginId;
    	$where = [['to_channel_id','=',$loginId],['status','=',0]];
    	$data = mdWithdraw::getExportData($where);
    	res_table($data);
    }
    
    //处理提现申请
    public function doWithdraw(){
    	global $loginId;
    	$rules = ['ids' => ['require|array',['require'=>'账单参数错误','array'=>'账单格式不规范']]];
    	$signs = myValidate::getData($rules);
    	$num = 0;
    	foreach ($signs as $v){
    		$re = mdWithdraw::doPass($v,$loginId);
    		if($re){
    			$num++;
    		}
    	}
    	res_api(['num'=>$num]);
    }
    
}