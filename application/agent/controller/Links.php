<?php
namespace app\agent\controller;

use site\myCache;
use site\myLinks;
use app\common\model\mdSpread;

class Links extends Common{
	
	//获取链接
	public function index(){
		global $loginId;
		$list = myLinks::getAll();
		$url = mdSpread::getSpreadUrl($loginId);
		$code = myCache::getAgentCode($loginId);
		$param = ['kc_code'=>$code];
		$str = '?'.http_build_query($param);
		foreach ($list as &$v){
			foreach ($v['links'] as &$val){
				$val['url'] .= $str;
				$val['long_url'] = $url.$val['url'];
			}
		}
		return $this->fetch('common@links/index',['list'=>$list]);
	}
}