<?php

use think\Db;
use site\myHttp;
use think\facade\Request;

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * 打印数据
 * @param mixed $data 需要打印的数据
 * @param string $isdump 是否打印数据类型
 */
function my_print($data,$isdump=false){
    echo '<meta charset="utf-8"><pre>';
    if(!$isdump){
        print_r($data);
    }else{
        var_dump($data);
    }
    exit;
}

/**
 * 通用api返回信息
 * @param string $msg 消息
 * @param number $code 消息代码
 * @param mixed $data 返回数据
 */
function res_api($msg='ok',$code=2,$data=null){
	if(is_array($msg)){
		$data = $msg;
		$msg = 'ok';
		$code = $code === 2 ? 1 : $code;
	}else{
		$code = ($msg === 'ok' && $code === 2) ? 1 : $code;
	}
	$res = ['code' => $code,'msg' => $msg,'data' => $data];
	exit(json_encode($res,JSON_UNESCAPED_UNICODE));
}

/**
 * 输出错误信息
 * @param string $msg
 */
function res_error($msg){
	if(Request::isAjax()){
		$res = [
			'code' => 2,
			'msg' => $msg,
			'data' => null,
		];
		exit(json_encode($res,JSON_UNESCAPED_UNICODE));
	}else{
		header("Content-type:text/html;charset=utf-8");
		$str = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
		$str .= '<html>';
		$str .= '<head>';
		$str .= '<title>出错了！！！</title>';
		$str .= '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">';
		$str .= '<style type="text/css">';
		$str .= 'html,body,div,span,a,img,header{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline;}';
		$str .= 'header{display: block;}';
		$str .= 'a{text-decoration:none;}';
		$str .= 'img{max-width:100%;}';
		$str .= 'body{background: url(/static/common/images/errorbg.png);font-family: "Century Gothic",Arial, Helvetica, sans-serif;font-weight:100%;}';
		$str .= '.header{height:80px;}';
		$str .= '.content p{margin: 15px 0px 22px 0px;font-family: "Century Gothic";font-size: 1.5em;color:red;text-align:center;}';
		$str .= '.content  span,.logo h1 a{color:#e54040;}';
		$str .= '.content span{font-size: 1.5em;}';
		$str .= '.content{text-align:center;padding: 25px 0px 0px 0px;margin: 0px 12px;}';
		$str .= '.content a{color:#fff;font-family: "Century Gothic";background: #666666;padding: 8px 17px;}';
		$str .= '.content a:hover{color:#e54040;}';
		$str .= '</style>';
		$str .= '</head>';
		$str .= '<body>';
		$str .= '<div class="wrap">';
		$str .= '<div class="header"></div>';
		$str .= '<div class="content">';
		$str .= '<img src="/static/common/images/errorimg.png" title="error" />';
		$str .= '<p>'.$msg.'</p>';
		$str .= '<a href="javascript:void(0);" onclick="javascript:history.go(-1);">Back</a>';
		$str .= '</div>';
		$str .= '</div>';
		$str .= '</body>';
		$str .= '</html>';
		echo $str;
		exit;
	}
	
}

/**
 * 返回表格数据
 * @param mixed $data
 * @param number $count
 */
function res_table($data,$count=0,$other=''){
	$res = [
		'code' => 1,
		'msg' => 'ok',
		'data' => $data,
		'count' => $count,
	];
    if ($other){
        $res['other'] = $other;
    }
	exit(json_encode($res,JSON_UNESCAPED_UNICODE));
}

/**
 * 构建url
 * @param string $url 模块名
 * @param array $param 参数
 * @param array $is_http 是否加入域名
 */
function my_url($url,$param=[],$is_http=false){
    $link = url($url);
    if($param){
        $str = is_array($param) ? http_build_query($param) : $param;
        $link .= '?'.$str;
    }
    return $link;
}

//获取js资源路径
function getJs($name,$module='admin',$param=[]){
	$path = '/static/resource/'.$module.'/'.$name.'.js';
	if(!empty($param) && is_array($param)){
		$path .= '?'.http_build_query($param);
	}
	return $path;
}

/**
 * 构建单选html
 * @param array $config 单选配置
 * @param string $val 默认选中值
 * @return string 单选表单
 */
function createRadioHtml($config,$val=''){
    $str = '';
    foreach ($config['option'] as $v){
        $checked = '';
        if(strlen($val) > 0){
            $checked = ($val == $v['val']) ? 'checked="checked"' : '';
        }else{
            $checked = $v['default'] ? 'checked="checked"' : '';
        }
        $str .= '<input type="radio" lay-filter="'.$config['name'].'" name="'.$config['name'].'" value="'.$v['val'].'" title="'.$v['text'].'" '.$checked.' />';
    }
    return $str;
}

/**
 * 构建下拉框选项列表
 * @param array $list 选项值
 * @param string $val 选中值
 * @param string $default_str 没有选中值时选项
 * @return string
 */
function createSelectHtml($list,$val='',$default_str=''){
    $str = '';
    if($default_str){
        $str = '<option value="">'.$default_str.'</option>';
    }
    foreach ($list as $v){
        $selected = '';
        if($val != '' && $val == $v['id']){
            $selected = 'selected="selected"';
        }
        $str .= '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
    }
    return $str;
}

//构建复选框
function createCheckBoxHtml($list,$name,$cur){
	$str = '';
	if($list && is_array($list)){
		$check = [];
		if($cur){
			if(!is_array($cur)){
				$check = explode(',', trim($cur,','));
			}else{
				$check = $cur;
			}
		}
		
		foreach ($list as $v){
			$checked = '';
			if(is_array($v)){
				if(in_array($v['id'], $check)){
					$checked = 'checked';
				}
				$str .= '<input type="checkbox" name="'.$name.'[]" value="'.$v['id'].'" lay-skin="primary" title="'.$v['name'].'" '.$checked.' >';
			}else{
				if(in_array($v,$check)){
					$checked = 'checked';
				}
				$str .= '<input type="checkbox" name="'.$name.'[]" value="'.$v.'" lay-skin="primary" title="'.$v.'" '.$checked.' >';
			}
		}
	}
	return $str;
}

/**
 * 读取级联菜单
 * @param array $list
 * @param string $pk
 * @param string $pid
 * @param string $child
 * @param number $startPid
 * @return unknown
 */
function list_to_tree($list, $pk='id', $pid = 'pid', $child = '_child', $startPid = 0) {
    $tree = array();
    if(is_array($list)) {
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] =& $list[$key];
        }
        foreach ($list as $key => $data) {
            $parentId =  $data[$pid];
            if ($startPid == $parentId) {
                $tree[] =& $list[$key];
            }else{
                if(isset($refer[$parentId])) {
                    $parent =& $refer[$parentId];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
    }
    $tree = getIslast($tree);
    return $tree;
}

/**
 * 判断级联菜单在同一级中是否为最后一个
 * @param unknown $list
 * @param number $level
 * @return unknown
 */
function getIslast($list,$level=0){
    foreach($list as $k=>&$v){
        $v['level'] = $level+1;
        $count = count($list)-1;
        if($k==$count){
            $v['islast'] = "Y";
        }else{
            $v['islast'] = "N";
        }
        if(isset($v['_child'])){
            getIslast($v['_child'],$v['level']);
        }
    }
    return $list;
}

/**
 * 构建后台主菜单
 * @param array $menu 菜单数组
 * @param string $target 指向iframe
 */
function createMenu($menu){
    $str = '';
    foreach ($menu as $v){
        $line_str = '';
        if(!array_key_exists('child', $v)){
            $line_str .= 'lay-tips="'.$v['name'].'" lay-direction="2" ';
            if($v['url']){
                if(stripos($v['url'],'http')){
                    $line_str .= 'href="'.$v['url'].'" target="iframe-main"';
                }else{
                    $line_str .= 'href="'.my_url($v['url']).'" target="iframe-main"';
                }
            }
        }
        $str .= '<li class="layui-nav-item">';
        $str .= '<a '.$line_str.' >';
        $str .= '<i class="layui-icon '.$v['icon'].'"></i>';
        $str .= '<cite>'.$v['name'].'</cite>';
        $str .= '</a>';
        if(array_key_exists('child', $v)){
            $str .= createChildMenu($v['child']);
        }
        $str .= '</li>';
    }
    return $str;
}

/**
 * 构建后台子菜单
 * @param array $menu
 * @param string $target 指向iframe
 * @return string
 */
function createChildMenu($menu){
    $str = '<dl class="layui-nav-child">';
    foreach ($menu as $v){
        if(array_key_exists('child', $v)){
            $str .= '<dd class="layui-nav-item">';
            $str .= '<a href="javascript:;">'.$v['name'].'</a>';
            $str .= createChildMenu($v['child']);
        }else{
            $line_str = '';
            if(!array_key_exists('child', $v)){
                $line_str .= 'lay-tips="'.$v['name'].'" lay-direction="2" ';
                if($v['url']){
                    if(stripos($v['url'], 'http')){
                        $line_str .= 'href="'.$v['url'].'" target="iframe-main"';
                    }else{
                        $line_str .= 'href="'.my_url($v['url']).'" target="iframe-main"';
                    }
                }
            }
            $str .= '<dd>';
            $str .= '<a '.$line_str.'>'.$v['name'].'</a>';
        }
        $str .= '</dd>';
    }
    $str .= '</dl>';
    return $str;
}

/**
 * 获取我的配置
 * @param string $key
 * @return number|mixed|\think\cache\Driver|boolean
 */
function getMyConfig($key){
	$re = cache('?'.$key);
	if($re){
		$data = cache($key);
	}else{
		$data = Db::name('Config')->where('key','=',$key)->where('status','=',1)->value('value');
		$data = json_decode($data,true);
		$data = $data ? : 0;
		cache($key,$data);
	}
	return $data;
}


//创建登录密码
function createPwd($str){
    $key = 'kaichiwangluo';
    $str .= $key;
    $res = md5(sha1($str));
    return $res;
}

function handleImg($img){
    return str_replace("http://comic.op1234.xyz","https://api.qqcmh.click",$img);
}


//写入静态缓存文件
function saveBlock($html,$fileKey,$dirname='book'){
    $res = false;
    $path = env('root_path');
    $path .= 'static/block/'.$dirname.'/';
    if(!is_dir($path)){
        mkdir($path,0777,true);
    }
    if(is_dir($path) && is_writable($path)){
        $path .= $fileKey.'.html';
        $obj = fopen($path,'w');
        fwrite($obj, $html);
        fclose($obj);
        if(is_file($path)){
            $res = true;
        }
    }
    return $res;
}

//获取静态缓存内容
function getBlockContent($filekey,$dirname='book'){
    $path = env('root_path');
    $path .= 'static/block/'.$dirname.'/'.$filekey.'.html';
    $content = '';
    if(@is_file($path)){
        $content = file_get_contents($path);
    }
    return $content;
}

//读取缓存静态文件
function getBlock($filekey,$dirname='book'){
    $filename = env('root_path');
    $filename .= 'static/block/'.$dirname.'/'.$filekey.'.html';
    if(@is_file($filename)){
        require_once $filename;
    }
}

/**
 * 写日志
 * @param mixed $data 记录内容
 * @param string $title 日志标题
 */
function writeLogs($data,$title=''){
	$log = [
			'create_time' => date('Y-m-d H:i:s'),
			'title' => $title,
			'data' => $data
	];
	$old_log = cache('sys_log');
	if($old_log){
		$old_log[] = $log;
		$old_log = array_reverse($old_log);
	}else{
		$old_log = [$log];
	}
	cache('sys_log',$old_log);
}

//处理跨域图片地址
function returnImg($url){
	if(stripos($url,'http') !== false){
		return '/img?url='.$url;
	}else{
		return $url;
	}
}

/**
 * 长链接转为短链接
 * @param  string $url_long 要转换的链接
 * @return string 短链接
 */
function getShortUrl($url_long){
	$url = urlencode($url_long);
	$request_url = "http://sa.sogou.com/gettiny?url=".$url;
	$res = myHttp::doGet($request_url,'','string');
	$short_url = (substr($res, 0,4) === 'http') ? $res : '';
	return $short_url;
}

//格式化数值
function formatNumber($number){
	$number = is_numeric($number) ? $number : 0;
	if ($number >= 1000 && $number < 10000){
		$number = round($number/1000,1).'k';
	}elseif ($number >= 10000){
		$number = round($number/10000,1).'w';
	}
	return $number;
}

function checkIsWeixin(){
	if (strpos($_SERVER['HTTP_USER_AGENT'],'MicroMessenger') !== false ){
		return true;
	}
	return false;
}

function getClientIP($type = 0, $adv = false) {
    global $ip;
    $type = $type ? 1 : 0;
    if ($ip !== NULL)
        return $ip[$type];
    if ($adv) {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            $pos = array_search('unknown', $arr);
            if (false !== $pos)
                unset($arr[$pos]);
            $ip = trim($arr[0]);
        }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
    } elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $long = sprintf("%u", ip2long($ip));
    $ip = $long ? array(
        $ip,
        $long) : array(
        '0.0.0.0',
        0);
    return $ip[$type];
}
