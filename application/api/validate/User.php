<?php


namespace app\api\validate;


use think\Validate;

class User extends Validate
{
    protected $rule = [
        "id"=>"require|number",
        "username"=>"require|unique:member,username^id",
        "pwd"=>"require|length:6,16",
        "repwd"=>"require|confirm:pwd",
    ];

    protected $message = [
        'username.require'=>'请输入用户名',
        'username.unique'=>'该用户名已存在',
        'pwd.require'=>'请输入新密码',
        'pwd.length'=>'请输入6到16位新密码',
        'repwd.length'=>'请再次输入新密码',
        'pwd.confirm'=>'两次输入密码不一致',
    ];

    protected $scene = [
        "changepwd"=>["id","pwd","repwd"],//修改密码
    ];
}