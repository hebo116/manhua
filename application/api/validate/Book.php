<?php


namespace app\api\validate;


use think\Validate;

class Book extends Validate
{
    protected $rule = [
        "book_id"=>"require|number|gt:0",
        "invite_code"=>"alphaDash|length:6",
    ];

    protected $message = [
        'book_id.require'=>'书籍参数错误',
        'book_id.number'=>'书籍参数错误',
        'book_id.gt'=>'书籍参数错误',
        'invite_code.alphaDash'=>'非法访问',
        'invite_code.length'=>'非法访问e',
    ];

    protected $scene = [
        "info"=>["book_id","book_id"],//书籍详情
    ];
}