<?php

//根据id获取二位数组的一个项
function get_array_one($id,$arr){
    $data = [];
    foreach ($arr as $k=>$v){
        if ($v['id'] == $id){
            $data = $arr[$k];
        }
    }
    return $data;
}

function addZero($number){
    if(is_numeric($number) && $number > 0){
        return str_pad($number,6,"0",STR_PAD_LEFT);
    }
    return $number;
}
