<?php


namespace app\api\controller;


use app\common\model\mdBook;
use site\myCache;
use site\myDb;
use site\myHttp;
use think\Db;

class Video extends Common {

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取视频列表
     * @description 接口
     * @method get
     * @url /api/video/get_video_list
     * @param page 是 int 页码
     * @param size 否 int 每页显示条数
     * @return {}
     * @return_param data array 数据数组
     * @return_param id int 视频id
     */
    public function get_video_list(){

        $page = intval(input('page',1));
        $size = intval(input('size',10));

        $tag=addslashes(input("tag",""));
        $author=addslashes(input("author",""));

        $key="videos_".$page."_".$size;
        $res=cache($key);
//        if($res){
//            res_api($res,1);
//            return;
//        }

        $date_now=date('j');

        $model = Db::name('Videos')
	   ->field("id,author,des,title,img,tags,id%".$date_now." as ida")
            ->where(['status'=>1]);
        if($tag){
            $model=$model->where("json_contains(tags,'".$tag."')");
        }

        if($author){
            $model=$model->where("author",$author);
        }

//        $model->getLastSql();

        $list=$model->page($page,$size)
            ->order('ida','desc')
            ->order("id","desc")
            ->select();

        cache($key,$list,3600);

        res_api($list,1);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取视频详情
     * @description 接口
     * @method get
     * @url /api/video/get_video_info
     * @param video_id 是 int 视频id
     * @return {}
     * @return_param data array 数据数组
     * @return_param id int 视频id
     */
    public function get_video_info(){
        /*
        $video_id = input('video_id');
        if (!$video_id){
            res_api('视频ID参数错误');
        }
        $url = 'https://live.kkcomic.club/api/public/?service=Movie.getVideoInfo&id='.$video_id;
        $res = myHttp::doGet($url);
        if ($res['ret'] == 200 && $res['data']['code']===0){
            $rt = $res['data']['info'][0]['detail'];
            if (empty($rt['author'])){
               $rt['author'] = '小歪';
            }
            if (empty($rt['source'])){
                $rt['source'] = '歪歪出品';
            }
            if (!isset($rt['detail_img'])){
                $rt['detail_img'] = '';
            }
            if (empty($rt['summary'])){
                $rt['summary'] = $rt['name'];
            }
        }else{
            res_api($res['data']['msg']);
        }
        res_api($rt);

        die;
        */
        /*
        $video_id = input('video_id');
        if (!$video_id){
            res_api('视频ID参数错误');
        }
        $video = myCache::getVideoCache($video_id);
        if (!$video){
            res_api('没有该视频');
        }
        res_api($video);
        */
        $video_id = input('video_id');
        if (!$video_id){
            res_api('视频ID参数错误');
        }
        global $loginId;
        $videos_history_key = "videos_history_".$loginId."_".$video_id;
        if (!cache($videos_history_key)){
            myDb::add('VideosReadHistory',['uid'=>$loginId,'videos_id'=>$video_id,'create_time'=>time()]);
            $ttl = mktime(23,59,59,date('m'),date('d'),date('Y')) - time();
            cache($videos_history_key,1,$ttl);
        }


        $key="video_detail_".$video_id;
        $res=cache($key);
        if($res){
            res_api($res);
        }

        $video = Db::name('Videos')->where('id','=',$video_id)->where('status','=',1)->find();
        if (!$video){
            res_api('没有该视频');
        }

        cache($key,$video,3600);

        res_api($video);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取猜你喜欢视频列表
     * @description 接口
     * @method get
     * @url /api/video/get_like_video_list
     * @param page 是 int 页码
     * @param size 否 int 每页显示条数
     * @return {}
     * @return_param data array 数据数组
     * @return_param id int 视频id
     */
    public function get_like_video_list(){
        /*
        $video_id = input('video_id');
        if (!$video_id){
            res_api('视频ID参数错误');
        }
        $url = 'https://live.kkcomic.club/api/public/?service=Movie.getVideoInfo&id='.$video_id;
        $res = myHttp::doGet($url);
        $list = [];
        if ($res['ret'] == 200 && $res['data']['code']===0){
            $list = $res['data']['info'][0]['likes'];
        }else{
            res_api($res['data']['msg']);
        }
        foreach ($list as $k=>$v){
            if (empty($v['author'])){
                $list[$k]['author'] = '小歪';
            }
            if (empty($v['source'])){
                $list[$k]['source'] = '歪歪出品';
            }
            if (!isset($v['detail_img'])){
                $list[$k]['detail_img'] = '';
            }
            if (empty($v['summary'])){
                $list[$k]['summary'] = $v['name'];
            }
        }
        res_api($list,1);

        die;
        */
        /*
        $page = input('page',1);
        $size = input('size',3);
        $ids = Db::name('Video')
            ->where('status',1)
            ->order('hot_num','desc')
            ->page($page,$size)
            ->column('id');
        $list = [];
        foreach ($ids as $id){
            $list[] = myCache::getVideoCache($id);
        }
        res_api($list,1);
        */
        $ids = [mt_rand(234,279),mt_rand(432,477),mt_rand(502,539)];
        $page = input('page',1);
        $size = input('size',3);
        $list = Db::name('Videos')
	->field("id,author,des,title,img,tags")
            ->where('status',1)
            ->whereIn('id',$ids)
            ->order('create_time','desc')
            ->page($page,$size)
            ->select();

        res_api($list,1);
    }


}
