<?php
namespace app\api\controller;

use app\common\model\mdMember;
use site\myCache;
use site\myDb;
use think\App;
use think\Controller;
use think\Db;

class Common extends Controller {

    protected $check_login_path=[
        'api/book/collect',
        'api/book/readHistory',
        'api/book/doCollect',
        'api/user/getUserInfo',
        'api/user/contact',
        'api/user/message',
        'api/user/messageNum',
        'api/user/bill',
        'api/user/setAuto',
        'api/user/autoList',
        'api/user/changePwd',
        'api/user/setAutoLogin',
        'api/user/clearRecord',
        'api/user/delRecord',
        'api/charge/getChargeList',
        'api/charge/getPayList',
        'api/charge/doCharge',
        'api/book/duBuyNumber',
        'api/user/findUser',
        'api/user/cutUser',
        'api/user/loginSendMoney',
        //'api/video/get_video_info',
    ];//需要验证登录的接口

    protected $u_id;

    public function __construct(App $app = null){
        parent::__construct($app);
        $server_url = $_SERVER['HTTP_HOST'];
        if(!$server_url){
            res_api('非法访问');
        }
        $urlData = myCache::getUrlInfo($server_url);
        global $loginId;
        $usertoken = request()->header('usertoken');
        $loginId = cache('api_'.$usertoken);
        $loginId = $loginId ? : 0;

        if (in_array(request()->path(), $this->check_login_path)){
            $this->checkLogin();
        }
        $this->loginMoney();

        if (!empty($this->u_id) || !empty($loginId)){
            $uid = empty($this->u_id)?$loginId:$this->u_id;
            $this->saveActive($uid);
        }
    }

    //验证登录
    public function checkLogin(){
        $token =  request()->param('token');
        $id = cache('api_'.$token)?:0;
        if (empty($id)){
            res_api('请登录',3);
        }
        $user = Db::name('member')->where(['id'=>$id])->find();
        if (!$user){
            res_api('请登录',3);
        }

        $this->u_id = $id;
    }

    //发放每日登录奖励
    public function loginMoney(){
        global $loginId;
        $uid = $loginId;
        $where = [['uid','=',$uid],['type','=',2],['create_date','=',date('Y-m-d')]];
        $isSend = myDb::getValue('MemberLog',$where,'id');
        if (!$isSend){
            $config = getMyConfig('website');
            $money = $config && $config['login_money'] ? $config['login_money'] : 0;
            if($money){
                if (!cache('login_money_'.$uid) || cache('login_money_'.$uid)['is_send'] !=1){
                    $res = mdMember::addLoginMoney($uid, $money);
                    if($res){
                        //缓存用户每日登录奖励书币信息
                        $arr = ['is_send'=>1,'send_money'=>$money,'send_time'=>time()];
                        //缓存当天有效
                        $ttl = strtotime(date('Y-m-d 23:59:59')) - time();
                        cache('login_money_'.$uid,$arr,$ttl);
                    }
                }
            }
        }
    }

    //记录活动信息
    public function saveActive($uid){
        $date = date('Y-m-d');
        $key = 'user_active_'.$date.$uid;
        if(!cache('?'.$key)){
            $data = [
                'uid' => $uid,
                'create_date' => $date
            ];
            $re = myDb::add('MemberActive', $data);
            if($re){
                cache($key,1,86400);
            }
        }
    }
}