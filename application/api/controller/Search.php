<?php
namespace app\api\controller;


use site\myCache;
use think\Db;

class Search extends Common {
    /**
     * showdoc
     * @catalog 小说漫画h5项目/搜索
     * @title 搜索内容
     * @description 接口
     * @method get
     * @url /api/search/searchResult
     * @param keyword 是 string 关键词
     * @return {"code":1,"msg":"ok","data":[{"id":"622","name":"大嫂，哥哥的女人","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/abb99a8d5f2cb93fbf93a5d7014a296c.jpg","summary":"面对诱惑的大嫂，小叔还能顶多久？","hot_num":"1348702","over_type":"1","is_alone":"2","star":"9.72"},{"id":"650","name":"玩转女上司","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/f289f6e73e29d22417be6e02980c3f30.jpg","summary":"这家公司有点奇怪，新来的实习生必须要经过美女上司的重重考验才能成为正式职工。","hot_num":"1238509","over_type":"2","is_alone":"2","star":"9.37"},{"id":"694","name":"前女友变帮佣","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200605\/3ad4883ee76ca963793dede07053633e.jpg","summary":"泰俊在准备公务员考试时，受不了诱惑和再读会认识的又晴搞劈腿，也因此和初恋分手。多年以后，未婚妻雇来打扫新房的帮佣竟然是已经失去联络多年的旧爱...","hot_num":"980444","over_type":"1","is_alone":"2","star":"9.10"},{"id":"624","name":"按摩妹女友","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/34d4b51398d9b4b452b1d64e23132b78.jpg","summary":"在按摩店里面打工的青年男女产生了情愫，但是这份感情在这种环境下能够长久吗？","hot_num":"957526","over_type":"1","is_alone":"2","star":"9.40"},{"id":"652","name":"隔壁母女","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/791bf70d0a868813eef3821700171005.jpg","summary":"他是个小有名气的画家，一天，曾经出现在他梦里无数次的美丽少女和她的母亲，出现在他面前.....","hot_num":"876306","over_type":"1","is_alone":"2","star":"9.61"},{"id":"463","name":"前男友前女友","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/9928a4501d3ce739d8f40af146207184.jpg","summary":"舊愛還是最美? 那個曾經讓你(妳)悸動的那個人! 如果再次遇見的話...?","hot_num":"432552","over_type":"2","is_alone":"2","star":"9.75"},{"id":"374","name":"一个不会拒绝的女人","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/156fc18b33d6e575bdf52a357a15da83.jpg","summary":"利用女代理不会拒绝别人的弱点，男同事们纷纷提出了一个个过分的要求……","hot_num":"379842","over_type":"1","is_alone":"2","star":"9.34"},{"id":"371","name":"优质女人","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/1f597c7617cd00d6a4c2bd1b038ef75c.jpg","summary":"她就像是这污浊世界里的一股纯洁氧气\n一个如同天使般的女人~\n她能找到自己的真爱吗?","hot_num":"325686","over_type":"1","is_alone":"2","star":"9.94"},{"id":"2","name":"肤浅女","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200520\/48f55e264de93e271c3fe04789a7c7b3.jpg","summary":"呵呵，肤浅的女人...","hot_num":"286934","over_type":"2","is_alone":"2","star":"9.68"},{"id":"310","name":"男公关与富家女","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/fffc621bf1dd8d3648ad52b55b3a09e0.jpg","summary":"你要隐瞒你是男公关的事实并跟5位有钱的富家女拉关系！\n为了钱不顾一切的罗峰九，有一天入了一个说会给他钱的女人的骗局，并签下契约，之后便参加了一个奇怪的酒宴。","hot_num":"269549","over_type":"1","is_alone":"2","star":"9.71"},{"id":"8","name":"好朋友的女朋友","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200522\/fc33009cb61180bd9ff488fdec1b0580.jpg","summary":"深爱的女朋友和最好的基友劈腿，他发誓要报复！最终的结局，只因她离奇的身份而扑朔迷离。","hot_num":"236165","over_type":"2","is_alone":"2","star":"9.63"},{"id":"293","name":"初恋的女儿","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/b14acb41fec33a3592a6628a3ff59d93.jpg","summary":"","hot_num":"225703","over_type":"1","is_alone":"2","star":"9.81"},{"id":"131","name":"邻家少女第一季","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200523\/2f256c27278bb2c5a171b115e3410a20.jpg","summary":"成年鳏夫编剧、性感妈妈桑、情犊初开有着特殊癖好的的练习生女儿，演绎不设防的情感大戏。","hot_num":"218644","over_type":"2","is_alone":"2","star":"9.68"},{"id":"441","name":"生物学女性攻略","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/7ddfad2137401f0457a4d6e39e4ecb6f.jpg","summary":"用生物學來探討男生該如何吸引女生... 讓我們透過各種成功的案例來學習女生的攻略法！","hot_num":"218274","over_type":"2","is_alone":"2","star":"9.75"},{"id":"156","name":"女助教","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200523\/571fd34be8b62b266fe17e8a0f6d96da.jpg","summary":" 白天是弟子，晚上变奴隶","hot_num":"215278","over_type":"2","is_alone":"2","star":"9.71"},{"id":"372","name":"租赁女孩第二季","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/20feec70091d03c8a060e1c0c78fa165.jpg","summary":"欢迎来到成人视频租赁店！你喜欢公共场所吗？在公交车上面？和陌生人一起？还是一个学生和她老师？","hot_num":"213743","over_type":"1","is_alone":"2","star":"9.47"},{"id":"49","name":"爸爸的女人","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200522\/baf8ec61fdc0ca55dcae5bfed08a6397.jpg","summary":"爸爸在妈妈的床上用力干着那个陌生的女人，我非常愤怒！但看见她的脸，我的怒火竟然熄灭了。后来会怎样？我不知道！","hot_num":"213400","over_type":"2","is_alone":"2","star":"9.65"},{"id":"43","name":"妖女之祸","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200522\/e67dd82f44917ec0cec97f3e5c1c0a5e.jpg","summary":"公司的女高管，竟然是一个女妖精，她给全体男同事带来了无穷的灾难。。。","hot_num":"195237","over_type":"2","is_alone":"2","star":"9.53"},{"id":"173","name":"离婚男女","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200523\/4d62891a66d719db89d7e068c1ebdb0c.jpg","summary":"离婚了 却是另一个开始! 我的懊悔 妳的怀念 还有...她的报复","hot_num":"185158","over_type":"2","is_alone":"2","star":"9.56"},{"id":"313","name":"女上男下","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/131e50fad8202d1caddc475b70987b91.jpg","summary":"朋友上班都偷吃女主管，我怎么上班第一天就面临被炒的危机?为什么我没有办公室艳遇的命...我也想被主管骑啊~","hot_num":"176670","over_type":"2","is_alone":"2","star":"9.81"}]}
     * @return_param data array 数据数组
     * @return_param id int 书籍id
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param cover url 书籍封面图
     * @return_param summary string 简介
     * @return_param hot_num int 人气值
     * @return_param over_type int 1连载中2已完结
     * @return_param is_alone int 是否独家
     * @return_param star float 星级
     */
    public function searchResult(){
            $keyword = $this->request->get('keyword');
            $list = [];
            if($keyword){
                global $loginId;
                $field = 'id,name,author,cover,summary,hot_num,over_type,is_alone,star';
                $list = Db::name('Book')->where('status',1)->where('name|author','like','%'.$keyword.'%')->field($field)->order('hot_num','desc')->limit(20)->select();
                foreach ($list as &$v){
                    $v['cover'] = handleImg($v['cover']);
                }
                if($loginId){
                    myCache::addUserSearch($loginId, $keyword);
                }
            }
            if ($list){
                res_api($list,1);
            }else{
                res_api('暂无数据',1);
            }
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/搜索
     * @title 删除指定搜索记录
     * @description 接口
     * @method get
     * @url /api/search/delRecord
     * @param keyword 是 string 关键词
     * @return {}
     * @return_param data array 数据数组
     */
    public function delRecord(){
        global $loginId;
        if($loginId){
            $keyword = $this->request->get('keyword');
            if($keyword){
                myCache::rmUserSearch($loginId,$keyword);
            }
        }
        res_api();
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/搜索
     * @title 删除全部记录
     * @description 接口
     * @method get
     * @url /api/search/clearRecord
     * @return {}
     * @return_param data array 数据数组
     */
    public function clearRecord(){
        global $loginId;
        if($loginId){
            myCache::rmUserAllSearch($loginId);
        }
        res_api();
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/搜索
     * @title 历史记录
     * @description 接口
     * @method get
     * @url /api/search/history
     * @return {"code":1,"msg":"ok","data":["大嫂","教练"]}
     * @return_param data array 数据数组
     */
    public function history(){
        global $loginId;
        $history = $loginId ? myCache::getUserSearch($loginId) : '';
        if ($history){
            res_api($history);
        }else{
            res_api('暂无数据',1);
        }
    }
}