<?php
namespace app\api\controller;

use app\api\model\Book;
use app\common\model\mdBook;
use site\myCache;
use site\myDb;
use site\myValidate;

class Index extends Common {
    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取首页导航
     * @description 接口
     * @method get
     * @url /api/index/get_nav
     * @return {"code":1,"msg":"ok","data":{"cartoon":{"logo":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/e653b7c69e76ed1b564acade1fe63fbe.png","url":"\/index\/index\/index.html","name":"漫画","is_on":1},"novel":{"logo":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/6197a0d90af002a5e3712d624516f6ab.png","url":"\/index\/Novel\/index.html","name":"小说","is_on":1},"web_name":"渔网漫画"}}
     * @return_param data array 数据数组
     * @return_param cartoon array 漫画信息
     * @return_param novel array 小说信息
     * @return_param web_name string 站点名称
     * @return_param logo url 图片地址
     * @return_param url url 跳转地址
     * @return_param name string 注释
     * @return_param is_on string 注释
     */
    public function get_nav(){
        $website = getMyConfig('website');
        $web_block = getMyConfig('web_block');
        if (!$website || !$web_block){
            res_api('暂无数据',1);
        }
        $data =[
            'cartoon'=>[
                'logo'=>handleImg($website['logo']),
            ],
            'novel'=>[
                'logo'=>handleImg($website['novel_logo'])
            ],
            'web_name'=>$website['name'],
        ];
        foreach ($web_block as $k=>$v){
            if ($v['key'] == 'cartoon'){
                $data['cartoon']['url'] =$web_block[$k]['url'];
                $data['cartoon']['name'] =$web_block[$k]['name'];
                $data['cartoon']['is_on'] =$web_block[$k]['is_on'];
            }elseif($v['key'] == 'novel'){
                $data['novel']['url'] =$web_block[$k]['url'];
                $data['novel']['name'] =$web_block[$k]['name'];
                $data['novel']['is_on'] =$web_block[$k]['is_on'];
            }
        }
        return res_api($data,1);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取轮播图地址
     * @description 接口
     * @method get
     * @url /api/index/get_banner
     * @param banner_type 是 int 轮播图类型(1:漫画，2:小说)
     * @return {"code":1,"msg":"ok","data":[{"src":"https:\/\/nc-bk.oss-cn-hongkong.aliyuncs.com\/images\/20200406\/89d8e8c2ec1b78a89c6496e37caea7f6.jpg","link":"https:\/\/www.baidu.com\/"},{"src":"https:\/\/nc-bk.oss-cn-hongkong.aliyuncs.com\/images\/20200406\/f67c4475e9c14b694cdec6639f77a62d.jpg","link":"https:\/\/www.baidu.com\/"}]}
     * @return_param data array 数据数组
     * @return_param src url 图片地址
     * @return_param link url 跳转地址
     * @return_param book_id int 书籍id
     */
    public function get_banner(){
        $banner_type = input('banner_type');
        $data = [];
        if ($banner_type == 1){
            $data = getMyConfig('index_banner')?:[];
        }
        if ($banner_type == 2){
            $data = getMyConfig('novel_index_banner')?:[];
        }
        if ($data){
            foreach ($data as $k=>$v){
                if (!empty($v['link'])){
                    $tmp = explode('book_id=',$v['link']);
                    if (isset($tmp[1])){
                        $data[$k]['book_id'] = $tmp[1];
                    }else{
                        $data[$k]['book_id'] = '';
                    }
                }else{
                    $data[$k]['book_id'] = '';
                }
                $data[$k]["src"]=handleImg($data[$k]["src"]);
            }
        }
        return res_api($data,1);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取广告配置图
     * @description 接口
     * @method get
     * @url /api/index/get_ad
     * @param ad_id 是 int 广告配置图id(1:首页第一屏，2:首页第二屏，3:书籍详情页,4:首页弹窗广告)
     * @return {"code":1,"msg":"ok","data":{"id":"1","src":"https:\/\/nc-bk.oss-cn-hongkong.aliyuncs.com\/images\/20200406\/fe93aa42b32977837e34627bb84e1f4f.gif","url":"https:\/\/www.baidu.com\/"}}
     * @return_param data array 数据数组
     * @return_param id int 广告配置图id
     * @return_param src url 图片地址
     * @return_param url url 跳转地址
     * @return_param book_id int 书籍id
     */
    public function get_ad(){
        $ad_id = input('ad_id');
        $adList = myCache::getAdList();
        $data = get_array_one($ad_id,$adList);
        if ($data){
            $tmp = explode('book_id=',$data['url']);
            if (isset($tmp[1])){
                $data['book_id'] = $tmp[1];
            }else{
                $data['book_id'] = '';
            }
            $data["src"]=handleImg($data["src"]);
        }
        return res_api($data,1);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取书籍列表（首页更多列表通用）
     * @description 接口
     * @method get
     * @url /api/index/get_book_list
     * @param type 是 int book类型(1:精品，2:最新，3:限免书籍，4：推荐书籍，5：猜你喜欢书籍，6：热门免费专区)
     * @param page 是 int 页码
     * @param size 否 int 每页显示条数
     * @param category 是 int (1:漫画，2：小说)
     * @return {"code":200,"msg":"","data":[{"id":"18","over_type":"1","is_alone":"2","name":"神印王座","author":"快乐工场","hot_num":"2270038","cover":"https:\/\/nc-bk.oss-cn-hongkong.aliyuncs.com\/images\/20200409\/8a682a0af37352d1fbe0a04ed2dce64f.jpg","detail_img":"https:\/\/nc-bk.oss-cn-hongkong.aliyuncs.com\/images\/20200409\/fd108fe3fa417637d9fafa6eea3c8efc.jpg","star":"8.00","summary":"【每周一、五更新】魔族强势，在人类即将被灭绝之时，六大圣殿崛起，带领着人类守住最后的领土。一名少年，为救母加入骑士圣殿，奇迹、诡计，不断在他身上上演。在这人类6大圣殿与魔族72柱魔神相互倾轧的世界，他能否登上象征着骑士最高荣耀的神印王座？..."},{"id":"24","over_type":"1","is_alone":"2","name":"王爷你好贱","author":"追梦动漫","hot_num":"2270000","cover":"https:\/\/nc-bk.oss-cn-hongkong.aliyuncs.com\/images\/20200409\/198966c9d3ab1e0e65e737251addbec4.jpg","detail_img":"https:\/\/nc-bk.oss-cn-hongkong.aliyuncs.com\/images\/20200409\/1482b3fe1e923e27c7ed0527c529e780.jpg","star":"5.00","summary":"【每周二、五、六更新】这里有最丰富多彩的小贱人们，有最意想不到的扎堆撕，甚至还有最臭不要脸的莲花贱，只要0.1，0.1，0.1！啊，你问我0.1是啥？你不会自己看啊……..."}]}
     * @return_param data array 数据数组
     * @return_param id int 书籍id
     * @return_param over_type int (1连载中2已完结)
     * @return_param is_alone int 是否独家
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param hot_num int 人气值
     * @return_param cover url 封面图片
     * @return_param detail_img url 详情图片
     * @return_param star float 星级
     * @return_param summary string 小说简介
     */
    public function get_book_list(){
        $type = input('type',1);
        $page = input('page',1);
        $size = input('size',6);
        $category = input('category',1);//1漫画2小说

        $ids = [];
        if ($type == 1){
            $ids = myCache::getBeastBookIds($category);
        }
        if ($type == 2){
            $ids = myCache::getNewBookIds($category);
        }
        if ($type == 3){
            $ids = myCache::getFreeBookIds($category);
        }
        if ($type == 4){
            $ids = myCache::getRecommendBookIds($category);
        }
        if ($type == 5){
            $ids = myCache::getAllBookIds($category);
        }
        if ($type == 6){
            //$ids = myCache::getFreeAreaBookIds($category);
            $ids=[];//隐藏免费专区
        }

        if (!$ids){
            res_api('暂无数据',1);
        }
        $ids_arr = array_chunk($ids,$size);
        $page-=1;
        $ids_page = [];//当前页的书籍id集
        if (isset($ids_arr[$page])) {
            $ids_page = $ids_arr[$page];
        }

        //根据书籍id去缓存中获取书籍信息
        $book_arr = [];
        foreach ($ids_page as $v){
            $book_arr[] = mdBook::getIndexBookInfo($v);
        }
        res_api($book_arr,1);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取书籍排行榜(与首页更多列表通用)
     * @description 接口
     * @method get
     * @url /api/index/get_rank_book
     * @param cate_type 是 int 书籍类型(1:漫画,2:小说)
     * @param rank_type 是 int 排行榜类型(1:畅销榜，2:人气榜，3:连载榜，4：完结榜)
     * @param page 是 int 页码
     * @param size 是 int 每页显示条数
     * @return {"code": 1, "msg": "ok", "data": [{"id": "650", "over_type": "2", "is_alone": "2", "name": "玩转女上司", "author": "韩漫", "hot_num": "1237966", "cover": "http://630029.caigoubao.cc/630029/manhua/uploads/img/20200529/f289f6e73e29d22417be6e02980c3f30.jpg", "detail_img": "http://630029.caigoubao.cc/630029/manhua/uploads/img/20200529/ab408f8039e257d0473a4e8d8c498d08.jpg", "star": "9.37", "summary": "这家公司有点奇怪，新来的实习生必须要经过美女上司的重重考验才能成为正式职工。"}, {"id": "170", "over_type": "2", "is_alone": "2", "name": "出轨", "author": "韩漫", "hot_num": "1109148", "cover": "http://630029.caigoubao.cc/630029/manhua/uploads/img/20200523/1f2d031ca6feeaaace3ee82af26f7210.jpg", "detail_img": "http://630029.caigoubao.cc/630029/manhua/uploads/img/20200523/fdbaf6521fb14b9ae99715604ad42827.jpg", "star": "9.54", "summary": "\"等等你来我办公室，我好好给你解释\"，22岁女大学生的身体，引爆了他的沉寂的雄性本能。"}]}
     * @return_param data array 数据数组
     * @return_param id int 书籍id
     * @return_param over_type int (1连载中,2已完结)
     * @return_param is_alone int 是否独家
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param hot_num int 人气值
     * @return_param cover url 封面图片
     * @return_param detail_img url 是详情图片
     * @return_param star float 星级
     * @return_param summary string 小说简介
     */
    public function get_rank_book(){
        $cate_type = input('cate_type',1);
        $rank_type = input('rank_type',1);
        $page = input('page',1);
        $size = input('size',15);
        $data = myCache::getRankBookIds($cate_type);
        $data = $data[$rank_type];
        $ids_arr = array_chunk($data,$size);
        $page-=1;
        $ids_page = [];//当前页的书籍id集
        if (isset($ids_arr[$page])) {
            $ids_page = $ids_arr[$page];
        }

        //根据书籍id去缓存中获取书籍信息
        $book_arr = [];
        foreach ($ids_page as $v){
            $book_arr[] = mdBook::getIndexBookInfo($v);
        }
        res_api($book_arr,1);
    }

    public function getDownloadUrl(){
        res_api(["android"=>"https://mh.qqcmh.click/download/qqc.apk","ios"=>"https://mh.qqcmh.click/download/qqcc.mobileconfig"]);
    }

    public function test(){}
}