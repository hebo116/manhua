<?php


namespace app\api\controller;


use site\myCache;
use site\myHttp;

class Live extends Common {

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取直播列表
     * @description 接口
     * @method get
     * @url /api/live/get_live_list
     * @param page 是 int 页码
     * @param size 否 int 每页显示条数
     * @return {}
     * @return_param data array 数据数组
     * @return_param id int 直播id
     */
    public function get_live_list(){
        //$page = input('page',1);
        //$size = input('size',6);
        $url = 'https://live.kkcomic.club/api/public/?service=Home.getHot';
        $res = myHttp::doGet($url);
        $list = [];
        if ($res['ret'] == 200 && $res['data']['code']===0){
            $list = $res['data']['info'][0]['list'];
        }else{
            res_api($res['data']['msg']);
        }
        res_api($list,1);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取视频详情
     * @description 接口
     * @method get
     * @url /api/video/get_video_info
     * @param video_id 是 int 视频id
     * @return {}
     * @return_param data array 数据数组
     * @return_param id int 视频id
     */
    public function get_video_info(){
        $video_id = input('video_id');
        if (!$video_id){
            res_api('视频ID参数错误');
        }
        $video = myCache::getVideoCache($video_id);
        if (!$video){
            res_api('没有该视频');
        }
        res_api($video);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 获取猜你喜欢视频列表
     * @description 接口
     * @method get
     * @url /api/video/get_like_video_list
     * @param page 是 int 页码
     * @param size 否 int 每页显示条数
     * @return {}
     * @return_param data array 数据数组
     * @return_param id int 视频id
     */
    public function get_like_video_list(){

    }
}