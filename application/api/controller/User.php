<?php
namespace app\api\controller;


use app\common\model\mdBook;
use app\common\model\mdMember;
use site\myCache;
use site\myDb;
use site\myHttp;
use site\myValidate;
use think\Db;
use think\Request;
use think\Validate;

class User extends Common {
    /**
     * showdoc
     * @catalog 小说漫画h5项目/登录注册
     * @title 登录contact
     * @description 接口
     * @method POST
     * @url /api/user/sign_in
     * @param username 是 string 用户名
     * @param password 是 string 密码
     * @return {"code":1,"msg":"登录成功","data":{"token":"92b494dc83772938c3e41f79443d0449","ttl":604800,"user":{"id":"6308","username":"ElDwVG","headimgurl":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200530\/4851028a87fe62393f578bd564a3ae31.jpg"}}}
     * @return_param data array 数据数组
     * @return_param token string 用户登录标识
     * @return_param ttl time token有效期
     * @return_param user array 用户信息
     * @return_param id int 用户id
     * @return_param username string 用户名称
     * @return_param headimgurl url 用户头像地址
     *
     */
    public function sign_in(){
        $rules = [
            'username' => ['require|alphaDash|length:4,12',['require'=>'请输入用户名','alphaDash'=>'用户名仅支持字母数字','length'=>'用户名长度仅支持6-12位']],
            'password' => ['require|length:6,16',['require'=>'请输入密码','length'=>'密码长度为6-16位字符']],
        ];
        $post = myValidate::getData($rules);
        $where = [['username|phone','=',$post['username']],['password','=',createPwd($post['password'])]];
        $user = myDb::getCur('Member', $where);
        if(!$user){
            res_api('用户名或者密码输入错误');
        }
        if($user['status'] != 1){
            res_api('您的账号已被禁用，请联系客服');
        }
        cookie('INDEX_LOGIN_ID',$user['id']);
        //echo cache('api_92b494dc83772938c3e41f79443d0449');die;
        $token = $this->createToken($user['id']);
        $ttl = 365*86400;
        cache('api_'.$token,$user['id'],$ttl);
        res_api('登录成功',1,[
            'token'=>$token,
            'ttl'=>$ttl,
            'user'=>[
                'id'=>$user['id'],
                'username'=>$user['username'],
                'headimgurl'=>$user['headimgurl'],
            ]
        ]);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/登录注册
     * @title 注册
     * @description 接口
     * @method POST
     * @url /api/user/register
     * @param username 是 string 用户名
     * @param password 是 int 密码
     * @return {"code":1,"msg":"登录成功","data":{"token":"8a0d6907a9021ece0b546cf804fa1647","ttl":604800,"user":{"id":"6326","username":"SdDfdS","headimgurl":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200530\/4851028a87fe62393f578bd564a3ae31.jpg"}}}
     * @return_param data array 数据数组
     * @return_param token string 用户登录标识
     * @return_param ttl time token有效期
     * @return_param user array 用户信息
     * @return_param id int 用户id
     * @return_param username string 用户名称
     * @return_param headimgurl url 用户头像地址
     */
    public function register(){
        $rules = [
            'username' => ['require|alphaDash|length:4,12',['require'=>'请输入用户名','alphaDash'=>'用户名仅支持字母数字','length'=>'用户名长度仅支持6-12位']],
            'password' => ['require|length:6,16',['require'=>'请输入密码','length'=>'密码长度为6-16位字符']],
        ];
        $post = myValidate::getData($rules);
        $repeat = myDb::getValue('Member',[['username','=',$post['username']]], 'id');
        if($repeat){
            res_api('该用户名已被注册');
        }
        $invite_code = session('?INDEX_INVITE_CODE') ? session('INDEX_INVITE_CODE') : '';

        $headConfig = getMyConfig('site_headimg');
        $headimg = $headConfig ? $headConfig['headimg'] : '/static/templet/default/headimg.jpeg';
        $data = [
            'username' => $post['username'],
            'headimgurl' => $headimg,
            'money' => 0,
            'total_money' => 0,
            'password' => createPwd($post['password']),
            'invite_code' => myDb::createCode('Member','invite_code'),
            'create_time' => time()
        ];
        $config = getMyConfig('website');
        $invite = [];
        if($invite_code){
            $invite_uid = myCache::getUidByCode($invite_code);
            if($invite_uid){
                $invite_user = myCache::getMember($invite_uid);
                if($invite_user){
                    $data['invite_uid'] = $invite_user['id'];
                    $data['channel_id'] = $invite_user['channel_id'];
                    $data['agent_id'] = $invite_user['agent_id'];
                    if($config && $config['invite_money']){
                        $invite['money'] = $config['invite_money'];
                        $invite['uid'] = $invite_uid;
                    }
                }
            }
        }else{
            $agentInfo = mdMember::createAgentInfo();
            $data = array_merge($data,$agentInfo);
        }
//        if (session('INDEX_SPREAD_ID')){
//            $data['spread_id'] = session('INDEX_SPREAD_ID');
//        }
        Db::startTrans();
        $flag = false;
        $uid = Db::name('Member')->insertGetId($data);
        if($uid){
            $res = Db::name('MemberCharge')->insert(['uid'=>$uid]);
            if($res){
                $flag = true;
                if($invite){
                    $flag = false;
                    $invite_data = [
                        'money' => Db::raw('money+'.$invite['money']),
                        'total_money' => Db::raw('total_money+'.$invite['money']),
                    ];
                    $in_re = Db::name('Member')->where('id',$invite['uid'])->update($invite_data);
                    if($in_re){
                        $flag = self::addMemberLog($invite['uid'], '平台赠送', '平台赠送-邀请用户注册', $invite['money'],4);
                    }
                }
            }
        }
        if($flag){
            Db::commit();
            cookie('INDEX_LOGIN_ID',$uid);
            if($invite){
                myCache::doMemberMoney($invite['uid'], $invite['money']);
            }
        }else{
            Db::callback();
        }

        //
        if($flag){

            $token = $this->createToken($uid);
            $ttl = 365*86400;
            cache('api_'.$token,$uid,$ttl);

            res_api('注册成功',1,[
                'token'=>$token,
                'ttl'=>$ttl,
                'user'=>[
                    'id'=>$uid,
                    'username'=>$data['username'],
                    'headimgurl'=>$headimg,
                ]
            ]);
        }else{
            res_api('注册失败');
        }
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/登录注册
     * @title 随机生成一组用户名和密码
     * @description 接口
     * @method get
     * @url /api/user/randUser
     * @return {"code":1,"msg":"ok","data":{"nickname":"V9K6Be","password":123456}}
     * @return_param data array 数据数组
     * @return_param nikename string 随机用户名
     * @return_param password string 密码
     */
    public function randUser(){
        $nickname = mdMember::getNickname();
        $password = 123456;
        res_api(['nickname'=>$nickname,'password'=>$password]);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/登录注册
     * @title 退出登录
     * @description 接口
     * @method post
     * @url /api/user/sign_off
     * @param token 是 string 登录标识
     * @return {"code":2,"msg":"退出成功","data":null}
     * @remark 无
     */
    public function sign_off(){
        $token = input('token','');
        cache('api_' . $token,null);
        cookie('INDEX_LOGIN_ID',null);
        res_api('退出成功');
    }
    //生成随机token
    private function createToken($id){
        $str='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        $randStr = str_shuffle($str);//打乱字符串
        $rands= substr($randStr,0,6);//substr(string,start,length);返回字符串的一部分
        $time = time();
        $rand = "$rands" . "$id" . "$time";
        $token = md5(md5($rand));
        return $token;
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心
     * @title 获取用户信息
     * @description 接口
     * @method get
     * @url /api/user/getUserInfo
     * @param token 是 string 登录标识
     * @return {"code":1,"msg":"ok","data":{"id":"7251","channel_id":"5","agent_id":"0","invite_code":"NMO7FB","phone":null,"is_first_bind":"1","username":"popopo","bind_username":"664386245","pwd":"52cc161db27b72617ffb197422fe0d01","headimgurl":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200530\/4851028a87fe62393f578bd564a3ae31.jpg","viptime":"0","status":"1","spread_id":"0","is_charge":"2","auto_login":"1","create_time":"1593504748","money":135,"user_id":"007251","password":"123456","is_show_password":2}}
     * @return_param data array 数据数组
     * @return_param id int 用户id
     * @return_param user_id int 格式化的用户id
     * @return_param channel_id int 用户所属渠道
     * @return_param agent_id int 用户所属代理ID
     * @return_param invite_code string 邀请码
     * @return_param phone string 用户手机号
     * @return_param is_first_bind int 是否首次绑定手机号
     * @return_param username string 用户名
     * @return_param bind_username string 用户输入的绑定账号（优先取这个值）
     * @return_param headimgurl url 用户头像地址
     * @return_param viptime timestamp vip到期时间
     * @return_param status int (1启用,2禁用)
     * @return_param spread_id int 推广ID
     * @return_param is_charge int 是否充值(1是,2否)
     * @return_param auto_login int 自动登录(1是,2否)
     * @return_param create_time timestamp 用户创建时间
     * @return_param money int 用户书币余额
     * @return_param password string 用户初始密码
     * @return_param is_show_password int 是否显示初始密码(1是2否)
     * @remark 无
     */
    public function getUserInfo(){
        $user_id = $this->u_id;
        $user = myCache::getMember($user_id);
        $user['money'] = intval(myCache::getMemberMoney($user_id));
        $user['user_id'] = addZero($user['id']);
        $user['headimgurl'] = handleImg(@getMyConfig('site_headimg')['headimg']);
        $user['password'] = '123456';//默认初始密码
        if (empty($user['bind_username']) && ($user['pwd'] == createPwd('123456'))){
            $user['is_show_password'] = 1;
        }else{
            $user['is_show_password'] = 2;
        }
        if($user['phone']){
            $user['phone'] = substr_replace($user['phone'], '****', 3,4);
        }
        res_api($user);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心
     * @title 联系方式
     * @description 接口
     * @method get
     * @url /api/user/contact
     * @param token 是 string 登录标识
     * @return {"code":1,"msg":"ok","data":{"email":"","weibo":"渔网周边","qq":"3396408328","weixin":"","line":""}}
     * @return_param data array 数据数组
     * @return_param email string 邮箱号
     * @return_param weibo string 公众号
     * @return_param qq string qq号
     * @return_param weixin string 微信号
     * @return_param line string line号
     * @remark 无
     */
    public function contact(){
        $contact = getMyConfig('site_contact');
        if (!$contact){
            res_api('暂无联系方式');
        }
        res_api($contact);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心
     * @title 未读消息数量
     * @description 接口
     * @method get
     * @url /api/user/messageNum
     * @param token 是 string 登录标识
     * @return {"code":1,"msg":"ok","data":{"msg_num":2}}
     * @return_param data array 数据数组
     * @return_param msg_num int 未读消息数
     * @remark 无
     */
    public function messageNum(){
        $msg = Db::name('Notice a')
            ->join('notice_read b','a.id=b.msg_id and b.uid='.$this->u_id,'left')
            ->field('a.id,IFNULL(b.id,0) as read_id')
            ->having('read_id=0')
            ->select();
        $msg_num = $msg ? count($msg) : 0;
        res_api(['msg_num'=>$msg_num]);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心
     * @title 消息列表
     * @description 接口
     * @method get
     * @url /api/user/message
     * @param token 是 string 登录标识
     * @param page 否 int 页码（默认1）
     * @return {"code":1,"msg":"ok","data":[{"id":"4","title":"请忽略该消息2","content":"请忽略该消息2","create_time":"1592377927"},{"id":"3","title":"请忽略该消息1","content":"请忽略该消息1","create_time":"1592377866"}]}
     * @return_param data array 数据数组
     * @return_param id int 消息id
     * @return_param title string 消息标题
     * @return_param content string 消息内容
     * @return_param create_time timestamp 消息发布时间
     * @remark 无
     */
    public function message(){
        $loginId = $this->u_id;
        $page = input('page',1);
        $msg = Db::name('Notice a')
            ->join('notice_read b','a.id=b.msg_id and b.uid='.$loginId,'left')
            ->field('a.id,IFNULL(b.id,0) as read_id')
            ->having('read_id=0')
            ->select();
        if($msg){
            foreach ($msg as $v){
                Db::name('NoticeRead')->insert(['msg_id'=>$v['id'],'uid'=>$loginId]);
            }
        }

        $list = Db::name('Notice')->order('id','desc')->page($page,20)->select();

        res_api($list);

    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心
     * @title 消费记录(我的账单)
     * @description 接口
     * @method get
     * @url /api/user/bill
     * @param token 是 string 登录标识
     * @param page 否 int 页码（默认1）
     * @return {"code":1,"msg":"ok","data":[{"id":"12954","type":"2","uid":"6308","money":"+45","title":"平台赠送","summary":"平台赠送-每日登陸贈送","create_date":"2020-06-17","week":"周三","date":"06-17","month":"2020-06"},{"id":"12660","type":"2","uid":"6308","money":"+45","title":"平台赠送","summary":"平台赠送-每日登陸贈送","create_date":"2020-06-16","week":"周二","date":"06-16","month":"2020-06"}]}
     * @return_param data array 数据数组
     * @return_param id int 消费记录id
     * @return_param type int (1消费,2登录,3绑定手机号)
     * @return_param uid int 用户ID
     * @return_param money int 消费书币
     * @return_param title string 标题
     * @return_param summary string 消费描述
     * @return_param create_date datetime 消费时间
     * @return_param week string 周
     * @return_param date datetime 消费日期
     * @return_param month datetime 消费年月
     * @remark 无
     */
    public function bill(){
        $page = input('page',1);
        $list = mdMember::getMemberLog($this->u_id, $page);
        if ($list){
            res_api($list,1);
        }else{
            res_api('没有数据',1);
        }
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/书籍
     * @title 设置书籍自动订阅
     * @description 接口
     * @method get
     * @url /api/user/setAuto
     * @param token 是 string 用户标识
     * @param book_id 是 int 书籍id
     * @param type 是 int 自动订阅(1:开启,2:取消)
     * @return {"code":1,"msg":"ok","data":null}
     * @return_param data array 数据数组
     * @return_param code int (1:成功,2:失败,3:登录)
     */
    public function setAuto(){
        $loginId = $this->u_id;
        $rules = [
            'type'=>['require|in:1,2',['require'=>'参数错误','in'=>'未指定该参数']],
            'book_id'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']]
        ];
        $subIds = myCache::getSubBookIds($loginId);
        $post = myValidate::getData($rules,'get');
        switch ($post['type']){
            case 1:
                if($subIds && in_array($post['book_id'], $subIds)){
                    res_api();
                }
                $re = myDb::add('BookSub', ['uid'=>$loginId,'book_id'=>$post['book_id']]);
                break;
            case 2:
                if(!$subIds || !in_array($post['book_id'], $subIds)){
                    res_api();
                }
                $re = myDb::delByWhere('BookSub', [['uid','=',$loginId],['book_id','=',$post['book_id']]]);
                break;
        }
        if($re){
            myCache::rmSubBookIds($loginId);
            res_api();
        }else{
            res_api('操作失败',2);
        }
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心
     * @title 自动订阅列表
     * @description 接口
     * @method get
     * @url /api/user/autoList
     * @param token 是 string 登录标识
     * @return {"code":1,"msg":"ok","data":[{"id":"404","over_type":"1","is_alone":"2","name":"漂亮干姐姐","author":"佚名","hot_num":"1329199","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/14ed18e1a19b36f75a758135e307cf24.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/bf12e28ee33b48f9b44cf181f74013c9.jpg","star":"9.85","summary":"志豪與慧美從小一起長大，多熙則是慧美的大學時期閨蜜，三人的感情在志豪大二後開始有了化學變化..."},{"id":"624","over_type":"1","is_alone":"2","name":"按摩妹女友","author":"韩漫","hot_num":"957200","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/34d4b51398d9b4b452b1d64e23132b78.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/058cb5a59428f96e39ca78a1d27258b2.jpg","star":"9.40","summary":"在按摩店里面打工的青年男女产生了情愫，但是这份感情在这种环境下能够长久吗？"}]}
     * @return_param data array 数据数组
     * @return_param id int 书籍ID
     * @return_param over_type int (1连载中2已完结)
     * @return_param is_alone int 是否独家
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param hot_num string 人气值
     * @return_param cover url 封面图片
     * @return_param detail_img url 详情图片
     * @return_param star string 星级
     * @return_param summary string 小说简介
     * @remark 无
     */
    public function autoList(){
        $lists = [];
        $ids = myCache::getSubBookIds($this->u_id);
        if($ids){
            foreach ($ids as $v){
                $book = mdBook::getIndexBookInfo($v);
                if($book){
                    $lists[] = $book;
                }
            }
        }
        res_api($lists);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心/安全设置
     * @title 更改账号密码
     * @description 接口
     * @method post
     * @url /api/user/changePwd
     * @param token 是 string 登录标识
     * @param pwd 是 string 新密码
     * @param repwd 是 string 确认新密码
     * @return {"code":1,"msg":"修改成功","data":null}
     * @return_param data array 数据数组
     * @remark 无
     */
    public function changePwd(){
            $loginId = $this->u_id;
            $data = input('post.');
            $data['id'] = $loginId;
            $validate =validate("User");
            if (!$validate->scene('changepwd')->check($data)){
                res_api($validate->getError(),2);
            }

            $password = createPwd($data['pwd']);
            $re = myDb::setField('Member', [['id','=',$loginId]], ['password'=>$password],'');
            if($re){
                myCache::rmMember($loginId);
                res_api('修改成功',1);
            }else{
                res_api('修改失败',2);
            }
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心/安全设置
     * @title 设置自动登录
     * @description 接口
     * @method get
     * @url /api/user/setAutoLogin
     * @param token 是 string 登录标识
     * @param auto_login 是 int 自动登录(1:是,2:否)
     * @return {"code":1,"msg":"设置成功","data":null}
     * @return_param data array 数据数组
     * @remark 无
     */
    public function setAutoLogin(){
        global $loginId;
        $rules = ['auto_login' => ['require|in:1,2',['require'=>'参数错误','in'=>'未指定该参数']]];
        $autoLogin = myValidate::getData($rules,'get');
        $re = myDb::setField('Member', [['id','=',$loginId]], 'auto_login', $autoLogin);
        if($re){
            myCache::updateMember($loginId, 'auto_login',$autoLogin);
            res_api('设置成功',1);
        }else{
            res_api('设置失败',2);
        }
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/我的漫画(小说)
     * @title 清理记录
     * @description 接口
     * @method get
     * @url /api/user/clearRecord
     * @param token 是 string 登录标识
     * @param type 是 int 清理记录(1:清空用户收藏书籍,2:清空阅读记录)
     * @return {"code":1,"msg":"清理成功","data":null}
     * @return_param data array 数据数组
     * @remark 无
     */
    public function clearRecord(){
        $loginId = $this->u_id;
        $rules = ['type'=>['require|in:1,2',['require'=>'参数错误','in'=>'未指定该参数']]];
        $type = myValidate::getData($rules,'get');
        $where = [['uid','=',$loginId]];
        if($type == 1){
            $res = myDb::delByWhere('MemberCollect',$where);
            if($res){
                myCache::rmCollectBookIds($loginId);
            }
        }else{
            $where[] = ['status','=',1];
            $res = myDb::setField('ReadHistory', $where, 'status', 2);
            if($res){
                myCache::rmReadCache($loginId);
            }
        }
        if($res){
            res_api('清理成功',1);
        }else{
            res_api('删除失败',2);
        }
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/我的漫画(小说)
     * @title 删除指定记录
     * @description 接口
     * @method post
     * @url /api/user/delRecord
     * @param token 是 string 登录标识
     * @param type 是 int 清理记录(1:删除收藏书籍,2:删除阅读记录)
     * @param book_id 是 int 书籍id
     * @return {"code":1,"msg":"ok","data":null}
     * @return_param data array 数据数组
     * @remark 无
     */
    public function delRecord(){
        $loginId = $this->u_id;
        $rules = [
            'type' => ['require|in:1,2',['require'=>'参数错误','in'=>'未指定该参数']],
            'book_id' => ['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数不规范','gt'=>'书籍参数不规范']]
        ];
        $post = myValidate::getData($rules);
        $where = [['uid','=',$loginId],['book_id','=',$post['book_id']]];
        if($post['type'] == 1){
            $res = myDb::delByWhere('MemberCollect', $where);
            if($res){
                myCache::rmCollectBookIds($loginId);
            }
        }else{
            $where[] = ['status','=',1];
            $res = myDb::setField('ReadHistory', $where, 'status', 2);
            if($res){
                myCache::rmReadCache($loginId);
            }
        }
        if($res){
            res_api();
        }else{
            res_api('删除失败');
        }
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心
     * @title 每日登录送书币信息
     * @description 接口
     * @method get
     * @url /api/user/loginSendMoney
     * @param token 是 string 用户标识
     * @return {"code":1,"msg":"ok","data":{"is_send":1,"send_money":"45","send_time":1593426778}}
     * @return_param data array 数据数组
     * @return_param is_send int 是否赠送书币（1是2否）[1显示2不显示提示框]
     * @return_param send_money int 赠送书币数
     * @return_param send_time timestamp 书币发放时间
     * @remark 无
     */
    public function loginSendMoney(){
        global $loginId;
        $uid = $loginId;
        $data = cache('login_money_'.$uid);
        if ($data){
            if (time() - $data['send_time']>15){
                $data['is_send'] = 2;
            }
            res_api($data);
        }else{
            $data = [
                'is_send'=>2,
                'send_money'=>0,
                'send_time'=>0,
            ];
            res_api($data);
        }
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心
     * @title 根据订单号找回账号
     * @description 接口
     * @method get
     * @url /api/user/findUser
     * @param token 是 string 用户标识
     * @param order_no 是 string 订单号
     * @return {"code":1,"msg":"ok","data":{"order_no":"20200701143033692503","username":"pHI8s9"}}
     * @return_param data array 数据数组
     * @return_param order_no string 订单号
     * @return_param username string 用户名
     * @remark 无
     */
    public function findUser(){
        $order_no = input('order_no','');
        if (!$order_no){
            res_api('订单号格式错误');
        }
        $uid = Db::name('order')->where(['third_order_no'=>$order_no])->value('uid');
        if (!$uid){
            res_api('没有找到该订单');
        }
        $user = myCache::getMember($uid);
        //重置密码为123456
        $password = createPwd('123456');
        $re = myDb::setField('Member', [['id','=',$uid]], ['password'=>$password],'');
        if ($re) {
            $order = ['order_no' => $order_no, 'username' => $user['username']];
            res_api($order);
        }else{
            res_api('密码重置失败');
        }
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/个人中心
     * @title 切换账号
     * @description 接口
     * @method post
     * @url /api/user/cutUser
     * @param token 是 string 用户标识
     * @param username 是 string 随机账号
     * @param bind_username 是 string 用户输入的绑定账号
     * @param password 是 string 密码
     * @return {"code":2,"msg":"该账号已经注册，您输入的密码错误","data":null}{"code":1,"msg":"登录成功","data":{"token":"f9e19f3b40d0e6299e17ec2511879471","ttl":31536000,"user":{"id":"7251","username":"popopo","bind_username":"664386245","headimgurl":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200530\/4851028a87fe62393f578bd564a3ae31.jpg"}}}
     * @return_param data array 数据数组
     * @remark 无
     */
    public function cutUser(){
        $loginId = $this->u_id;
        $validate = new Validate([
            'username' => 'require',
            'bind_username' => 'require|length:6,20',
            'password' => 'require|length:6,12',
        ], [
            'username.require' => '现用账号不能为空',
            'bind_username.require' => '新账号不能为空',
            'bind_username.length' => '新账号为6-20个字符',
            'password.require' => '密码必填',
            'password.length' => '密码长度为6-12个字符',
        ]);
        $post = input('post.');
        if (!$validate->check($post)) {
            res_api($validate->getError(),2);
        }
        $user = myCache::getMember($loginId);
        //判断用户输入的新账号是否存在
        $usr = Db::name('member')->where(['bind_username|username'=>$post['bind_username']])->field('password')->find();
        if ($usr){
            if (createPwd($post['password']) == $usr['password']){
                $where = ['bind_username|username'=>$post['bind_username'],'password'=>createPwd($post['password'])];
                $users = myDb::getCur('Member', $where);
                if($users['status'] != 1){
                    res_api('您的账号已被禁用，请联系客服');
                }
                cookie('INDEX_LOGIN_ID',$users['id']);
                //echo cache('api_92b494dc83772938c3e41f79443d0449');die;
                $token = $this->createToken($users['id']);
                $ttl = 365*86400;
                cache('api_'.$token,$users['id'],$ttl);
                res_api('登录成功',1,[
                    'token'=>$token,
                    'ttl'=>$ttl,
                    'user'=>[
                        'id'=>$users['id'],
                        'username'=>$users['username'],
                        'bind_username'=>$users['bind_username'],
                        'headimgurl'=>$users['headimgurl'],
                    ]
                ]);
            }else{
                res_api('该账号已经注册，您输入的密码错误',2);
            }
        }else{
            if (($post['username'] == $user['username']) && (createPwd($post['password']) == $user['pwd'])){
                $flag = myDb::setField('member',['id'=>$loginId],'bind_username',$post['bind_username']);
                if ($flag){
                    myCache::rmMember($loginId);
                    res_api('绑定账号成功',1);
                }else{
                    res_api('绑定账号失败',2);
                }
            }else{
                res_api('您输入的密码错误，初始密码123456',2);
            }
        }
    }

    public function test(){
        global $loginId;
        myCache::rmMember($loginId);
        $user = myCache::getMember($loginId);
        dump($user);
    }

    public function saves(){
        $sites = input("sites");
        file_put_contents("./sites.txt",$sites.'==='.date('Y-m-d H:i:s').PHP_EOL,FILE_APPEND);

    }
}