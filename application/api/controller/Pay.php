<?php
namespace app\api\controller;


use antPay\antPay;
use otherPay\otherPay;
use qibaPay\qibaPay;
use site\myDb;
use site\myHttp;
use think\Controller;
use weixin\wx;
use weixin\wxpay;
use wuyisanPay\wuyisanPay;
use xubenPay\xubenPay;

class Pay extends Controller {
    //微信h5支付
    public function h5pay(){
        if(!$this->request->isMobile()){
            res_api('请在手机上打开该链接进行支付');
        }
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order',$where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['pay_type'] != 1){
            res_api('支付方式有误');
        }
        $config = getMyConfig('wxh5pay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        wx::$config = $config;
        wxpay::$config = $config;
        $params = [
            'body' => '书币充值',
            'out_trade_no' => $order['order_no'],
            'total_fee' => $order['money'],
            'trade_type' => 'MWEB',
            'notify_url' => 'http://'.$website['url'].'/index/Notify/wxh5pay.html'
        ];
        $payurl = wxpay::getPayMsg($params);
        if(!$payurl){
            res_api('拉起微信支付失败');
        }
        $this->redirect($payurl);
    }

    ///三方支付支付
    public function otherwxpay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $config = getMyConfig('site_otherwxpay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order', $where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['status'] != 1){
            res_api('该订单已支付');
        }
        if($order['pay_type'] != 2){
            res_api('支付方式异常');
        }
        otherPay::$config = $config;
        $noticeurl = 'http://'.$website['url'].'/index/Notify/otherwxpay.html';
        $param = [
            'sdcustomno' => $order['order_no'],
            'orderAmount' => $order['money']*100,
            'noticeurl' => $noticeurl,
            'backurl' => $get['back_url'],
            'mark' => $order['id']
        ];
        $form = otherPay::getPayForm($param,'wxh5');
        echo $form;
        exit;
    }

    //三方支付支付
    public function otheralipay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $config = getMyConfig('site_otheralipay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order', $where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['status'] != 1){
            res_api('该订单已支付');
        }
        if($order['pay_type'] != 3){
            res_api('支付方式异常');
        }
        if(checkIsWeixin()){
            $cur = ['order_no'=>$order['order_no'],'money'=>$order['money']];
            //res_api('点击右上角在浏览器中打开',2,$cur);
            return $this->fetch('index@charge/notice',['cur'=>['order_no'=>$cur['order_no'],'money'=>$cur['money']]]);
        }

        otherPay::$config = $config;
        $noticeurl = 'http://'.$website['url'].'/index/Notify/otheralipay.html';
        $param = [
            'sdcustomno' => $order['order_no'],
            'orderAmount' => $order['money']*100,
            'noticeurl' => $noticeurl,
            'backurl' => $get['back_url'],
            'mark' => $order['id']
        ];
        $form = otherPay::getPayForm($param,'alipayh5');
        echo $form;
        exit;
    }

    //三方--旭本微信支付
    public function xuben666pay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $config = getMyConfig('site_xuben666pay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order', $where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['status'] != 1){
            res_api('该订单已支付');
        }
        if($order['pay_type'] != 4){
            res_api('支付方式异常');
        }
//        if(checkIsWeixin()){
//            $cur = ['order_no'=>$order['order_no'],'money'=>$order['money']];
//            //res_api('点击右上角在浏览器中打开',2,$cur);
//            return $this->fetch('index@charge/notice',['cur'=>['order_no'=>$cur['order_no'],'money'=>$cur['money']]]);
//        }
        xubenPay::$config = $config;
        $noticeurl = 'http://'.$website['url'].'/index/Notify/xuben666pay.html';
        $param = [
            "pay_orderid" => $order['order_no'],
            "pay_amount" => $order['money'],
            "pay_applydate" => date("Y-m-d H:i:s"),
            "pay_bankcode" => "9",
            "pay_notifyurl" => $noticeurl,
            "pay_callbackurl" => $get['back_url'],
        ];
        $form = xubenPay::getPayForm($param);
        echo $form;
        exit;
    }

    //三方--旭本支付宝支付
    public function xuben666alipay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $config = getMyConfig('site_xuben666alipay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order', $where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['status'] != 1){
            res_api('该订单已支付');
        }
        if($order['pay_type'] != 5){
            res_api('支付方式异常');
        }
        if(checkIsWeixin()){
            $cur = ['order_no'=>$order['order_no'],'money'=>$order['money']];
            //res_api('点击右上角在浏览器中打开',2,$cur);
            return $this->fetch('index@charge/notice',['cur'=>['order_no'=>$cur['order_no'],'money'=>$cur['money']]]);
        }
        xubenPay::$config = $config;
        $noticeurl = 'http://'.$website['url'].'/index/Notify/xuben666alipay.html';
        $param = [
            "pay_orderid" => $order['order_no'],
            "pay_amount" => $order['money'],
            "pay_applydate" => date("Y-m-d H:i:s"),
            "pay_bankcode" => "4",
            "pay_notifyurl" => $noticeurl,
            "pay_callbackurl" => $get['back_url'],
        ];
        $form = xubenPay::getPayForm($param);
        echo $form;
        exit;
    }

    //三方--78微信支付
    public function wx78pay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $config = getMyConfig('site_78wxpay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order', $where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['status'] != 1){
            res_api('该订单已支付');
        }
        if($order['pay_type'] != 6){
            res_api('支付方式异常');
        }
//        if(checkIsWeixin()){
//            $cur = ['order_no'=>$order['order_no'],'money'=>$order['money']];
//            //res_api('点击右上角在浏览器中打开',2,$cur);
//            return $this->fetch('index@charge/notice',['cur'=>['order_no'=>$cur['order_no'],'money'=>$cur['money']]]);
//        }
        qibaPay::$config = $config;
        $noticeurl = 'http://'.$website['url'].'/index/Notify/qibapay.html';

        $params = [
            'sdcustomno' => $order['order_no'],
            'orderAmount' => $order['money']*100,
            'cardno'=>32,
            'noticeurl'=>$noticeurl,
            'backurl'=>$get['back_url'],
            'mark' => $order['id'],
        ];
        $form = qibaPay::getPayForm($params);
        echo $form;
        exit;
    }

    //三方--78支付宝支付
    public function ali78pay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $config = getMyConfig('site_78alipay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order', $where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['status'] != 1){
            res_api('该订单已支付');
        }
        if($order['pay_type'] != 7){
            res_api('支付方式异常');
        }
//        if(checkIsWeixin()){
//            $cur = ['order_no'=>$order['order_no'],'money'=>$order['money']];
//            //res_api('点击右上角在浏览器中打开',2,$cur);
//            return $this->fetch('index@charge/notice',['cur'=>['order_no'=>$cur['order_no'],'money'=>$cur['money']]]);
//        }
        qibaPay::$config = $config;
        $noticeurl = 'http://'.$website['url'].'/index/Notify/qibaalipay.html';

        $params = [
            'sdcustomno' => $order['order_no'],
            'orderAmount' => $order['money']*100,
            'cardno'=>32,
            'noticeurl'=>$noticeurl,
            'backurl'=>$get['back_url'],
            'mark' => $order['id'],
        ];
        $form = qibaPay::getaliPayForm($params);
        echo $form;
        exit;
    }

    //蚂蚁支付微信原生
    public function wxantpay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $config = getMyConfig('site_wxantpay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order', $where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['status'] != 1){
            res_api('该订单已支付');
        }
        if($order['pay_type'] != 9){
            res_api('支付方式异常');
        }
        antPay::$config = $config;
        $noticeurl = 'https://'.$website['url'].'/index/Notify/wxantpay.html';
        $params = [
            'mchOrderNo' => $order['order_no'],
            'amount' => $order['money']*100,
            'currency'=>'cny',
            'notifyUrl'=>$noticeurl,
            'returnUrl'=>$get['back_url'],
            'subject'=>'书币充值',
            'body'=>'漫画书币充值',
            'reqTime'=>date('YmdHis'),
            'version'=>'1.0',
            'param1' => $order['id'],
        ];
        $res = antPay::getPayForm($params);
        if ($res['retCode'] == 0){
            header('Location:' . $res['payParams']["payUrl"]); //转入支付页面
            exit;
        }else{
            res_error($res['retMsg']);
        }
    }

    //蚂蚁支付支付宝H5
    public function alih5antpay(){
        //site_alih5antpay
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $config = getMyConfig('site_alih5antpay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order', $where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['status'] != 1){
            res_api('该订单已支付');
        }
        if($order['pay_type'] != 10){
            res_api('支付方式异常');
        }
        antPay::$config = $config;
        $noticeurl = 'https://'.$website['url'].'/index/Notify/alih5antpay.html';
        $params = [
            'mchOrderNo' => $order['order_no'],
            'amount' => $order['money']*100,
            'currency'=>'cny',
            'notifyUrl'=>$noticeurl,
            'returnUrl'=>$get['back_url'],
            'subject'=>'书币充值',
            'body'=>'漫画书币充值',
            'reqTime'=>date('YmdHis'),
            'version'=>'1.0',
            'param1' => $order['id'],
        ];
        $res = antPay::getPayForm($params);
        if ($res['retCode'] == 0){
            header('Location:' . $res['payParams']["payUrl"]); //转入支付页面
            exit;
        }else{
            res_error($res['retMsg']);
        }
    }

    //蚂蚁支付支付宝超级H5
    public function alih5antsuperpay(){

    }

    //三方--513支付
    public function other513pay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_api('非法访问');
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_api('订单参数异常');
        }
        $config = getMyConfig('site_513pay');
        if(!$config){
            res_api('未配置支付参数');
        }
        if($config['is_on'] != 1){
            res_api('该支付方式已关闭');
        }
        $field = 'id,order_no,money,status,pay_type';
        $where = [['order_no','=',$get['order_no']],['status','=',1]];
        $order = myDb::getCur('Order', $where,$field);
        if(!$order){
            res_api('订单数据异常');
        }
        if($order['status'] != 1){
            res_api('该订单已支付');
        }
        if($order['pay_type'] != 8){
            res_api('支付方式异常');
        }
//        if(checkIsWeixin()){
//            $cur = ['order_no'=>$order['order_no'],'money'=>$order['money']];
//            //res_api('点击右上角在浏览器中打开',2,$cur);
//            return $this->fetch('index@charge/notice',['cur'=>['order_no'=>$cur['order_no'],'money'=>$cur['money']]]);
//        }
        wuyisanPay::$config = $config;
        $noticeurl = 'http://'.$website['url'].'/index/Notify/wuyisanpay.html';

        $params = [
            "fxddh" => $order['order_no'], //商户订单号
            "fxdesc" => "漫画阅读", //商品名
            "fxfee" => $order['money'], //支付金额 单位元
            "fxattch" => $order['id'], //附加信息
            "fxnotifyurl" => $noticeurl, //异步回调 , 支付结果以异步为准
            "fxbackurl" => $get['back_url'], //同步回调 不作为最终支付结果为准，请以异步回调为准
            "fxpay" => $config['code'],//"dyzfb", //支付类型 此处可选项为 支付宝1:zfbH5   微信1:wxhf  支付宝2:dyzfb
            "fxip" => getClientIP(0, true) //支付端ip地址
        ];
        $res = wuyisanPay::getaliPayForm($params);
        if ($res['status'] == 1){
            header('Location:' . $res["payurl"]); //转入支付页面
            exit;
        }else{
            res_error($res['error']);
        }
    }

}