<?php
namespace app\api\controller;


use app\common\model\mdBook;
use site\myValidate;

class Novel extends Common {
    /**
     * showdoc
     * @catalog 小说漫画h5项目/分类
     * @title 获取小说分类
     * @description 接口
     * @method get
     * @url /api/novel/get_category
     * @param cate_type 是 int 类别(1:全部，2:最新,3:连载中，4:已完结,5:vip)
     * @param page 是 int 页码
     * @return {"code":1,"msg":"ok","data":[{"id":"570","name":"醉清风","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200525\/18c41670ac6e4fd4de101111de7eaa8b.jpg","summary":"  最是你身上的味道，让我欲罢不能。","hot_num":"79","over_type":"2","is_alone":"1","star":"9.68"},{"id":"697","name":"爱上监护人","author":"佚名","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200606\/d84f860ef263092650749dee2afee106.jpg","summary":"世间最可悲的莫过于给了你希望有又亲手把你推入绝望。 他以监护人的身份出现，强行踏入她的生命。 遇上爱，他一句话就要退出。 “顾黎，我最后问你一遍，你爱过我吗？” “没有。” “顾黎，你就是个胆小鬼，爱就是爱","hot_num":"7","over_type":"2","is_alone":"2","star":"9.38"}]}
     * @return_param data array 数据数组
     * @return_param id int 书籍id
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param cover url 封面图片
     * @return_param summary string 小说简介
     * @return_param hot_num int 人气值
     * @return_param over_type int (1连载中,2已完结)
     * @return_param is_alone int 是否独家
     * @return_param star float 星级
     */
    public function get_category(){
        $rules = [
            'type'=>['between:1,5',['in'=>'参数错误']],
            'page' => ['require|number|gt:0',['require'=>'参数错误','number'=>'参数错误','gt'=>'参数错误']]
        ];
        $post = myValidate::getData($rules,'get');
        $type = $post['type'] ? : 1;
        $list = mdBook::getNovelCategoryList($type, $post['page']);
        $list = $list ? : '';
        res_api($list);
    }
}