<?php
namespace app\api\controller;

use site\myDb;
use think\Db;
use site\myCache;
use think\Validate;
use think\Controller;
use app\common\model\mdConfig;

class Sync extends Controller{
	
	//发布书籍(漫画)
	public function createBook(){
		self::checkHeader();
		$config = getMyConfig('suiyiyun');
		$suiyiyun = $config ? $config['url'] : '';
		$rules = [
			'name' => ['require|max:200',['require'=>'未获取到书籍名称参数','max'=>'书籍名称长度超出限制']],
			'author' => ['max:50',['max'=>'作者名称长度超出限制']],
			'cover' => ['url',['url'=>'封面图片地址参数异常']],
			'detail_img' => ['url',['url'=>'详情图片地址参数异常']],
			'summary' => ['max:500',['max'=>'书籍简介长度超出限制']],
			'category' => ['max:50',['max'=>'书籍类型长度超出限制']],
			'over_type' => ['in:1,2',['in'=>'书籍连载属性错误']],
			'star' => ['float|elt:10',['float'=>'书籍星级参数异常','elt'=>'书籍星级必须小于等于10']],
			'hot_num' => ['number',['number'=>'书籍人气值参数异常']],
			'text_tag' => ['max:500',['max'=>'书籍标签长度超出限制']],
		];
		$post = $this->request->post();
		self::doValidate($rules, $post);
		$repeat = Db::name('Book')->where('name',$post['name'])->value('id');
		if($repeat){
			self::resMsg(['book_id'=>$repeat]);
		}else{
			$data = [
				'name' => $post['name'],
				'author' => $post['author'],
				'summary' => $post['summary'],
				'over_type' => $post['over_type'],
				'hot_num' => $post['hot_num'] ? : 0,
				'star' => $post['star'] ? : 0,
				'status' => 1,
				'free_type' => 2,
				'free_chapter' => 2,
				'money' => 58,
				'create_time' => time()
			];
			if($post['text_tag']){
				$text_tag = explode(',', trim($post['text_tag'],','));
				$data['text_tag'] = json_encode($text_tag,JSON_UNESCAPED_UNICODE);
			}
			if($post['category']){
				$catearr = explode(',', trim($post['category']));
				$key = 'cartoon_category';
				$cur = mdConfig::getConfig($key);
				if($cur === false){
					$cur = $catearr;
					mdConfig::addConfig($key, $cur);
				}else{
					foreach ($catearr as $v){
						if(!in_array($v, $cur)){
							$cur[] = $v;
						}
					}
					mdConfig::saveConfig($key, $cur);
				}
				cache($key,$cur);
				$data['category'] = ','.implode(',', $catearr).',';
			}
			if($post['cover'] || $post['detail_img']){
				$path = './uploads/img/'.date('Ymd');
				if(!is_dir($path)){
					mkdir($path,0777,true);
				}
				if(!is_dir($path)){
					self::resMsg('图片目录生成失败');
				}
				if($post['cover']){
					$coverinfo = pathinfo($post['cover']);
					if($coverinfo && isset($coverinfo['extension'])){
						$name = md5($post['cover']).'.'.$coverinfo['extension'];
						$filename = $path.'/'.$name;
						$imgcontent = self::getImgContent($post['cover']);
						if($imgcontent){
							$myfile = fopen($filename,"w");
							fwrite($myfile,$imgcontent);
							fclose($myfile);
							$data['cover'] = $suiyiyun.ltrim($filename,'.');
						}
					}
				}
				if($post['detail_img']){
					$detailinfo = pathinfo($post['detail_img']);
					if($detailinfo && isset($detailinfo['extension'])){
						$name = md5($post['detail_img']).'.'.$detailinfo['extension'];
						$filename = $path.'/'.$name;
						$imgcontent = self::getImgContent($post['detail_img']);
						if($imgcontent){
							$myfile = fopen($filename,"w");
							fwrite($myfile,$imgcontent);
							fclose($myfile);
							$data['detail_img'] = $suiyiyun.ltrim($filename,'.');
						}
					}
				}
			}
			$book_id = Db::name('Book')->insertGetId($data);
			if($book_id){
				self::getBookId($data['name'],$book_id);
				self::resMsg(['book_id'=>$book_id]);
			}else{
				self::resMsg('发布书籍失败');
			}
		}
	}
	
	//同步漫画内容
	public function cartoon(){
		self::checkHeader();
		set_time_limit(1800);
		$config = getMyConfig('suiyiyun');
		$suiyiyun = $config ? $config['url'] : '';
		$rules = [
				'book_name' => ['require',['require'=>'未获取到书籍名称参数']],
				'chapter_name' => ['require',['require'=>'未获取到章节名称参数']],
				'referer' => ['require|url',['require'=>'未检测到来源地址','url'=>'来源地址格式不规范']],
				'content' => ['require',['require'=>'未获取到章节内容']]
		];
		$post = $this->request->post();
		self::doValidate($rules, $post);
		$book_id = self::getBookId($post['book_name']);
		if(!$book_id){
			self::resMsg('该书籍尚未发布');
		}
		$time = time();
		$number = self::getFileNumber($post['chapter_name']);
		if(!$number){
			self::resMsg('章节名称解析失败');
		}
		$table = 'BookChapter';
		$repeat = Db::name($table)->where('book_id',$book_id)->where('number',$number)->value('id');
		if($repeat){
			self::resMsg('该章节已存在');
		}
		$chapterData = [
				'book_id'=>$book_id,
				'name'=> $post['chapter_name'],
				'src' => '',
				'number'=> $number,
				'files' => [],
				'create_time'=>time()
		];
		$html = '';
		$fRes = true;
		if(is_array($post['content'])){
			$content = $post['content'];
		}else{
			$content = explode(',', $post['content']);
		}
		$path = './uploads/book/'.$book_id;
		if(!is_dir($path)){
			mkdir($path,0777,true);
		}
		if(!is_dir($path)){
			self::resMsg('图片目录生成失败');
		}
		foreach ($content as $k=>$v){
			if($v){
				$imgUrl = trim($v);
				$doRes = false;
				$pathInfo = pathinfo($imgUrl);
				if($pathInfo && is_array($pathInfo)){
					$imgData = self::getImgContent($imgUrl,$post['referer']);
					if($imgData){
						$savename = md5($imgUrl).'.'.$pathInfo['extension'];
						$filename = $path.'/'.$savename;
						$myfile = fopen($filename,"w");
						fwrite($myfile,$imgData);
						fclose($myfile);
						if(@is_file($filename)){
							$url = $suiyiyun.ltrim($filename,'.');
							if(!$chapterData['src']){
								$chapterData['src'] = $url;
							}
							$chapterData['files'][] = $url;
							$html .= '<img src="'.$url.'" />';
							$doRes = true;
						}else{
							self::resMsg('漫画内容图片保存失败');
						}
					}
				}
				if($doRes){
					continue;
				}else{
					$fRes = false;
					break;
				}
			}
		}
		if($fRes){
			$chapterData['files'] = json_encode($chapterData['files']);
			Db::startTrans();
			$flag = false;
			$chapter_id = Db::name($table)->insertGetId($chapterData);
			if($chapter_id){
				$block_res = saveBlock($html,$number,'book/'.$book_id);
				if($block_res){
					$flag = true;
				}
			}
			if($flag){
				Db::commit();
				myCache::rmBookChapterNum($book_id);
				myCache::rmBookChapterList($book_id);
				self::checkUpdateTime($book_id);
				self::resMsg();
			}else{
				Db::rollback();
				self::resMsg('插入章节失败');
			}
		}else{
			self::resMsg('解析章节内容失败');
		}
	}

    //发布小说
    public function createNovel(){
        self::checkHeader();
        $config = getMyConfig('suiyiyun');
        $suiyiyun = $config ? $config['url'] : '';
        $rules = [
            'name' => ['require|max:200',['require'=>'未获取到书籍名称参数','max'=>'书籍名称长度超出限制']],
            'author' => ['max:50',['max'=>'作者名称长度超出限制']],
            'cover' => ['url',['url'=>'封面图片地址参数异常']],
            'detail_img' => ['url',['url'=>'详情图片地址参数异常']],
            'summary' => ['max:500',['max'=>'书籍简介长度超出限制']],
            'category' => ['max:50',['max'=>'书籍类型长度超出限制']],
            'over_type' => ['in:1,2',['in'=>'书籍连载属性错误']],
            'star' => ['float|elt:10',['float'=>'书籍星级参数异常','elt'=>'书籍星级必须小于等于10']],
            'hot_num' => ['number',['number'=>'书籍人气值参数异常']],
            'text_tag' => ['max:500',['max'=>'书籍标签长度超出限制']],
        ];
        $post = $this->request->post();
        self::doValidate($rules, $post);
        $repeat = Db::name('Book')->where('name',$post['name'])->value('id');
        if($repeat){
            self::resMsg(['book_id'=>$repeat]);
        }else{
            $data = [
                'name' => $post['name'],
                'author' => $post['author'],
                'summary' => $post['summary'],
                'over_type' => $post['over_type'],
                'hot_num' => $post['hot_num'] ? : 0,
                'star' => $post['star'] ? : 0,
                'status' => 1,
                'free_type' => 2,
                'free_chapter' => 2,
                'money' => 58,
                'create_time' => time()
            ];
            if($post['text_tag']){
                $text_tag = explode(',', trim($post['text_tag'],','));
                $data['text_tag'] = json_encode($text_tag,JSON_UNESCAPED_UNICODE);
            }
            if($post['category']){
                $catearr = explode(',', trim($post['category']));
                $key = 'cartoon_category';
                $cur = mdConfig::getConfig($key);
                if($cur === false){
                    $cur = $catearr;
                    mdConfig::addConfig($key, $cur);
                }else{
                    foreach ($catearr as $v){
                        if(!in_array($v, $cur)){
                            $cur[] = $v;
                        }
                    }
                    mdConfig::saveConfig($key, $cur);
                }
                cache($key,$cur);
                $data['category'] = ','.implode(',', $catearr).',';
            }
            if($post['cover'] || $post['detail_img']){
            	$path = './uploads/img/'.date('Ymd');
            	if(!is_dir($path)){
            		mkdir($path,0777,true);
            	}
            	if(!is_dir($path)){
            		self::resMsg('图片目录生成失败');
            	}
            	if($post['cover']){
            		$coverinfo = pathinfo($post['cover']);
            		if($coverinfo && isset($coverinfo['extension'])){
            			$name = md5($post['cover']).'.'.$coverinfo['extension'];
            			$filename = $path.'/'.$name;
            			$imgcontent = self::getImgContent($post['cover']);
            			if($imgcontent){
            				$myfile = fopen($filename,"w");
            				fwrite($myfile,$imgcontent);
            				fclose($myfile);
            				$data['cover'] = $suiyiyun.ltrim($filename,'.');
            			}
            		}
            	}
            	if($post['detail_img']){
            		$detailinfo = pathinfo($post['detail_img']);
            		if($detailinfo && isset($detailinfo['extension'])){
            			$name = md5($post['detail_img']).'.'.$detailinfo['extension'];
            			$filename = $path.'/'.$name;
            			$imgcontent = self::getImgContent($post['detail_img']);
            			if($imgcontent){
            				$myfile = fopen($filename,"w");
            				fwrite($myfile,$imgcontent);
            				fclose($myfile);
            				$data['detail_img'] = $suiyiyun.ltrim($filename,'.');
            			}
            		}
            	}
            }
            $data['type'] = 2;//1漫画 2小说
            $book_id = Db::name('Book')->insertGetId($data);
            if($book_id){
                self::getBookId($data['name'],$book_id);
                self::resMsg(['book_id'=>$book_id]);
            }else{
                self::resMsg('发布小说失败');
            }
        }
    }

    //同步小说内容
    public function novel(){
        self::checkHeader();
        set_time_limit(1800);
        $rules = [
            'book_name' => ['require',['require'=>'未获取到书籍名称参数']],
            'chapter_name' => ['require',['require'=>'未获取到章节名称参数']],
            'content' => ['require',['require'=>'未获取到章节内容']]
        ];
        $post = $this->request->post();
        self::doValidate($rules, $post);
        $book_id = self::getBookId($post['book_name']);
        if(!$book_id){
            self::resMsg('该书籍尚未发布');
        }
        $time = time();
        $number = self::getFileNumber($post['chapter_name']);
        if(!$number){
            self::resMsg('章节名称解析失败');
        }
        $table = 'BookChapter';
        $repeat = Db::name($table)->where('book_id',$book_id)->where('number',$number)->value('id');
        if($repeat){
            self::resMsg('该章节已存在');
        }
        $chapterData = [
            'book_id'=>$book_id,
            'name'=> $post['chapter_name'],
            'src' => '',
            'number'=> $number,
            'files' => [],
            'create_time'=>time()
        ];
        $html = '';
        $fRes = true;
        if(strpos($post['content'], '<p>') !== false){
            $html = $post['content'];
        }else{
            $html = "<p>".$post['content'];
            $html = preg_replace('/\n|\r\n/','</p><p>',$html);
        }
        if($fRes){
            Db::startTrans();
            $flag = false;
            $chapter_id = Db::name($table)->insertGetId($chapterData);
            if($chapter_id){
                $block_res = saveBlock($html,$number,'book/'.$book_id);
                if($block_res){
                    $flag = true;
                }
            }
            if($flag){
                Db::commit();
                myCache::rmBookChapterNum($book_id);
                myCache::rmBookChapterList($book_id);
                self::checkUpdateTime($book_id);
                self::resMsg();
            }else{
                Db::rollback();
                self::resMsg('插入章节失败');
            }
        }else{
            self::resMsg('解析章节内容失败');
        }
    }

    //发布视频
    public function create_videos(){
        $videos = $this->request->post();
        if (!is_array($videos) || empty($videos)){
            res_api("格式不正确");
        }
        $fail_videos = $success_video =[];
        foreach ($videos as $key=>$video){
            if (empty($video['title'])){
                res_api("标题不能为空");
            }
            if (empty($video['img'])){
                res_api("封面图不能为空");
            }
            if (empty($video['m3u8'])){
                res_api("视频地址不能为空");
            }
            $repeat = myDb::getCount('Videos',['title'=>$video['title']]);
            if ($repeat){
                $fail_videos[] = $video['title'];
                unset($videos[$key]);
                continue;
            }
            $success_video[] = $video['title'];
            $videos[$key]['create_time'] = time();
        }
        if (empty($videos)){
            res_api('视频已存在');
        }
        $res = myDb::addAll('Videos',$videos);
        if ($res){
            if (empty($fail_videos)){
                res_api('发布视频成功：'.json_encode($success_video,256),1);
            }else{
                res_api('发布视频成功：'.json_encode($success_video,256).'。重复视频标题：'.json_encode($fail_videos,256),1);
            }
        }else{
            res_api('发布视频失败');
        }
    }

	
	//获取图像内容
	private function getImgContent($img_url,$referer=''){
		$header = array('User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:45.0) Gecko/20100101 Firefox/45.0', 'Accept-Language: zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3', 'Accept-Encoding: gzip, deflate', );
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $img_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		if($referer){
			curl_setopt($curl, CURLOPT_REFERER,$referer);
		}
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip');
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		$data = curl_exec($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		return $data;
	}
	
	//获取章节
	private function getFileNumber($title){
		$number = 0;
		$pattern = '/[(\d)|(零一壹二贰三叁四肆五伍六陆七柒八捌九玖十拾百佰千仟万两)]+/u';
		$data = preg_match($pattern, $title,$match);
		if(!empty($match)){
			$number = $match[0];
			if(is_numeric($number)){
				$number = intval($number);
			}else{
				$number = self::chrtonum($number);
			}
		}
		return $number;
	}
	
	//中文转阿拉伯
	private function chrtonum($string){
		if(is_numeric($string)){
			return $string;
		}
		$string = str_replace('仟', '千', $string);
		$string = str_replace('佰', '百', $string);
		$string = str_replace('拾', '十', $string);
		$num = 0;
		$wan = explode('万', $string);
		if (count($wan) > 1) {
			$num += self::chrtonum($wan[0]) * 10000;
			$string = $wan[1];
		}
		$qian = explode('千', $string);
		if (count($qian) > 1) {
			$num += self::chrtonum($qian[0]) * 1000;
			$string = $qian[1];
		}
		$bai = explode('百', $string);
		if (count($bai) > 1) {
			$num += self::chrtonum($bai[0]) * 100;
			$string = $bai[1];
		}
		$shi = explode('十', $string);
		if (count($shi) > 1) {
			$num += self::chrtonum($shi[0] ? $shi[0] : '一') * 10;
			$string = $shi[1] ? $shi[1] : '零';
		}
		$ling = explode('零', $string);
		if (count($ling) > 1) {
			$string = $ling[1];
		}
		$d = [
				'一' => '1','二' => '2','三' => '3','四' => '4','五' => '5','六' => '6','七' => '7','八' => '8','九' => '9',
				'壹' => '1','贰' => '2','叁' => '3','肆' => '4','伍' => '5','陆' => '6','柒' => '7','捌' => '8','玖' => '9',
				'零' => 0, '0' => 0, 'O' => 0, 'o' => 0,
				'两' => 2
		];
		return $num + @$d[$string];
	}
	
	//根据书籍名称获取书籍ID
	private function getBookId($book_name,$book_id=0){
		$key = md5('book_name_'.$book_name);
		if($book_id){
			cache($key,$book_id);
		}else{
			if(cache('?'.$key)){
				$book_id = cache($key);
			}else{
				$book_id = Db::name('Book')->where('name',$book_name)->value('id');
				if($book_id){
					cache($key,$book_id);
				}
			}
			return $book_id;
		}
	}
	
	//检查并更新书籍最后更新时间
	private function checkUpdateTime($book_id){
		$chapter = Db::name('BookChapter')->where('book_id',$book_id)->order('number','desc')->field('id,create_time')->find();
		if($chapter){
			$update_date = date('Y-m-d',$chapter['create_time']);
			$book = myCache::getBook($book_id);
			if($book && $book['update_date'] != $update_date){
				$re = Db::name('Book')->where('id',$book['id'])->setField('update_date',$update_date);
				if($re){
					myCache::doBook($book['id'], 'update_date',$update_date);
				}
			}
		}
	}
	
	/**
	 * 重组验证参数
	 * @param array 验证规则
	 * @return array 验证参数和错误信息
	 */
	private function getValidateParam($rules){
		$rule = $msg = array();
		foreach ($rules as $k=>$v){
			$rule[$k] = $v[0];
			$rules_msg = $v[1];
			foreach ($rules_msg as $key=>$val){
				$rk = $k.'.'.$key;
				$msg[$rk] = $val;
			}
		}
		$res = ['rule'=>$rule,'msg'=>$msg];
		return $res;
	}
	
	//验证数据
	private function doValidate($rules,$data){
		$res = self::getValidateParam($rules);
		$validate = Validate::make($res['rule'],$res['msg']);
		$result = $validate->check($data);
		if(!$result){
			self::resMsg($validate->getError());
		}
	}
	
	//返回接口消息
	private function resMsg($msg='ok',$code=1,$data=null){
		if(is_array($msg)){
			$data = $msg;
			$msg = 'ok';
		}else{
			if($msg !== 'ok'){
				$code = 2;
			}
		}
		$res = ['code' => $code,'msg' => $msg,'data'=>$data];
		exit(json_encode($res,JSON_UNESCAPED_UNICODE));
	}
	
	//验证header参数
	private function checkHeader(){
		$res = false;
		$key = strtoupper('http_viewtoken');
		if(isset($_SERVER[$key]) && $_SERVER[$key] === 'ef927cecbe990d688b6f364be2ae303c'){
			$res = true;
		}
		if(!$res){
			self::resMsg('验证token失败');
		}
	}
}
