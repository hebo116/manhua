<?php
namespace app\api\controller;

use app\common\model\mdOrder;
use site\myCache;
use site\myDb;
use site\myHttp;
use site\myValidate;

class Charge extends Common {
    public function index(){
        global $loginId;
        $cur = myCache::getMember($loginId);
        $cur['money'] = myCache::getMemberMoney($loginId);
        $get = myHttp::getData('book_id','get');
        $charge = myCache::getSiteCharge();
        $coins = $vips = [];
        $check_money = 0;
        foreach ($charge as $k=>$v){
            if($v['status'] != 1){
                unset($charge[$k]);
                continue;
            }
            if($v['pay_times']){
                $charge_num = myCache::getChargeNum($loginId, $v['id']);
                if($charge_num >= $v['pay_times']){
                    unset($charge[$k]);
                    continue;
                }
            }
            if($v['type'] == 1){
                $coins[] = $v;
            }else{
                $vips[] = $v;
            }
            if($v['is_check'] == 1){
                $check_money = $v['money'];
            }
        }
        $variable = [
            'get' => $get,
            'cur' => $cur,
            'coins' => $coins,
            'vips' => $vips,
            'check_money' => $check_money,
            'paylist' => self::getPayList()
        ];
       dump($variable);die;
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/储值中心
     * @title 获取充值数据
     * @description 接口
     * @method get
     * @url /api/charge/getChargeList
     * @param token 是 string 用户标识
     * @return {"code":1,"msg":"ok","data":{"charge":{"8":{"id":"8","type":"2","money":"365.00","content":{"coin":0,"vip_type":"4","send_coin":0},"is_hot":"2","is_check":"1","pay_times":"0","sort_num":"8","status":"1"},"7":{"id":"7","type":"1","money":"199.00","content":{"coin":"20000","vip_type":0,"send_coin":"7000"},"is_hot":"2","is_check":"2","pay_times":"0","sort_num":"7","status":"1"},"6":{"id":"6","type":"2","money":"199.00","content":{"coin":0,"vip_type":"3","send_coin":0},"is_hot":"2","is_check":"2","pay_times":"0","sort_num":"6","status":"1"},"5":{"id":"5","type":"1","money":"19.90","content":{"coin":"2000","vip_type":0,"send_coin":"300"},"is_hot":"2","is_check":"2","pay_times":"0","sort_num":"5","status":"1"},"4":{"id":"4","type":"2","money":"89.00","content":{"coin":0,"vip_type":"2","send_coin":0},"is_hot":"2","is_check":"2","pay_times":"0","sort_num":"4","status":"1"},"3":{"id":"3","type":"2","money":"39.00","content":{"coin":0,"vip_type":"1","send_coin":0},"is_hot":"2","is_check":"2","pay_times":"0","sort_num":"3","status":"1"},"2":{"id":"2","type":"1","money":"99.00","content":{"coin":"10000","vip_type":0,"send_coin":"3000"},"is_hot":"2","is_check":"2","pay_times":"0","sort_num":"2","status":"1"},"1":{"id":"1","type":"1","money":"49.00","content":{"coin":"5000","vip_type":0,"send_coin":"2000"},"is_hot":"1","is_check":"2","pay_times":"0","sort_num":"1","status":"1"}},"check_money":"365.00"}}
     * @return_param data array 数据数组
     * @return_param type int (1书币2vip)
     * @return_param money int 充值金额
     * @return_param content int 套餐内容
     * @return_param coin int 书币
     * @return_param vip_type int (1:日卡,2:月卡,3:季卡,4:半年卡,5:年卡,6:至尊卡)
     * @return_param send_coin int 赠送书币
     * @return_param is_hot int 是否热门
     * @return_param is_check int 是否默认选中
     * @return_param pay_times int 充值次数限制(0不限)
     * @return_param sort_num int 排序
     * @return_param status int (1显示2隐藏)
     * @return_param check_money int 默认选中的金额
     */
    public function getChargeList(){
        $loginId = $this->u_id;
        $charge = myCache::getSiteCharge();
        $check_money = 0;
        foreach ($charge as $k=>$v){
            if($v['status'] != 1){
                unset($charge[$k]);
                continue;
            }
            if($v['pay_times']){
                $charge_num = myCache::getChargeNum($loginId, $v['id']);
                if($charge_num >= $v['pay_times']){
                    unset($charge[$k]);
                    continue;
                }
            }
            if($v['is_check'] == 1){
                $check_money = $v['money'];
            }
        }
        $data = ['charge'=>$charge,'check_money'=>$check_money];
        res_api($data,1);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/储值中心
     * @title 获取选择支付方式
     * @description 接口
     * @method get
     * @url /api/charge/getPayList
     * @param token 是 string 用户标识
     * @return {"code":1,"msg":"ok","data":[{"id":2,"is_check":2,"icon":"\/static\/resource\/index\/images\/weixinpay.png","name":"微信支付","summary":"充值"},{"id":3,"is_check":1,"icon":"\/static\/resource\/index\/images\/alipay.png","name":"支付宝","summary":"充值"}]}
     * @return_param data array 数据数组
     * @return_param id int 主键id
     * @return_param is_check int 充值金额
     * @return_param icon uri 图标相对路径
     * @return_param name string 名称
     * @return_param summary string 支付类型
     */
    public function getPayList(){
        $website = getMyConfig('website');
        $url="https://".$website["url"];
        $list = [
            'wxh5pay'			=>	['id'=>1,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/weixinpay.png'],
            'site_78alipay'	    =>	['id'=>7,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/alipay.png'],
            'site_78wxpay'	    =>	['id'=>6,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/weixinpay.png'],
            'site_otherwxpay'	=>	['id'=>2,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/weixinpay.png'],
            'site_otheralipay'	=>	['id'=>3,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/alipay.png'],
            'site_xuben666pay'	=>	['id'=>4,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/weixinpay.png'],
            'site_xuben666alipay'	=>	['id'=>5,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/alipay.png'],
            'site_513pay'	    =>	['id'=>8,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/alipay.png'],
            'site_wxantpay'	    =>	['id'=>9,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/weixinpay.png'],
            'site_alih5antpay'	    =>	['id'=>10,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/alipay.png'],
            'site_alih5antsuperpay'	    =>	['id'=>11,'is_check'=>2,'icon'=>$url.'/static/resource/index/images/alipay.png'],
        ];

        //if(!checkIsWeixin()){
            unset($list['wxh5pay']);
            //unset($list['site_otherwxpay']);
        //}
        $is_check = false;
        $config = getMyConfig('website');
        foreach ($list as $key=>&$val){
            $payConfig = getMyConfig($key);
            if($payConfig && $payConfig['is_on'] == 1){
                $val['name'] = $payConfig['name'];
                $val['summary'] = $payConfig['summary'];
                if($config['pay_type'] == $val['id']){
                    $val['is_check'] = 1;
                    $is_check = true;
                }
            }else{
                unset($list[$key]);
            }
        }
        if($list){
            $list = array_values($list);
            if(!$is_check){
                $list[0]['is_check'] = 1;
            }
        }
        res_api($list,1);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/储值中心
     * @title 充值
     * @description 接口
     * @method post
     * @url /api/charge/doCharge
     * @param token 是 string 用户标识
     * @param pay_type 是 int 支付方式(2:微信,3:支付宝)
     * @param charge_id 是 int 充值数据id
     * @param back_uri 是 uri 支付成功后的跳转地址(用户首页的uri)
     * @param book_id 否 int 书籍id（用户在读到付费章节去付费的时候传该参数）
     * @return {}
     * @return_param data array 数据数组
     * @return_param url url 支付链接
     */
    public function doCharge(){
        $loginId = $this->u_id;
        $book_id = myValidate::getData(['book_id' => ['number|gt:0',['number'=>'书籍参数错误','gt'=>'书籍参数错误']]],'post');
        $rules = [
            'pay_type' => ['require|between:1,11',['require'=>'请选择支付方式','between'=>'未指定该支付方式']],
            'charge_id' => ['require|number|gt:0',['require'=>'充值信息异常','number'=>'充值信息异常','gt'=>'充值信息异常']],
            'back_uri' =>['require',['require'=>'跳转地址必填']]
        ];
        $post = myValidate::getData($rules);
        $chargeList = myCache::getSiteCharge();
        if(!$chargeList || !isset($chargeList[$post['charge_id']])){
            res_api('未开启该支付方式');
        }
        $charge = $chargeList[$post['charge_id']];
        $time = time();
        if($charge['pay_times']){
            $chargeNum = myCache::getChargeNum($loginId, $post['charge_id']);
            if($chargeNum >= $charge['pay_times']){
                res_api('该套餐仅限充值'.$charge['pay_times'].'次');
            }
        }
        $member = myCache::getMember($loginId);
        $data = mdOrder::createOrderInfo($member);
        $data['charge_id'] = $post['charge_id'];
        //$data['money'] = $charge['money'] - 1;
        $data['money'] = $charge['money'];
        if($charge['type'] == 1){
            $data['send_money'] = $charge['content']['coin'];
            if($charge['content']['send_coin']){
                $data['send_money'] += $charge['content']['send_coin'];
            }
        }else{
            $data['package'] = $charge['content']['vip_type'];
        }
        $data['type'] = 1;
        $data['create_date'] = date('Ymd',$time);
        $data['create_time'] = $time;
        $data['order_no'] = mdOrder::createOrderNo();
        $config = getMyConfig('website');
        $data['pay_type'] = $post['pay_type'];
        if($book_id){
            $data['book_id'] = $book_id;
        }
        if($member['spread_id']){
            $data['spread_id'] = $member['spread_id'];
        }
        $count_info = [];
        if($data['channel_id']){
            $curInfo =  mdOrder::getCountInfo($data['channel_id'], $data['money']);
            if($curInfo){
                $count_info['channel'] = $curInfo;
            }
        }
        if($data['agent_id']){
            $curInfo = mdOrder::getCountInfo($data['agent_id'], $data['money']);
            if($curInfo){
                $count_info['agent'] = $curInfo;
            }
        }
        $data['count_info'] = json_encode($count_info);
        $res = myDb::add('Order', $data);
        if($res){
            //$back_url = 'http://'.$_SERVER['HTTP_HOST'].'/#/recharge';
            $back_url = 'http://mh.qqcmh.click/#/recharge';
            $pay_url = mdOrder::createPayUrl($data['order_no'], $data['pay_type'],$back_url,'h5');
            res_api(['url'=>$pay_url]);
        }else{
            res_api('创建订单失败',2);
        }
    }
}