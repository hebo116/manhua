<?php

namespace app\api\controller;


use app\common\model\mdBook;
use app\common\model\mdMember;
use site\myCache;
use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use think\Validate;

class Book extends Common {
    /**
     * showdoc
     * @catalog 小说漫画h5项目/分类
     * @title 获取漫画分类
     * @description 接口
     * @method get
     * @url /api/book/get_category
     * @param cate_type 是 int 类别(1:全部，2:最新,3:连载中，4:已完结,5:vip)
     * @param page 是 int 页码
     * @return {"code":1,"msg":"ok","data":[{"id":"18","name":"神印王座","author":"快乐工场","cover":"https:\/\/nc-bk.oss-cn-hongkong.aliyuncs.com\/images\/20200409\/8a682a0af37352d1fbe0a04ed2dce64f.jpg","summary":"【每周一、五更新】魔族强势，在人类即将被灭绝之时，六大圣殿崛起，带领着人类守住最后的领土。一名少年，为救母加入骑士圣殿，奇迹、诡计，不断在他身上上演。在这人类6大圣殿与魔族72柱魔神相互倾轧的世界，他能否登上象征着骑士最高荣耀的神印王座？...","hot_num":"2270038","over_type":"1","is_alone":"2","star":"8.00"}]}
     * @return_param data array 数据数组
     * @return_param id int 书籍id
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param cover url 封面图片
     * @return_param summary string 小说简介
     * @return_param hot_num int 人气值
     * @return_param over_type int (1连载中,2已完结)
     * @return_param is_alone int 是否独家
     * @return_param star float 星级
     */
    public function get_category(){
        $cate_type = input('cate_type');
        $page = input('page');
        $data = mdBook::getCategoryList($cate_type,$page);
        res_api($data);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/首页
     * @title 今日更新（漫画、小说通用）
     * @description 接口
     * @method get
     * @url /api/book/refresh
     * @param type 是 int 类别(1:漫画，2：小说)
     * @return {"code":1,"msg":"ok","data":[{"name":"周三","list":[{"id":"605","over_type":"1","is_alone":"2","name":"难言之隐之中医馆","author":"韩漫","hot_num":"45732","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/30d6c7f940589c1c1cd65bf6e5be4b9c.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/8d5dff3d507528ead96cc19bda9a70c8.jpg","star":"9.45","summary":"有难言之隐在身的他，总是无法让女朋友感到快乐。他无意间来到一家中医馆诊治，美女医师的治疗方法有点与众不同啊！"},{"id":"606","over_type":"1","is_alone":"2","name":"爱徒","author":"韩漫","hot_num":"57605","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/7d1ea85fc53587165acae46fb3dff25b.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/721eb60d37c9dd1c9fdf31581101fab2.jpg","star":"9.85","summary":"曾红极一时的落魄画家，收了朋友的女儿当学生。天使的面孔魔鬼的身材，让单纯的美术课走向了另一门艺术。"},{"id":"594","over_type":"1","is_alone":"2","name":"老婆的妹妹","author":"韩漫","hot_num":"58996","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/10dec411831d879a9de7f905838e0ae8.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/55ba79882bafb91f23e75fd56c961349.jpg","star":"9.51","summary":"结婚纪念日旅行途中发生意外，导致妻子双眼失明。但是没有在那场灾难中去世，是幸运还是不幸？我开始注意住在一起的老婆的妹妹"},{"id":"602","over_type":"1","is_alone":"2","name":"亲爱的大叔","author":"韩漫","hot_num":"65468","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/35f2b3cacfe8879949ae12c2fa614f0f.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/550168d3771c86daf8eec591f8fcf608.jpg","star":"9.70","summary":"对大叔而言我只是一个小女孩，但是大叔....你知道吗，我每天都幻想着和你....."},{"id":"600","over_type":"1","is_alone":"2","name":"超会卖的女业务","author":"韩漫","hot_num":"66499","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/ed60974752ce64cf1ad76dc69789cbd3.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/bfe94510626b7205e46a2cc2391293da.jpg","star":"9.19","summary":"经营不善面临倒闭的公司突然迎来了一位超级厉害的女销售员，她一人就让公司起死回生，到底她有什么样的能力？"},{"id":"651","over_type":"1","is_alone":"2","name":"致命游戏","author":"韩漫","hot_num":"99582","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/0e5060eedf4dcbaa0a95e019fa4a10c8.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/e114aa9eb3e51c3a15b7fa3f79aa0221.jpg","star":"9.71","summary":"曾经犯下杀人案的慈善家知道自己时日不多，为了实现年轻的梦想，他找来了17个人.,..."},{"id":"615","over_type":"1","is_alone":"2","name":"新闻主播","author":"韩漫","hot_num":"99590","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/ad7b60b03fd16799523397e5a52a8825.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/d8a7df9b371d81becd7ddcfaa91f2c56.jpg","star":"9.87","summary":"最强新闻主播被打压，沦为网络小主播，可是还有最强美女女友啊，但是别忘了，还有惦记他女友的台长！"},{"id":"647","over_type":"1","is_alone":"2","name":"媳妇的诱惑","author":"韩漫","hot_num":"99623","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/01facfb59da46bb50d1d6dfce377f142.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/257da3965bdbf52c6b16c6865d0f31a7.jpg","star":"9.97","summary":"新当媳妇的她又漂亮又懂事还很勤快，但是谁知道她最大的目标竟然是自己的公公！"},{"id":"614","over_type":"1","is_alone":"2","name":"与前妻同居","author":"韩漫","hot_num":"99634","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/0c81fce1d732025dc49d003b05795a47.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/091d39687e8f8fba0b989b89ffcafd70.jpg","star":"9.25","summary":"貌合神离的夫妻，在去法院办理离婚手续的路上，丈夫提出了最后一个要求。没想到这个要求竟然让两个的关系出现了一些变化。"},{"id":"190","over_type":"1","is_alone":"2","name":"淫新小套房","author":"佚名","hot_num":"336469","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200523\/cbb2966f06485417601407fed1c1300d.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200523\/638d790c45aa8592d385e5416aa2ff21.jpg","star":"9.81","summary":"江霖在租屋处与卉美姐初嚐禁果后，卉美姐便经常到小套房去为他「亲身」授课..."},{"id":"641","over_type":"1","is_alone":"2","name":"老师","author":"韩漫","hot_num":"995664","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/cf65430da7cf63a8eda465c9bfe790d6.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/ff4d697c67338e6631693cd11a022699.jpg","star":"9.67","summary":"他迷恋着自己的数学老师，但是他并不是痴痴的等着，而是一步一步的实行着自己的计划，把老师变成自己的女人。"}],"num":1},{"name":"周四","list":[],"num":2}]}
     * @return_param data array 数据数组
     * @return_param name string 周
     * @return_param list array 数据数组
     * @return_param id int 书籍id
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param cover url 封面图片
     * @return_param summary string 小说简介
     * @return_param hot_num int 人气值
     * @return_param over_type int (1连载中,2已完结)
     * @return_param is_alone int 是否独家
     * @return_param detail_img url 详情图片
     * @return_param star float 星级
     */
    public function refresh(){
        $category = input('get.type',1,'intval');
        $updateIds = myCache::getUpdateBookIds($category);
        //dump($updateIds);die;
        $start_time = strtotime(date('Y-m-d',strtotime('-6 days')));
        $list = [];
        $today_time =  strtotime('today');
        $num = 0;
        while ($start_time <= $today_time){
            $num++;
            $name = '';
            $bookList = [];
            $date = date('Y-m-d',$start_time);
            if($start_time == $today_time){
                $name = '今日';
            }else{
                $week = date('N',$start_time);
                switch ($week){
                    case 1:$name = '周一';break;
                    case 2:$name = '周二';break;
                    case 3:$name = '周三';break;
                    case 4:$name = '周四';break;
                    case 5:$name = '周五';break;
                    case 6:$name = '周六';break;
                    case 7:$name = '周日';break;
                }
            }
            if(isset($updateIds[$date])){
                foreach ($updateIds[$date] as $v){
                    $book = mdBook::getIndexBookInfo($v);
                    if($book){
                        $bookList[] = $book;
                    }
                }
            }
            $list[] = ['name'=>$name,'list'=>$bookList,'num'=>$num];
            $start_time += 86400;
        }
        res_api($list);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/书籍
     * @title 书籍详情(漫画、小说通用)
     * @description 接口
     * @method get
     * @url /api/book/info
     * @param book_id 是 int 书籍id
     * @param invite_code 否 string 邀请码
     * @return {"code":1,"msg":"ok","data":{"user":[],"book":{"id":"613","type":"1","name":"健身教练","author":"韩漫","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/ad2bbef228a979a37d4b74b989cdc2cf.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/331c71db193db68f6dd9701a77a69608.jpg","summary":"你以为高大帅气的男教练为什么总是能保持销售榜第一？你以为教练天天和你耳鬓厮磨只是训练需要？其实他们是一举两得。","category":"韩漫,都市","free_type":"2","over_type":"1","is_beast":"2","is_recommend":"1","is_alone":"2","is_vip":"1","free_chapter":"2","money":"48","hot_num":"1247664","star":"9.30","status":"1","is_top":"2","top_num":"0","text_tag":[""],"update_date":"2020-06-13","create_time":"1590647168","chapter_num":"34","end_chapter":{"name":"34","number":"34","create_time":"2020-06-13 12:49"}},"likes":[{"id":"622","over_type":"1","is_alone":"2","name":"大嫂，哥哥的女人","author":"韩漫","hot_num":"1347684","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/abb99a8d5f2cb93fbf93a5d7014a296c.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/73734df2dad62c817e74b0ee57dc2aec.jpg","star":"9.72","summary":"面对诱惑的大嫂，小叔还能顶多久？"},{"id":"404","over_type":"1","is_alone":"2","name":"漂亮干姐姐","author":"佚名","hot_num":"1328475","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/14ed18e1a19b36f75a758135e307cf24.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/bf12e28ee33b48f9b44cf181f74013c9.jpg","star":"9.85","summary":"志豪與慧美從小一起長大，多熙則是慧美的大學時期閨蜜，三人的感情在志豪大二後開始有了化學變化..."},{"id":"613","over_type":"1","is_alone":"2","name":"健身教练","author":"韩漫","hot_num":"1247664","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/ad2bbef228a979a37d4b74b989cdc2cf.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/331c71db193db68f6dd9701a77a69608.jpg","star":"9.30","summary":"你以为高大帅气的男教练为什么总是能保持销售榜第一？你以为教练天天和你耳鬓厮磨只是训练需要？其实他们是一举两得。"}],"is_sub":"no","share_url":"http:\/\/www.changjinman.cn\/index\/book\/info.html?book_id=613","is_collect":"no","read_number":1}}
     * @return_param data array 数据数组
     * @return_param user array 用户数据
     * @return_param username string 用户名
     * @return_param viptime timestamp vip到期时间
     * @return_param headimg url 头像
     * @return_param money int 用户剩余书币
     * @return_param book array 书籍详情数据
     * @return_param likes array 猜你喜欢
     * @return_param id int 书籍id
     * @return_param type int 类型(1:漫画，2：小说)
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param cover url 封面图片
     * @return_param detail_img url 详情图片
     * @return_param summary string 小说简介
     * @return_param category string 小说分类
     * @return_param free_type int 1免费2收费
     * @return_param over_type int 1连载中2已完结
     * @return_param is_beast int 是否精品
     * @return_param is_recommend int 是否推荐
     * @return_param is_alone int 是否独家
     * @return_param is_vip int 是否VIP
     * @return_param free_chapter int 免费章节
     * @return_param money int 每章节金额
     * @return_param hot_num int 人气值
     * @return_param star float 星级
     * @return_param status int 1上架，2下架
     * @return_param is_top int 是否置顶1是2否
     * @return_param top_num int 顶,排序值
     * @return_param text_tag json 书籍标签
     * @return_param update_date datetime 最后更新日期
     * @return_param create_time int 创建时间
     * @return_param chapter_num int 书总章节数
     * @return_param end_chapter int 最新章节
     * @return_param is_sub string 是否订阅
     * @return_param share_url url 分享链接
     * @return_param is_collect string 是否收藏到书架
     * @return_param read_number int 用户最后一次阅读的章节
     */
    public function info(){
        global $loginId;

        $get = input('');
        $validate =validate("Book");
        if (!$validate->scene('info')->check($get)){
            res_api($validate->getError(),2);
        }

        if(isset($get['invite_code'])){
            $invite_uid = myCache::getUidByCode($get['invite_code']);
            if($invite_uid){
                if(!$loginId){
                    $session_invite_code = '';
                    if(session('?INDEX_INVITE_CODE')){
                        $session_invite_code = session('INDEX_INVITE_CODE');
                    }
                    if($session_invite_code !== $get['invite_code']){
                        session('INDEX_INVITE_CODE',$get['invite_code']);
                        mdMember::addShareMoney($invite_uid,30);
                    }
                }
            }
        }
        $book = myCache::getBook($get['book_id']);
        if(empty($book)){
            res_api('书籍不存在');
        }
        if(!in_array($book['status'], [1,2])){
            res_api('书籍已下架');
        }
        $read_number = 1;
        if($loginId){
            $readCache = myCache::getReadCache($loginId);
            $rkey = 'book_'.$book['id'];
            if($readCache && isset($readCache[$rkey])){
                $read_number = $readCache[$rkey]['last_number'];
            }
        }
        mdBook::addHot($book['id']);
        $book['hot_num'] = myCache::getBookHotNum($book['id']);
        $book['chapter_num'] = myCache::getBookChapterNum($book['id']);
        if($book['category']){
            $book['category'] = trim($book['category'],',');
        }
        $chapter_list = myCache::getBookChapterList($book['id']);
        $chapter = [];
        if($chapter_list){
            reset($chapter_list);
            $end = end($chapter_list);
            $chapter['name'] = '第'.$end['name'].'話 ';
            $chapter['number'] = $end['number'];
            $chapter['create_time'] = date('Y-m-d H:i',$end['create_time']);
        }
        $book['end_chapter'] = $chapter;
        $is_collect = $is_sub = 'no';
        $user = [];
        //$share_url = 'http://'.$_SERVER['HTTP_HOST'].'/index.html#/detail?book_id='.$book['id'];
        $share_url = 'http://mh.qqcmh.click/index.html#/detail?book_id='.$book['id'];
        if($loginId){
            $member = myCache::getMember($loginId);
            $collectIds = myCache::getCollectBookIds($loginId);
            if($collectIds && in_array($book['id'], $collectIds)){
                $is_collect = 'yes';
            }
            $subIds = myCache::getSubBookIds($loginId);
            if($subIds && in_array($book['id'], $subIds)){
                $is_sub = 'yes';
            }
            $user['username'] = $member['username'];
            $user['viptime'] = '';
            if($member['viptime']){
                if($member['viptime'] == 1){
                    $user['viptime'] = 'VIP：永久有效';
                }else{
                    $user['viptime'] = 'VIP：'.date('Y-m-d',$member['viptime']).'到期';
                }
            }
            $user['headimg'] = $member['headimgurl'];
            $user['money'] = myCache::getMemberMoney($loginId);
            $share_url .= '&invite_code='.$member['invite_code'];
        }
        $likes = mdBook::getCacheBookData(5,3,0,$book['type']);
        $variable = [
            'user' => $user,
            'book' => $book,
            'likes' => $likes,
            'is_sub' => $is_sub,
            'share_url' => $share_url,
            'is_collect' => $is_collect,
            'read_number' => $read_number,
        ];
        res_api($variable);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/书籍
     * @title 书籍收藏(加入书架)
     * @description 接口
     * @method get
     * @url /api/book/doCollect
     * @param token 是 string 用户标识
     * @param book_id 是 int 书籍id
     * @return {"code":1,"msg":"收藏成功","data":null}
     * @return_param data array 数据数组
     */
    public function doCollect(){
        $loginId = $this->u_id;
        //$rules = ['book_id'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']]];
        $book_id = input('book_id','');
        if (!$book_id){
            res_api('书籍信息异常',2);
        }

        $ids = myCache::getCollectBookIds($loginId);
        if($ids && in_array($book_id, $ids)){
            res_api();
        }
        $data = [
            'uid' => $loginId,
            'book_id' => $book_id,
            'create_time' => time()
        ];
        $re = myDb::add('MemberCollect', $data);
        if($re){
            myCache::rmCollectBookIds($loginId);
            res_api('收藏成功',1);
        }else{
            res_api('收藏失败',2);
        }
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/书籍
     * @title 章节信息（漫画、小说通用）
     * @description 接口
     * @method get
     * @url /api/book/chapter
     * @param book_id 是 int 书籍id
     * @param size 否 int 每页显示条数（默认30）
     * @return {"code":1,"msg":"ok","data":{"book_id":"613","list":[{"title":"1~34","is_check":"yes","id":"block_1","list":[{"id":"31639","name":"1","number":"1","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/a967a74fb8322bbb6c4a8c7e0e1343e7.jpg","money":"0","create_time":"1590659404"},{"id":"47180","name":"2","number":"2","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/86054747d522f7e4c623bac7093fad99.jpg","money":"0","create_time":"1591755949"},{"id":"31641","name":"3","number":"3","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/21d6a2e8865194dcf155c70460c38a88.jpg","money":"48","create_time":"1590659405"},{"id":"31642","name":"4","number":"4","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/b2d6bfec065248bff10629c88f042a2f.jpg","money":"48","create_time":"1590659405"},{"id":"31643","name":"5","number":"5","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/d7e0480bc15e7b5ba68d2fd4333a395b.jpg","money":"48","create_time":"1590659406"},{"id":"31644","name":"6","number":"6","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/f022e56ea13f0efc0e6854f8a4f79a0e.jpg","money":"48","create_time":"1590659406"},{"id":"31645","name":"7","number":"7","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/25fac06168c8304352e08517f29cfdcd.jpg","money":"48","create_time":"1590659406"},{"id":"31646","name":"8","number":"8","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/fa81028b84d5a1b20e6309c46463d6f4.jpg","money":"48","create_time":"1590659406"},{"id":"31647","name":"9","number":"9","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/1dcb37024465e09339204e480ae9f1e7.jpg","money":"48","create_time":"1590659406"},{"id":"38817","name":"10","number":"10","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/9836110ce15182e4991037daf02da9ad.jpg","money":"48","create_time":"1591284895"},{"id":"31648","name":"11","number":"11","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/8ebeddae86a49dc08e1f298e0757b29d.jpg","money":"48","create_time":"1590659407"},{"id":"31649","name":"12","number":"12","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/f8a62540d4289cca2545c469e3d606a8.jpg","money":"48","create_time":"1590659407"},{"id":"31650","name":"13","number":"13","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/8157edfa7b760ab9cbc19b37b6348119.jpg","money":"48","create_time":"1590659408"},{"id":"31651","name":"14","number":"14","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/304a4c0257b232d05e4476cb01cd3e1e.jpg","money":"48","create_time":"1590659408"},{"id":"31652","name":"15","number":"15","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/dee0eeb67742f7ad999ac6d4737820fd.jpg","money":"48","create_time":"1590659409"},{"id":"31653","name":"16","number":"16","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/76586f943a0db778500b898c36e69dde.jpg","money":"48","create_time":"1590659409"},{"id":"31654","name":"17","number":"17","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/f4e323004f9ef871605121c89374243f.jpg","money":"48","create_time":"1590659409"},{"id":"31655","name":"18","number":"18","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/8795fa13aa10fc3d20f9821cf9b4da24.jpg","money":"48","create_time":"1590659410"},{"id":"31656","name":"19","number":"19","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/6d4da695689503c982283cb858ce0037.jpg","money":"48","create_time":"1590659410"},{"id":"31657","name":"20","number":"20","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/550b60dbb81668378fb48729c84bb9d5.jpg","money":"48","create_time":"1590659410"},{"id":"31658","name":"21","number":"21","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/30faa1522794dd1820d74e7d51654bdc.jpg","money":"48","create_time":"1590659411"},{"id":"31659","name":"22","number":"22","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/e76ce50ec1adeffdc7a91b16d80da366.jpg","money":"48","create_time":"1590659411"},{"id":"31660","name":"23","number":"23","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/83e5deb2b7b549c1ecd14e021177ab5a.jpg","money":"48","create_time":"1590659412"},{"id":"31661","name":"24","number":"24","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/79f0b32a54305a402abe8f8864d4ba73.jpg","money":"48","create_time":"1590659412"},{"id":"31662","name":"25","number":"25","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/977dd3327721804ec23e42dcbd7b68b6.jpg","money":"48","create_time":"1590659412"},{"id":"31663","name":"26","number":"26","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/7834f73866852367adcb138abb169056.jpg","money":"48","create_time":"1590659413"},{"id":"31664","name":"27","number":"27","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/71f413d2a53cdba641b5c4fc284aae71.jpg","money":"48","create_time":"1590659413"},{"id":"31665","name":"28","number":"28","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/41878745e190ee5e87ab52efe928077d.jpg","money":"48","create_time":"1590659413"},{"id":"31666","name":"29","number":"29","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/119cffae5b159986bde06fffc5d9d4b5.jpg","money":"48","create_time":"1590659414"},{"id":"31667","name":"30","number":"30","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/ea077f33bd7f12c50da820fe63851482.jpg","money":"48","create_time":"1590659414"},{"id":"31668","name":"31","number":"31","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/0017b4f820b7cbe9c8b260a8b44a3326.jpg","money":"48","create_time":"1590659415"},{"id":"38734","name":"32","number":"32","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/23c6f8e711ddf9a4bb33b008bc22462b.jpg","money":"48","create_time":"1591118602"},{"id":"38735","name":"33","number":"33","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/35158b25b39926b6a00017943fada3f8.jpg","money":"48","create_time":"1591118683"},{"id":"47194","name":"34","number":"34","src":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/book\/613\/369c65de993ad830c419f227f973635d.jpg","money":"48","create_time":"1592023749"}]}],"total":34,"user":[]}}
     * @return_param data array 数据数组
     * @return_param title string 页码
     * @return_param id int 章节id
     * @return_param name int 章节名称
     * @return_param number int 阅读人数
     * @return_param src url 章节封面图
     * @return_param money int 章节所需书币（0免费,-1已购,>0表示章节所需的书币）
     * @return_param create_time int 录入时间
     * @return_param total int 总章节数
     */
    public function chapter(){
        global $loginId;
        $size = input('size',30);
        $book_id = myHttp::getId('书籍','book_id');
        $book = myCache::getBook($book_id);
        if(!$book){
            res_api('书籍不存在');
        }
        if(!in_array($book['status'], [1,2])){
            res_api('书籍已下架');
        }
        $num = 0;
        $temp = [];
        $is_free = myCache::checkBookFree($book['id']);
        $cache = myCache::getBookChapterList($book['id']);
        if($cache){
            foreach ($cache as &$vv){
                $vv['src'] = handleImg($vv['src']);
            }
            $list = self::getReadChapterList($cache, $book, $is_free);
            $list = array_chunk($list, $size);
            foreach ($list as $k=>$v){
                $length = count($v);
                $title  = ($num+1).'~'.($num+$length);
                $is_check = $num === 0 ? 'yes' : 'no';
                $temp[] = ['title'=>$title,'list'=>$v];
                $num += $length;
            }
        }
        $user = [];
        if($loginId){
            $member = myCache::getMember($loginId);
            $user['username'] = $member['username'];
            $user['viptime'] = '';
            if($member['viptime']){
                if($member['viptime'] == 1){
                    $user['viptime'] = 'VIP：永久有效';
                }else{
                    $user['viptime'] = 'VIP：'.date('Y-m-d',$member['viptime']).'到期';
                }
            }
            $user['headimg'] = $member['headimgurl'];
            $user['money'] = myCache::getMemberMoney($loginId);
        }
        $variable = [
            'book_id'=>$book['id'],
            'list'=>$temp,
            'total'=>$num,
            'user'=>$user
        ];
        res_api($variable);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/书籍
     * @title 书籍正文(漫画、小说通用)
     * @description 接口
     * @method get
     * @url /api/book/read
     * @param book_id 是 int 书籍id
     * @param number 是 int 章节
     * @param kc_code 否 string 推广码
     * @return {}
     * @return_param data array 数据数组
     * @return_param id int 书籍id
     * @return_param type int 类型(1:漫画，2：小说)
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param cover url 封面图片
     * @return_param detail_img url 详情图片
     * @return_param summary string 小说简介
     * @return_param category string 小说分类
     * @return_param free_type int 1免费2收费
     * @return_param over_type int 1连载中2已完结
     * @return_param is_beast int 是否精品
     * @return_param is_recommend int 是否推荐
     * @return_param is_alone int 是否独家
     * @return_param is_vip int 是否VIP
     * @return_param free_chapter int 免费章节
     * @return_param money int 每章节金额
     * @return_param hot_num int 人气值
     * @return_param star float 星级
     * @return_param status int 1上架，2下架
     * @return_param is_top int 是否置顶1是2否
     * @return_param top_num int 顶,排序值
     * @return_param text_tag json 书籍标签
     * @return_param update_date datetime 最后更新日期
     * @return_param create_time int 创建时间
     * @return_param is_sub string 是否订阅
     * @return_param read_type int (1:可读,2需登录，3需订阅，4需订阅)
     * @return_param share_url url 分享链接
     * @return_param invite_code string 邀请码
     * @return_param user_money int 用户剩余书币
     * @return_param number array (current:当前章节，prev：上一章节，next：下一章节)
     * @return_param content text 书籍正文
     */
    public function read(){
        global $loginId;
        //$kc_code = myValidate::getData(['kc_code'=>['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]],'get');
        $kc_code = input('kc_code');
        if($kc_code){
            if (strlen($kc_code) != 6){
                res_api('非法访问');
            }
            $codeInfo = myCache::getKcCodeInfo($kc_code);
            if($codeInfo && $codeInfo['type'] == 1){
                $spread = myCache::getSpread($codeInfo['sid']);
                if(!$spread){
                    res_api('该链接已失效');
                }

                if ($loginId){
                    $members = myCache::getMember($loginId);
                    if ($members['spread_id'] == 0){
                        $results = myDb::setField('member',['id'=>$loginId],'spread_id',$spread['id']);
                        if ($results){
                            myCache::rmMember($loginId);
                        }
                    }
                }

                session('INDEX_KC_CODE',$kc_code);
                session('INDEX_SPREAD_ID',$spread['id']);
                myDb::add('SpreadView',['sid'=>$spread['id'],'create_time'=>time()]);
                $get = [
                    'book_id' => $spread['book_id'],
                    'number' => $spread['chapter_number']
                ];
                $get['number'] = self::getReadNumber($get['book_id'],$get['number']);
                //$get['android_uid'] = input('android_uid');
                //$get['android_token'] = input('android_token');
                //$get['number'] = input('number',1);
            }else{
                res_api('非法访问');
            }
        }else{
            //$rules = [
            //    'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
            //    'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
            //];
            //$get = myValidate::getData($rules,'get');

            $get =input();
            if (empty($get['book_id'])){
                res_api('书籍参数错误');
            }
            if (empty($get['number'])){
                res_api('章节参数错误');
            }
//            if (empty($get['android_uid'])){
//                res_api('安卓uid错误');
//            }
//            if (empty($get['android_token'])){
//                res_api('安卓token错误');
//            }
        }
        $book = myCache::getBook($get['book_id']);
        if(!$book){
            res_api('书籍不存在');
        }
        if(!in_array($book['status'], [1,2])){
            res_api('书籍已下架');
        }
        $totalChapter = myCache::getBookChapterNum($book['id']);
        if($totalChapter == 0){
            res_api('该书籍尚无可读章节');
        }
        $list = myCache::getBookChapterList($book['id']);
        $number = ($get['number'] > $totalChapter) ? $totalChapter : $get['number'];
        if(!isset($list[$number])){
            res_api('该章节不存在');
        }
        $next_number = (($number + 1) > $totalChapter) ? 0 : ($number + 1);
        $is_free = myCache::checkBookFree($book['id']);
        $read_type = 1;;
        $chapterList = self::getReadChapterList($list, $book, $is_free);
        $chapter = $chapterList[$number];
        $is_sub = 'no';
        //$share_url = 'http://'.$_SERVER['HTTP_HOST'].'/index.html#/detail?book_id='.$book['id'];
        $share_url = 'http://mh.qqcmh.click/index.html#/detail?book_id='.$book['id'];
        $user_money = 0;
        $invite_code = '';
        if(!$loginId){
            if(!$is_free){
                $login_res = self::checkIsLogin($book, $number);
                if($login_res){
                    $read_type = 2;
                }
            }
        }else{
            $member = myCache::getMember($loginId);
            $invite_code = $member['invite_code'];
            $share_url .= '&invite_code='.$member['invite_code'];
            $subIds = myCache::getSubBookIds($loginId);
            if($subIds && in_array($book['id'], $subIds)){
                $is_sub = 'yes';
            }
            $isAdd = false;
            $is_read = myCache::checkIsRead($loginId, $book['id'], $number);
            if(!$is_read){
                $isAdd = true;
                if(!$is_free){
                    if($book['free_type'] == 2 && $number > $book['free_chapter']){
                        $is_money = true;;
                        if($member['viptime']){
                            if($member['viptime'] == 1 || $member['viptime'] > time()){
                                $is_money = false;
                                $res = mdBook::addReadhistory($book,$chapter,$loginId);
                                if($res){
                                    $isAdd = false;
                                }
                            }
                        }
                        if($is_money){
                            $isAdd = false;
                            if($is_sub == 'yes'){
                                $money = myCache::getMemberMoney($loginId);
                                if($money >= $chapter['money']){
                                    $res = mdBook::addReadhistory($book, $chapter, $loginId,$chapter['money']);
                                    if(!$res){
                                        res_api('扣费失败');
                                    }
                                }else{
                                    //调用接口扣书币
                                    //$rt =mdBook::doCion($get['android_uid'],$get['android_token'],$chapter['money']);
                                    //if (!$rt){
                                        $read_type = 4;
                                    //}
                                }
                            }else{
                                $read_type = 3;
                            }
                        }
                    }
                }
            }
            if($isAdd){
                mdBook::addReadhistory($book,$chapter,$loginId);
            }
            $user_money = myCache::getMemberMoney($loginId);
        }
        $variable = [
            'book' => $book,
            'is_sub' => $is_sub,
            'dir' => 'book/'.$book['id'],
            'chapter' => $chapter,
            'read_type' => $read_type,
            'share_url' => $share_url,
            'invite_code' => $invite_code,
            'user_money' => $user_money,
            'number' => ['current'=>$number,'prev'=>($number-1),'next'=>$next_number]
        ];
        if($read_type == 1){
            mdBook::addChapterRead($chapter['id']);
        }
        if ($read_type == 2){
            res_api('需登录',3);
        }
        $content_img = getBlockContent($variable['number']['current'],$variable['dir']);
        $variable['content'] = $content_img;
        if ($book['type'] == 1){

            $header=getallheaders();
            if(isset($header["plat"])){
                /**yy
                 *
                 */
                $yykey = 'yy_files_'.$get['book_id']."_".$get['number'];
                if (cache('?'.$yykey)){
                    $yy_files = cache($yykey);
                }else{
                    $yy_files = myDb::getValue('BookChapter',['book_id'=>$get['book_id'],'number'=>$get['number']],'yy_files');
                    cache($yykey,$yy_files,30);
                }

                if ($yy_files){
                    $variable['content'] = json_decode($yy_files,true);
                }else{
                    $variable['content'] = [];
                }
            }else{
                preg_match_all ("/<img src=\"(.*)\" \/>/U", $content_img, $pat_array);
                $variable['content'] =$pat_array[1];
            }






            foreach ($variable['content'] as $k=>$v){
                //$variable['content'][$k] = str_replace('http://630029.caigoubao.cc','http://h1.haituie.com',$v);
                $variable['content'][$k] = handleImg($v);
            }
           
           
            if ($read_type == 3 || $read_type == 4){
                if ($variable['content']){
                    $variable['content'] = [$variable['content'][0]];
                }
            }
        }

        if ($book['type'] == 2){
            if ($read_type == 3 || $read_type == 4){
                $contents  = $variable['content'];
                if ($contents){
                    $str = mb_substr($contents,0,150);
                    //dump($str);die;
                    $variable['content'] = $str.'......';
                }
            }
        }
        unset($variable['dir']);
        res_api($variable);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/书籍
     * @title 购买章节
     * @description 接口
     * @method get
     * @url /api/book/duBuyNumber
     * @param token 是 string 用户标识
     * @param book_id 是 int 书籍id
     * @param number 是 int 章节
     * @return {}
     * @return_param data array 数据数组
     * @return_param code int (1成功2失败)
     */
    public function duBuyNumber(){
        $validate = new Validate([
            'book_id' => 'require|number|gt:0',
            'number' => 'require|number|gt:0',
            //'android_uid'=>'number',
            //'android_token'=>'require',
        ], [
            'book_id.require' => '书籍参数错误',
            'book_id.number' => '书籍参数错误',
            'book_id.gt' => '书籍参数错误',
            'number.require' => '章节参数错误',
            'number.number' => '章节参数错误',
            'number.gt' => '章节参数错误',
            //'android_uid.number'=>'androiduid为数字',
            //'android_token.require'=>'androidtoken必填',
        ]);
        $post = input();
        if (!$validate->check($post)) {
             res_api($validate->getError(),2);
        }

        $book = myCache::getBook($post['book_id']);
        if(!$book){
            res_api('书籍信息异常');
        }
        global $loginId;
        $member = myCache::getMember($loginId);
        $is_read = myCache::checkIsRead($member['id'], $book['id'], $post['number']);
        if(!$is_read){
            $list = myCache::getBookChapterList($book['id']);
            if(!isset($list[$post['number']])){
                res_api('该章节不存在');
            }
            $chapter = $list[$post['number']];
            $is_free = myCache::checkBookFree($book['id']);
            if(!$is_free){
                $is_money = true;;
                if($member['viptime']){
                    if($member['viptime'] == 1 || $member['viptime'] > time()){
                        $is_money = false;
                        mdBook::addReadhistory($book,$chapter,$loginId);
                    }
                }
                if($is_money){
                    $money = $book['money'];
                    if($chapter['money']){
                        $money = $chapter['money'];
                    }
                    $userMoney = myCache::getMemberMoney($member['id']);
                    if($userMoney >= $money){
                        $res = mdBook::addReadhistory($book, $chapter, $member['id'],$money);
                        if(!$res){
                            res_api('购买失败，请重试',2);
                        }
                    }else{
                        //调用接口扣书币
                        //$rt =mdBook::doCion($post['android_uid'],$post['android_token'],$money);
                        //if (!$rt){
                            res_api('您的书币余额不足，是否立即充值',2);
                        //}
                    }
                }
            }else{
                mdBook::addReadhistory($book, $chapter, $member['id']);
            }
        }
        res_api();
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/公共
     * @title 检测章节是否可读
     * @description 接口
     * @method get
     * @url /api/book/checkChapter
     * @param book_id 是 int 书籍id
     * @param number 是 int 章节
     * @return {}
     * @return_param data array 数据数组
     * @return_param url url 章节接口uri
     */
    public function checkChapter(){
        global $loginId;
        $rules = [
            'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
            'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
        ];
        $post = myValidate::getData($rules,'get');
        $book = myCache::getBook($post['book_id']);
        if(!$book){
            res_api('书籍不存在');
        }
        if(!in_array($book['status'], [1,2])){
            res_api('书籍已下架');
        }
        $totalChapter = myCache::getBookChapterNum($book['id']);
        if($totalChapter == 0){
            res_api('该书籍尚无可读章节');
        }
        if($post['number'] > $totalChapter){
            res_api('已经是最后一章了');
        }
        $chapterList = myCache::getBookChapterList($book['id']);
        if(!isset($chapterList[$post['number']])){
            res_api('该章节不存在');
        }
        $chapter = $chapterList[$post['number']];
        $spread_id = session('INDEX_SPREAD_ID');
        if($spread_id){
            $spread = myCache::getSpread($spread_id);
            if($spread){
                if($post['number'] >= $spread['number']){
                    if($spread['is_sub'] == 1){
                        if(!$loginId){
                            res_api('您尚未登录，是否立即登录',3);
                        }
                    }
                }
            }
        }
        $is_free = myCache::checkBookFree($book['id']);
        if($loginId){
            if($book['free_type'] == 2 && $post['number'] > $book['free_chapter']){
                $is_read = myCache::checkIsRead($loginId, $book['id'], $post['number']);
                if(!$is_read){
                    if(!$is_free){
                        $member = myCache::getMember($loginId);
                        $is_money = true;
                        if($member['viptime'] > 0){
                            $is_money = false;
                            if($member['viptime'] != 1){
                                if(time() > $member['viptime']){
                                    $is_money = true;
                                }
                            }
                        }
                        if($is_money){
                            $money = myCache::getMemberMoney($loginId);
                            $book_money = $chapter['money'] ? $chapter['money'] : $book['money'];
                            if($money < $book_money){
                                res_api('您的书币余额不足，是否立即充值',3,['url'=>my_url('Charge/index',['book_id'=>$book['id']])]);
                            }
                        }
                    }
                }
            }
        }else{
            if(!$is_free){
                if($book['free_type'] == 2 && $post['number'] > $book['free_chapter']){
                    res_api('您尚未登录，是否立即登录',3);
                }
            }
        }
        res_api(['url'=>my_url('read',['book_id'=>$book['id'],'number'=>$post['number']])]);
    }
    /**
     * showdoc
     * @catalog 小说漫画h5项目/我的漫画(小说)
     * @title 阅读历史
     * @description 接口
     * @method get
     * @url /api/book/readHistory
     * @param token 是 string 用户标识
     * @return {"code": 1,"msg": "ok","data": null}
     * @return_param data array 数据数组
     * @return_param uid int 用户id
     * @return_param num int 阅读记录数量
     * @return_param list array 阅读记录
     * @return_param id int 书籍id
     * @return_param name string 书籍名称
     * @return_param cover url 封面图地址
     * @return_param url url 书籍地址
     * @return_param create_time timestamp 阅读时间
     * @remark 无
     */
    public function readHistory(){
        $variable = mdMember::getMyReadHistory($this->u_id);
        unset($variable['type']);
        unset($variable['update_num']);
        res_api($variable);
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/我的漫画(小说)
     * @title 书架
     * @description 接口
     * @method get
     * @url /api/book/collect
     * @param token 是 string 用户标识
     * @return {"code":1,"msg":"ok","data":{"type":1,"list":[{"name":"漂亮干姐姐","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200524\/14ed18e1a19b36f75a758135e307cf24.jpg","id":"404","url":"\/api\/book\/info.html?book_id=404"},{"name":"兼职奶妈","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/4ca891a3a99a167c9ffd3a3303af5a27.jpg","id":"619","url":"\/api\/book\/info.html?book_id=619"},{"name":"大嫂，哥哥的女人","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/abb99a8d5f2cb93fbf93a5d7014a296c.jpg","id":"622","url":"\/api\/book\/info.html?book_id=622"},{"name":"按摩妹女友","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200528\/34d4b51398d9b4b452b1d64e23132b78.jpg","id":"624","url":"\/api\/book\/info.html?book_id=624"},{"name":"KTV情人","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/b94edbb495d0fa0b5202480befbf4bda.jpg","id":"636","url":"\/api\/book\/info.html?book_id=636"},{"name":"玩转女上司","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200529\/f289f6e73e29d22417be6e02980c3f30.jpg","id":"650","url":"\/api\/book\/info.html?book_id=650"},{"name":"前女友变帮佣","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200605\/3ad4883ee76ca963793dede07053633e.jpg","id":"694","url":"\/api\/book\/info.html?book_id=694"},{"name":"双妻生活","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200605\/9136d0e33e3bbff14bf547b396640a36.jpg","id":"696","url":"\/api\/book\/info.html?book_id=696"}],"uid":"6324","num":8,"update_num":0}}
     * @return_param data array 数据数组
     * @remark 无
     */
    public function collect(){
        $variable = mdMember::getMyCollect($this->u_id);
        unset($variable['type']);
        unset($variable['update_num']);
        res_api($variable);
    }

    //整理章节列表
    private function getReadChapterList($chapterList,$book,$is_free){
        global $loginId;
        if($is_free || $book['free_type'] == 1){
            foreach ($chapterList as &$cv){
                $cv['money'] = 0;
            }
        }else{
            $read_numbers = [];
            if($loginId){
                $readCache = myCache::getReadCache($loginId);
                $lkey = 'book_'.$book['id'];
                if(isset($readCache[$lkey])){
                    $read_numbers = $readCache[$lkey]['numbers'];
                }
            }
            foreach ($chapterList as &$cv){
                $cv['name'] = '第'.$cv['name'].'話 ';
                if($cv['number'] > $book['free_chapter']){
                    if(!$cv['money']){
                        $cv['money'] = $book['money'];
                    }
                    if($cv['money']){
                        if($read_numbers && in_array($cv['number'], $read_numbers)){
                            $cv['money'] = -1;
                        }
                    }
                }
            }
        }
        return $chapterList;
    }

    //检测是否强制登录
    private function checkIsLogin($book,$number){
        $res = false;
        if($book['free_type'] == 2){
            if($number > $book['free_chapter']){
                $res = true;
            }else{
                $spread_id = session('INDEX_SPREAD_ID');
                if($spread_id){
                    $spread = myCache::getSpread($spread_id);
                    if($spread){
                        if(($number >= $spread['number']) && ($book['id'] == $spread['book_id'])){
                            if($spread['is_sub'] == 1){
                                $res = true;
                            }
                        }
                    }
                }
            }
        }
        return $res;
    }

    //获取阅读章节
    private function getReadNumber($book_id,$number=0){
        $number = $number ? : 1;
        if(checkIsWeixin()){
            global $loginId;
            $cache = myCache::getReadCache($loginId);
            $key = 'book_'.$book_id;
            if(isset($cache[$key])){
                $number = $cache[$key]['last_number'];
            }
        }
        return $number;
    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/分类
     * @title 分类类型列表
     * @description 接口
     * @method get
     * @url /api/book/categoryList
     * @param book_type 是 int 书籍类别(1:漫画，2：小说)
     * @return {"code":1,"msg":"ok","data":{"free_type":{"name":"free_type","option":[{"val":1,"text":"免费","default":0},{"val":2,"text":"收费","default":1}]},"over_type":{"name":"over_type","option":[{"val":1,"text":"连载中","default":1},{"val":2,"text":"已完结","default":0}]},"category":["韩漫","国漫","爱情","耽美","恐怖","搞笑","校园","多肉","豪门","悬疑","暴力","灵异","异能","重生","黑帮","都市","伦理","真人漫"]}}
     * @return_param data array 数据数组
     * @return_param free_type array 书籍属性
     * @return_param over_type array 更新状态
     * @return_param category array 书籍分类
     * @return_param text string 名称
     * @return_param val int 值（分类搜索的时候传这个值。）
     * @return_param default int （暂时没用，先不管）
     */
    public function categoryList(){
        $book_type = input('book_type',1);
        if (!in_array($book_type,[1,2])){
            res_api('非法访问');
        }
        $cate =[];
        if ($book_type == 1){
            $cate = getMyConfig('cartoon_category');
        }elseif($book_type == 2){
            $cate = getMyConfig('novel_category');
        }

        $op = mdBook::getOptions();

        $options = [
            'free_type'=>$op['free_type'],
            'over_type'=>$op['over_type'],
            'category'=>$cate
        ];

        res_api($options);

    }

    /**
     * showdoc
     * @catalog 小说漫画h5项目/分类
     * @title 分类书籍列表
     * @description 接口
     * @method get
     * @url /api/book/categoryBookList
     * @param page 否 int 页码(默认1)
     * @param limit 否 int 每页显示条数(默认10)
     * @param type 是 int 书籍类别(1:漫画，2：小说)
     * @param free_type 否 int (1免费2收费)
     * @param over_type 否 int (1连载中2已完结)
     * @param category 否 int 书籍分类名称(直接传中文)
     * @return {"code":1,"msg":"ok","data":[{"id":"847","type":"2","name":"阴阳神帝","author":"摩北","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200720\/3605fbbc780eaae1bbd5b9da9160f8aa.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200720\/7af4e93900bdf2175f98a168c99ff911.jpg","summary":"腾空突破到武气四层之后由于内体神秘岩珠导致四年修为无法提升，被家族放弃之后腾空和岩珠中神秘老者达成协议，开始疯狂力提升自己修为，为了成为强者他敢与天搏命，为了红颜知己他独闯龙潭虎穴，只为那曾经的誓言，要想不被人凌辱，更好地照顾身边之人，他唯有踏上最强之路：与天争锋，同天夺命！","category":",玄幻,男频,","free_type":"1","over_type":"1","is_beast":"1","is_recommend":"2","is_alone":"2","is_vip":"1","free_chapter":"15","money":"28","hot_num":"1397","star":"9.70","status":"1","is_top":"2","top_num":"0","text_tag":"[\"\"]","update_date":"2020-07-20","create_time":"1595218066"},{"id":"842","type":"2","name":"一梦封神","author":"明月当朱","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200720\/41e3b2a2dd7a5d71607a8b6b6d129267.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200720\/d107ed16139dda32dfb823e2776a11ce.jpg","summary":"洛阳做了一个梦。    在梦中，他挥手之间翻山填海，动作之间风流云散，王座之下万人臣服，一个眼神神魔寂灭……    他醒后发现，自己竟然拥有了梦中的一切。    一梦之后，天下无双！","category":",玄幻,男频,","free_type":"1","over_type":"1","is_beast":"1","is_recommend":"2","is_alone":"2","is_vip":"1","free_chapter":"15","money":"28","hot_num":"0","star":"9.12","status":"1","is_top":"2","top_num":"0","text_tag":"[\"\"]","update_date":"2020-07-20","create_time":"1595217075"},{"id":"825","type":"2","name":"武逆天下","author":"步履","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200720\/94e6ea35bf341007ae8d46817ae4f815.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200720\/7549796b80ea56168230fcd1b7bc880d.jpg","summary":"无意中获得一个神奇方鼎，少年踏上了武修和神修的道路。离家历练，从此崛起！武宗武神，又能如何？挥手间八方云动，诸天幻灭，唯我武尊天下！","category":",玄幻,男频,","free_type":"1","over_type":"1","is_beast":"1","is_recommend":"2","is_alone":"2","is_vip":"1","free_chapter":"15","money":"28","hot_num":"3","star":"9.31","status":"1","is_top":"2","top_num":"0","text_tag":"[\"\"]","update_date":"2020-07-20","create_time":"1595213286"},{"id":"812","type":"2","name":"万能任务系统","author":"一切随缘","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200719\/42ccf16880b2825632b6d782a3654e7b.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200719\/0af12bbf02379b1da187528cfc11c014.jpg","summary":"作死不装逼的系统是没有灵魂的！作死之后再装逼岂不是美滋滋？意外得到万能系统的郑吒在异界开始了一段作死装逼加复仇的奇异旅程。你以为是打怪、升级、拍卖会？不不不，实际上是偷窥、裸奔、逛青楼！总之这个系统只能在中午用，因为早晚会出事......","category":",都市异能,玄幻,男频,古代言情,","free_type":"1","over_type":"1","is_beast":"1","is_recommend":"2","is_alone":"2","is_vip":"1","free_chapter":"15","money":"28","hot_num":"0","star":"9.19","status":"1","is_top":"2","top_num":"0","text_tag":"[\"\"]","update_date":"2020-07-20","create_time":"1595148039"},{"id":"807","type":"2","name":"太古神帝","author":"小轩","cover":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200719\/c55ee40bdb1e697bc2814e6b73fe7d1c.jpg","detail_img":"http:\/\/630029.caigoubao.cc\/630029\/manhua\/uploads\/img\/20200719\/af2b38899022b15eba69165ae8b0cf97.jpg","summary":" 当罗尘修炼任何灵术的速度都比别人快时，当罗尘服用任何丹药的效果都比别人好时，当罗尘使用任何灵器的威力都比别人强时。一个反派眼中又贱又不讲理还特么打不过的妖孽横空出世了！","category":",玄幻,男频,古代言情,","free_type":"1","over_type":"1","is_beast":"1","is_recommend":"2","is_alone":"2","is_vip":"1","free_chapter":"15","money":"28","hot_num":"0","star":"9.11","status":"1","is_top":"2","top_num":"0","text_tag":"[\"\"]","update_date":"2020-07-20","create_time":"1595146238"}]}
     * @return_param data array 数据数组
     * @return_param id int 书籍id
     * @return_param name string 书籍名称
     * @return_param author string 作者
     * @return_param cover url 封面图片
     * @return_param summary string 小说简介
     * @return_param hot_num int 人气值
     * @return_param over_type int (1连载中,2已完结)
     * @return_param is_alone int 是否独家
     * @return_param star float 星级
     */
    public function categoryBookList(){
        $config = [
            'default' => [['status','=',1],['type','=',1]],
            'eq' => 'type,over_type,free_type',
            'like' => 'keyword:name,category'
        ];
        $pages = myHttp::getPageParams();
        $where = mySearch::getWhere($config);
        $res = mdBook::getBookPageList($where, $pages);
        res_api($res['data']);
    }

    public function test(){
        $novel = getMyConfig('novel_category');
        dump($novel);
        $cartoon = getMyConfig('cartoon_category');
        dump($cartoon);
        $option = mdBook::getOptions();
        dump($option);
    }
}
