<?php
namespace app\common\model;

use think\Db;
use site\myCache;
use site\myDb;

class mdConsole{
    
    //获取总站概览数据
    public static function getNumbersData(){
    	$field = 'sum(charge_money) as charge_money,sum(withdraw_pay) as withdraw_pay,sum(withdraw_wait) as withdraw_wait';
    	$cur = Db::name('ChannelMoney')->where('pid',0)->field($field)->group('pid')->find();
    	$start_time = strtotime('today');
    	$order = [
    		'num' => $cur['charge_money'],
    		'sub' => Db::name('Order')->where('create_time','>=',$start_time)->where('status',2)->sum('money'),
    	];
    	$withdraw = [
    		'num' => $cur['withdraw_pay'],
    		'sub' => $cur['withdraw_wait']
    	];
    	$member = [
    		'num' => Db::name('Member')->count(),
    		'sub' => 0
    	];
    	$channel = ['num' => 0,'sub' => 0];
    	$list = Db::name('Channel')->where('status','between',[0,1])->field('count(*) as count,status')->group('status')->select();
    	if($list){
    		foreach ($list as $v){
    			if($v['status'] == 1){
    				$channel['num'] += $v['count'];
    			}else{
    				$channel['sub'] += $v['count'];
    			}
    		}
    	}
    	return ['order' => $order,'withdraw' => $withdraw,'member' => $member,'channel'=>$channel];
    }
    
    //获取渠道概览数据
    public static function getChannelNumbersData(){
    	global $loginId;
    	$rel = myDb::getCur('ChannelMoney', [['channel_id','=',$loginId]]);
    	$today = myDb::getCur('ChannelChart', [['create_date','=',date('Y-m-d')],['channel_id','=',$loginId]],'id,charge_money,income_money');
    	$order = [
    		'num' => $rel['charge_money'],
    		'sub' => 0
    	];
    	$income = [
    		'num' => $rel['income_money'],
    		'sub' => 0
    	];
    	if($today){
    		$order['sub'] = $today['charge_money'];
    		$income['sub'] = $today['income_money'];
    	}
    	$withdraw = [
    		'num' => $rel['withdraw_pay'],
    		'sub' => $rel['withdraw_wait']
    	];
    	$member = [
    		'num' => Db::name('Member')->where('channel_id',$loginId)->count(),
    		'sub' => Db::name('Member')->where('channel_id',$loginId)->where('subscribe',1)->count()
    	];
    	return ['order' => $order,'income' => $income,'withdraw' => $withdraw,'member' => $member];
    }
    
    //获取代理概览数据
    public static function getAgentNumbersData(){
    	$time = strtotime('today');
    	global $loginId;
    	$cur = myCache::getChannel($loginId);
    	$key = ($cur['type'] == 1) ? 'channel_id' : 'agent_id';
    	$rel = myDb::getCur('ChannelMoney', [['channel_id','=',$loginId]]);
    	$today = myDb::getCur('ChannelChart', [['create_date','=',date('Y-m-d')],['channel_id','=',$loginId]],'id,charge_money,income_money');
    	$order = [
    		'num' => $rel['charge_money'],
    		'sub' => 0
    	];
    	$income = [
    		'num' => $rel['income_money'],
    		'sub' => 0
    	];
    	if($today){
    		$order['sub'] = $today['charge_money'];
    		$income['sub'] = $today['income_money'];
    	}
    	$withdraw = [
    		'num' => $rel['withdraw_pay'],
    		'sub' => $rel['withdraw_wait']
    	];
    	$member = [
    		'num' => Db::name('Member')->where($key,$loginId)->count(),
    	];
    	return ['order' => $order,'income' => $income,'withdraw' => $withdraw,'member' => $member];
    }
    
    //获取充值排名
    public static function getChargeRank($where){
        $list = Db::name('Order a')
        ->join('member b','a.uid=b.id')
        ->where($where)
        ->field('sum(a.money) as money,a.uid,ANY_VALUE(b.username) as nickname,ANY_VALUE(b.create_time) as create_time')
        ->group('a.uid')
        ->order('money','desc')
        ->limit(10)
        ->select();
        if($list){
            foreach ($list as &$v){
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
        }
        return $list;
    }
    
    //获取投诉排名
    public static function getComplaintRank($channel_id=0){
    	$where = [];
    	if($channel_id){
    		$where[] = ['a.channel_id','=',$channel_id];
    	}
        $list = Db::name('Complaint a')
        ->join('book b','a.book_id=b.id')
        ->where($where)
        ->field('count(a.id) as count,b.name as book_name')
        ->group('a.book_id')
        ->order('count','desc')
        ->limit(10)
        ->select();
        return $list;
    }
    
    //获取近30日用户增长趋势图
    public static function getUserChartData($channel_id=0){
        $cur_time = strtotime('today');
        $start_time = $cur_time - 30*86400;
        $start_date = date('Y-m-d',$start_time);
        $list = Db::name('TaskMember')->where('channel_id','=',$channel_id)->where('create_date','>=',$start_date)->field('create_date,add_num')->select();
        $temp = [];
        foreach ($list as $v){
            $temp[$v['create_date']] = $v;
        }
        unset($list);
        $add = $key = [];
        while ($start_time < $cur_time){
            $cur_date = date('Y-m-d',$start_time);
            $key[] = date('m/d',$start_time);
            $add_num = 0;
            if(isset($temp[$cur_date])){
                $add_num += $temp[$cur_date]['add_num'];
            }
            $add[] = $add_num;
            $start_time += 86400;
        }
        return ['key'=>$key,'add'=>$add];
    }
    
    //获取账号关联列表
    public static function getRelAccountList($where,$pages,$curId){
    	$list = Db::name('ChannelRel a')
    	->join('channel b','a.channel_id=b.id')
    	->where($where)
    	->field('a.id,a.channel_id,a.create_time,b.name')
    	->page($pages['page'],$pages['limit'])
    	->order('a.id','desc')
    	->select();
    	$count = 0;
    	if($list){
    		foreach ($list as &$v){
    			$v['is_cur'] = $v['channel_id'] == $curId ? 1 : 2; 
    			$v['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
    		}
    		$count = Db::name('ChannelRel a')->join('channel b','a.channel_id=b.id')->where($where)->count();
    	}
    	return ['data'=>$list,'count'=>$count];
    }
    
    //绑定账号
    public static function bindAccount($curId,$bindId){
    	$cur = myCache::getChannel($curId);
    	$code = $cur['relation_code'];
    	Db::startTrans();
    	$flag = false;
    	$time = time();
    	if($code){
    		$data = ['channel_id'=>$bindId,'code'=>$code,'create_time'=>$time];
    		$re = Db::name('ChannelRel')->insert($data);
    		if($re){
    			$res = Db::name('Channel')->where('id',$bindId)->setField('relation_code',$code);
    			if($res){
    				$flag = true;
    			}
    		}
    	}else{
    		$code = myDb::createCode('ChannelRel');
    		$re = Db::name('Channel')->where('id','in',[$curId,$bindId])->setField('relation_code',$code);
    		if($re){
    			$data = [
    				['channel_id'=>$curId,'code'=>$code,'create_time'=>$time],
    				['channel_id'=>$bindId,'code'=>$code,'create_time'=>$time],
    			];
    			$res = Db::name('ChannelRel')->insertAll($data);
    			if($res){
    				$flag = true;
    			}
    		}
    	}
		if($flag){
			Db::commit();
			myCache::doChannel($bindId, 'relation_code',$code);
			myCache::doChannel($curId, 'relation_code',$code);
		}else{
			Db::rollback();
		}
    	return $flag;
    }
    
    //解除绑定
    public static function deleteAccount($curId,$bindId,$code){
    	Db::startTrans();
    	$flag = false;
    	$count = myDb::getCount('ChannelRel', [['code','=',$code]]);
    	if($count === 2){
    		$re = Db::name('Channel')->where('id','in',[$curId,$bindId])->setField('relation_code','');
    		if($re){
    			$res = Db::name('ChannelRel')->where('code',$code)->delete();
    			if($res){
    				$flag = true;
    			}
    		}
    	}else{
    		$re = Db::name('Channel')->where('id',$bindId)->setField('relation_code','');
    		if($re){
    			$res = Db::name('ChannelRel')->where('channel_id',$bindId)->delete();
    			if($res){
    				$flag = true;
    			}
    		}
    	}
    	if($flag){
    		Db::commit();
    		myCache::doChannel($bindId, 'relation_code','');
    		if($count === 2){
    			myCache::doChannel($curId, 'relation_code','');
    		}
    	}else{
    		Db::rollback();
    	}
    	return $flag;
    }
    
}