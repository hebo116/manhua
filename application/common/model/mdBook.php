<?php
namespace app\common\model;

use site\myHttp;
use think\Db;
use site\myDb;
use site\myCache;
use site\myValidate;

class mdBook{
    
    //获取书籍列表
    public static function getBookPageList($where,$pages){
        $list = Db::name('Book')
        ->where($where)
        ->page($pages['page'],$pages['limit'])
        ->order('top_num','DESC')
        ->order('id','DESC')
        ->select();
        $count = 0;
        if($list){
            $count = Db::name('Book')->where($where)->count();
        }
        foreach ($list as &$item){
            $item["cover"]=handleImg($item["cover"]);
            $item["detail_img"]=handleImg($item["detail_img"]);
        }
        return ['count'=>$count,'data'=>$list];
    }
    
    //获取限免书籍列表
    public static function getFreeBookList($where,$pages){
    	$field = 'a.*,b.name,b.cover';
        $list = Db::name('BookFree a')
        ->join('book b','a.book_id=b.id')
        ->where($where)
        ->field($field)
        ->group('a.id')
        ->page($pages['page'],$pages['limit'])
        ->order('a.id','DESC')
        ->select();
        $count = 0;
        if($list){
            foreach ($list as &$v){
                $v['cover'] = handleImg($v['cover']);
            }
        	$count = Db::name('BookFree a')->join('book b','a.book_id=b.id')->where($where)->count();
        }
        return ['count'=>$count,'data'=>$list];
    }
    
    //处理更新书籍
    public static function doneBook($data){
    	$data['is_beast'] = 2;
    	$data['is_recommend'] = 2;
    	$data['is_alone'] = 2;
    	$data['is_vip'] = 2;
    	$bookTag = $data['book_tag'];
    	unset($data['book_tag']);
    	if($bookTag){
    		if(in_array(1, $bookTag)){
    			$data['is_beast'] = 1;
    		}
    		if(in_array(2, $bookTag)){
    			$data['is_recommend'] = 1;
    		}
    		if(in_array(3, $bookTag)){
    			$data['is_alone'] = 1;
    		}
    		if(in_array(4, $bookTag)){
    			$data['is_vip'] = 1;
    		}
    	}
    	$data['text_tag'] = $data['text_tag'] ? json_encode($data['text_tag'],JSON_UNESCAPED_UNICODE) : '[]';
        if($data['category']){
            $data['category'] = ','.implode(',', $data['category']).',';
        }
        if(isset($data['id'])){
        	$flag = myDb::saveIdData('Book', $data);
        }else{
        	$data['create_time'] = time();
        	$flag = myDb::add('Book', $data);
        }
        if($flag){
        	if(isset($data['id'])){
        		myCache::rmBook($data['id']);
        		myCache::rmBookHotNum($data['id']);
        		myCache::rmFreeAreaBookIds(1);
        	}
            res_api();
        }else{
            res_api('保存失败，请重试');
        }
    }
    
    //处理书籍事件
    public static function doBookEvent(){
    	$flag = false;
    	$rules = [
    		'id' =>  ["require|number|gt:0",['require'=>'主键参数错误','number'=>'主键参数不规范',"gt"=>"主键参数不规范"]],
    		'event' => ["require|in:on,off,offread,delete,godon,godoff,topon,topoff",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
    	];
    	$data = myValidate::getData($rules);
    	if(in_array($data['event'], ['on','off','offread'])){
    		$status = 1;
    		switch ($data['event']){
    			case 'on':$status=1;break;
    			case 'offread':$status=2;break;
    			case 'off':$status=3;break;
    		}
    		$flag = myDb::setField('Book', [['id','=',$data['id']]], 'status', $status);
    	}else{
    		switch ($data['event']){
    			case 'delete':
    				$re = self::delAllChapter($data['id']);
    				if($re){
    					$res = Db::name('Book')->where('id','=',$data['id'])->delete();
    					if($res){
    						$flag = true;
    					}
    				}
    				break;
    			case 'topon':
    				$top_num = Db::name('Book')->max('top_num');
    				$top_num = $top_num ? $top_num+1 : 1;
    				$flag = myDb::save('Book', [['id','=',$data['id']]],['top_num'=>$top_num,'is_top'=>1]);
    				break;
    			case 'topoff':
    				$flag = myDb::save('Book', [['id','=',$data['id']]],['top_num'=>0,'is_top'=>2]);
    				break;
    			case 'godon':
    				$flag = myDb::setField('Book', [['id','=',$data['id']]], 'is_god', 1);
    				break;
    			case 'godoff':
    				$flag = myDb::setField('Book', [['id','=',$data['id']]], 'is_god', 2);
    				break;
    		}
    	}
    	if($flag){
    		myCache::rmBook($data['id']);
    	}
    	return $flag;
    }
    
    //读取并解析上传漫画章节
    public static function readAndSaveCartoon($filename,$book_id){
    	$config = getMyConfig('suiyiyun');
    	if(!$config){
    		res_api('未配置随意云参数');
    	}
    	$suiyiyun = $config['url'] ? : '';
    	$zipFile = env('root_path').'static/temp/zip/'.$filename;
    	if(@is_file($zipFile)){
    		$zip = new \ZipArchive();
    		$rs = $zip -> open($zipFile);
    		if(!$rs){
    			@unlink($zipFile);
    			res_api('读取压缩文件失败');
    		}
    		$table = 'BookChapter';
    		$docnum = $zip->numFiles;
    		$res = $temp = $chapter = [];
    		for ($i=0;$i<$docnum;$i++){
    			$stateInfo = $zip->statIndex($i);
    			if($stateInfo['crc'] != 0 && $stateInfo['size'] > 0){
    				$name = $zip->getNameIndex($i, \ZipArchive::FL_ENC_RAW);
    				$encode = mb_detect_encoding($name,['ASCII','GB2312','GBK','UTF-8']);
    				$encode = $encode ? $encode : 'GBK';
    				$thisName = mb_convert_encoding($name,'UTF-8',$encode);
    				$pathInfo = pathinfo($thisName);
    				if($pathInfo && is_array($pathInfo)){
    					if(is_numeric($pathInfo['filename'])){
    						if(isset($pathInfo['dirname']) && $pathInfo['dirname']){
    							if(in_array($pathInfo['dirname'],$temp)){
    								$number = array_search($pathInfo['dirname'],$temp);
    							}else{
    								$dirInfo = self::getDirNumber($pathInfo['dirname']);
    								$number = $dirInfo['number'];
    							}
    							if($number > 0){
    								$fileKey = $pathInfo['filename'];
    								if(!isset($chapter[$number])){
    									$chapter[$number] = [
    											'title'=>$dirInfo['title'],
    											'number'=>$number,
    											'child'=>[
    												$fileKey => [
    													'name' => $stateInfo['name'],
    													'savename' => md5($book_id.'-'.$number.'-'.$fileKey.microtime()).'.'.$pathInfo['extension']
    												]
    											]
    									];
    								}else{
    									$chapter[$number]['child'][$fileKey] = [
    										'name' => $stateInfo['name'],
    										'savename' => md5($book_id.'-'.$number.'-'.$fileKey.microtime()).'.'.$pathInfo['extension']
    									];
    								}
    							}
    						}
    					}
    				}
    			}
    		}
    		if(!empty($chapter)){
    			$path = './uploads/book/'.$book_id;
    			if(!is_dir($path)){
    				mkdir($path,0777,true);
    			}
    			if(!is_dir($path)){
    				$zip->close();
    				@unlink($zipFile);
    				res_api('创建漫画目录失败');
    			}
    			ksort($chapter);
    			foreach ($chapter as $v){
    				if($v['child']){
    					$child = $v['child'];
    					ksort($child);
    					$repeat = Db::name($table)->where('book_id','=',$book_id)->where('number','=',$v['number'])->value('id');
    					if(!$repeat){
    						$data = [
    								'book_id'=>$book_id,
    								'name'=> $v['title'],
    								'src' => '',
    								'number'=> $v['number'],
    								'files' => [],
    								'create_time'=>time()
    						];
    						$html = '';
    						foreach ($child as $val){
    							$local_file = 'zip://'.$zipFile.'#'.$val['name'];
                                $content = file_get_contents($local_file);
                                //$content = myHttp::doGet($local_file,[],'string');
    							if($content){
    								$filename = $path.'/'.$val['savename'];
    								$myfile = fopen($filename,"w");
    								fwrite($myfile,$content);
    								fclose($myfile);
    								$url = $suiyiyun.ltrim($filename,'.');
    								$data['files'][] = $url;
    								$html .= '<img src="'.$url.'" />';
    							}else{
    								$zip->close();
    								@unlink($zipFile);
    								res_api('章节：'.$v['title'].'=>读取失败');
    							}
    						}
    						if(!empty($data['files'])){
    							$data['src'] = $data['files'][0];
    						}
    						$data['files'] = json_encode($data['files']);
    						$chapter_id = Db::name($table)->insertGetId($data);
    						if($chapter_id){
    							$block_res = saveBlock($html,$v['number'],'book/'.$book_id);
    							if(!$block_res){
    								Db::name($table)->where('id','=',$chapter_id)->delete();
    							}
    						}
    					}
    				}
    			}
    		}
    		$zip -> close();
    		@unlink($zipFile);
    		myCache::rmBookChapterNum($book_id);
    		myCache::rmBookChapterList($book_id);
    		self::checkUpdateTime($book_id);
    	}else{
    		res_api('zip文件不存在');
    	}
    }

    //读取zip并保存小说章节
    public static function readAndSaveNovel($filename,$book_id){
        $zipFile = env('root_path').'static/temp/zip/'.$filename;
        if(@is_file($zipFile)){
            $zip = new \ZipArchive();
            $rs = $zip -> open($zipFile);
            if(!$rs){
                @unlink($zipFile);
                res_api('读取压缩文件失败');
            }
            $docnum = $zip->numFiles;
            $table = 'BookChapter';
            for($i = 0; $i < $docnum; $i++) {
                $stateInfo = $zip->statIndex($i);
                if($stateInfo['crc'] != 0 && $stateInfo['size'] > 0){
                    $name = $zip->getNameIndex($i, \ZipArchive::FL_ENC_RAW);
                    $encode = mb_detect_encoding($name,['ASCII','GB2312','GBK','UTF-8']);
                    $encode = $encode ? $encode : 'GBK';
                    $thisName = mb_convert_encoding($name,'UTF-8',$encode);
                    $pathInfo = pathinfo($thisName);
                    if($pathInfo && is_array($pathInfo)){
                        if($pathInfo['extension'] === 'txt'){
                            $title = $pathInfo['filename'];
                            $number = self::getFileNumber($title);
                            if($number > 0){
                                $repeat = Db::name($table)->where('book_id','=',$book_id)->where('number','=',$number)->value('id');
                                if(!$repeat){
                                    $content = file_get_contents('zip://'.$zipFile.'#'.$stateInfo['name']);
                                    //$content = myHttp::doGet('zip://'.$zipFile.'#'.$stateInfo['name'],[],'string');
                                    $encode = mb_detect_encoding($content,['ASCII','GB2312','GBK','UTF-8']);
                                    $encode = $encode ? $encode : 'GBK';
                                    $content = mb_convert_encoding($content, 'UTF-8', $encode);
                                    $html = "<p>".$content;
                                    $html = preg_replace('/\n|\r\n/','</p><p>',$html);
                                    $data = [
                                        'book_id'=>$book_id,
                                        'name'=>$title,
                                        'number'=>$number,
                                        'create_time'=>time()
                                    ];
                                    $chapter_id = Db::name($table)->insertGetId($data);
                                    if($chapter_id){
                                        $block_res = saveBlock($html,$number,'book/'.$book_id);
                                        if(!$block_res){
                                            Db::name($table)->where('id','=',$chapter_id)->delete();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $zip -> close();
            @unlink($zipFile);
            myCache::rmBookChapterNum($book_id);
            myCache::rmBookChapterList($book_id);
            self::checkUpdateTime($book_id);
        }else{
            res_api('zip文件不存在');
        }
    }
    
    //处理书籍压缩包文件
    public static function doBookFile(){
    	$rules = [
    		'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
    		'filename' =>  ["require|length:36",["require"=>"文件名异常",'max'=>'文件名错误']],
    	];
    	$data = myValidate::getData($rules);
    	$cur = myDb::getById('Book', $data['book_id'],'id,name');
    	if(empty($cur)){
    		res_api('书籍不存在');
    	}
    	self::readAndSaveCartoon($data['filename'],$data['book_id']);
    }
    
    //处理小说压缩包
    public static function doBookNovel()
    {
        $rules = [
            'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
            'filename' =>  ["require|length:36",["require"=>"文件名异常",'max'=>'文件名错误']],
        ];
        $data = myValidate::getData($rules);
        $cur = myDb::getById('Book', $data['book_id'],'id,name,type');
        if(empty($cur)){
            res_api('书籍不存在');
        }
        if($cur['type'] != 2){
            res_api('该书籍不是小说，请变更为小说');
        }
        self::readAndSaveNovel($data['filename'],$data['book_id']);
    }
    
    //获取书籍属性选项
    public static function getOptions(){
        $option = [
            'status' => [
                'name' => 'status',
            	'option' => [['val'=>1,'text'=>'上架','default'=>1],['val'=>2,'text'=>'下架可阅','default'=>0],['val'=>3,'text'=>'下架','default'=>0]]
            ],
            'free_type' => [
                'name' => 'free_type',
                'option' => [['val'=>1,'text'=>'免费','default'=>0],['val'=>2,'text'=>'收费','default'=>1]]
            ],
            'over_type' => [
                'name' => 'over_type',
                'option' => [['val'=>1,'text'=>'连载中','default'=>1],['val'=>2,'text'=>'已完结','default'=>0]]
            ],
            'is_beast' => [
                'name' => 'is_beast',
                'option' => [['val'=>1,'text'=>'是','default'=>0],['val'=>2,'text'=>'否','default'=>1]]
            ],
            'is_recommend' => [
                'name' => 'is_recommend',
                'option' => [['val'=>1,'text'=>'是','default'=>0],['val'=>2,'text'=>'否','default'=>1]]
            ],
        	'book_tag' => [
        		['id'=>1,'name'=>'精品'],
        		['id'=>2,'name'=>'推荐'],
        		['id'=>3,'name'=>'独家'],
        		['id'=>4,'name'=>'VIP'],
        	]
        ];
        return $option;
    }
    
    //获取分集列表
    public static function getChapterPageList($where,$pages){
    	$list = Db::name('BookChapter')->where($where)->page($pages['page'],$pages['limit'])->order('number','desc')->select();
    	$count = 0;
    	if($list){
    		foreach ($list as &$v){
                $v['src'] = handleImg($v['src']);
                $v['do_url'] = my_url('doChapter',['id'=>$v['id']]);
                $v['show_url'] = my_url('showInfo',['id'=>$v['id']]);
                $v['showyy_url'] = my_url('showyyInfo',['id'=>$v['id']]);
                $files = json_decode($v['files']);
                $_files = [];
                foreach ($files as $img){
                    $_files[] = handleImg($img);
                }
                $v['files'] = json_encode($_files);
    		}
    		$count = Db::name('BookChapter')->where($where)->count();
    	}
    	return ['data'=>$list,'count'=>$count];
    }

    //获取更换封面数据
    public static function getChapterCoverData(){
    	$rules = [
    		'id' =>  ["require|number|gt:0",['require'=>'主键参数错误','number'=>'主键参数不规范',"gt"=>"主键参数不规范"]],
    		'src' => ["require|url",["require"=>'请选择章节封面图片',"src"=>'图片地址异常']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取前10章内容
    public static function getTenChapter($book_id){
    	$list = Db::name('BookChapter')->where('book_id','=',$book_id)->where('number','<=',10)->field('number as id,name')->order('number','asc')->limit(10)->select();
    	return $list;
    }
    
    //检查并更新书籍最后更新时间
    public static function checkUpdateTime($book_id){
    	$chapter = Db::name('BookChapter')->where('book_id',$book_id)->order('number','desc')->field('id,create_time')->find();
    	if($chapter){
    		$update_date = date('Y-m-d',$chapter['create_time']);
    		$book = myCache::getBook($book_id);
    		if($book && $book['update_date'] != $update_date){
    			$re = Db::name('Book')->where('id',$book['id'])->setField('update_date',$update_date);
    			if($re){
    				myCache::doBook($book['id'], 'update_date',$update_date);
    			}
    		}
    	}
    }
    
    //检查书籍章节连贯性
    public static function checkChapter($book_id){
    	$field = 'id,chapter';
    	$list = Db::name('BookChapter')->where('book_id','=',$book_id)->field('id,number')->select();
    	$max = 0;
    	$chapter = $error = [];
    	if($list){
    		foreach ($list as $v){
    			if(in_array($v['number'], $chapter)){
    				$error[] = '第'.$v['number'].'章:重复';
    			}else{
    				$chapter[] = $v['number'];
    			}
    			if($v['number'] > $max){
    				$max = $v['number'];
    			}
    		}
    	}
    	if($max > 0){
    		$path = env('root_path').'static/block/book/'.$book_id;
    		for ($i=1;$i<=$max;$i++){
    			if(!in_array($i,$chapter)){
    				$error[] = '第'.$i.'章:缺失';
    			}else{
    				$file = $path.'/'.$i.'.html';
    				if(@is_file($file)){
    					continue;
    				}else{
    					$error[] = '第'.$i.'章:未检测到内容';
    				}
    			}
    		}
    	}
    	return $error;
    }

    //检查书籍章节是否有内容YY
    public static function checkChapteryy($book_id){
        $list = Db::name('BookChapter')->where('book_id','=',$book_id)->field('id,number,yy_files')->select();
        $error = [];

        foreach ($list as $chapter){
            if (!$chapter['yy_files'] || empty(json_decode($chapter['yy_files'],true))){
                $error[] = '第'.$chapter['number'].'章:未检测到内容';
            }
        }
        return $error;
    }
    
    //获取章节设置书币参数
    public static function getChapterMoneyData(){
    	$rules = [
    		'id'=>["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
    		'money'=>["require|number|egt:0",["require"=>"金额参数错误",'number'=>'金额参数错误',"egt"=>"金额参数错误"]],
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }

    //更新章节
    public static function doneChapter($data){
        $content = '';
        if(array_key_exists('content', $data)){
            $content = $data['content'];
            unset($data['content']);
        }
        $res = false;
        if(array_key_exists('id', $data)){
            $re = Db::name('BookChapter')->where('id','=',$data['id'])->update($data);
        }else{
            $repeat = Db::name('BookChapter')->where('book_id','=',$data['book_id'])->where('number','=',$data['number'])->value('id');
            if($repeat){
                res_api('该章节已存在');
            }
            $data['create_time'] = time();
            $re = Db::name('BookChapter')->insert($data);
        }
        if($re !== false){
            if($content){
                $dir = 'book/'.$data['book_id'];
                saveBlock($content,$data['number'],$dir);
            }
            myCache::rmBookChapterNum($data['book_id']);
            myCache::rmBookChapterList($data['book_id']);
            self::checkUpdateTime($data['book_id']);
            res_api('ok');
        }else{
            res_api('操作失败，请重试');
        }
    }

    //删除章节
    public static function delChapter(){
    	$rules = [
    		'id' =>  ["require|number|gt:0",["require"=>"分集主键参数错误",'number'=>'分集主键参数错误',"gt"=>"分集主键参数错误"]],
    		'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
    	];
    	$data = myValidate::getData($rules);
    	$book = myDb::getById('Book',$data['book_id'],'id,name');
    	if(empty($book)){
    		res_api('书籍信息异常');
    	}
    	$cur = myDb::getById('BookChapter',$data['id'],'id,book_id,number,src,files');
    	if(empty($cur)){
    		res_api('章节信息异常');
    	}
    	$res = false;
    	$re = Db::name('BookChapter')->where('id','=',$cur['id'])->delete();
    	if($re){
    		$res = true;
    		$filename = env('root_path').'static/block/book/'.$book['id'].'/'.$cur['number'].'.html';
    		if(@is_file($filename)){
    			@unlink($filename);
    		}
    		myCache::rmBookChapterNum($book['id']);
    		myCache::rmBookChapterList($book['id']);
    	}
    	return $res;
    }
    
    //删除所有章节
    public static function delAllChapter($book_id){
    	$res = false;
    	$book = myDb::getById('Book',$book_id,'id,name');
    	if(empty($book)){
    		res_api('书籍信息异常');
    	}
    	$list = $config = [];
    	$re = Db::name('BookChapter')->where('book_id','=',$book_id)->delete();
    	if($re !== false){
    		$dir = env('root_path').'static/block/book/'.$book_id;
    		if(@is_dir($dir)){
    			self::delDirAndFile($dir);
    		}
    		$res = true;
    		myCache::rmBookChapterNum($book_id);
    		myCache::rmBookChapterList($book_id);
    	}
    	return $res;
    }
    
    //获取小说章节内容
    public static function getChapterContent($book_id,$number){
    	$filename = env('root_path').'static/block/book/'.$book_id.'/'.$number.'.html';
    	$content = '';
    	if(@is_file($filename)){
            $content = file_get_contents($filename);
            //$content = myHttp::doGet($filename,[],'string');
    	}
    	return $content;
    }
    
    //解析路径中文件章节
    private static function getDirNumber($dir){
    	$arr = explode('/', $dir);
    	$title = end($arr);
    	$number = self::getFileNumber($title);
    	return ['number'=>$number,'title'=>$title];
    }
    
    //获取章节
    private static function getFileNumber($title){
    	$number = 0;
    	$pattern = '/[(\d)|(零一壹二贰三叁四肆五伍六陆七柒八捌九玖十拾百佰千仟万两)]+/u';
    	$data = preg_match($pattern, $title,$match);
    	if(!empty($match)){
    		$number = $match[0];
    		if(is_numeric($number)){
    			$number = intval($number);
    		}else{
    			$number = self::chrtonum($number);
    		}
    	}
    	return $number;
    }
    
    //删除文件夹及子目录和文件
    private static function delDirAndFile( $dirName){
    	if ( @$handle = opendir( "$dirName" ) ) {
    		while ( false !== ( $item = readdir( $handle ) ) ) {
    			if ( $item !== "." && $item !== ".." ) {
    				if ( is_dir( "$dirName/$item" ) ) {
    					self::delDirAndFile( "$dirName/$item" );
    				} else {
    					@unlink( "$dirName/$item" );
    				}
    			}
    		}
    		closedir( $handle );
    		rmdir( "$dirName/$item" );
    	}
    }
    
    //中文转阿拉伯
    private static function chrtonum($string){
    	if(is_numeric($string)){
    		return $string;
    	}
    	$string = str_replace('仟', '千', $string);
    	$string = str_replace('佰', '百', $string);
    	$string = str_replace('拾', '十', $string);
    	$num = 0;
    	$wan = explode('万', $string);
    	if (count($wan) > 1) {
    		$num += self::chrtonum($wan[0]) * 10000;
    		$string = $wan[1];
    	}
    	$qian = explode('千', $string);
    	if (count($qian) > 1) {
    		$num += self::chrtonum($qian[0]) * 1000;
    		$string = $qian[1];
    	}
    	$bai = explode('百', $string);
    	if (count($bai) > 1) {
    		$num += self::chrtonum($bai[0]) * 100;
    		$string = $bai[1];
    	}
    	$shi = explode('十', $string);
    	if (count($shi) > 1) {
    		$num += self::chrtonum($shi[0] ? $shi[0] : '一') * 10;
    		$string = $shi[1] ? $shi[1] : '零';
    	}
    	$ling = explode('零', $string);
    	if (count($ling) > 1) {
    		$string = $ling[1];
    	}
    	$d = [
    			'一' => '1','二' => '2','三' => '3','四' => '4','五' => '5','六' => '6','七' => '7','八' => '8','九' => '9',
    			'壹' => '1','贰' => '2','叁' => '3','肆' => '4','伍' => '5','陆' => '6','柒' => '7','捌' => '8','玖' => '9',
    			'零' => 0, '0' => 0, 'O' => 0, 'o' => 0,
    			'两' => 2
    	];
    	return $num + @$d[$string];
    }
    
    //获取书籍规则
    public static function getData($isAdd){
    	$rules = [
    		'name' =>  ["require|max:200",["require"=>"请输入书籍名称",'max'=>'书籍名称最多支持200个字符']],
    		'author' =>  ["max:50",['max'=>'作者名称最多支持50个字符']],
    		'cover' =>  ['max:255',['max'=>'书籍封面异常']],
    		'detail_img' =>  ['max:255',['max'=>'书籍详情图片异常']],
    		'summary' =>  ["max:500",['max'=>'书籍简介最多支持500个字符']],
    		'status' => ["require|in:1,2,3",["require"=>"请选择书籍状态","in"=>"未指定该书籍状态"]],
    		'book_tag' => ["array",["array"=>"发布属性参数异常"]],
    		'text_tag' => ["array",["array"=>"书籍属性参数异常"]],
    		'category' => ["array",["array"=>"小说分类参数异常"]],
    		'free_type' => ["require|in:1,2",["require"=>"请选择书籍是否免费","in"=>"未指定该书籍是否免费状态"]],
    		'over_type' => ["require|in:1,2",["require"=>"请选择书籍连载状态","in"=>"未指定该书籍连载状态"]],
    		'free_chapter' => ["number",["number"=>"免费章节必须为正整数"]],
    		'money' => ["require|number",["require"=>"请输入书籍章节收费书币数量","in"=>"书币数量必须为正整数"]],
    		'hot_num' => ["number",["require"=>"人气值必须为正整数"]],
    		'star' => ["float|elt:10",["float"=>"书籍评分格式必须是小于等于10的数字",'elt'=>'书籍评分格式必须是小于等于10的数字']],
    	];
    	if(!$isAdd){
    		$rules['id'] =  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取章节数据
    public static function getChapterData($isAdd=0)
    {
        $rules = [
            'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
            'content' =>  ["require",['require'=>'请输入分集内容']],
            'number' => ["require|number|gt:0",["require"=>"章节参数错误",'number'=>'章节参数错误',"gt"=>"章节参数错误"]],
            'name' =>  ["require|max:50",["require"=>"请输入章节名称",'max'=>'章节名称最多支持50个字符']],
        ];
        if(!$isAdd){
            $rules['id'] =  ["require|number|gt:0",["require"=>"分集主键参数错误",'number'=>'分集主键参数错误',"gt"=>"分集主键参数错误"]];
        }
        $data = myValidate::getData($rules);
        return $data;
    }
    
    /***********************以下为前端调用方法*******************************/
    
    //新增阅读量
    public static function addHot($book_id){
    	$re = Db::name('Book')->where('id','=',$book_id)->setInc('hot_num');
    	if($re){
    		myCache::incBookHotNum($book_id);
    	}
    }
    
    /**
     * 添加阅读历史
     * @param array $book 书籍信息
     * @param number $number 章节数
     * @param array $member 用户信息
     * @param boolean $is_money 是否需要书币购买 1是2否
     * @return boolean
     */
    public static function addReadhistory($book,$chapter,$uid,$money=0){
    	Db::startTrans();
    	$flag = false;
    	$time = time();
    	$data = [
    		'book_id' => $book['id'],
    		'number' => $chapter['number'],
    		'uid' => $uid,
    		'create_time' => $time
    	];
    	$re = Db::name('ReadHistory')->insert($data);
    	if($re){
    		$flag = true;
    		if($money){
    			$flag = false;
    			$res = Db::name('Member')->where('id','=',$uid)->setDec('money',$money);
    			if($res){
    				$log_res = mdMember::addMemberLog($uid, $book['name'], $chapter['name'], '-'.$money,2);
    				if($log_res){
    					$flag = true;
    				}
    			}
    		}
    	}
    	if($flag){
    		Db::commit();
    		myCache::addRead($uid, $book['id'], $chapter['number'],$time);
    		if($money){
    			myCache::doMemberMoney($uid,$money,'dec');
    		}
    	}else{
    		Db::rollback();
    	}
    	return $flag;
    }


    public static function doCion($android_uid,$android_token,$cion){
        if ($android_uid==1 && $android_token=='aabbb'){
            return false;
        }
        $url = 'http://live.xyj6.best/api/public/?service=Cartoon.subCartoon&uid='.$android_uid.'&token='.$android_token.'&coin='.$cion;
        $flag = false;
        $res = myHttp::doGet($url);
        if ($res && $res['ret']==200 && $res['data']['code']===0){
            $flag = true;
        }
        return $flag;
    }
    
    /**
     * 增加章节阅读数
     * @param unknown $chapter_id
     */
    public static function addChapterRead($chapter_id){
    	Db::name('BookChapter')->where('id',$chapter_id)->setInc('read_num');
    }
    
    //获取分类页面数据
    public static function getCategoryList($type,$page){
    	$where = [['status','=',1],['type','=',1]];
    	$order = ['hot_num'=>'desc'];
    	switch ($type){
    		case 2:
    			$order = ['id'=>'desc'];
    			break;
    		case 3:
    			$where[] = ['over_type','=',1];
    			break;
    		case 4:
    			$where[] = ['over_type','=',2];
    			break;
    		case 5:
    			$where[] = ['is_vip','=',1];
    			break;
    	}
    	$field = 'id,name,author,cover,summary,hot_num,over_type,is_alone,star';
    	$list = Db::name('Book')->where($where)->field($field)->page($page,10)->order($order)->select();
    	if($list){
    		foreach ($list as &$v){
    			$v['cover'] = $v['cover'] ? $v['cover'] : '/static/templet/default/cover.png';
    		}
    	}
    	return $list;
    }

    //获取小说分类页面数据
    public static function getNovelCategoryList($type,$page){
        $where = [['status','=',1],['type','=',2]];
        $order = ['hot_num'=>'desc'];
        switch ($type){
            case 2:
                $order = ['id'=>'desc'];
                break;
            case 3:
                $where[] = ['over_type','=',1];
                break;
            case 4:
                $where[] = ['over_type','=',2];
                break;
            case 5:
                $where[] = ['is_vip','=',1];
                break;
        }
        $field = 'id,name,author,cover,summary,hot_num,over_type,is_alone,star';
        $list = Db::name('Book')->where($where)->field($field)->page($page,10)->order($order)->select();
        if($list){
            foreach ($list as &$v){
                $v['cover'] = $v['cover'] ? $v['cover'] : '/static/templet/default/cover.png';
            }
        }
        return $list;
    }
    
    
    //获取缓存书籍分页数据
    public static function getCacheBookData($type,$size,$key=0,$category=1){
    	$list = $cache = [];
    	switch ($type){
    		case 1:
    			myCache::rmBeastBookIds($category);
    			$cache = myCache::getBeastBookIds($category);
    			break;
    		case 2:
    			myCache::rmNewBookIds($category);
    			$cache = myCache::getNewBookIds($category);
    			break;
    		case 3:
    			$cache = myCache::getFreeBookIds($category);
    			break;
    		case 4:
    			myCache::rmRecommendBookIds($category);
    			$cache = myCache::getRecommendBookIds($category);
    			break;
    		case 5:
    			myCache::rmAllBookIds($category);
    			$cache = myCache::getAllBookIds($category);
    			break;
    	}
    	if($cache){
    		$forList = [];
    		if($size > 1){
    			$arr = array_chunk($cache,$size);
    			if(isset($arr[$key])){
    				$forList = $arr[$key];
    			}
    		}else{
    			$forList = $cache;
    		}
    		foreach ($forList as $v){
    			$one = self::getIndexBookInfo($v);
    			if($one){
    				$list[] = $one;
    			}
    		}
    	}
    	return $list;
    }
    
    //获取书籍排行榜数据
    public static function getRankBookData($category=1){
        myCache::rmRankBookIds($category);
        myCache::getRankBookIds($category);
        $data = [];
        for ($type=1;$type<=4;$type++){
            $ids = $list = [];
            switch ($type){
                case 1: //热销
                    $history = Db::name('ReadHistory a')->join('book b','a.book_id=b.id','left')->where('b.status',1)->field('count(a.id) as count,a.book_id')->where('b.type','=',$category)->group('book_id')->order('count','desc')->select();
                    if($history){
                        foreach ($history as $v){
                            $ids[] = $v['book_id'];
                        }
                    }
                    break;
                case 2://人气
                    $ids = Db::name('Book')->where('status',1)->where('type','=',$category)->order('hot_num','desc')->limit(100)->column('id');
                    break;
                case 3://连载
                    $ids = Db::name('Book')->where('status',1)->where('over_type',1)->where('type','=',$category)->order('hot_num','desc')->limit(100)->column('id');
                    break;
                case 4://完结
                    $ids = Db::name('Book')->where('status',1)->where('over_type',2)->where('type','=',$category)->order('hot_num','desc')->limit(100)->column('id');
                    break;
            }
            if($ids){
                foreach ($ids as $book_id){
                    $one = self::getIndexBookInfo($book_id);
                    if($one){
                        $list[] = $one;
                    }
                }
            }
            $data[$type] = $list;
        }
        return $data;
    }
    /*
    public static function getRankBookData($category=1){
    	$data = [];
    	for ($type=1;$type<=4;$type++){
    		$ids = $list = [];
    		switch ($type){
    			case 1: //热销
    				$history = Db::name('ReadHistory a')->join('book b','a.book_id=b.id','left')->where('b.status',1)->field('count(a.id) as count,a.book_id')->where('b.type','=',$category)->group('book_id')->order('count','desc')->select();
    				if($history){
    					foreach ($history as $v){
    						$ids[] = $v['book_id'];
    					}
    				}
    				break;
    			case 2://人气
    				$ids = Db::name('Book')->where('status',1)->where('type','=',$category)->order('hot_num','desc')->limit(100)->column('id');
    				break;
    			case 3://连载
    				$ids = Db::name('Book')->where('status',1)->where('over_type',1)->where('type','=',$category)->order('hot_num','desc')->limit(100)->column('id');
    				break;
    			case 4://完结
    				$ids = Db::name('Book')->where('status',1)->where('over_type',2)->where('type','=',$category)->order('hot_num','desc')->limit(100)->column('id');
    				break;
    		}
    		if($ids){
    			foreach ($ids as $book_id){
    				$one = self::getIndexBookInfo($book_id);
    				if($one){
    					$list[] = $one;
    				}
    			}
    		}
    		$data[$type] = $list;
    	}
    	return $data;
    }
    */
    //获取书籍详情
    public static function getIndexBookInfo($book_id){
    	$one = [];
    	$book = myCache::getBook($book_id);
    	if($book){
    		$one = [
    			'id' => $book['id'],
    			'over_type' => $book['over_type'],
    			'is_alone' => $book['is_alone'],
    			'name' => $book['name'],
    			'author' => $book['author'],
    			'hot_num' => myCache::getBookHotNum($book['id']),
    			'cover' => handleImg($book['cover']),
    			'detail_img' => handleImg($book['detail_img']),
    			'star' => $book['star'],
    			'summary' => $book['summary']
    		];
    	}
    	return $one;
    }
}
