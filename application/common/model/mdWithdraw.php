<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myCache;
use site\myValidate;

class mdWithdraw{
	
	//获取待支付列表
	public static function getWaitList($where){
		$list = Db::name('Withdraw')->where($where)->order('cur_date','desc')->select();
		if($list){
			foreach ($list as &$v){
				$v['name'] = '未知';
				$v['remark'] = '';
				$v['detail_url'] = '';
				$channel = myCache::getChannel($v['channel_id']);
				if($channel){
					$v['name'] = $channel['name'];
				}
				$v['bank_info'] = json_decode($v['bank_info'],true);
			}
		}
		return $list;
	}
	
	//获取已支付列表
	public static function getPassList($where,$pages,$is_channel=false){
		$list = Db::name('Withdraw')->where($where)->order('cur_date','desc')->page($pages['page'],$pages['limit'])->select();
		$count = 0;
		if($list){
			$vips = $opts = [];
			foreach ($list as &$v){
				$v['name'] = '未知';
				$v['opt_name'] = '--';
				$v['detail_url'] = '';
				$channel = myCache::getChannel($v['channel_id']);
				if($channel){
					$v['name'] = $channel['name'];
				}
				if($v['opt_id']){
					if($is_channel){
						$optChannel = myCache::getChannel($v['opt_id']);
						if($optChannel){
							$v['opt_name'] = $optChannel['name'];
						}
					}else{
						if(!array_key_exists($v['opt_id'], $opts)){
							$opts[$v['opt_id']] = myDb::getValue('Manage',[['id','=',$v['opt_id']]],'name');
						}
						if($opts[$v['opt_id']]){
							$v['opt_name'] = $opts[$v['opt_id']];
						}
					}
				}
				$v['pay_time'] = date('Y-m-d H:i:s',$v['pay_time']);
				$v['bank_info'] = json_decode($v['bank_info'],true);
			}
			$count = Db::name('Withdraw')->where($where)->count();
		}
		return ['data'=>$list,'count'=>$count];
	}
    
    //处理结算
    public static function doPass($id,$channel_id=0){
    	$flag = false;
    	$time = time();
    	$list = Db::name('Withdraw')->where('id',$id)->where('status',0)->field('id,channel_id,to_channel_id,money')->select();
    	if($list){
    		global $loginId;
    		$wData = ['pay_time'=>$time,'status'=>1,'opt_id'=>$loginId];
    		Db::startTrans();
    		$flag = true;
    		foreach ($list as $v){
    			$flag = false;
    			if($v['to_channel_id'] == $channel_id){
    				$re = Db::name('Withdraw')->where('id',$v['id'])->update($wData);
    				if($re){
    					$chartData = [
    						'withdraw_wait' => Db::raw('withdraw_wait-'.$v['money']),
    						'withdraw_pay' => Db::raw('withdraw_pay+'.$v['money'])
    					];
    					$res = Db::name('ChannelMoney')->where('channel_id',$v['channel_id'])->update($chartData);
    					if($res){
    						$flag = true;
    					}
    				}
    			}
    			if(!$flag){
    				break;
    			}
    		}
    		if($flag){
    			Db::commit();
    		}else{
    			Db::rollback();
    		}
    	}
    	return $flag;
    }
    
    //获取导出数据
    public static function getExportData($where){
    	$data = [];
    	$list = myDb::getList('Withdraw', $where,'id,channel_id,money,cur_date');
    	if($list){
    		$money = 0;
    		$one = [];
    		foreach ($list as $v){
    			$channel = myCache::getChannel($v['channel_id']);
    			$one['name'] = $channel['name'];
    			$channel = null;
    			$money += $v['money'];
    			$one['money'] = $v['money'];
    			$one['week'] = $v['cur_date'];
    			$data[] = $one;
    		}
    		$data[] = [
    			'name' => '合计：',
    			'money' => $money,
    			'week' => ''
    		];
    	}
    	return $data;
    }
    
    //获取我的结算概要信息
    public static function getMyCountData($channel_id){
    	$data = ['money' => 0,'done' => 0,'wait' => 0];
    	$data['money'] = myCache::getChannelAmount($channel_id);
    	$channel = Db::name('Withdraw')->where('channel_id','=',$channel_id)->where('status','between',[0,1])->field('status,sum(money) as money')->group('status')->select();
    	foreach ($channel as $v){
    		if($v['status'] == 1){
    			$data['done'] += $v['money'];
    		}else{
    			$data['wait'] += $v['money'];
    		}
    	}
    	return $data;
    }
    
    //获取下级代理概要信息
    public static function getChildCountData($channel_id=0){
    	$data = ['money' => 0,'done' => 0,'wait' => 0];
    	$agent = Db::name('Withdraw')->where('to_channel_id','=',$channel_id)->where('status','between',[0,1])->field('status,sum(money) as money')->group('status')->select();
    	foreach ($agent as $val){
    		if($val['status'] == 1){
    			$data['done'] += $val['money'];
    		}else{
    			$data['wait'] += $val['money'];
    		}
    	}
    	$agent_money = Db::name('ChannelMoney')->where('channel_id','>',0)->where('pid','=',$channel_id)->sum('withdraw_total');
    	$data['money'] += $agent_money;
    	return $data;
    }
    
    //获取事件
    public static function getSiteEventData(){
    	$rules = [
    		'sign' =>  ["require|array",["require"=>"主键参数错误",'array'=>'主键参数错误']],
    		'event' => ["require|eq:pass",["require"=>'请选择按钮绑定事件',"eq"=>'按钮绑定事件错误']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
}