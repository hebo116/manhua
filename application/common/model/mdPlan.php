<?php
namespace app\common\model;

use think\Db;
use site\myCache;

class mdPlan{
	
	//每小时执行，统计用户信息
	public static function doSysCount($start_time,$end_time){
		$create_time = time();
		$count_hour = date('G',$end_time);
		$count_date = date('Y-m-d',$start_time);
		$repeat = Db::name('PlanRecord')->where('create_date',$count_date)->where('hour',$count_hour)->value('id');
		if($repeat){
			return false;
		}
		$remark = [];
		$is_add = false;
		$real_date = date('Y-m-d',$create_time);
		if($real_date != $count_date){
			if(($end_time + 3600) < $create_time){
				$is_add = true;
			}
		}
		$timeArr = [$start_time,$end_time];
		$memberData = self::getCountMember($timeArr,[],[]);
		$orderData = self::getCountOrder($timeArr,$memberData['chData'], $memberData['spData']);
		$chData = $orderData['chData'];
		$spData = self::getCountSpreadView($timeArr, $orderData['spData']);
		Db::startTrans();
		try{
			if($spData){
				foreach ($spData as $sk=>$sv){
					$scId = myCache::getSpreadChartId($sk,$count_date);
					if($scId){
						$spreadChartData = self::getSpUpdateData($sv);
						if($spreadChartData){
							Db::name('SpreadChart')->where('id',$scId)->update($spreadChartData);
						}
					}else{
						$spreadChartData = $sv;
						$spreadChartData['sid'] = $sk;
						$spreadChartData['create_date'] = $count_date;
						Db::name('SpreadChart')->insert($spreadChartData);
					}
				}
			}
			if($chData){
				foreach ($chData as $ck=>$cv){
					$data = $cv;
					if($is_add){
						$one = ['channel_id'=>$ck,'money'=>0,'from_date'=>$count_date,'from_h'=>$count_hour,'to_date'=>$real_date];
						if($data['income_money']){
							$curAddId = myCache::getChannelChartId($ck,$real_date);
							if($curAddId){
								Db::name('ChannelChart')->where('id',$curAddId)->setInc('income_money',$data['income_money']);
							}else{
								$addData = [
									'status' => 2,
									'channel_id' => $ck,
									'create_date' => $real_date,
									'income_money' => $data['income_money']
								];
								Db::name('ChannelChart')->insert($addData);
							}
							$one['money'] = $data['income_money'];
							$data['income_money'] = 0;
						}
						$remark[] = $one;
					}
					$chId = myCache::getChannelChartId($ck,$count_date);
					if($chId){
						$channelChartData = self::getChUpdateData($data);
						if($channelChartData){
							Db::name('ChannelChart')->where('id',$chId)->update($channelChartData);
						}
					}else{
						$channelChartData = $data;
						$channelChartData['status'] = 2;
						$channelChartData['channel_id'] = $ck;
						$channelChartData['create_date'] = $count_date;
						Db::name('ChannelChart')->insert($channelChartData);
					}
				}
			}
			$planData = ['hour'=>$count_hour,'create_date'=>$count_date,'create_time'=>$create_time,'remark'=>json_encode($remark)];
			Db::name('PlanRecord')->insert($planData);
			Db::commit();
			return true;
		}catch (\Exception $e){
			Db::rollback();
			$log_date = date('Y_m_d_G',$start_time);
			$log_path = env('root_path').'runtime/log';
			$log_name = $log_path.'/count_'.$log_date.'_error.txt';
			$myfile = fopen($log_name,"w");
			fwrite($myfile,$e->getMessage());
			fclose($myfile);
			return false;
		}
		
	}
	
	//整理渠道统计插入数据
	private static function getChUpdateData($tmp){
		$data = [];
		if($tmp['add_num']){
			$data['add_num'] = Db::raw('add_num+'.$tmp['add_num']);
		}
		if($tmp['charge_num']){
			$data['charge_num'] = Db::raw('charge_num+'.$tmp['charge_num']);
		}
		if($tmp['charge_money']){
			$data['charge_money'] = Db::raw('charge_money+'.$tmp['charge_money']);
		}
		if($tmp['income_money']){
			$data['income_money'] = Db::raw('income_money+'.$tmp['income_money']);
		}
		return $data;
	}
	
	//整理推广统计插入数据
	private static function getSpUpdateData($tmp){
		$data = [];
		if($tmp['add_num']){
			$data['add_num'] = Db::raw('add_num+'.$tmp['add_num']);
		}
		if($tmp['view_num']){
			$data['view_num'] = Db::raw('view_num+'.$tmp['view_num']);
		}
		if($tmp['charge_num']){
			$data['charge_num'] = Db::raw('charge_num+'.$tmp['charge_num']);
		}
		if($tmp['charge_money']){
			$data['charge_money'] = Db::raw('charge_money+'.$tmp['charge_money']);
		}
		return $data;
	}
	
	//获取推广访问量统计情况
	private static function getCountSpreadView($timeArr,$spData){
		$list = Db::name('SpreadView')->where('create_time','between',$timeArr)->select();
		if($list){
			foreach ($list as $v){
				if(isset($spData[$v['sid']])){
					$spData[$v['sid']]['view_num'] += 1;
				}else{
					$spData[$v['sid']] = ['add_num'=>0,'view_num'=>0,'total_charge_num'=>0,'charge_num'=>0,'charge_money'=>0];
					$spData[$v['sid']]['view_num'] += 1;
				}
			}
		}
		return $spData;
	}
	
	//获取订单统计数据
	private static function getCountOrder($timeArr,$chData,$spData){
		$order_field = 'id,channel_id,agent_id,money,is_count,count_info,spread_id,status';
		$order = Db::name('Order')->where('pay_time','between',$timeArr)->field($order_field)->select();
		if($order){
			foreach ($order as $ov){
				if($ov['status'] == 2){
					if($ov['is_count'] == 1){
						$count_info = json_decode($ov['count_info'],true);
						if($count_info){
							if(isset($count_info['channel'])){
								$channel_id = $count_info['channel']['id'];
								if(isset($chData[$channel_id])){
									$chData[$channel_id]['charge_num'] += 1;
									$chData[$channel_id]['charge_money'] += $ov['money'];
									$chData[$channel_id]['income_money'] += $count_info['channel']['money'];
								}else{
									$chData[$channel_id] = ['add_num'=>0,'charge_num'=>0,'charge_money'=>0,'income_money'=>0];
									$chData[$channel_id]['charge_num'] += 1;
									$chData[$channel_id]['charge_money'] += $ov['money'];
									$chData[$channel_id]['income_money'] += $count_info['channel']['money'];
								}
							}
							if(isset($count_info['agent'])){
								$agent_id = $count_info['agent']['id'];
								if(isset($chData[$agent_id])){
									$chData[$agent_id]['charge_num'] += 1;
									$chData[$agent_id]['charge_money'] += $ov['money'];
									$chData[$agent_id]['income_money'] += $count_info['agent']['money'];
								}else{
									$chData[$agent_id] = ['add_num'=>0,'charge_num'=>0,'charge_money'=>0,'income_money'=>0];
									$chData[$agent_id]['charge_num'] += 1;
									$chData[$agent_id]['charge_money'] += $ov['money'];
									$chData[$agent_id]['income_money'] += $count_info['agent']['money'];
								}
							}
						}
						if($ov['spread_id']){
							if(isset($spData[$ov['spread_id']])){
								$spData[$ov['spread_id']]['charge_num'] += 1;
								$spData[$ov['spread_id']]['total_charge_num'] += 1;
								$spData[$ov['spread_id']]['charge_money'] += $ov['money'];
							}else{
								$spData[$ov['spread_id']] = ['add_num'=>0,'total_charge_num'=>0,'view_num'=>0,'charge_num'=>0,'charge_money'=>0];
								$spData[$ov['spread_id']]['charge_num'] += 1;
								$spData[$ov['spread_id']]['total_charge_num'] += 1;
								$spData[$ov['spread_id']]['charge_money'] += $ov['money'];
							}
						}
					}
				}else{
					if($ov['spread_id']){
						if(isset($spData[$ov['spread_id']])){
							$spData[$ov['spread_id']]['total_charge_num'] += 1;
						}else{
							$spData[$ov['spread_id']] = ['add_num'=>0,'view_num'=>0,'total_charge_num'=>0,'charge_num'=>0,'charge_money'=>0];
							$spData[$ov['spread_id']]['total_charge_num'] += 1;
						}
					}
				}
			}
		}
		return ['chData'=>$chData,'spData'=>$spData];
	}
	
	//获取统计时间段用户
	private static function getCountMember($timeArr,$chData,$spData){
		$member_field = 'id,channel_id,agent_id,spread_id';
		$member = Db::name('Member')->where('create_time','between',$timeArr)->field($member_field)->select();
		if($member){
			foreach ($member as $mv){
				if($mv['channel_id']){
					if(isset($chData[$mv['channel_id']])){
						$chData[$mv['channel_id']]['add_num'] += 1;
					}else{
						$chData[$mv['channel_id']] = ['add_num'=>1,'charge_num'=>0,'charge_money'=>0,'income_money'=>0];
					}
				}
				if($mv['agent_id']){
					if(isset($chData[$mv['agent_id']])){
						$chData[$mv['agent_id']]['add_num'] += 1;
					}else{
						$chData[$mv['agent_id']] = ['add_num'=>1,'charge_num'=>0,'charge_money'=>0,'income_money'=>0];
					}
				}
				if($mv['spread_id']){
					if(isset($spData[$mv['spread_id']])){
						$spData[$mv['spread_id']]['add_num'] += 1;
					}else{
						$spData[$mv['spread_id']] = ['add_num'=>1,'view_num'=>0,'total_charge_num'=>0,'charge_num'=>0,'charge_money'=>0];
					}
				}
			}
		}
		return ['chData'=>$chData,'spData'=>$spData];
	}
}