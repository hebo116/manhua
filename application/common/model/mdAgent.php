<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myCache;
use site\myValidate;

class mdAgent{
	
	public static function getList($where,$pages){
		$field = 'a.id,a.name,a.login_name,a.url,a.ratio,a.status,b.charge_money';
		$list = Db::name('Channel a')
		->join('channel_money b','a.id=b.channel_id','left')
		->where($where)
		->field($field)
		->page($pages['page'],$pages['limit'])
		->order('a.id','desc')
		->select();
		$count = 0;
		if($list){
			$count = Db::name('Channel a')->where($where)->count();
		}
		return ['data'=>$list,'count'=>$count];
	}
    
    //检查域名是否重复
    public static function checkUrlRepeat($url,$id=0,$is_site=false){
        $where = [['url','=',$url]];
        if($id){
            $where[] = ['id','<>',$id];
        }
        $repeat = myDb::getCur('Channel', $where,'id,name');
        if($repeat){
            res_api('该域名已占用，请更换');
        }
        if(!$is_site){
            $site = getMyConfig('website');
            if($url == $site['url']){
                res_api('该域名已占用，请更换');
            }
        }
    }
    
    //获取更新代理选项
    public static function getAgentOptions(){
        $option = [
            'status' => [
                'name' => 'status',
                'option' => [
                    ['val'=>1,'text'=>'启用','default'=>1],
                    ['val'=>2,'text'=>'禁用','default'=>0]
                ]
            ]
        ];
        return $option;
    }
    
    //保存代理信息
    public static function doneAgent($data){
        if(array_key_exists('id', $data)){
        	if($data['url']){
        		self::checkUrlRepeat($data['url'],$data['id']);
        	}
            $cur = myDb::getById('Channel',$data['id'],'id,url');
            if(!$cur){
                res_api('代理信息异常');
            }
            $key = 'channel_info_'.$data['id'];
            $flag = myDb::saveIdData('Channel', $data);
            if($flag){
            	myCache::rmChannel($cur['id']);
            	if($cur['url']){
            		myCache::rmUrlInfo($cur['url']);
            	}
            }
        }else{
        	if(isset($data['login_name'])){
        		$repeat = Db::name('Channel')->where('login_name','=',$data['login_name'])->value('id');
        		if($repeat){
        			res_api('该账号已存在');
        		}
        	}
        	if($data['url']){
        		self::checkUrlRepeat($data['url']);
        	}
        	Db::startTrans();
        	$flag = false;
            $data['password'] = createPwd($data['password']);
            $data['create_time'] = time();
            $cid = Db::name('Channel')->insertGetId($data);
            if($cid){
            	$moneyData = ['channel_id'=>$cid];
            	if(isset($data['parent_id']) && $data['parent_id']){
            		$moneyData['pid'] = $data['parent_id'];
            	}
            	$res = Db::name('ChannelMoney')->insert($moneyData);
            	if($res){
            		$info = self::getLevelInfo($cid);
	            	if($info){
	            		$codeInfo = [
	            			'type' => 2,
	            			'cid' => $cid,
	            			'code' => myDb::createCode('Code'),
	            			'info' => json_encode($info)
	            		];
	            		$codeRes = Db::name('Code')->insert($codeInfo);
	            		if($codeRes){
	            			$flag = true;
	            		}
	            	}
            	}
            }
            if($flag){
            	Db::commit();
            }else{
            	Db::rollback();
            }
        }
        if($flag){
            res_api();
        }else{
            res_api('保存失败，请重试');
        }
    }
    
    //获取代理层级关系
    public static function getLevelInfo($channel_id=0){
    	$data = [
    		'channel_id' => 0,
    		'agent_id' => 0,
    	];
    	if($channel_id){
    		$channel = myCache::getChannel($channel_id);
    		if(!$channel){
    			return false;
    		}
    		if($channel['type'] == 1){
    			$data['channel_id'] = $channel['id'];
    		}else{
    			$data['channel_id'] = $channel['parent_id'];
    			$data['agent_id'] = $channel['id'];
    		}
    	}
    	return $data;
    }
    
    //跳转代理后台
    public static function intoBackstage($channel_id){
        $cur = myDb::getById('Channel', $channel_id);
        if(!$cur){
            res_api('代理参数异常');
        }
        $url = '/agent';
        $key = 'AGENT_LOGIN_ID';
        session($key,$cur['id']);
        return $url;
    }
    
    /**
     * 获取代理状态名称
     * @param number $status 状态值
     * @return string
     */
    public static function getStatusName($status){
        $name = '未知';
        switch ($status){
            case 0:
                $name = '待审核';
                break;
            case 1:
                $name = '正常';
                break;
            case 2:
                $name = '禁用';
                break;
            case 3:
                $name = '审核不通过';
                break;
        }
        return $name;
    }
    
    //获取渠道更新规则
    public static function getRules($isAdd=1){
    	$rules = [
    		'name' =>  ["require|max:20",["require"=>"请输入渠道名称",'max'=>'渠道名称最多支持20个字符']],
    		'url' =>  ["max:50",['url'=>'绑定域名长度超出限制']],
    		'deduct_min' => ["number",["number"=>"请输入正确格式的距扣量数"]],
    		'deduct_num' => ["number",["number"=>"请输入正确格式的扣量数"]],
    		'ratio' => ["number|between:0,100",["number"=>"请输入正确格式的返额比例",'between'=>'请输入0-100区间的返额比例']],
    		'wefare_days' => ["number",["number"=>"请输入正确格式的代理福利时长"]],
    	];
    	$rules['bank_user'] = ["max:20",['max'=>'开户人姓名长度超出限制']];
    	$rules['bank_name'] = ["max:20",['max'=>'开户网点长度超出限制']];
    	$rules['bank_no'] = ["max:20",['max'=>'账号长度超出限制']];
    	if(!$isAdd){
    		$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}else{
    		$rules['login_name'] = ["require|alphaDash|length:5,12",["require"=>"请输入登陆账户名","alphaDash"=>'登陆账户名必须是英文、数字、下划线和破折号',"length"=>"请输入5至12位符合规范的登陆账户名"]];
    		$rules['password'] = ["require|length:6,16",["require"=>"请输入登陆密码","length"=>"请输入6-16位登陆密码"]];
    	}
    	return $rules;
    }
    
    //获取提现信息规则
    public static function getAccountData(){
    	$rules = [
    		'bank_user' => ["require|max:10",['require'=>'请输入开户人姓名','max'=>'开户人姓名长度超出限制']],
    		'bank_name' => ["require|max:20",['require'=>'请输入开户银行名称','max'=>'开户银行长度超出限制']],
    		'bank_no' => ["max:30",['require'=>'请输入银行卡号','max'=>'银行卡号长度超出限制']],
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取事件规则
    public static function getEventRules(){
    	$rules = [
    		'id' => ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
    		'event' => ["require|in:on,off,delete,resetpwd,pass,fail",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
    	];
    	return $rules;
    }
    
    
}