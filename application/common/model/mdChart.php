<?php
namespace app\common\model;

use think\Db;
use site\myCache;

class mdChart{
    
    //获取充值统计相关数据
    public static function getChargeData(){
        $list = Db::name('Order')->where('status',2)->field('money,book_id')->select();
        $data = [
            'guide' => ['money' => 0,'count' => 0],
            'charge' => ['money' => 0,'count' => 0]
        ];
        if($list){
            foreach ($list as $v){
                if($v['book_id'] > 0){
                    $data['guide']['money'] += $v['money'];
                    $data['guide']['count'] += 1;
                }else{
                    $data['charge']['money'] += $v['money'];
                    $data['charge']['count'] += 1;
                }
            }
        }
        return $data;
    }
    
    //获取充值详情
    public static function getChargeList(){
        $list = Db::name('Order')->where('status',2)->where('book_id','>',0)->field('book_id,sum(money) as money')->group('book_id')->order('money','DESC')->select();
        if($list){
        	$yesterday = date('Ymd',strtotime('yesterday'));
        	$where = [['status','=',2],['book_id','=',0],['create_date','=',$yesterday]];
            foreach ($list as &$v){
            	$book = myCache::getBook($v['book_id']);
            	$v['name'] = $book ? $book['name'] : '<font class="text-red">未知</font>';
            	$book = null;
            	$where[2][2] = $v['book_id'];
            	$money = Db::name('Order')->where($where)->sum('money');
            	$v['yesterday'] = $money ? : 0;
            }
        }
        return $list;
    }
    
    //获取用户统计信息
    public static function getMemberCount($channel_id=0){
        $cur_time = strtotime('today');
        $today = $yesterday = $month = $total = $temp = [
            'add_num' => 0,
            'charge_money' => 0,
            'charge_nums' => 0
        ];
        $where = [['create_time','>=',$cur_time]];
        if($channel_id){
        	$channel = myCache::getChannel($channel_id);
        	if(!$channel){
        		res_api('渠道信息异常');
        	}
        	$keyword = $channel['type'] == 1 ? 'channel_id' : 'agent_id';
        	$where[] = [$keyword,'=',$channel_id];
        }
        $today_list = Db::name('Member')->where($where)->field('id,is_charge')->select();
        //dump($today_list);die;
        if($today_list){
            foreach($today_list as $v){
                $today['add_num'] += 1;
                if($v['is_charge'] == 1){
                    $order_money = Db::name('Order')->where('uid','=',$v['id'])->where('status','=',2)->sum('money');
                    $today['charge_money'] += $order_money;
                    $today['charge_nums'] += 1;
                }
            }
        }
        $start = $cur_time - 86400*30;
        $start_date = date('Y-m-d',$start);
        $select_date = date('Y-m-d',($cur_time-40*86400));
        $field = 'create_date,add_num,charge_money,charge_nums';
        $list = Db::name('TaskMember')->where('channel_id','=',$channel_id)->field($field)->select();
        $temps = $data = [];
        if($list){
            $yes_day = date('Y-m-d',strtotime('-1 day'));
            $month_day = date('Y-m').'-01';
            foreach ($list as $val){
                $temps[$val['create_date']] = $val;
                if($val['create_date'] === $yes_day){
                    $yesterday = $val;
                }
                if($val['create_date'] >= $month_day){
                    $month['add_num'] += $val['add_num'];
                    $month['charge_money'] += $val['charge_money'];
                    $month['charge_nums'] += $val['charge_nums'];
                }
                $total['add_num'] += $val['add_num'];
                $total['charge_money'] += $val['charge_money'];
                $total['charge_nums'] += $val['charge_nums'];
            }
        }
        $cur_date = date('Y-m-d');
        while ($start_date < $cur_date){
            if(isset($temps[$start_date])){
                $data[] = $temps[$start_date];
            }else{
                $temp['create_date'] = $start_date;
                $data[] = $temp;
            }
            $start_date = date('Y-m-d',(strtotime($start_date)+86400));
        }
        $data = array_reverse($data);
        $res = ['today'=>$today,'yesterday'=>$yesterday,'month'=>$month,'total'=>$total,'data'=>$data];
        return $res;
    }
    
    //获取订单今日统计
    public static function getTodayOrderMsg($channel_id=0){
    	$cur_time = strtotime('today');
    	$today = [
    		'n_pay' => 0,
    		'n_notpay' => 0,
    		'n_money' => 0,
    		'n_user' => 0,
    		'n_rate' => 0,
    		'p_pay' => 0,
    		'p_notpay' => 0,
    		'p_money' => 0,
    		'p_user' => 0,
    		'p_rate' => 0,
    		'total_money' => 0,
    		'type1_money' => 0,
    		'type2_money' => 0,
    	];
    	$where = [['create_time','>=',$cur_time]];
    	if($channel_id){
    		$channel = myCache::getChannel($channel_id);
    		if(!$channel){
    			res_api('渠道信息异常');
    		}
    		$keyword = $channel['type'] == 1 ? 'channel_id' : 'agent_id';
    		$where[] = [$keyword,'=',$channel_id];
            $where[] = ['is_count','=',1];
    	}
    	$today_list = Db::name('Order')->where($where)->field('id,type,package,uid,status,money')->select();
    	if($today_list){
    		$n_uids = $p_uids = [];
    		foreach($today_list as $v){
    			if($v['status'] == 2){
    				if($v['package'] > 0){
    					$today['p_pay'] += 1;
    					$today['p_money'] += $v['money'];
    					if(!in_array($v['uid'], $p_uids)){
    						$today['p_user'] += 1;
    						$p_uids[] = $v['uid'];
    					}
    				}else{
    					$today['n_pay'] += 1;
    					$today['n_money'] += $v['money'];
    					if(!in_array($v['uid'], $n_uids)){
    						$today['n_user'] += 1;
    						$n_uids[] = $v['uid'];
    					}
    				}
    				$today['total_money'] += $v['money'];
    				$type_key = in_array($v['type'], [1,2,3]) ? 'type'.$v['type'].'_money' : 'type1_money';
    				$today[$type_key] += $v['money'];
    			}else{
    				if($v['package'] > 0){
    					$today['p_notpay'] += 1;
    				}else{
    					$today['n_notpay'] += 1;
    				}
    			}
    		}
    	}
    	if($today['n_notpay'] || $today['n_pay']){
    		$today['n_rate'] = (round($today['n_pay']/($today['n_pay']+$today['n_notpay']),2)*100);
    	}
    	if($today['p_notpay'] || $today['p_pay']){
    		$today['p_rate'] = (round($today['p_pay']/($today['p_pay']+$today['p_notpay']),2)*100);
    	}
    	return $today;
    }
    
    //获取订单统计信息
    public static function getOrderCount($channel_id=0){
        $cur_time = strtotime('today');
        $today = $yesterday = $month = $total = $temp = [
            'n_pay' => 0,
            'n_notpay' => 0,
            'n_money' => 0,
            'n_user' => 0,
            'n_rate' => 0,
            'p_pay' => 0,
            'p_notpay' => 0,
            'p_money' => 0,
            'p_user' => 0,
            'p_rate' => 0,
            'total_money' => 0,
            'type1_money' => 0,
            'type2_money' => 0,
        ];
        $where = [['create_time','>=',$cur_time]];
        if($channel_id){
        	$channel = myCache::getChannel($channel_id);
        	if(!$channel){
        		res_api('渠道信息异常');
        	}
        	$keyword = $channel['type'] == 1 ? 'channel_id' : 'agent_id';
        	$where[] = [$keyword,'=',$channel_id];
            $where[] = ['is_count','=',1];
        }
        $today_list = Db::name('Order')->where($where)->field('id,type,package,uid,status,money')->select();
        if($today_list){
            $n_uids = $p_uids = [];
            foreach($today_list as $v){
                if($v['status'] == 2){
                    if($v['package'] > 0){
                        $today['p_pay'] += 1;
                        $today['p_money'] += $v['money'];
                        if(!in_array($v['uid'], $p_uids)){
                            $today['p_user'] += 1;
                            $p_uids[] = $v['uid'];
                        }
                    }else{
                        $today['n_pay'] += 1;
                        $today['n_money'] += $v['money'];
                        if(!in_array($v['uid'], $n_uids)){
                            $today['n_user'] += 1;
                            $n_uids[] = $v['uid'];
                        }
                    }
                    $today['total_money'] += $v['money'];
                    $type_key = in_array($v['type'], [1,2,3]) ? 'type'.$v['type'].'_money' : 'type1_money';
                    $today[$type_key] += $v['money'];
                }else{
                    if($v['package'] > 0){
                        $today['p_notpay'] += 1;
                    }else{
                        $today['n_notpay'] += 1;
                    }
                }
            }
        }
        if($today['n_notpay'] || $today['n_pay']){
            $today['n_rate'] = (round($today['n_pay']/($today['n_pay']+$today['n_notpay']),2)*100);
        }
        if($today['p_notpay'] || $today['p_pay']){
            $today['p_rate'] = (round($today['p_pay']/($today['p_pay']+$today['p_notpay']),2)*100);
        }
        $start = $cur_time - 86400*30;
        $start_date = date('Y-m-d',$start);
        $select_date = date('Y-m-d',($cur_time-40*86400));
        $field = 'create_date,n_pay,n_notpay,n_money,n_user,n_rate,p_pay,p_notpay,p_money,p_user,p_rate,total_money,type1_money,type2_money,type3_money';
        $list = Db::name('TaskOrder')->where('channel_id','=',$channel_id)->field($field)->select();
        $temps = $data = [];
        if($list){
            $yes_day = date('Y-m-d',strtotime('-1 day'));
            $month_day = date('Y-m').'-01';
            foreach ($list as $val){
                $temps[$val['create_date']] = $val;
                if($val['create_date'] === $yes_day){
                    $yesterday = $val;
                }
                if($val['create_date'] >= $month_day){
                    $month['n_pay'] += $val['n_pay'];
                    $month['n_notpay'] += $val['n_notpay'];
                    $month['n_user'] += $val['n_user'];
                    $month['n_money'] += $val['n_money'];
                    $month['p_pay'] += $val['p_pay'];
                    $month['p_notpay'] += $val['p_notpay'];
                    $month['p_user'] += $val['p_user'];
                    $month['p_money'] += $val['p_money'];
                    $month['total_money'] += $val['total_money'];
                    $month['type1_money'] += $val['type1_money'];
                    $month['type2_money'] += $val['type2_money'];
                }
                $total['n_pay'] += $val['n_pay'];
                $total['n_notpay'] += $val['n_notpay'];
                $total['n_user'] += $val['n_user'];
                $total['n_money'] += $val['n_money'];
                $total['p_pay'] += $val['p_pay'];
                $total['p_notpay'] += $val['p_notpay'];
                $total['p_user'] += $val['p_user'];
                $total['p_money'] += $val['p_money'];
                $total['total_money'] += $val['total_money'];
                $total['type1_money'] += $val['type1_money'];
                $total['type2_money'] += $val['type2_money'];
            }
        }
        if($month['n_notpay'] || $month['n_pay']){
            $month['n_rate'] = (round($month['n_pay']/($month['n_pay']+$month['n_notpay']),2)*100);
        }
        if($month['p_notpay'] || $month['p_pay']){
            $month['p_rate'] = (round($month['p_pay']/($month['p_pay']+$month['p_notpay']),2)*100);
        }
        if($total['n_notpay'] || $total['n_pay']){
            $total['n_rate'] = (round($total['n_pay']/($total['n_pay']+$total['n_notpay']),2)*100);
        }
        if($total['p_notpay'] || $total['p_pay']){
            $total['p_rate'] = (round($total['p_pay']/($total['p_pay']+$total['p_notpay']),2)*100);
        }
        
        $cur_date = date('Y-m-d');
        while ($start_date < $cur_date){
            if(isset($temps[$start_date])){
                $data[] = $temps[$start_date];
            }else{
                $temp['create_date'] = $start_date;
                $data[] = $temp;
            }
            $start_date = date('Y-m-d',(strtotime($start_date)+86400));
        }
        foreach ($data as &$value){
            $value['n_svg'] = $value['p_svg'] = 0;
            if($value['n_user']){
                $value['n_svg'] = round($value['n_money']/$value['n_user'],2);
            }
            if($value['p_user']){
                $value['p_svg'] = round($value['p_money']/$value['p_user'],2);
            }
        }
        $data = array_reverse($data);
        $res = ['today'=>$today,'yesterday'=>$yesterday,'month'=>$month,'total'=>$total,'data'=>$data];
        return $res;
    }
    
    //获取用户活跃度统计
    public static function getActiveList($start_date,$end_date){
    	$list = Db::name('MemberActive')->where('create_date','between',[$start_date,$end_date])->field('count(*) as count,create_date')->group('create_date')->select();
    	$temps = [];
    	if($list){
    		$temps = array_column($list, null,'create_date');
    	}
    	$title = $data = [];
    	while ($start_date <= $end_date){
    		$title[] = $start_date;
    		if(isset($temps[$start_date])){
    			$data[] = $temps[$start_date]['count'];
    		}else{
    			$data[] = 0;
    		}
    		$start_date = date('Y-m-d',(strtotime($start_date)+86400));
    	}
    	return ['title'=>$title,'data'=>$data];
    }
    
}
