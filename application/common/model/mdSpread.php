<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myCache;
use site\myValidate;
use Endroid\QrCode\QrCode;

class mdSpread{
	
	//获取推广链接列表
	public static function getSpreadList($where,$pages){
		$field = 'a.id,a.name,a.is_sub,a.url,a.book_id,a.book_name,a.chapter_number,a.number,a.cost_money,a.create_time';
		$field .= ',sum(b.view_num) as view_num,sum(b.add_num) as add_num,sum(b.charge_money) as charge_money,sum(b.total_charge_num) as total_charge_num,sum(charge_num) as charge_num';
		$list = Db::name('Spread a')
		->join('spread_chart b','a.id=b.sid','left')
		->where($where)
		->field($field)
		->group('a.id')
		->page($pages['page'],$pages['limit'])
		->order('a.id','desc')
		->select();
		$count = 0;
		if($list){
			foreach ($list as &$v){
				$v['view_num'] = $v['view_num'] ? : 0;
				$v['add_num'] = $v['add_num'] ? : 0;
				$v['charge_num'] = $v['charge_num'] ? : 0;
				$v['total_charge_num'] = $v['total_charge_num'] ? : 0;
				$v['charge_ratio'] = '0%';
				if($v['charge_num']){
					if($v['charge_num'] > $v['total_charge_num']){
						$v['charge_ratio'] = '100%';
					}else{
						$v['charge_ratio'] = (round($v['charge_num']/$v['total_charge_num'],2)*100).'%';
					}
				}
				$v['book_url'] = my_url('Cartoon/index',['keyword'=>$v['book_name']]);
				$v['charge_money'] = $v['charge_money'] ? : 0;
				$chapterList = myCache::getBookChapterList($v['book_id']);
				$v['read_chapter'] = isset($chapterList[$v['chapter_number']]) ? $chapterList[$v['chapter_number']]['name'] : '--';
				$v['sub_chapter'] = $v['is_sub'] = 1 && isset($chapterList[$v['number']]) ? $chapterList[$v['number']]['name'] : '';
				$chapterList = null;
				$v['qrcode_url'] = my_url('qrcode',['id'=>$v['id']]);
				$v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
				$v['do_url'] = my_url('doLink',['id'=>$v['id']]);
				$v['detail_url'] = my_url('detail',['sid'=>$v['id']]);
				$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
			}
			$count = Db::name('Spread a')->where($where)->count();
		}
		return ['data'=>$list,'count'=>$count];
	}
	
	//更新推广链接
	public static function doneSpread($data){
		$flag = false;
		$time = time();
		$book = myCache::getBook($data['book_id']);
		if(!$book){
			res_api('书籍信息异常');
		}
		$data['book_name'] = $book['name'];
		if($data['chapter_number'] >= $data['number']){
			res_api('限制章节必须大于推广章节');
		}
		if(array_key_exists('id',$data)){
			$sid = $data['id'];
			unset($data['id']);
			$re = Db::name('Spread')->where('id','=',$sid)->update($data);
			if($re !== false){
				myCache::rmSpread($sid);
				$back = 'ok';
				$flag = true;
			}
		}else{
			$code = myDb::createCode('Code');
			$infoMsg = mdAgent::getLevelInfo($data['channel_id']);
			$codeInfo = [
				'type' => 1,
				'code' => $code,
				'cid' => $data['channel_id'],
				'info' => json_encode($infoMsg)
			];
			$site = getMyConfig('website');
			if(!$site){
				res_api('您尚未配置站点信息');
			}
			$url = self::getSpreadUrl($data['channel_id']);
			//$url .= '/index/Book/read.html?kc_code='.$code;
            $url .= '/index.html#/read?book_id='.$data['book_id'].'&number='.$data['chapter_number'].'&kc_code='.$code;
            $data['url'] = $url;
			$data['create_time'] = $time;
			Db::startTrans();
			$flag = false;
			$sid = Db::name('Spread')->insertGetId($data);
			if($sid){
				$back = ['id'=>$sid,'url'=>$url];
				$codeInfo['sid'] = $sid;
				$res = Db::name('Code')->insert($codeInfo);
				if($res){
					$shortUrl =  getShortUrl($url);
					if($shortUrl){
						$result = Db::name('Spread')->where('id',$sid)->setField('short_url',$shortUrl);
						if($result){
							$flag = true;
						}
					}else{
						$flag = true;
					}
				}
			}
			if($flag){
				Db::commit();
			}else{
				Db::rollback();
			}
		}
		if($flag){
			res_api($back);
		}else{
			res_api('生成推广链接失败');
		}
	}
	
	public static function createQrcode($id){
		$cur = myDb::getById('Spread', $id,'id,url');
		if(!$cur){
			res_api('推广信息异常');
		}
		if(!$cur['url']){
			res_api('推广链接不存在');
		}
		$path = './uploads/qrcode/'.$id;
		if(!is_dir($path)){
			mkdir($path,0777,true);
		}
		$filename = $path.'/qrcode.png';
		if(!@is_file($filename)){
			$qrcode = new QrCode();
			$qrcode->setText($cur['url']);
			$qrcode->setSize(400);
			$qrcode->writeFile($filename);
			if(!@is_file($filename)){
				res_api('二维码生成失败');
			}
			$bgs = [
					'./static/templet/qrbg/qrcode_bg1.jpg',
					'./static/templet/qrbg/qrcode_bg2.jpg',
					'./static/templet/qrbg/qrcode_bg3.jpg',
					'./static/templet/qrbg/qrcode_bg4.jpg',
					'./static/templet/qrbg/qrcode_bg5.jpg'
			];
			foreach($bgs as $k=>$v){
				$key = $k+1;
				$im_dst = imagecreatefromjpeg($v);
				$imgsize = getimagesize($filename);
				if(strpos($imgsize['mime'],'png') !== false){
					$im_src = imagecreatefrompng($filename);
				}else{
					$im_src = imagecreatefromjpeg($$filename);
				}
				$width = $imgsize[0];
				switch ($key){
					case 1:
						imagecopyresized ( $im_dst, $im_src,216, 25, 0, 0, 150, 150, $width, $width);
						break;
					case 2:
						imagecopyresized ( $im_dst, $im_src,365, 26, 0, 0, 180, 180, $width, $width);
						break;
					case 3:
						imagecopyresized ( $im_dst, $im_src,340, 60, 0, 0, 146, 146, $width, $width);
						break;
					case 4:
						imagecopyresized ( $im_dst, $im_src,105, 25, 0, 0, 150, 150, $width, $width);
						break;
					case 5:
						imagecopyresized ( $im_dst, $im_src,210, 56, 0, 0, 152, 152, $width, $width);
						break;
				}
				$picname = 'qrcode_pic_'.$key.'.png';
				$newfile = './uploads/qrcode/'.$id.'/'.$picname;
				imagejpeg($im_dst,$newfile);
				imagedestroy($im_src);
				imagedestroy($im_dst);
			}
		}
	}
	
	//获取推广域名
	public static function getSpreadUrl($channel_id=0){
		$url = 'http://';
		if($channel_id){
			$channel = myCache::getChannel($channel_id);
			if($channel['url']){
				$url .= $channel['url'];
			}else{
				$config = getMyConfig('website');
				if(!array_key_exists('url', $config) || !$config['url']){
					res_api('您尚未配置站点url');
				}
				$url .= $config['url'];
			}
		}else{
			$config = getMyConfig('website');
			if(!array_key_exists('url', $config) || !$config['url']){
				res_api('您尚未配置站点url');
			}
			$url .= $config['url'];
		}
		return $url;
	}
	
	/**
	 * 获取推广选项
	 * @param unknown $bookId
	 * @return 
	 */
	public static function getOptions($bookId){
		$chapter = mdBook::getTenChapter($bookId);
		if(!$chapter){
			res_api('该书籍未添加章节');
		}
		$options = [
			'is_sub' => [
				'name' => 'is_sub',
				'option' => [['val'=>1,'text'=>'是','default'=>1],['val'=>2,'text'=>'否','default'=>0]]
			],
			'chapter' => $chapter
		];
		return $options;
	}
	
	//获取生成规则
	public static function getRules($isAdd=1){
		$rules = [
			'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
			'chapter_number' => ["require|number|gt:0",["require"=>"请选择推广章节",'number'=>'推广章节参数错误',"gt"=>"推广章节参数错误"]],
			'name' =>  ["require|max:100",["require"=>"请输入推广名称",'max'=>'推广名称最多支持100个字符']],
			'is_sub' => ["require|in:1,2",["require"=>"请选择是否强制关注","in"=>"未指定推广事件"]],
			'number' => ["require|number|gt:0",["require"=>"请输入强制关注章节",'number'=>'请输入强制关注章节',"gt"=>"强制关注章节必须大于0"]],
			'cost_money' =>  ["require|float|egt:0",["require"=>"请输入成本金额",'number'=>'金额格式不规范',"egt"=>"金额必须大于等于0"]],
			'remark' =>  ["max:255",['max'=>'备注最多支持255个字符']]
		];
		if(!$isAdd){
			$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
		}
		return $rules;
	}
	
	//获取生成规则
	public static function getChannelAddRules(){
		$rules = [
			'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
			'chapter_number' => ["require|number|gt:0",["require"=>"请选择推广章节",'number'=>'推广章节参数错误',"gt"=>"推广章节参数错误"]],
			'name' =>  ["require|max:100",["require"=>"请输入推广名称",'max'=>'推广名称最多支持100个字符']],
			'is_sub' => ["require|in:1,2",["require"=>"请选择是否强制关注","in"=>"未指定推广事件"]],
			'number' => ["require|number|gt:0",["require"=>"请输入强制关注章节",'number'=>'请输入强制关注章节',"gt"=>"强制关注章节必须大于0"]],
			'cost_money' =>  ["require|float|egt:0",["require"=>"请输入成本金额",'number'=>'金额格式不规范',"egt"=>"金额必须大于等于0"]],
			'remark' =>  ["max:255",['max'=>'备注最多支持255个字符']],
			'channel_id' => ['require|array',['require'=>'您尚未选择推广生成渠道','array'=>'渠道参数有误']]
		];
		return $rules;
	}
	
	//获取成本金额数据
	public static function getCostMoneyData(){
		$rules = [
			'id' => ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
			'cost_money' =>  ["require|float|egt:0",["require"=>"请输入成本金额",'number'=>'金额格式不规范',"egt"=>"金额必须大于等于0"]]
		];
		$data = myValidate::getData($rules);
		return $data;
	}
}