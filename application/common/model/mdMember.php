<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myCache;
use site\myValidate;

class mdMember{
    
    /**
     * 获取用户列表
     * @param array $where 查询条件
     * @param array $pages 分页参数
     * @return array
     */
    public static function getMemberList($where,$pages){
        $list = Db::name('Member')
        ->where($where)
        ->page($pages['page'],$pages['limit'])
        ->order('id','desc')
        ->select();
        $count = 0;
        if($list){
            foreach ($list as &$v){
                $v['info_url'] = my_url('Member/info',['id'=>$v['id']]);
                $v['status_name'] = ($v['status'] == 1) ? '正常' : '禁用';
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                if($v['channel_id']){
                	$channel = myCache::getChannel($v['channel_id']);
                	$v['channel_name'] = $channel ? $channel['name'] : '<font class="text-red">未知</font>';
                    if($v['agent_id']){
                    	$agent = myCache::getChannel($v['agent_id']);
                    	$v['agent_name'] = $agent ? $agent['name'] : '<font class="text-red">未知</font>';
                    }else{
                    	$v['agent_name'] = '/';
                    }
                }else{
                    $v['channel_name'] = '总站';
                    $v['agent_name'] = '/';
                }
                $v['vip_str'] = 'N/A';
                if($v['viptime'] > 0){
                	if($v['viptime'] == 1){
                		$v['vip_str'] = '终身';
                	}else{
                		$v['vip_str'] = '到期时间:'.date('Y-m-d',$v['viptime']);
                	}
                }
            }
            $count = Db::name('Member')->where($where)->count();
        }
        return ['data'=>$list,'count'=>$count];
    }
    
    //获取用户累计信息
    public static function getMemberCountMsg($uid,$channel_id=0){
    	$where = [['uid','=',$uid],['status','=',2]];
    	if($channel_id){
    		$where[] = ['is_count','=',1];
    		$channel = myCache::getChannel($channel_id);
    		$key = $channel['type'] == 1 ? 'channel_id' : 'agent_id';
    		$where[] = [$key,'=',$channel_id];
    	}
        $chargeMoney = Db::name('Order')->where($where)->sum('money');
        $consumeMoney = Db::name('MemberLog')->where('uid',$uid)->where('type',1)->sum('money');
        return ['charge'=>$chargeMoney,'consume'=>$consumeMoney];
    }
    
    //获取用户订单列表
    public static function getChargeOrder($where,$pages){
    	$field = 'id,type,order_no,is_count,money,status,book_id,create_time';
    	$list = Db::name('Order')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
    	$count = 0;
    	if($list){
    		foreach ($list as $k=>&$v){
    			$one['from_name'] = '--';
    			if($v['book_id']){
    				$book = myCache::getBook($v['book_id']);
    				$one['from_name'] = $book ? '书籍：'.$book['name'] : '<font class="text-read">未知</font>';
    				$book = null;
    			}
    			$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
    		}
    		$count = myDb::getCount('Order', $where);
    	}
    	return ['count'=>$count,'data'=>$list];
    }
    
    //获取阅读历史记录
    public static function getReadHistory($uid){
    	$list = [];
    	if($uid){
    		$cache = myCache::getReadCache($uid);
    		if($cache){
    			foreach ($cache as $v){
    				$book = myCache::getBook($v['book_id']);
    				if(!$book || !in_array($book['status'], [1,2])){
    					continue;
    				}
    				$one = [
    					'book_id' => $v['book_id'],
    					'number' => $v['last_number'],
    					'create_time' => date('Y-m-d H:i',$v['last_time'])
    				];
    				$chapter = myCache::getBookChapterList($book['id']);
    				$one['chapter_name'] = $chapter && isset($chapter[$one['number']]) ? $chapter[$one['number']]['name'] : '第'.$one['number'].'章';
    				$chapter = null;
    				if($book['cover']){
    					$one['cover'] = $book['cover'];
    					$one['name'] = $book['name'];
    				}else{
    					$one['cover'] = '/static/templet/default/cover.png';
    					$one['name'] = '未知';
    				}
    				$list[] = $one;
    			}
    			$create_times = array_column($list,'create_time');
    			array_multisort($create_times,SORT_DESC,$list);
    		}
    	}
    	return ['data'=>$list,'count'=>0];
    }
    
    //获取用户消费记录
    public static function getConsumeList($where,$pages){
        $list = Db::name('MemberLog')
        ->where($where)
        ->page($pages['page'],$pages['limit'])
        ->order('id','desc')
        ->select();
        $count = 0;
        if($list){
            foreach ($list as &$v){
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            $count = Db::name('MemberLog')
            ->where($where)
            ->count();
        }
        return ['count'=>$count,'data'=>$list];
    }
    
    //更新用户书币余额
    public static function setMemberMoney($id,$money){
        if($money > 0){
            $data = [
                'money' => Db::raw('money+'.$money),
                'total_money' => Db::raw('total_money+'.$money)
            ];
        }else{
            $data = [
                'money' => Db::raw('money'.$money)
            ];
        }
        $re = Db::name('Member')->where('id','=',$id)->update($data);
        if($re){
        	myCache::doMemberMoney($id,$money,'inc');
        	if($money > 0){
        		self::addMemberLog($id,'平台赠送','平台补偿书币',$money);
        	}else{
        		self::addMemberLog($id,'平台扣减','平台扣减书币',$money);
        	}
            return true;
        }else{
            return false;
        }
    }
    
    //获取事件规则
    public static function getEventRules(){
    	$rules = [
    		'id' =>  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
    		'event' => ["require|in:charge,vipon,vipoff,statuson,statusoff",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']],
    		'money' =>  ["requireIf:event,charge|integer",["requireIf"=>"请输入要调整的书币数",'integer'=>'书币格式错误']],
    		'month' =>  ["requireIf:event,vipon|number|gt:0",["requireIf"=>"请输入vip充值月数",'number'=>'vip月数必须为大于0的数值',"gt"=>"vip月数必须为大于0的数值"]]
    	];
    	return $rules;
    }
    
    
    
    
    /****************************以下代码为前端所用*****************************/
    
    //自动登录
    public static function autoLogin(){
    	$username = self::getNickname();
    	$password = mt_rand(100000,999999);
    	$invite_code = session('?INDEX_INVITE_CODE') ? session('INDEX_INVITE_CODE') : '';
    	$res = self::regByUsername($username, $password, $invite_code);
    	if(!$res){
    		res_error('自动登录失败');
    	}
    }
    
    //获取用户账号
    public static function getNickname(){
    	$nickname = self::createRandomStr(6);
    	$repeat = Db::name('Member')->where('username',$nickname)->value('id');
    	if(!$repeat){
    		return $nickname;
    	}else{
    		self::getNickname();
    	}
    }
    
    //创建随机字符串
    private static function createRandomStr($len){
    	$chars = [
    			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
    			"l", "m", "n", "p", "q", "r", "s", "t", "u", "v",
    			"w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G",
    			"H", "I", "J", "K", "L", "M", "N", "P", "Q", "R",
    			"S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2",
    			"3", "4", "5", "6", "7", "8", "9"
    	];
    	$charsLen = count($chars) - 1;
    	shuffle($chars);
    	$str = '';
    	for($i=0; $i<$len; $i++){
    		$str .= $chars[mt_rand(0, $charsLen)];
    	}
    	return $str;
    }
    
    //账号注册用户
    public static function regByUsername($username,$password,$invite_code){
    	$headConfig = getMyConfig('site_headimg');
    	$headimg = $headConfig ? $headConfig['headimg'] : '/static/templet/default/headimg.jpeg';
    	$data = [
    		'username' => $username,
    		'headimgurl' => $headimg,
    		'money' => 0,
    		'total_money' => 0,
    		'password' => createPwd($password),
    		'invite_code' => myDb::createCode('Member','invite_code'),
    		'create_time' => time()
    	];
    	$config = getMyConfig('website');
    	$invite = [];
    	if($invite_code){
    		$invite_uid = myCache::getUidByCode($invite_code);
    		if($invite_uid){
    			$invite_user = myCache::getMember($invite_uid);
    			if($invite_user){
    				$data['invite_uid'] = $invite_user['id'];
    				$data['channel_id'] = $invite_user['channel_id'];
    				$data['agent_id'] = $invite_user['agent_id'];
    				if($config && $config['invite_money']){
    					$invite['money'] = $config['invite_money'];
    					$invite['uid'] = $invite_uid;
    				}
    			}
    		}
    	}else{
    		$agentInfo = self::createAgentInfo();
    		$data = array_merge($data,$agentInfo);
    	}
    	Db::startTrans();
    	$flag = false;
    	$uid = Db::name('Member')->insertGetId($data);
    	if($uid){
    		$res = Db::name('MemberCharge')->insert(['uid'=>$uid]);
    		if($res){
    			$flag = true;
    			if($invite){
    				$flag = false;
    				$invite_data = [
    					'money' => Db::raw('money+'.$invite['money']),
    					'total_money' => Db::raw('total_money+'.$invite['money']),
    				];
    				$in_re = Db::name('Member')->where('id',$invite['uid'])->update($invite_data);
    				if($in_re){
    					$flag = self::addMemberLog($invite['uid'], '平台赠送', '平台赠送-邀请用户注册', $invite['money'],4);
    				}
    			}
    		}
    	}
    	if($flag){
    		Db::commit();
    		cookie('INDEX_LOGIN_ID',$uid);
    		if($invite){
    			myCache::doMemberMoney($invite['uid'], $invite['money']);
    		}
    	}else{
    		Db::callback();
    	}
    	return $flag;
    }
    
    //手机号注册用户
    public static function regByPhone($phone){
    	$headConfig = getMyConfig('site_headimg');
    	$headimg = $headConfig ? $headConfig['headimg'] : '/static/templet/default/headimg.jpeg';
    	$data = [
    		'username' => $phone,
    		'phone' => $phone,
    		'headimgurl' => $headimg,
    		'money' => 0,
    		'is_first_bind' => 2,
    		'total_money' => 0,
    		'invite_code' => myDb::createCode('Member','invite_code'),
    		'create_time' => time()
    	];
    	$config = getMyConfig('website');
    	if($config){
    		if($config['bind_money']){
    			$data['money'] = $config['bind_money'];
    			$data['total_money'] = $config['bind_money'];
    		}
    	}
    	$invite = [];
    	if(session('?INDEX_INVITE_CODE')){
    		$invite_code = session('INDEX_INVITE_CODE');
    		$invite_uid = myCache::getUidByCode($invite_code);
    		if($invite_uid){
    			$invite_user = myCache::getMember($invite_uid);
    			if($invite_user){
    				$data['invite_uid'] = $invite_user['id'];
    				$data['channel_id'] = $invite_user['channel_id'];
    				$data['agent_id'] = $invite_user['agent_id'];
    				if($config && $config['invite_money']){
    					$invite['money'] = $config['invite_money'];
    					$invite['uid'] = $invite_uid;
    				}
    			}
    		}
    	}else{
    		$agentInfo = self::createAgentInfo();
    		$data = array_merge($data,$agentInfo);
    	}
    	Db::startTrans();
    	$flag = false;
    	$uid = Db::name('Member')->insertGetId($data);
    	if($uid){
    		$res = Db::name('MemberCharge')->insert(['uid'=>$uid]);
    		if($res){
    			$bind_res = true;
    			if($data['money']){
    				$bind_res = self::addMemberLog($uid, '平台赠送', '平台赠送-綁定手機送', $data['money'],3);
    			}
    			if($bind_res){
    				$flag = true;
    				if($invite){
    					$flag = false;
    					$invite_data = [
    						'money' => Db::raw('money+'.$invite['money']),
    						'total_money' => Db::raw('total_money+'.$invite['money']),
    					];
    					$in_re = Db::name('Member')->where('id',$invite['uid'])->update($invite_data);
    					if($in_re){
    						$flag = self::addMemberLog($invite['uid'], '平台赠送', '平台赠送-邀请用户注册', $invite['money'],5);
    					}
    				}
    			}
    		}
    	}
    	if($flag){
    		Db::commit();
    		cookie('INDEX_LOGIN_ID',$uid);
    		if($invite){
    			myCache::doMemberMoney($invite['uid'], $invite['money']);
    		}
    	}else{
    		Db::callback();
    	}
    	return $flag;
    }
    
    //增加书币充值记录
    public static function addMemberLog($uid,$title,$summary,$money,$type=1){
    	$flag = false;
    	$time = time();
    	$data = [
    		'uid' => $uid,
    		'type' => $type,
    		'title' => $title,
    		'money' => $money,
    		'summary' => $summary,
    		'create_date' => date('Y-m-d',$time),
    		'create_time' => $time
    	];
    	$re = Db::name('MemberLog')->insert($data);
    	if($re){
    		$flag = true;
    	}
    	return $flag;
    }
    
    /**
     * 创建用户渠道代理信息
     * @return 
     */
    public static function createAgentInfo(){
    	$urlInfo = myCache::getUrlInfo();
    	$data = [
    		'channel_id' => $urlInfo['channel_id'],
    		'agent_id' => $urlInfo['agent_id'],
    		'spread_id' => 0
    	];
    	if(session('?INDEX_KC_CODE')){
    		$kc_code = session('INDEX_KC_CODE');
    		$codeInfo = myCache::getKcCodeInfo($kc_code);
    		if($codeInfo){
    			$data = array_merge($data,$codeInfo['info']);
    			if($codeInfo['type'] == 1){
    				$data['spread_id'] = $codeInfo['sid'];
    			}
    		}
    	}
    	return $data;
    }
    
    //获取手机号提交信息
    public static function getPhoneData(){
    	$rules = [
    		'phone' => ['require|mobile',['require'=>'请输入手机号','mobile'=>'手机号格式不规范']],
    		'code' => ['require|number|length:6',['require'=>'请输入验证码','number'=>'验证码为6位数字','length'=>'验证码为6位数字']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //新增登录奖励
    public static function addLoginMoney($uid,$money){
    	Db::startTrans();
    	$flag = false;
    	$data = ['money'=>Db::raw('money+'.$money),'total_money'=>Db::raw('total_money+'.$money)];
    	$re = Db::name('Member')->where('id',$uid)->update($data);
    	if($re){
    		$res = mdMember::addMemberLog($uid,'平台赠送','平台赠送-每日登陸贈送', $money,2);
    		if($res){
    			$flag = true;
    		}
    	}
    	if($flag){
    		myCache::doMemberMoney($uid, $money);
    		Db::commit();
    	}else{
    		Db::rollback();
    	}
    	return $flag;
    }
    
    //新增分享奖励
    public static function addShareMoney($uid,$money){
    	$shareId = myCache::getShareId($uid);
    	if($shareId){
    		$share_num = myCache::getMemberShareNum($uid);
    		if($share_num < 3){
    			Db::startTrans();
    			$flag = false;
    			$data = ['money'=>Db::raw('money+'.$money),'total_money'=>Db::raw('total_money+'.$money)];
    			$re = Db::name('Member')->where('id',$uid)->update($data);
    			if($re){
    				$res = mdMember::addMemberLog($uid,'平台赠送','平台赠送-每日分享链接', $money,4);
    				if($res){
    					$inc_res = myDb::setIncById('MemberShare', $shareId, 'click_num');
    					if($inc_res){
    						$flag = true;
    					}
    				}
    			}
    			if($flag){
    				myCache::doMemberMoney($uid, $money);
    				myCache::incMemberShareNum($uid);
    				Db::commit();
    			}else{
    				Db::rollback();
    			}
    		}else{
    			$inc_res = myDb::setIncById('MemberShare', $shareId, 'click_num');
    			if($inc_res){
    				myCache::incMemberShareNum($uid);
    			}
    		}
    		
    	}
    }
    
    //绑定手机号
    public static function doBindPhone($phone){
    	global $loginId;
    	$user = myCache::getMember($loginId);
    	if($user['phone']){
    		res_api('该用户已绑定手机号');
    	}
    	Db::startTrans();
    	$flag = false;
    	$bind_money = 0;
    	$data = [
    		'phone' => $phone,
    		'is_first_bind' => 2
    	];
    	if($user['is_first_bind'] == 1){
    		$config = getMyConfig('website');
    		if($config && $config['bind_money']){
    			$bind_money = $config['bind_money'];
    			$data['money'] = Db::raw('money+'.$bind_money);
    			$data['total_money'] = Db::raw('total_money+'.$bind_money);
    		}
    	}
    	$re = Db::name('Member')->where('id',$loginId)->update($data);
    	if($re){
    		$flag = true;
    		if($bind_money){
    			$flag = false;
    			$log_res = self::addMemberLog($user['id'], '平台赠送', '平台赠送-綁定手機送',$bind_money,3); 
    			if($log_res){
    				$flag = true;
    			}
    		}
    	}
    	if($flag){
    		Db::commit();
    		if($bind_money){
    			myCache::updateMember($user['id'],['phone'=>$phone,'is_first_bind'=>2]);
    			myCache::doMemberMoney($user['id'], $bind_money);
    		}
    	}else{
    		Db::rollback();
    	}
    	return $flag;
    }
    
    //解绑手机号
    public static function doUnBindPhone(){
    	$flag = false;
    	global $loginId;
    	$user = myCache::getMember($loginId);
    	$re = myDb::setField('Member', [['id','=',$user['id']]], 'phone', '');
    	if($re){
    		myCache::rmUidByPhone($user['phone']);
    		myCache::updateMember($user['id'], 'phone', '');
    		$flag = true;
    	}
    	return $flag;
    }
    
    
    //获取我的收藏列表
    public static function getMyCollect($uid){
    	$num = 0;
    	$list = [];
    	if($uid){
    		$bookIds = myCache::getCollectBookIds($uid);
    		if($bookIds){
    			foreach ($bookIds as $v){
    				$book = myCache::getBook($v);
    				if(!$book || !in_array($book['status'], [1,2])){
    					continue;
    				}
    				$num++;
    				$one = ['name' => $book['name']];
    				$one['cover'] = $book['cover'] ? $book['cover'] : '/static/templet/default/cover.png';
                    $one['id'] = $v;
                    $one['type'] = $book['type'];
    				$one['url'] = my_url('book/info',['book_id'=>$v]);
    				$list[] = $one;
    			}
    		}
    	}
    	return ['type'=>1,'list'=>$list,'uid'=>$uid,'num'=>$num,'update_num'=>0];
    }
    
    
    
    //获取我的阅读记录
    public static function getMyReadHistory($uid){
    	$num = 0;
    	$list = [];
    	if($uid){
    		$cache = myCache::getReadCache($uid);
    		if($cache){
    			foreach ($cache as $v){
    				$book = myCache::getBook($v['book_id']);
    				if(!$book || !in_array($book['status'], [1,2])){
    					continue;
    				}
    				$num++;
    				$one = [
    					'id' => $v['book_id'],
    					'name' => $book['name'],
    					'cover' => $book['cover'] ? $book['cover'] : '',
    					'url' => my_url('book/read',['book_id'=>$v['book_id'],'number'=>$v['last_number']]),
    					'create_time' => $v['last_time']
    				];
    				$list[] = $one;
    			}
    			$create_times = array_column($list,'create_time');
    			array_multisort($create_times,SORT_DESC,$list);
    		}
    	}
    	return ['type'=>2,'list'=>$list,'uid'=>$uid,'num'=>$num,'update_num'=>0];
    }
    
    //获取用户日志
    public static function getMemberLog($uid,$page){
    	$list = Db::name('MemberLog')->where('uid',$uid)->order('id','desc')->page($page,20)->select();
    	if($list){
    		foreach ($list as &$v){
    			$week = '未知';
    			switch (date('N',$v['create_time'])){
    				case 1:$week='周一';break;
    				case 2:$week='周二';break;
    				case 3:$week='周三';break;
    				case 4:$week='周四';break;
    				case 5:$week='周五';break;
    				case 6:$week='周六';break;
    				case 7:$week='周日';break;
    			}
    			$v['week'] = $week;
    			$v['date'] = date('m-d',$v['create_time']);
    			$v['month'] = date('Y-m',$v['create_time']);
    			$v['money'] = $v['money'] > 0 ? '+'.$v['money'] : $v['money'];
    			unset($v['create_time']);
    		}
    	}else{
    		$list = 0;
    	}
    	return $list;
    }
    
    //获取我的消息
    public static function getMyMsg($page){
    	$list = Db::name('Notice')->order('id','desc')->page($page,20)->select();
    	if($list){
    		foreach ($list as &$v){
    			$v['content'] = str_replace("\n", '<br />', $v['content']);
    			$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
    		}
    	}
    	return $list ? : 0;
    }
    
}