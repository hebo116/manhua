<?php
namespace app\common\model;

use site\myValidate;
use think\Db;

class mdConfig{
    
    //保存配置
    public static function saveConfig($key,$value){
        $flag = false;
        $json = json_encode($value,JSON_UNESCAPED_UNICODE);
        $data = [
        	'value' => $json,
        	'status' => 1
        ];
        $re = Db::name('Config')->where('key','=',$key)->update($data);
        if($re !== false){
        	cache($key,$value);
            $flag = true;
        }
        return $flag;
    }
    
    //新增配置
    public static function addConfig($key,$data){
        $flag = false;
        $json = json_encode($data,JSON_UNESCAPED_UNICODE);
        $save = [
            'key' => $key,
            'value' => $json,
        	'status' => 2
        ];
        $re = Db::name('Config')->insert($save);
        if($re){
            $flag = true;
        }
        return $flag;
    }
    
    //获取配置
    public static function getConfig($key){
        $config = Db::name('Config')->where('key','=',$key)->field('key,value')->find();
        if($config){
            $cur = json_decode($config['value'],true);
        }else{
            $cur = false;
        }
        return $cur;
    }
    
    //获取导航图片参数
    public static function getBannerData(){
    	$rules = [
    		'link' => ["array",['array'=>'链接不规范']],
    		'src' => ["array",['array'=>'图片格式格式不规范']],
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	if($data['src']){
    		$num = 0;
    		foreach ($data['src'] as $k=>$v){
    			$num++;
    			$one = ['src'=>$v];
    			if(!$one['src']){
    				res_api('第'.$num.'张轮播图片未上传');
    			}
    			$one['link'] = $data['link'][$k];
    			$config[] = $one;
    		}
    	}
    	return $config;
    }
    
    //获取分类参数
    public static function getCategoryData(){
    	$rules = ['category' => ["require|array",['require'=>'请输入小说类型','array'=>'类型参数格式不规范']]];
    	$data = myValidate::getData($rules);
    	$data = $data ? : [];
    	return $data;
    }
    
    //获取发布区域参数
    public static function getAreaData(){
    	$rules = ['area' => ["require|array",['require'=>'请输入小说发布区域','array'=>'发布区域参数格式不规范']]];
    	$data = myValidate::getData($rules);
    	$data = $data ? : [];
    	return $data;
    }
    
    //获取导航菜单
    public static function getNavData(){
    	$rules = [
    		'src' => ["array",['array'=>'图片格式格式不规范']],
    		'text' => ["array",['array'=>'标题格式不规范']],
    		'link' => ["array",['array'=>'链接不规范']],
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	if($data['src']){
    		$num = 0;
    		foreach ($data['src'] as $k=>$v){
    			$num++;
    			$one = ['src'=>$v];
    			if(!$one['src']){
    				res_api('第'.$num.'张图标未上传');
    			}
    			$one['link'] = $data['link'][$k];
    			$one['text'] = $data['text'][$k];
    			$config[] = $one;
    		}
    		if($num > 5){
    			res_api('最多可配置5个菜单导航图标');
    		}
    	}
    	return $config;
    }
    
    //获取视频导航参数
    public static function getVideoNavData(){
    	$rules = [
    		'text' => ["array",['array'=>'标题格式不规范']],
    		'link' => ["array",['array'=>'链接不规范']]
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	if($data['text']){
    		$num = 0;
    		foreach ($data['text'] as $k=>$v){
    			$num++;
    			$one = ['text'=>$v];
    			if(!$one['text']){
    				res_api('第'.$num.'条标题未填写');
    			}
    			$one['link'] = $data['link'][$k];
    			$config[] = $one;
    		}
    		if($num > 5){
    			res_api('最多上传5个菜单导航');
    		}
    	}
    	return $config;
    }
    
    //获取打赏提交参数
    public static function getRewardData(){
    	$rules = [
    		'name' => ['array',['array'=>'礼物名称格式不规范']],
    		'src' => ['array',['array'=>'礼物图标格式不规范']],
    		'money' => ['array',['array'=>'礼物价格格式不规范']],
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	if($data['src']){
    		$num = 0;
    		foreach ($data['src'] as $k=>$v){
    			$num++;
    			$one = ['src'=>$v];
    			if(!$one['src']){
    				res_api('第'.$num.'张图标未上传');
    			}
    			if(!$data['name'][$k]){
    				res_api('未填写第'.$num.'个礼物名称');
    			}
    			$one['name'] = $data['name'][$k];
    			if(!$data['money'][$k]){
    				res_api('未填写第'.$num.'个礼物价格');
    			}
    			if(!is_numeric($data['money'][$k]) || $data['money'][$k] < 0){
    				res_api('第'.$num.'项礼物价格格式不规范');
    			}
    			$one['money'] = $data['money'][$k];
    			$config[] = $one;
    		}
    	}
    	return $config;
    }
    
    //获取站点设置选项
    public static function getWebsiteOptions(){
    	$variable = [
    		'pay_type' => [
    			'name' => 'pay_type',
    			'option' => [
    					['val'=>1,'text'=>'微信H5支付','default'=>1],
    					['val'=>2,'text'=>'三方微信H5支付','default'=>0],
                        ['val'=>3,'text'=>'三方支付宝H5支付','default'=>0],
                        ['val'=>4,'text'=>'旭本微信支付','default'=>0],
                        ['val'=>5,'text'=>'旭本支付宝支付','default'=>0],
                        ['val'=>6,'text'=>'78微信H5支付','default'=>0],
                        ['val'=>7,'text'=>'78支付宝H5支付','default'=>0],
                        ['val'=>8,'text'=>'513支付','default'=>0],
                        ['val'=>9,'text'=>'蚂蚁微信原生','default'=>0],
                        ['val'=>10,'text'=>'蚂蚁支付宝H5','default'=>0],
                        ['val'=>11,'text'=>'蚂蚁支付宝超级H5','default'=>0],
                ]
            ]
    	];
    	return $variable;
    }
    
    //获取站点配置提交数据
    public static function getWebsiteData(){
    	$rules = [
    		'logo' =>  ["url",["url"=>"漫画logo地址不规范"]],
    		'novel_logo' =>  ["url",["url"=>"小说logo地址不规范"]],
    		'name' =>  ["require|max:20",["require"=>"请输入站点名称",'max'=>'站点名称最多支持20个字符']],
    		'url' =>  ["require|max:50",["require"=>"请输入安全域名",'url'=>'安全域名长度超出限制']],
    		'pay_type' => ["require|between:1,11",["require"=>"请选择支付方式","between"=>"未指定该支付方式"]],
    		'bind_money' => ["number",["number"=>"书币格式不规范"]],
    		'login_money' => ["number",["number"=>"书币格式不规范"]],
    		'invite_money' => ["number",["number"=>"书币格式不规范"]],
    	];
    	$data = myValidate::getData($rules);
    	mdAgent::checkUrlRepeat($data['url'],0,true);
    	return $data;
    }

    //小说首页logo配置
    public static function getNovelLogo()
    {
        $rules = [
            'logo' =>  ["url",["url"=>"logo地址不规范"]],
        ];
        $data = myValidate::getData($rules);
        return ['logo'=>$data];
    }
    
    //获取联系方式配置
    public static function getContactData(){
    	$rules = [
    		'email' => ['email',['email'=>'请输入正确的邮箱地址']],
    		'weibo' => ['max:30',['max'=>'微博账号长度超出限制']],
    		'qq' => ['max:20',['max'=>'qq号长度超出限制']],
    		'weixin' => ['max:40',['max'=>'微信号长度超出限制']],
    		'line' => ['max:20',['max'=>'line号长度超出限制']],
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取微信支付配置数据
    public static function getWxPayData(){
    	$rules = [
    		'APPID' => ['require',['require'=>'请输入微信支付APPID']],
    		'MCHID' => ['require',['require'=>'请输入微信支付商户ID']],
    		'APIKEY' => ['require',['require'=>'请输入微信支付KEY']],
    		'name' => ['max:5',['max'=>'显示名称最多支持5个字符']],
    		'summary' => ['max:15',['max'=>'显示描述最多支持15个字符']],
    		'is_on' => ['require|in:1,2',['require'=>'请选择是否开启该支付方式','in'=>'未指定该配置']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }

    //获取三方支付数据
    public static function getOtherPayData(){
        $rules = [
            'mchid' => ['require',['require'=>'请输入商户号']],
            'api_key' => ['require',['require'=>'请输入支付key']],
            'name' => ['max:5',['max'=>'显示名称最多支持5个字符']],
            'summary' => ['max:15',['max'=>'显示描述最多支持15个字符']],
            'is_on' => ['require|in:1,2',['require'=>'请选择是否开启该支付方式','in'=>'未指定该配置']]
        ];
        $data = myValidate::getData($rules);
        return $data;
    }


    //获取三方蚂蚁原生支付数据
    public static function getOtherAntPayData(){
        $rules = [
            'mchid' => ['require',['require'=>'请输入商户ID']],
            'appid' => ['require',['require'=>'请输入应用ID']],
            'productid'=>['require',['require'=>'请输入支付产品ID']],
            'api_key' => ['require',['require'=>'请输入支付key']],
            'name' => ['max:5',['max'=>'显示名称最多支持5个字符']],
            'summary' => ['max:15',['max'=>'显示描述最多支持15个字符']],
            'is_on' => ['require|in:1,2',['require'=>'请选择是否开启该支付方式','in'=>'未指定该配置']]
        ];
        $data = myValidate::getData($rules);
        return $data;
    }

    //获取三方支付数据
    public static function getOtherFxPayData(){
        $rules = [
            'mchid' => ['require',['require'=>'请输入商户号']],
            'api_key' => ['require',['require'=>'请输入支付key']],
            'code' => ['require',['require'=>'请输入支付通道编码']],
            'name' => ['max:5',['max'=>'显示名称最多支持5个字符']],
            'summary' => ['max:15',['max'=>'显示描述最多支持15个字符']],
            'is_on' => ['require|in:1,2',['require'=>'请选择是否开启该支付方式','in'=>'未指定该配置']]
        ];
        $data = myValidate::getData($rules);
        return $data;
    }

    //获取三方支付数据
    public static function getMyPayData(){
        $rules = [
            'mchid' => ['require',['require'=>'请输入商户号']],
            'HASH_KEY' => ['require',['require'=>'请输入HASH_KEY']],
            'TOKEN' => ['require',['require'=>'请输入TOKEN']],
            'ENC_KEY' => ['require',['require'=>'请输入ENC_KEY']],
            'ENC_IV' => ['require',['require'=>'请输入ENC_IV']],
            'name' => ['max:5',['max'=>'显示名称最多支持5个字符']],
            'summary' => ['max:15',['max'=>'显示描述最多支持15个字符']],
            'is_on' => ['require|in:1,2',['require'=>'请选择是否开启该支付方式','in'=>'未指定该配置']]
        ];
        $data = myValidate::getData($rules);
        return $data;
    }
    
    //获取用户头像配置
    public static function getHeadimgData(){
    	$rules = ['headimg' => ['require|url',['require'=>'请上传头像','url'=>'头像地址格式不规范']]];
    	$headimg = myValidate::getData($rules);
    	$data = ['headimg'=>$headimg];
    	return $data;
    }
    
    //获取赛邮短信配置数据
    public static function getSaiyouData(){
    	$rules = [
    		'appid' => ['require',['require'=>'请输入短信appid']],
    		'appkey' => ['require',['require'=>'请输入短信appkey']],
    		'sign' => ['require',['require'=>'请输入短信签名']],
    		'content' => ['require',['require'=>'请输入内容']]
    	];
    	$data = myValidate::getData($rules);
    	if(strpos($data['content'],'CODE') === false){
    		res_api('短信内容CODE变量不存在');
    	}
    	return $data;
    }
    
    //获取随意云配置数据
    public static function getSuiyiyunData(){
    	$rules = ['url' => ['require|url',['require'=>'请输入随意云转换地址','url'=>'随意云地址无效']]];
    	$url = myValidate::getData($rules);
    	return ['url'=>$url];
    }
    
    //获取充值配置数据
    public static function getChargeData(){
    	$rules = [
    		'money' => ['require|array',['require'=>'充值金额列数据异常','array'=>'充值金额列数据异常']],
    		'reward' => ['require|array',['require'=>'赠送金额列数据异常','array'=>'赠送金额列数据异常']],
    		'coin' => ['require|array',['require'=>'充值书币列数据异常','array'=>'充值书币列数据异常']],
    		'is_hot' => ['require|array',['require'=>'是否热门列数据异常','array'=>'是否热门列数据异常']],
    		'package' => ['require|array',['require'=>'套餐列数据异常','array'=>'套餐列数据异常']],
    		'is_on' => ['require|array',['require'=>'是否开启列数据异常','array'=>'是否开启列数据异常']],
    		'is_checked' => ['require|array',['require'=>'是否选中列数据异常','array'=>'是否选中列数据异常']]
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	$is_check = 0;
    	foreach ($data['money'] as $k=>$v){
    		$type = 0;
    		$one = ['money'=>$v];
    		$one['package'] = $data['package'][$k];
    		if(!in_array($one['package'],[0,1,2,3,4,5,6])){
    			res_api('套餐数据选择错误');
    		}
    		$one['reward'] = $data['reward'][$k];
    		if($one['reward'] && (!is_numeric($one['reward']) || $one['reward'] < 0)){
    			res_api('赠送书币必须大于等于0的数字');
    		}
    		$one['is_hot'] = $data['is_hot'][$k];
    		if(!in_array($one['is_hot'], [0,1,2])){
    			res_api('是否热门参数错误');
    		}
    		if($one['package'] > 0){
    			$one['coin'] = 0;
    		}else{
    			$one['coin'] = floor($data['coin'][$k]);
    			if(!is_numeric($one['coin']) || $one['coin'] < 0){
    				res_api('充值书币必须为数值且必须大于0');
    			}
    		}
    		$one['is_on'] = $data['is_on'][$k];
    		if(!in_array($one['is_on'], [0,1,2])){
    			res_api('是否启用参数错误');
    		}
    		$one['is_checked'] = $data['is_checked'][$k];
    		if($one['is_checked'] == 1){
    			if($is_check == 0){
    				$is_check = 1;
    			}else{
    				res_api('是否选中只能选择一行');
    			}
    		}
    		$config[] = $one;
    	}
    	return $config;
    }
}