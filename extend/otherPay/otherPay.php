<?php
namespace otherPay;

class otherPay{
	
	public static $config;
	
	//获取支付url
	public static function getPayForm($param,$pay_key){
		$url = '';
		switch ($pay_key){
			case 'wxh5':
				$url = 'http://apt.3un.cn/intf/wapwpay.html';
				break;
			case 'alipayh5':
				$url = 'http://apt.3un.cn/intf/wapali.html';
				break;
		}
		$data = [
			'customerid' => self::$config['mchid'],	
			'cardno' => 32
		];
		$data = array_merge($data,$param);
		$data['sign'] = self::getPaySign($data);
		$form = self::createFormStr($data, $url);
		return $form;
	}
	
	private static function createFormStr($data,$url){
		$html = "<form id='requestForm' action='".$url."' method='post'>";
		foreach ($data as $key=>$val){
			$html.= "<input type='hidden' name='".$key."' value='".$val."' />";
		}
		$html .= "<input type='submit' value='确定' style='display:none;'></form>";
		$html .= "<script>document.forms['requestForm'].submit();</script>";
		return $html;
	}
	
	//创建支付签名
	private static function getPaySign($data){
		$signstr="customerid=".$data['customerid']."&sdcustomno=".$data['sdcustomno']."&orderAmount=".$data['orderAmount']."&cardno=".$data['cardno']."&noticeurl=".$data['noticeurl']."&backurl=".$data['backurl'].self::$config['api_key'];
		$sign=strtoupper(md5($signstr,false));
		return $sign;
	}
	
	//检查异步通知签名
	public static function checkSign($data){
		$res = false;
		if($data['state'] == 1){
			$sign = trim($data['sign']);
			$resign = trim($data['resign']);
			$signstr='customerid='.$data['customerid'].'&sd51no='.$data['sd51no'].'&sdcustomno='.$data['sdcustomno'].'&mark='.$data['mark'].'&key='.self::$config['api_key'].'';
			$signtmp=strtoupper(md5($signstr,false));
			$signstr2='sign='.$signtmp.'&customerid='.$data['customerid'].'&ordermoney='.$data['ordermoney'].'&sd51no='.$data['sd51no'].'&state='.$data['state'].'&key='.self::$config['api_key'].'';
			$signtmp2=strtoupper(md5($signstr2,false));
			if($sign==$signtmp && $resign==$signtmp2){
				$res = true;
			}
		}
		return $res;
	}
	
	
}