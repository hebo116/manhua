<?php
namespace xubenPay;

class xubenPay{
	
	public static $config;
	
	//获取支付url
	public static function getPayForm($param,$pay_key='defaults'){
		$url = '';
		switch ($pay_key){
            case 'defaults':
                $url = 'http://www.xuben666.com/Pay_Index.html';
                break;
		}
		$data = [
			//'customerid' => self::$config['mchid'],
            "pay_memberid" => self::$config['mchid'],
		];
		$data = array_merge($data,$param);
		$data['pay_md5sign'] = self::getPaySign($data);
        $data['pay_productname'] = '团购商品';
		$form = self::createFormStr($data, $url);
		return $form;
	}
	
	private static function createFormStr($data,$url){
		$html = "<form id='requestForm' action='".$url."' method='post'>";
		foreach ($data as $key=>$val){
			$html.= "<input type='hidden' name='".$key."' value='".$val."' />";
		}
		$html .= "<input type='submit' value='确定' style='display:none;'></form>";
		$html .= "<script>document.forms['requestForm'].submit();</script>";
		return $html;
	}
	
	//创建支付签名
	private static function getPaySign($native){
        ksort($native);
        $md5str = "";
        foreach ($native as $key => $val) {
            $md5str = $md5str . $key . "=" . $val . "&";
        }
        $sign = strtoupper(md5($md5str . "key=" . self::$config['api_key']));
        return $sign;
	}
	
	//检查异步通知签名
	public static function checkSign($data,$api_key=''){
		$res = false;
		if($data['returncode'] == "00"){
			$sign = trim($data['sign']);
            $returnArray = array( // 返回字段
                "memberid" => $data["memberid"], // 商户ID
                "orderid" =>  $data["orderid"], // 订单号
                "amount" =>  $data["amount"], // 交易金额
                "datetime" =>  $data["datetime"], // 交易时间
                "transaction_id" =>  $data["transaction_id"], // 支付流水号
                "returncode" => $data["returncode"],
            );
            $md5key = $api_key;
            ksort($returnArray);
            reset($returnArray);
            $md5str = "";
            foreach ($returnArray as $key => $val) {
                $md5str = $md5str . $key . "=" . $val . "&";
            }
            $resSign = strtoupper(md5($md5str . "key=" . $md5key));
			if($sign == $resSign){
				$res = true;
			}
		}
		return $res;
	}
	
	
}