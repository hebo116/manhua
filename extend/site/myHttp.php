<?php
namespace site;

use think\facade\Request;

class myHttp{
	
    //get获取数据
    public static function doGet($url,$header=null,$back = 'json'){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        /**/
        curl_setopt($ch, CURLOPT_TIMEOUT, 1800);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if($header){
            curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        }
        $output = curl_exec($ch);
        $error = '';
        if (curl_errno($ch)) {
            $error = curl_error($ch);
        }
        curl_close($ch);
        $result = '';
        if($error){
            res_api($error);
        }else{
            switch ($back){
                case 'json':
                    $result = json_decode($output,true);
                    break;
                case 'xml':
                    $xmlObj = simplexml_load_string($output,'SimpleXMLElement',LIBXML_NOCDATA);
                    $xmlStr = json_encode($xmlObj);
                    $result = json_decode($xmlStr,true);
                    break;
                case 'string':
                    $result = $output;
                    break;
            }
            return $result;
        }
        
    }

    //get获取HTTPS数据
    public static function doGetHTTPS($url,$header=null,$back = 'json'){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        /**/
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,true); ;
        curl_setopt($ch,CURLOPT_CAINFO,dirname(__FILE__).'/cacert.pem');
        /**/
        curl_setopt($ch, CURLOPT_TIMEOUT, 36000);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if($header){
            curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        }
        $output = curl_exec($ch);
        $error = '';
        if (curl_errno($ch)) {
            $error = curl_error($ch);
        }
        curl_close($ch);
        $result = '';
        if($error){
            res_api($error);
        }else{
            switch ($back){
                case 'json':
                    $result = json_decode($output,true);
                    break;
                case 'xml':
                    $xmlObj = simplexml_load_string($output,'SimpleXMLElement',LIBXML_NOCDATA);
                    $xmlStr = json_encode($xmlObj);
                    $result = json_decode($xmlStr,true);
                    break;
                case 'string':
                    $result = $output;
                    break;
            }
            return $result;
        }

    }
    
    //post获取数据
    public static function doPost($url,$data=[],$header=null,$back='json'){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        if($header){
            curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        }
        $output = curl_exec($ch);
        $error = '';
        if (curl_errno($ch)){
            $error = curl_error($ch);
        }
        curl_close($ch);
        if($error){
            res_api($error);
        }else{
            switch ($back){
                case 'json':
                    $result = json_decode($output,true);
                    break;
                case 'xml':
                    $xmlObj = simplexml_load_string($output,'SimpleXMLElement',LIBXML_NOCDATA);
                    $xmlStr = json_encode($xmlObj);
                    $result = json_decode($xmlStr,true);
                    break;
                case 'string':
                    $result = $output;
                    break;
            }
            return $result;
        }
    }
    
    /**
     * 接收数据
     * @param mixed $field 获取数据
     * @param string $method get/post 请求方式
     * @return array|string[]|mixed[]
     */
    public static function getData($field='',$method='post'){
    	$res = [];
    	switch ($method){
    		case 'post':
    			$data = Request::post();
    			break;
    		case 'get':
    			$data = Request::get();
    			break;
    		default:
    			$data = [];
    			break;
    	}
    	if($field){
    		$fields = is_array($field) ? $field : explode(',', $field);
    		foreach ($fields as $v){
    			if(isset($data[$v])){
    				$res[$v] = $data[$v];
    			}else{
   					$res[$v] = '';
   				}
   			}
    	}else{
    		if($data && is_array($data)){
    			$res = $data;
    		}
    	}
    	return $res;
    }
    
    //检测是否是国内ip
    public static function checkIpIsInChina(){
    	$result = false;
    	$ip = request()->ip();
    	$ip = '115.191.200.34';
    	$url = 'http://ip-api.com/json/'.$ip.'?lang=zh-CN';
    	$res = self::doGet($url);
    	if($res && isset($res['countryCode']) && $res['countryCode'] === 'CN'){
    		$result = true;
    	}
    	return $result;
    }
    
    /**
     * GET获取ID
     * @param string $title 字段释义
     * @param string $field 字段名称
     * @return mixed|array
     */
    public static function getId($title='',$field='id'){
    	$rules = [$field=>['require|number|gt:0',['require'=>'参数错误','number'=>'参数格式不规范','gt'=>'参数格式不规范']]];
    	$id = myValidate::getData($rules,'get');
    	return $id;
    }
    
    /**
     * POST获取ID
     * @param string $title 字段释义
     * @param string $field 字段名称
     * @return mixed|array
     */
    public static function postId($title='',$field='id'){
    	$rules = [$field=>['require|number|gt:0',['require'=>'参数错误','number'=>'参数格式不规范','gt'=>'参数格式不规范']]];
    	$id = myValidate::getData($rules,'post');
    	return $id;
    }
    
    /**
     * 获取分页参数
     * @return mixed[]
     */
    public static function getPageParams(){
    	$field = 'page,limit';
    	$data = self::getData($field,'get');
    	if(!$data['page']){
    		$data['page'] = 1;
    	}
    	if(!$data['limit']){
    		$data['limit'] = 10;
    	}
    	return $data;
    }
}