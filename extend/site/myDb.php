<?php
namespace site;

use think\Db;

class myDb{
    /**
     * 查询列表
     * @param string $table 数据表
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return []
     */
    public static function getList($table,$where,$field='*',$order=['id'=>'desc']){
        $obj = Db::name($table)->where($where)->field($field);
        if($order){
        	$obj->order($order);
        }
        $list = $obj->select();
        return $list;
    }
    
    /**
     * 查询分页列表
     * @param string $table 数据表
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return []
     */
    public static function getPageList($table,$where,$field,$pages,$order=['id'=>'desc']){
    	$obj = Db::name($table)->where($where)->field($field)->page($pages['page'],$pages['limit']);
    	if($order){
    		$obj->order($order);
    	}
        $list = $obj->select();
        $count = 0;
        if($list){
            $count = Db::name($table)->where($where)->count();
        }
        return ['data'=>$list,'count'=>$count];
    }
    
    /**
     * 查询分页列表
     * @param string $table 数据表
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return []
     */
    public static function getOnlyPageList($table,$where,$field,$pages,$order=['id'=>'desc']){
        $list = Db::name($table)->where($where)->field($field)->page($pages['page'],$pages['limit'])->order($order)->select();
        return $list;
    }
    
    /**
     * 查询指定行数数据
     * @param string $table 数据表
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @param number $limit 行数
     * @return []
     */
    public static function getLimitList($table,$where,$field='*',$limit=1){
    	$list = Db::name($table)->where($where)->field($field)->order('id','desc')->limit($limit)->select();
    	return $list;
    }
    
    /**
     * 获取指定的一条记录
     * @param string $table 查询数据表
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return array|NULL
     */
    public static function getCur($table,$where,$field='*'){
        $cur = Db::name($table)->where($where)->field($field)->find();
        return $cur;
    }
    
    /**
     * 查询指定字段值
     * @param string $table 表名
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return mixed 字段值
     */
    public static function getValue($table,$where,$field){
    	$value = Db::name($table)->where($where)->value($field);
    	return $value;
    }
    
    /**
     * 查询指定列之和
     * @param string $table 表名
     * @param array $where 查询条件
     * @param string $field 查询字段
     * @return mixed 字段值
     */
    public static function getSumValue($table,$where,$field){
    	$value = Db::name($table)->where($where)->sum($field);
    	return $value;
    }
    
    /**
     * 获取指定的一条记录
     * @param string $table 查询数据表
     * @param array $id 主键
     * @param string $field 查询字段
     * @return array|NULL
     */
    public static function getById($table,$id,$field='*'){
        $cur = Db::name($table)->where('id','=',$id)->field($field)->find();
        return $cur;
    }
    
    /**
     * 修改字段值
     * @param string $table 数据表
     * @param array $where 条件
     * @param string $field 更改字段
     * @param string $value 值
     * @return boolean
     */
    public static function setField($table,$where,$field,$value){
        $flag = false;
        $re = Db::name($table)->where($where)->setField($field,$value);
        if($re !== false){
            $flag = true;
        }
        return $flag;
    }
    
    /**
     * 通过ID自增指定值
     * @param unknown $table 数据表
     * @param unknown $id ID值
     * @param unknown $field 字段名
     * @param number $step 步长
     * @return boolean
     */
    public static function setIncById($table,$id,$field,$step=1){
    	$flag = false;
    	$re = Db::name($table)->where('id',$id)->setInc($field,$step);
    	if($re !== false){
    		$flag = true;
    	}
    	return $flag;
    }
    
    /**
     * 修改数据
     * @param string $table 数据表
     * @param array $where 条件
     * @param array $data 保存数据
     * @return boolean
     */
    public static function save($table,$where,$data){
        $flag = false;
        $re = Db::name($table)->where($where)->update($data);
        if($re !== false){
            $flag = true;
        }
        return $flag;
    }
    
    /**
     * 修改数据
     * @param string $table 数据表
     * @param array $data 保存数据
     * @return boolean
     */
    public static function saveIdData($table,$data){
        $id = $data['id'];
        unset($data['id']);
        $flag = false;
        $re = Db::name($table)->where('id','=',$id)->update($data);
        if($re !== false){
            $flag = true;
        }
        return $flag;
    }
    
    /**
     * 添加数据
     * @param string $table 数据表
     * @param array $data 新增数据
     * @return boolean
     */
    public static function add($table,$data){
        $flag = false;
        $re = Db::name($table)->insert($data);
        if($re){
            $flag = true;
        }
        return $flag;
    }
    
    /**
     * 批量添加数据
     * @param string $table 数据表
     * @param array $data 新增数据
     * @return boolean
     */
    public static function addAll($table,$data){
        $flag = false;
        $re = Db::name($table)->insertAll($data);
        if($re){
            $flag = true;
        }
        return $flag;
    }
    
    //行数
    public static function getCount($table,$where){
        $count = Db::name($table)->where($where)->count();
        return $count;
    }
    
    /**
     * 删除数据
     * @param string $table 数据表
     * @param number $id 主键
     * @return boolean
     */
    public static function delById($table,$id){
        $flag = false;
        $re = Db::name($table)->where('id','=',$id)->delete();
        if($re){
            $flag = true;
        }
        return $flag;
    }
    
    /**
     * 删除数据
     * @param string $table 数据表
     * @param number $id 主键
     * @return boolean
     */
    public static function delByWhere($table,$where){
        $flag = false;
        $re = Db::name($table)->where($where)->delete();
        if($re !== false){
            $flag = true;
        }
        return $flag;
    }
    
    //生成推广code
    public static function createCode($table,$field='code'){
    	$res = '';
    	$str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    	$arr = str_split($str);
    	$len = count($arr)-1;
    	for($i=0;$i<6;$i++){
    		$key = mt_rand(0,$len);
    		$res .= $arr[$key];
    	}
    	$repeat = DB::name($table)->where($field,'=',$res)->value('id');
    	if($repeat){
    		self::createCode($table,$field);
    	}else{
    		return $res;
    	}
    }
    
    
    /**
     * 获取新增数据所需字段
     * @param string $field 字段列表
     * @return
     */
    public static function buildArr($field){
        $arr = is_array($field) ? $field : explode(',', $field);
        $cur = [];
        foreach ($arr as $v){
            $temp = explode(':', $v);
            if(count($temp) == 1){
                $cur[$v] = '';
            }else{
                $cur[$temp[0]] = $temp[1];
            }
        }
        return $cur;
    }
}