<?php
namespace site;

class myLinks{
    
    public static function getAll(){
        $list = [
            'cartoon' => [
                'name' => '漫画相关链接',
                'links' => [
                    ['url'=>'/','summary'=>'漫画首页'],
                    ['url'=>'/index.html#/classification','summary'=>'分类'],
                    ['url'=>'/index.html#/ranking','summary'=>'排行榜'],
                    ['url'=>'/index.html#/more?category=1&type=2','summary'=>'今日更新'],
                ]
            ],
            'novel' => [
                'name' => '小说相关链接',
                'links' => [
                    ['url'=>'/index/Novel/index.html','summary'=>'小说首页'],
                    ['url'=>'/index/Novel/category.html','summary'=>'分类'],
                    ['url'=>'/index/Book/rank.html?category=2','summary'=>'排行榜'],
                    ['url'=>'/index/Book/refresh.html?category=2','summary'=>'今日更新'],
                ]
            ],
            'other' => [
                'name' => '其他链接',
                'links' => [
                    ['url'=>'/index/User/index.html','summary'=>'个人中心'],
                    ['url'=>'/index/book/readHistory.html','summary'=>'阅读历史'],
                    ['url'=>'/index/book/collect.html','summary'=>'我的书架'],
                    ['url'=>'/index/User/log.html','summary'=>'我的账单'],
                    ['url'=>'/index/User/charge.html','summary'=>'充值记录'],
                    ['url'=>'/index/User/message.html','summary'=>'我的消息'],
                    ['url'=>'/index/Charge/index.html','summary'=>'书币充值']
                ]
            ],
        ];
        return $list;
    }
}
