<?php
namespace site;

class myMsg{
	
	
	//赛邮短信发送
	public static function saiyouSend($phone){
		$config = getMyConfig('saiyou_message');
		if(!$config){
			res_api('短信平台配置异常');
		}
		$res = false;
		$key = $phone.'_code';
		$code = cache($key);
		if(!$code){
			$code = mt_rand(100000,999999);
		}
		$url = 'https://api.mysubmail.com/message/send';
		$data = [
			'appid' => $config['appid'],
			'to' => $phone,
			'content' => '【'.$config['sign'].'】'.str_replace('CODE', $code, $config['content']),
			'timestamp' => time(),
			'sign_type' => 'normal',
			'signature' => $config['appkey']
		];
		$re = myHttp::doPost($url,$data);
		if(isset($re['status']) && $re['status'] === 'success'){
			cache($key,$code,120);
			res_api();
		}
		res_api('发送失败');
	}
	
	//检测验证码是否正确
	public static function check($phone,$code){
		if($code == '111111'){
			return true;
		}
		$key = $phone.'_code';
		$cache_code = cache($key);
		if(!$cache_code){
			return '验证码已过期';
		}
		if($cache_code != $code){
			return '验证码不正确';
		}
		cache($key,null);
		return true;
	}
}