<?php
namespace site;
use think\Validate;

class myValidate{
	
	/**
	 * 处理参数验证
	 * @param array $rules 规则
	 * @param mixed $field 接收字段
	 * @param string $type 提交方式
	 * @return mixed|array
	 */
	public static function getData($rules,$method='post'){
		$data = myHttp::getData('',$method);
		$rule = self::getValidateParam($rules);
		$result = self::doValidateData($data,$rule['rule'], $rule['msg']);
		if(count($result) === 1){
			return array_shift($result);
		}else{
			return $result;
		}
	}
	
	/**
	 * 验证现有数据
	 * @param unknown $rules
	 * @param unknown $data
	 */
	public static function checkData($rules,$data){
		$rule = self::getValidateParam($rules);
		self::doValidateData($data,$rule['rule'], $rule['msg']);
	}
	
	/**
	 * 数据验证
	 * @param array $field 需验证字段
	 * @param array $data 需验证数据
	 * @param array $rules 验证规则
	 * @param array $msgs 验证不通过提示错误信息
	 * @return array 验证通过的数据
	 */
	private static function doValidateData($data,$rules,$msgs){
		$validate = Validate::make($rules,$msgs);
		$res = $validate->check($data);
		if(!$res){
			res_error($validate->getError());
		}
		$result = [];
		foreach ($rules as $k=>$v){
			$result[$k] = isset($data[$k]) ? $data[$k] : '';
		}
		return $result;
	}
	
	/**
	 * 重组验证参数
	 * @param array 验证规则
	 * @return array 验证参数和错误信息
	 */
	private static function getValidateParam($rules){
		$rule = $msg = [];
		foreach ($rules as $k=>$v){
			$rule[$k] = $v[0];
			foreach ($v[1] as $key=>$val){
				$mkey = $k.'.'.$key;
				$msg[$mkey] = $val;
			}
		}
		$res = ['rule'=>$rule,'msg'=>$msg];
		return $res;
	}
}