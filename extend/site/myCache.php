<?php
namespace site;

use think\Db;
use think\facade\Cache;

class myCache{
	
	/**
	 * 清理所有缓存
	 */
	public static function clearAll(){
		Cache::clear();
	}
	
	/**
	 * 获取code信息
	 * @param string $code 编码
	 * @return mixed
	 */
	public static function getKcCodeInfo($code){
		$key = 'kc_code_'.$code;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getcur('Code', [['code','=',$code]]);
			if($data){
				$data['info'] = json_decode($data['info'],true);
				cache($key,$data);
			}
		}
		return $data;
	}
	
	
	/**
	 * 获取渠道代理缓存
	 * @param number $channel_id 渠道代理ID
	 * @return array 渠道代理信息
	 */
	public static function getChannel($channel_id){
		$key = 'channel_info_'.$channel_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getById('Channel', $channel_id);
			if($data){
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 更新渠道信息
	 * @param number $channel_id 渠道ID
	 * @param mixed $field 字段名，批量更新为二维数组
	 * @param mixed $value 字段值
	 */
	public static function doChannel($channel_id,$field,$value=''){
		$key = 'channel_info_'.$channel_id;
		if(cache('?'.$key)){
			$data = cache($key);
			if(!$data){
				cache($key,null);
			}else{
				if(is_array($field)){
					foreach ($field as $k=>$v){
						if(array_key_exists($k, $data)){
							$data[$k] = $v;
						}
					}
				}else{
					if(array_key_exists($field, $data)){
						$data[$field] = $value;
					}
				}
				cache($key,$data);
			}
		}
		return true;
	}
	
	/**
	 * 清除渠道代理缓存
	 * @param number $channel_id 渠道代理ID
	 */
	public static function rmChannel($channel_id){
		$key = 'channel_info_'.$channel_id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取代理code
	 * @param number $id
	 */
	public static function getAgentCode($id){
		$key = 'agent_code_'.$id;
		if(cache('?'.$key)){
			$code = cache($key);
		}else{
			$code = Db::name('Code')->where('cid',$id)->where('type',2)->value('code');
			if($code){
				cache($key,$code);
			}
		}
		return $code;
	}
	
	/**
	 * 获取收藏书籍id集
	 * @param number $uid 用户ID
	 * @return mixed
	 */
	public static function getCollectBookIds($uid){
		$key = 'member_collect_bookids_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('MemberCollect')->where('uid','=',$uid)->column('book_id');
			$data = $data ? $data : [];
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 清空用户收藏书籍ID集
	 * @param number $uid 用户ID
	 */
	public static function rmCollectBookIds($uid){
		$key = 'member_collect_bookids_'.$uid;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取用户搜索记录
	 * @param number $uid 用户ID
	 * @return array
	 */
	public static function getUserSearch($uid){
		$key = 'member_search_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = [];
		}
		return $data;
	}
	
	//热门搜索记录
	public static function getHotSearch(){
		$key = 'hot_search';
		$data = cache($key);
		if(!$data){
			$data = Db::name('Book')->where('status',1)->field('id,name')->order('hot_num','desc')->order('id','desc')->limit(6)->select();
			if($data){
				cache($key,$data,86400);
			}
		}
		return $data;
	}
	
	/**
	 * 保存搜索记录
	 * @param number $uid 用户
	 * @param number $keyword 关键字
	 */
	public static function addUserSearch($uid,$keyword){
		$key = 'member_search_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
			if(!in_array($keyword, $data)){
				$data[] = $keyword;
			}
		}else{
			$data = [$keyword];
		}
		cache($key,$data);
	}
	
	/**
	 * 删除用户搜索记录
	 * @param number $uid 用户ID
	 */
	public static function rmUserSearch($uid,$keyword=''){
		$key = 'member_search_'.$uid;
		if(cache('?'.$key)){
			if($keyword){
				$data = cache($key);
				if(in_array($keyword, $data)){
					foreach ($data as $k=>$v){
						if($v == $keyword){
							unset($data[$k]);
						}
					}
					$data = array_values($data);
					cache($key,$data);
				}
			}else{
				cache($key,null);
			}
		}
	}
	
	/**
	 * 删除用户搜索记录
	 * @param number $uid 用户ID
	 */
	public static function rmUserAllSearch($uid){
		$key = 'member_search_'.$uid;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取用户缓存
	 * @param number $uid 用户ID
	 * @return array
	 */
	public static function getMember($uid){
		$key = 'member_info_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$field = 'id,channel_id,agent_id,invite_code,phone,is_first_bind,username,bind_username,password as pwd,headimgurl,viptime,status,spread_id,is_charge,auto_login,create_time';
			$data = myDb::getById('Member',$uid,$field);
			if($data){
				if($data['status'] != 1){
					res_api('用户信息异常');
				}
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 清除用户缓存
	 * @param number $uid 用户ID
	 */
	public static function rmMember($uid){
		$key = 'member_info_'.$uid;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取用户主题
	public static function getMemberTheme($uid){
		$key = 'member_theme_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('MemberTheme')->where('uid',$uid)->value('color');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//设置用户主题
	public static function setMemberTheme($uid,$color){
		$key = 'member_theme_'.$uid;
		cache($key,$color);
	}
	
	/**
	 * 获取用户账户余额
	 * @param unknown $uid
	 * @return number
	 */
	public static function getMemberMoney($uid){
		$key = 'member_money_'.$uid;
		if(cache('?'.$key)){
			$money = cache($key);
		}else{
			$user = Db::name('Member')->where('id',$uid)->field('id,money')->find();
			if($user){
				$money = $user['money'];
				cache($key,$money);
			}else{
				$money = 0;
			}
		}
		return $money;
	}
	
	/**
	 * 增减用户账户余额
	 * @param number $uid 用户ID
	 * @param number $value 数量
	 * @param string $opt inc增加dec减少
	 */
	public static function doMemberMoney($uid,$value,$opt='inc'){
		$key = 'member_money_'.$uid;
		if(cache('?'.$key)){
			switch ($opt){
				case 'inc':
					Cache::inc($key,$value);
					break;
				case 'dec':
					Cache::dec($key,$value);
					break;
			}
		}
	}
	
	/**
	 * 根据邀请码获取用户ID
	 * @param string $openid 微信openID
	 * @return number
	 */
	public static function getUidByCode($code){
		$uid = 0;
		if($code){
			$key = $code.'_userid';
			if(cache('?'.$key)){
				$uid = cache($key);
			}else{
				$uid = myDb::getValue('Member', [['invite_code','=',$code]],'id');
				if($uid){
					cache($key,$uid);
				}
			}
		}
		return $uid;
	}
	
	/**
	 * 根据手机号获取用户ID
	 * @param string $openid 微信openID
	 * @return number
	 */
	public static function getUidByPhone($phone){
		$uid = 0;
		if($phone){
			$key = $phone.'_userid';
			if(cache('?'.$key)){
				$uid = cache($key);
			}else{
				$uid = myDb::getValue('Member', [['phone','=',$phone]],'id');
				if($uid){
					cache($key,$uid);
				}
			}
		}
		return $uid;
	}
	
	//删除手机号缓存ID
	public static function rmUidByPhone($phone){
		$key = $phone.'_userid';
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 修改用户信息
	 */
	public static function updateMember($uid,$field,$value=''){
		$key = 'member_info_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
			if($data){
				if(is_array($field)){
					foreach ($field as $k=>$v){
						if(array_key_exists($k, $data)){
							$data[$k] = $v;
						}
					}
				}else{
					if(array_key_exists($field, $data)){
						$data[$field] = $value;
					}
				}
			}
			cache($key,$data);
		}
	}
	
	/**
	 * 获取书籍缓存
	 * @param number $bookId 书籍ID
	 * @return array
	 */
	public static function getBook($bookId){
		$key = 'book_'.$bookId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getById('Book',$bookId);
			if($data){
				$data['cover'] = $data['cover'] ? $data['cover'] : '/static/templet/default/cover.png';
				$data['detail_img'] = $data['detail_img'] ? $data['detail_img'] : '/static/templet/default/detail_img.png';
				$data['text_tag'] = $data['text_tag'] ? json_decode($data['text_tag']) : [];
				cache($key,$data);
			}
		}
		$data["cover"]=handleImg($data['cover']);
		$data["detail_img"]=handleImg($data['detail_img']);
		return $data;
	}
	
	//更新书籍缓存
	public static function doBook($bookId,$field,$value=''){
		$key = 'book_'.$bookId;
		if(cache('?'.$key)){
			$data = cache($key);
			if(is_array($field)){
				foreach ($field as $k=>$v){
					if(array_key_exists($k, $data)){
						$data[$k] = $v;
					}
				}
			}else{
				if($data && array_key_exists($field, $data)){
					$data[$field] = $value;
				}
			}
			cache($key,$data);
		}
	}
	
	/**
	 * 删除书籍缓存
	 * @param number $bookId 书籍ID
	 */
	public static function rmBook($bookId){
		$key = 'book_'.$bookId;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取书籍章节数量
	 * @param number $book_id 书籍ID
	 * @return number|string
	 */
	public static function getBookChapterNum($book_id){
		$key = 'book_chapter_num_'.$book_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getCount('BookChapter',[['book_id','=',$book_id]]);
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 删除章节数量缓存
	 * @param number $book_id 书籍ID
	 */
	public static function rmBookChapterNum($book_id){
		$key = 'book_chapter_num_'.$book_id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取书籍热度
	 * @param number $book_id 书籍ID
	 * @return number
	 */
	public static function getBookHotNum($book_id){
		$key = 'book_hot_num_'.$book_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getValue('Book', [['id','=',$book_id]], 'hot_num');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 增加书籍热度数量
	 * @param number $book_id 书籍ID
	 */
	public static function incBookHotNum($book_id){
		$key = 'book_hot_num_'.$book_id;
		if(cache('?'.$key)){
			Cache::inc($key);
		}
	}
	
	/**
	 * 删除书籍热度缓存
	 * @param number $book_id 书籍ID
	 */
	public static function rmBookHotNum($book_id){
		$key = 'book_hot_num_'.$book_id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取书籍章节列表缓存
	 * @param number $book_id 书籍ID
	 * @return number
	 */
	public static function getBookChapterList($book_id){
		$key = 'book_chapter_list_'.$book_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('BookChapter')->where('book_id','=',$book_id)->field('id,name,number,src,money,create_time')->order('number','asc')->select();
			if($data){
				$data = array_column($data, null,'number');
				cache($key,$data);
			}
			$data = $data ? : 0;
		}
		return $data;
	}
	
	/**
	 * 删除书籍列表章节缓存
	 * @param number $book_id 书籍ID
	 */
	public static function rmBookChapterList($book_id){
		$key = 'book_chapter_list_'.$book_id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取用户阅读记录缓存
	public static function getReadCache($uid){
		$key = 'member_read_'.$uid;
		if(cache('?'.$key)){
			$list = cache($key);
		}else{
			$list = [];
			$temp = Db::name('ReadHistory')->where('uid',$uid)->where('status',1)->field('book_id,number,create_time')->order('create_time','desc')->select();
			if($temp){
				foreach ($temp as $v){
					$ck = 'book_'.$v['book_id'];
					if(isset($list[$ck])){
						$list[$ck]['numbers'][] = $v['number'];
						if($v['create_time'] > $list[$ck]['last_time']){
							$list[$ck]['last_number'] = $v['number'];
							$list[$ck]['last_time'] = $v['create_time'];
						}
					}else{
						$list[$ck] = [
							'book_id' => $v['book_id'],
							'last_number' => $v['number'],
							'last_time' => $v['create_time'],
							'numbers' => [$v['number']]
						];
					}
				}
			}
			cache($key,$list);
		}
		return $list;
	}
	
	//删除阅读缓存
	public static function rmReadCache($uid){
		$key = 'member_read_'.$uid;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	
	/**
	 * 检测用户是否阅读过该章节
	 * @param number $uid 用户ID
	 * @param number $book_id 书籍ID
	 * @param number $number 章节数量
	 * @return boolean 是否已阅读过
	 */
	public static function checkIsRead($uid,$book_id,$number){
		$list = self::getReadCache($uid);
		$res = false;
		if($list){
			$lkey = 'book_'.$book_id;
			if(isset($list[$lkey])){
				if(in_array ($number,$list[$lkey]['numbers'])){
					$res = true;
				}
			}
		}
		return $res;
	}
	
	/**
	 * 添加阅读记录
	 * @param number $uid 用户ID
	 * @param number $book_id 书籍ID
	 * @param number $number 章节数量
	 */
	public static function addRead($uid,$book_id,$number,$time){
		$key = 'member_read_'.$uid;
		if(cache('?'.$key)){
			$list = cache($key);
			$ck = 'book_'.$book_id;
			if(isset($list[$ck])){
				if(!in_array($number, $list[$ck]['numbers'])){
					$list[$ck]['last_time'] = $time;
					$list[$ck]['last_number'] = $number;
					$list[$ck]['numbers'][] = $number;
					cache($key,$list);
				}
			}else{
				$cur = [
					'book_id' => $book_id,
					'last_number' => $number,
					'last_time' => $time,
					'numbers' => [$number]
				];
				$list[$ck] = $cur;
				cache($key,$list);
			}
		}
	}
	
	/**
	 * 获取推广缓存
	 * @param number $id 推广ID
	 * @return array
	 */
	public static function getSpread($id){
		$key = 'spread_'.$id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$field = 'id,name,channel_id,book_id,chapter_number,number,is_sub';
			$data = myDb::getById('Spread', $id,$field);
			if($data){
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 删除推广缓存
	 * @param number $id 推广ID
	 */
	public static function rmSpread($id){
		$key = 'spread_'.$id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	
	/**
	 * 获取缓存的url信息
	 * @param string $server_url 当前链接
	 * @return array
	 */
	public static function getUrlInfo($server_url=''){
		$server_url = $server_url ? : $_SERVER['HTTP_HOST'];
		$key = md5($server_url.'#info');
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$site = getMyConfig('website');
			if(!$site){
				res_api('尚未配置站点信息');
			}
			if($server_url === $site['url']){
				$data = [
					'name' => $site['name'],
					'url' => $site['url'],
					'channel_id' => 0,
					'agent_id' => 0
				];
			}else{
				$field = 'id,name,type,parent_id';
				$channel = Db::name('Channel')->where('url','=',$server_url)->field($field)->find();
				if(!$channel){
					res_api('该域名尚未启用');
				}
				$data = [
					'name' => $site['name'],
					'url' => $server_url,
					'channel_id' => 0,
					'agent_id' => 0
				];
				if($channel['type'] == 1){
					$data['channel_id'] = $channel['id'];
				}else{
					$data['agent_id'] = $channel['id'];
					$data['channel_id'] = $channel['parent_id'];
				}
			}
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 删除url缓存
	 * @param string $url 链接
	 */
	public static function rmUrlInfo($url){
		$key = md5($url.'#info');
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取充值数据
	public static function getSiteCharge(){
		$key = 'site_charge';
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = [];
			$list = Db::name('Charge')->order('sort_num','desc')->select();
			if($list){
				foreach ($list as $v){
					$v['content'] = json_decode($v['content'],true);
					$data[$v['id']] = $v;
				}
				cache($key,$data);
			}
		}
		return $data;
	}
	
	//删除充值数据
	public static function rmSiteCharge(){
		$key = 'site_charge';
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取用户套餐充值次数
	public static function getChargeNum($uid,$chargeId){
		$key = 'member_charge_num_'.$uid.'_'.$chargeId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Order')->where('uid',$uid)->where('status',2)->where('charge_id',$chargeId)->count();
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//新增用户套餐充值次数
	public static function incChargeNum($uid,$chargeId){
		$key = 'member_charge_num_'.$uid.'_'.$chargeId;
		if(cache('?'.$key)){
			Cache::inc($key);
		}
	}
	
	//获取渠道可提现余额
	public static function getChannelAmount($channel_id){
		$date = date('Y-m-d');
		$key = md5('channel_amount_'.$date.'_'.$channel_id);
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$where = [['create_date','<',$date],['channel_id','=',$channel_id],['status','=',2]];
			$data = Db::name('ChannelChart')->where($where)->sum('income_money');
			$data = $data ? : 0;
			cache($key,$data,86400);
		}
		return $data;
	}
	
	//删除渠道提现余额
	public static function rmChannelAmount($channel_id){
		$date = date('Y-m-d');
		$key = md5('channel_amount_'.$date.'_'.$channel_id);
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取渠道统计ID
	public static function getChannelChartId($channel_id,$date=null){
		$date = $date ? $date : date('Y-m-d');
		$key = md5('channel_chart_'.$date.'_'.$channel_id);
		if(cache($key)){
			$data = cache($key);
		}else{
			$data = Db::name('ChannelChart')->where('create_date',$date)->where('channel_id',$channel_id)->value('id');
			if($data){
				cache($key,$data,86400);
			}
		}
		return $data;
	}
	
	//当日充值统计ID
	public static function getSpreadChartId($spread_id,$date=null){
		$date = $date ? $date : date('Y-m-d');
		$key = md5('spread_chart_'.$spread_id.'_'.$date);
		if(cache($key)){
			$data = cache($key);
		}else{
			$data = Db::name('SpreadChart')->where('sid',$spread_id)->where('create_date',$date)->value('id');
			if($data){
				cache($key,$data,86400);
			}
		}
		return $data;
	}
	
	//获取用户订单记录信息
	public static function getMemberChargeId($uid,$updateData=null){
		$key = 'member_charge_'.$uid;
		if($updateData){
			cache($key,$updateData);
		}else{
			if(cache('?'.$key)){
				$data = cache($key);
			}else{
				$data = Db::name('MemberCharge')->where('uid',$uid)->find();
				if(!$data){
					$res = Db::name('MemberCharge')->insert(['uid'=>$uid]);
					if($res){
						$data = Db::name('MemberCharge')->where('uid',$uid)->find();
						if($data){
							cache($key,$data);
						}
					}
				}
			}
			return $data;
		}
	}
	
	//获取限免书籍ID
	public static function getFreeBookIds($category=1){
		$key = 'free_book_ids';
		$date = date('Y-m-d');
		$key = md5('free_book_ids_'.$category.'_'.$date);
		//dump(cache($key));die;
		if(cache($key)){
			$data = cache($key);
		}else{
			$data = Db::name('BookFree')->where('type','=',$category)->where('start_date','<=',$date)->where('end_date','>=',$date)->column('book_id');
			$data = $data ? : 0;
			cache($key,$data,86400);
		}
		//dump($data);die;
		return $data;
	}
	
	//获取限免书籍ID
	public static function checkBookFree($book_id){
		$data = self::getFreeBookIds();
		return ($data && in_array($book_id, $data)) ? true : false;
	}
	
	//删除限免书籍缓存
	public static function rmFreeBookIds($category=1){
		$date = date('Y-m-d');
		$key = md5('free_book_ids_'.$category.'_'.$date);
        cache($key,null);
//		if(cache('?'.$key)){
//			cache($key,null);
//		}
	}
    //获取免费专区书籍ID集合
	public static function getFreeAreaBookIds($category=1){
	    $key = 'free_area_book_ids_'.$category;
        if(cache('?'.$key)){
            $data = cache($key);
        }else{
            $data = Db::name('Book')->where('status',1)->where('free_type',1)->where('type','=',$category)->order('hot_num','desc')->column('id');
            $data = $data ? : 0;
            cache($key,$data);
        }
        return $data;
    }

    //删除免费专区书籍缓存
    public static function rmFreeAreaBookIds($category=1){
        $key = 'free_area_book_ids_'.$category;
        if(cache('?'.$key)){
            cache($key,null);
        }
    }
	
	//获取精品书籍ID
	public static function getBeastBookIds($category=1){
		$key = 'beast_book_ids_'.$category;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Book')->where('status',1)->where('is_beast',1)->where('type','=',$category)->order('hot_num','desc')->column('id');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//删除精品书籍缓存
	public static function rmBeastBookIds($category=1){
		$key = 'beast_book_ids_'.$category;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取推荐书籍ID
	public static function getRecommendBookIds($category=1){
		$key = 'recommend_book_ids_'.$category;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Book')->where('status',1)->where('is_recommend',1)->where('type','=',$category)->order('hot_num','desc')->column('id');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//获取推荐书籍ID
	public static function rmRecommendBookIds($category=1){
		$key = 'recommend_book_ids_'.$category;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取猜你喜欢书籍ID
	public static function getAllBookIds($category=1){
		$key = 'all_book_ids_'.$category;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Book')->where('status',1)->where('type','=',$category)->order('hot_num','desc')->column('id');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//删除猜你喜欢书籍ID
	public static function rmAllBookIds($category=1){
		$key = 'all_book_ids_'.$category;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取上新书籍ID
	public static function getNewBookIds($category=1){
		$key = 'create_date_book_ids_'.$category;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Book')->where('status',1)->where('type','=',$category)->order('id','desc')->column('id');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//删除上新书籍ID
	public static function rmNewBookIds($category=1){
		$key = 'create_date_book_ids_'.$category;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取更新书籍ID
	public static function getUpdateBookIds($category=1){
		$date = date('Y_m_d');
		$key = 'update_date_book_ids_'.$category.'_'.$date;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = [];
			$start_date = date('Y-m-d',strtotime('-6 days'));
			$list = Db::name('Book')->where('status',1)->where('type','=',$category)->where('update_date','>=',$start_date)->field('id,update_date')->order('hot_num')->select();
			if($list){
				foreach ($list as $v){
					if(isset($data[$v['update_date']])){
						$data[$v['update_date']][] = $v['id'];
					}else{
						$data[$v['update_date']] = [$v['id']];
					}
				}
			}
			$data = $data ? : 0;
			cache($key,$data,86400);
		}

        return $data;
	}
	
	//删除更新书籍ID
	public static function rmUpdateBookIds(){
		$date = date('Y_m_d');
        $key1 = 'update_date_book_ids_1_'.$date;
        $key2 = 'update_date_book_ids_2_'.$date;
        if(cache('?'.$key1)){
            cache($key1,null);
        }
        if(cache('?'.$key2)){
            cache($key2,null);
        }
	}
	
	//获取排行数据缓存
    public static function getRankBookIds($category=1){
        $key = 'book_rank_list_'.$category;
        if(cache('?'.$key)){
            $data = cache($key);
        }else{
            $data = [];
            for ($type=1;$type<=4;$type++){
                $list = [];
                switch ($type){
                    case 1: //热销
                        $history = Db::name('ReadHistory a')->join('book b','a.book_id=b.id','left')->where('b.status',1)->field('count(a.id) as count,a.book_id')->where('b.type','=',$category)->group('book_id')->order('count','desc')->select();
                        if($history){
                            foreach ($history as $v){
                                $list[] = $v['book_id'];
                            }
                        }
                        break;
                    case 2://人气
                        $list = Db::name('Book')->where('status',1)->where('type','=',$category)->order('hot_num','desc')->limit(100)->column('id');
                        break;
                    case 3://连载
                        $list = Db::name('Book')->where('status',1)->where('over_type',1)->where('type','=',$category)->order('hot_num','desc')->limit(100)->column('id');
                        break;
                    case 4://完结
                        $list = Db::name('Book')->where('status',1)->where('over_type',2)->where('type','=',$category)->order('hot_num','desc')->limit(100)->column('id');
                        break;
                }
                if($list){
                    $data[$type] = $list;
                }
            }
            cache($key,$data);
        }
        return $data;
    }
    /*
	public static function getRankBookIds(){
		$key = 'book_rank_list';
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = [];
			for ($type=1;$type<=4;$type++){
				$list = [];
				switch ($type){
					case 1: //热销
						$history = Db::name('ReadHistory')->field('count(*) as count,book_id')->group('book_id')->order('count','desc')->select();
						if($history){
							foreach ($history as $v){
								$list[] = $v['book_id'];
							}
						}
						break;
					case 2://人气
						$list = Db::name('Book')->where('status',1)->order('hot_num','desc')->limit(100)->column('id');
						break;
					case 3://连载
						$list = Db::name('Book')->where('status',1)->where('over_type',1)->order('hot_num','desc')->limit(100)->column('id');
						break;
					case 4://完结
						$list = Db::name('Book')->where('status',1)->where('over_type',2)->order('hot_num','desc')->limit(100)->column('id');
						break;
				}
				if($list){
					$data[$type] = $list;
				}
			}
			cache($key,$data);
		}
		return $data;
	}
	*/
	//清空排行数据缓存
    public static function rmRankBookIds($category=1){
        $key = 'book_rank_list_'.$category;
        if(cache('?'.$key)){
            cache($key,null);
        }
    }
	/*
	public static function rmRankBookIds(){
		$key = 'book_rank_list';
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	 */
	
	//获取书籍订阅用户列表
	public static function getSubBookIds($uid){
		$key = 'book_sub_ids_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('BookSub')->where('uid',$uid)->order('id','desc')->column('book_id');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//删除书籍订阅ID
	public static function rmSubBookIds($uid){
		$key = 'book_sub_ids_'.$uid;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取广告配置
	public static function getAdList(){
		$key = 'site_ad_list';
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = [];
			$list = Db::name('Ad')->where('status',1)->field('id,src,url')->select();
			if($list){
				foreach ($list as $v){
					if($v['src'] && $v['url']){
						$data[$v['id']] = $v;
					}
				}
			}
			cache($key,$data);
		}
		return $data;
	}
	
	//删除广告缓存
	public static function rmAdList(){
		$key = 'site_ad_list';
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//检测是否发送未登录奖励消息
	public static function checkIsMsg($uid,$flag=0){
		$res = false;
		$str = date('Ymd').'#msgnotice#'.$uid;
		$key = md5($str);
		if($flag){
			cache($key,1,86400);
		}else{
			if(cache('?'.$key)){
				$res = true;
			}
			return $res;
		}
	}
	
	//获取分享点击日记录
	public static function getShareId($uid){
		$date = date('Y-m-d');
		$str = $date.'#inviteshareid#'.$uid;
		$key = md5($str);
		if(cache('?'.$key)){
			$id = cache($key);
		}else{
			$id = Db::name('MemberShare')->where('uid',$uid)->where('date',$date)->value('id');
			if(!$id){
				$id = Db::name('MemberShare')->insertGetId(['uid'=>$uid,'date'=>$date,'click_num'=>0]);
			}
			if($id){
				cache($key,$id,86400);
			}
		}
		return $id;
	}
	
	
	//获取邀请数
	public static function getMemberShareNum($uid){
		$date = date('Y-m-d');
		$str = $date.'#invitenumuid#'.$uid;
		$key = md5($str);
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = 0;
			$shareId = self::getShareId($uid);
			if($shareId){
				$data = Db::name('MemberShare')->where('id',$shareId)->value('click_num');
			}
			cache($key,$data,86400);
		}
		return $data;
	}
	
	//邀请数自增
	public static function incMemberShareNum($uid){
		$date = date('Y-m-d');
		$str = $date.'#invitenumuid#'.$uid;
		$key = md5($str);
		if(cache('?'.$key)){
			Cache::inc($key);
		}
	}


    //获取视频缓存
    public static function getVideoCache($video_id){
        $key = 'video_'.$video_id;
        $data = cache($key);
        if(!$data){
            $data = Db::name('Video')->where('id','=',$video_id)->where('status','=',1)->find();
            if($data){
                $data['cover'] = $data['cover'] ? : '/static/templet/default/cover.png';
                $data['summary'] = $data['summary'] ? : '暂无简介';
                cache($key,$data);
            }
        }
        return $data;
    }

    //清理单个视频缓存
    public static function clearVideo($video_id){
        $key = 'video_'.$video_id;
        if (cache('?'.$key)){
	        cache($key,null);
        }
    }

    //清理全部视频缓存
    public static function clearVideoAll(){
	    $ids = Db::name('video')->column('id');
	    foreach ($ids as $id){
	        self::clearVideo($id);
        }
    }
}
