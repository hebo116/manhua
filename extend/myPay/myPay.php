<?php
namespace myPay;

class myPay{
	
	public static $config;
	public static function getPayHtml($param){
		$url = 'https://deposit.eplutusoffline.com/api/1.0/receivable/add/';
		$data = [
			'uts' => time()*1000,
			'bid' => self::$config['mchid'],
			'req_order' => $param['order_no'],
			'req_amount' => $param['money'],
			'auto_dispatch' => true,
			'dispatch_type' => 'html',
			'reconcile_report_url' => 'http://'.$_SERVER['HTTP_HOST'].'/mypay'
		];
		$html = self::doPost($url, $data);
		return $html;
	}
	
	//提交post
	private static function doPost($url,$data){
		$postfield = http_build_query($data);
		$signature = hash_hmac('sha256', $postfield, self::$config['HASH_KEY']);
		$header = [
			'Authorization: Token '.self::$config['TOKEN'],
			'Content-Type: application/x-www-form-urlencoded',
			'X-EOL-Signature:'.$signature,
			'cache-control:no-cache'
		];
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$postfield);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		$output = curl_exec($ch);
		$error = '';
		if (curl_errno($ch)){
			$error = curl_error($ch);
		}
		curl_close($ch);
		if($error){
			echo 'cURL Error #:' . $error;
			exit;
		}else{
			return $output;
		}
	}
	
	//检查签名
	public static function checkSignature($data){
		$res = false;
		$key = 'HTTP_X_EOL_SIGNATURE';
		if(isset($_SERVER[$key])){
			$signature = $_SERVER[$key];
			$postfield = http_build_query($data);
			$my_signature = hash_hmac('sha256',$postfield,self::$config['HASH_KEY']);
			if($signature == $my_signature){
				$res = true;
			}
		}
		return $res;
	}
	
	
}