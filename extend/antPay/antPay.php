<?php
namespace antPay;

use site\myHttp;

class antPay{
	
	public static $config;

    //获取微信支付url
    public static function getPayForm($param,$pay_key='defaults'){
        $url = '';
        switch ($pay_key){
            case 'defaults':
                //$url = 'http://16.162.202.172:56700/api/pay/create_order';
                //$url = 'https://paycn2.a724fbf.com/api/pay/create_order';
                $url = 'https://stuy.nsxjkeyg.com/api/pay/create_order';
                break;
        }
        $data = [
            "mchId" => self::$config['mchid'],
            "appId" => self::$config['appid'],
            "productId" => self::$config['productid'],
        ];

        $data = array_merge($data,$param);
        $data['sign'] = self::getPaySign($data);
        return myHttp::doPost($url,$data);
        //return self::createFormStr($data,$url);
    }

    //获取支付宝支付url
    public static function getaliPayForm($param,$pay_key='defaults'){
        $url = '';
        switch ($pay_key){
            case 'defaults':
                $url = 'http://open.78pay.com/intf/wapali.html';
                break;
        }
        $data = [
            "customerid" => self::$config['mchid'],
        ];

        $data = array_merge($data,$param);
        unset($data['mark']);
        $data['sign'] = self::getPaySign($data);
        $data['mark'] = $param['mark'];
        return self::createFormStr($data,$url);
    }
	
	private static function createFormStr($data,$url){
        $html = "<form id='requestForm' action='".$url."' method='get'>";
        foreach ($data as $key=>$val){
            $html.= "<input type='hidden' name='".$key."' value='".$val."' />";
        }
        $html .= "<input type='submit' value='确定' style='display:none;'></form>";
        $html .= "<script>document.forms['requestForm'].submit();</script>";
        return $html;
	}
	
	//创建支付签名
	private static function getPaySign($data){
        $param = [];
        foreach ($data as $k=>$v){
            if(strlen($v) > 0){
                $param[$k] = $v;
            }
        }
        ksort($param);
        $sign = strtoupper(md5(urldecode(http_build_query($param)).'&key='.self::$config['api_key']));
        return $sign;
	}
	
	//检查异步通知签名
    public static function checkSign($paramArray){
        $res = false;
        if(isset($paramArray['status']) && in_array($paramArray['status'],[2,3])){
            $notifySign = $paramArray['sign'];
            unset($paramArray['sign']);
            ksort($paramArray);  //字典排序
            reset($paramArray);

            $md5str = "";
            foreach ($paramArray as $key => $val) {
                if( strlen($key)  && strlen($val) ){
                    $md5str = $md5str . $key . "=" . $val . "&";
                }
            }
            $sign = strtoupper(md5($md5str . "key=" . self::$config['api_key']));  //签名

            if ($notifySign == $sign){
                $res = true;
            }
        }
        return $res;
    }
}