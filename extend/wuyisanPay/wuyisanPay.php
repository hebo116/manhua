<?php
namespace wuyisanpay;

use site\myHttp;

class wuyisanPay{
	
	public static $config;

    //获取微信支付url
    public static function getPayForm($param,$pay_key='defaults'){
        $url = '';
        switch ($pay_key){
            case 'defaults':
                $url = 'http://open.78pay.com/intf/wapwpay.html';
                break;
        }
        $data = [
            "fxid" => self::$config['mchid'],
        ];

        $data = array_merge($data,$param);
        unset($data['mark']);
        $data['sign'] = self::getPaySign($data);
        $data['mark'] = $param['mark'];
        return self::createFormStr($data,$url);
    }

    //获取支付宝h5支付url
    public static function getaliPayForm($param,$pay_key='defaults'){
        $url = '';
        switch ($pay_key){
            case 'defaults':
                $url = "http://pay.jinxuhong.com/Pay";
                break;
        }
        $data = [
            "fxid" => self::$config['mchid'],//商户号
        ];

        $data = array_merge($data,$param);
        $data["fxsign"] = md5($data["fxid"] . $data["fxddh"] . $data["fxfee"] . $data["fxnotifyurl"] . self::$config['api_key']); //加密
        return myHttp::doPost($url,$data);
    }
	
	//创建支付签名
	private static function getPaySign($data){
        $param = [];
        foreach ($data as $k=>$v){
            if(strlen($v) > 0){
                $param[$k] = $v;
            }
        }
        $sign = strtoupper(md5(urldecode(http_build_query($param)).self::$config['api_key']));
        return $sign;
	}
	
	//检查异步通知签名
    public static function checkSign($data){
        $flag = false;
        if($data['fxstatus'] == 1){
            $mysign = md5($data['fxstatus'] . $data['fxid'] . $data['fxddh'] . $data['fxfee'] . self::$config['api_key']); //验证签名
            if ($mysign == $data['fxsign']){
                $flag = true;
            }
        }
        return $flag;
    }
}