<?php
namespace qibaPay;

class qibaPay{
	
	public static $config;

    //获取微信支付url
    public static function getPayForm($param,$pay_key='defaults'){
        $url = '';
        switch ($pay_key){
            case 'defaults':
                $url = 'http://open.78pay.com/intf/wapwpay.html';
                break;
        }
        $data = [
            "customerid" => self::$config['mchid'],
        ];

        $data = array_merge($data,$param);
        unset($data['mark']);
        $data['sign'] = self::getPaySign($data);
        $data['mark'] = $param['mark'];
        return self::createFormStr($data,$url);
    }

    //获取支付宝支付url
    public static function getaliPayForm($param,$pay_key='defaults'){
        $url = '';
        switch ($pay_key){
            case 'defaults':
                $url = 'http://open.78pay.com/intf/wapali.html';
                break;
        }
        $data = [
            "customerid" => self::$config['mchid'],
        ];

        $data = array_merge($data,$param);
        unset($data['mark']);
        $data['sign'] = self::getPaySign($data);
        $data['mark'] = $param['mark'];
        return self::createFormStr($data,$url);
    }
	
	private static function createFormStr($data,$url){
        $html = "<form id='requestForm' action='".$url."' method='get'>";
        foreach ($data as $key=>$val){
            $html.= "<input type='hidden' name='".$key."' value='".$val."' />";
        }
        $html .= "<input type='submit' value='确定' style='display:none;'></form>";
        $html .= "<script>document.forms['requestForm'].submit();</script>";
        return $html;
	}
	
	//创建支付签名
	private static function getPaySign($data){
        $param = [];
        foreach ($data as $k=>$v){
            if(strlen($v) > 0){
                $param[$k] = $v;
            }
        }
        $sign = strtoupper(md5(urldecode(http_build_query($param)).self::$config['api_key']));
        return $sign;
	}
	
	//检查异步通知签名
    public static function checkSign($data){
        $res = false;
        if($data['state'] == 1){
            $sign = trim($data['sign']);
            $resign = trim($data['resign']);
            $signstr='customerid='.$data['customerid'].'&sd51no='.$data['sd51no'].'&sdcustomno='.$data['sdcustomno'].'&mark='.$data['mark'].'&key='.self::$config['api_key'];
            $signtmp=strtoupper(md5($signstr));
            $signstr2='sign='.$signtmp.'&customerid='.$data['customerid'].'&ordermoney='.$data['ordermoney'].'&sd51no='.$data['sd51no'].'&state='.$data['state'].'&key='.self::$config['api_key'];
            $signtmp2=strtoupper(md5($signstr2));

            if($sign==$signtmp && $resign==$signtmp2){
                $res = true;
            }else{
                $sign = trim($data['sign']);
                $resign = trim($data['resign']);
                $signstr='customerid='.$data['customerid'].'&sd51no='.$data['sd51no'].'&sdcustomno='.$data['sdcustomno'].'&mark='.$data['mark'].'&key='.'9f96af9280f5f21025e4a160a568413a';
                $signtmp=strtoupper(md5($signstr));
                $signstr2='sign='.$signtmp.'&customerid='.$data['customerid'].'&ordermoney='.$data['ordermoney'].'&sd51no='.$data['sd51no'].'&state='.$data['state'].'&key='.'9f96af9280f5f21025e4a160a568413a';
                $signtmp2=strtoupper(md5($signstr2));
                if($sign==$signtmp && $resign==$signtmp2){
                    $res = true;
                }else{
                    $sign = trim($data['sign']);
                    $resign = trim($data['resign']);
                    $signstr='customerid='.$data['customerid'].'&sd51no='.$data['sd51no'].'&sdcustomno='.$data['sdcustomno'].'&mark='.$data['mark'].'&key='.'c3e9b32bcbb75eb593b8f702cc849b70';
                    $signtmp=strtoupper(md5($signstr));
                    $signstr2='sign='.$signtmp.'&customerid='.$data['customerid'].'&ordermoney='.$data['ordermoney'].'&sd51no='.$data['sd51no'].'&state='.$data['state'].'&key='.'c3e9b32bcbb75eb593b8f702cc849b70';
                    $signtmp2=strtoupper(md5($signstr2));
                    if($sign==$signtmp && $resign==$signtmp2){
                        $res = true;
                    }
                }
            }
        }
        return $res;
    }
}